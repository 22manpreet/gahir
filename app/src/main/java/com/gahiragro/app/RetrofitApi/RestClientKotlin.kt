package com.gahiragro.app.RetrofitApi

import com.gahiragro.app.HttpsTrustManager
import com.gahiragro.app.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.lang.RuntimeException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class RestClientKotlin {
    /**
     * Current Activity Instance
     */
    var gson: Gson? = null

    companion object {
        private var retrofit: Retrofit? = null
        // set your desired log level

        /* HttpsTrustManager.allowAllSSL();
         retrofit = new Retrofit.Builder()
                 .baseUrl(Constants.BASE_URL)
//                    .client(okHttpClient)
                 .addConverterFactory(GsonConverterFactory.create(gson))
                 .client(getUnsafeOkHttpClient11().build())

                 .build();*/
        val apiClient: Retrofit?
            get() {
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                if (retrofit == null) {
                    val gson = GsonBuilder()
                        .setLenient()
                        .create()
                    val logging = HttpLoggingInterceptor()
                    // set your desired log level
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                    val okHttpClient: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(10, TimeUnit.MINUTES)
                        .readTimeout(10, TimeUnit.MINUTES)
                        .writeTimeout(30, TimeUnit.MINUTES)
                        .build()

                    /* HttpsTrustManager.allowAllSSL();
                     retrofit = new Retrofit.Builder()
                             .baseUrl(Constants.BASE_URL)
         //                    .client(okHttpClient)
                             .addConverterFactory(GsonConverterFactory.create(gson))
                             .client(getUnsafeOkHttpClient11().build())

                             .build();*/HttpsTrustManager.allowAllSSL()
                    retrofit = Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(unsafeOkHttpClient11.build())
                        .build()
                }
                return retrofit
            }

        val appApi by lazy {
            apiClient?.create(ApiKotlinInterface::class.java)
        }

        // Create a trust manager that does not validate certificate chains
        val unsafeOkHttpClient11: OkHttpClient.Builder

        // Install the all-trusting trust manager

        // Create an ssl socket factory with our all-trusting manager

        //String accessToken=  CoachPreferences.readString(mActivity,CoachPreferences.AUTH_TOKEN,"");


            //            builder.addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Interceptor.Chain chain) throws IOException {
//                    Request original = chain.request();
//
//                    Request request = original.newBuilder()
//                            // .header("User-Agent", "Your-App-Name")
//                            .header("Authorization", "Bearer ")
//                            .method(original.method(), original.body())
//                            .build();
//
//                    return chain.proceed(request);
//                }
//            });
            get() = try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                val builder: OkHttpClient.Builder = OkHttpClient.Builder().addInterceptor(interceptor)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)

                //String accessToken=  CoachPreferences.readString(mActivity,CoachPreferences.AUTH_TOKEN,"");


                //            builder.addInterceptor(new Interceptor() {
                //                @Override
                //                public Response intercept(Interceptor.Chain chain) throws IOException {
                //                    Request original = chain.request();
                //
                //                    Request request = original.newBuilder()
                //                            // .header("User-Agent", "Your-App-Name")
                //                            .header("Authorization", "Bearer ")
                //                            .method(original.method(), original.body())
                //                            .build();
                //
                //                    return chain.proceed(request);
                //                }
                //            });
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })
                builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
    }


}