package com.gahiragro.app.RetrofitApi;


import com.gahiragro.app.model.ALlProductsModel;
import com.gahiragro.app.model.AddEnquiryModel;
import com.gahiragro.app.model.AddOrderModel;
import com.gahiragro.app.model.AddPartEnquiryModel;
import com.gahiragro.app.model.AdminLoginModel;
import com.gahiragro.app.model.AllCategoryModel;
import com.gahiragro.app.model.AllOrderModel;
import com.gahiragro.app.model.AllcartItemsModel;
import com.gahiragro.app.model.CartListModel;
import com.gahiragro.app.model.ChangePasswordModel;
import com.gahiragro.app.model.CheckWarrantyService;
import com.gahiragro.app.model.ChekWarrentyDetailModel;
import com.gahiragro.app.model.ContactUsModel;
import com.gahiragro.app.model.CustomerComplainModel;
import com.gahiragro.app.model.CustomerComplaintModel;
import com.gahiragro.app.model.CustomerPhoneLogin;
import com.gahiragro.app.model.CustomerReviewModel;
import com.gahiragro.app.model.CustomersListModel;
import com.gahiragro.app.model.DealorLoginModel;
import com.gahiragro.app.model.DealorPhoneLoginModel;
import com.gahiragro.app.model.DealorSignUpModel;
import com.gahiragro.app.model.EmailLoginModel;
import com.gahiragro.app.model.FilterCategoryModel;
import com.gahiragro.app.model.FilterProductModel;
import com.gahiragro.app.model.FilterReviewModel;
import com.gahiragro.app.model.ForgetPasswordModel;
import com.gahiragro.app.model.GetAllDealerComplainsModel;
import com.gahiragro.app.model.LocationModel;
import com.gahiragro.app.model.MessageModel;
import com.gahiragro.app.model.NotificationModel;
import com.gahiragro.app.model.ProductDetailModel;
import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.model.RecentSearchModel;
import com.gahiragro.app.model.GetRegisterProductModel;
import com.gahiragro.app.model.RegProductDetailModel;
import com.gahiragro.app.model.RemoveCartModel;
import com.gahiragro.app.model.SearchDealerModel;
import com.gahiragro.app.model.SearchLocationModel;
import com.gahiragro.app.model.SearchModel;
import com.gahiragro.app.model.SendDealerSpecificPushModel;
import com.gahiragro.app.model.SendMessageToAllCustomersModel;
import com.gahiragro.app.model.ServiceComplaintModel;
import com.gahiragro.app.model.StateDistrictModel;
import com.gahiragro.app.model.UpdateProfileModel;
import com.gahiragro.app.model.UserDetailModel;
import com.gahiragro.app.model.VerifyDealerModel;
import com.gahiragro.app.model.adminDealorEnquiry.DealorAdminEnquiry;
import com.gahiragro.app.model.adminDealorOrderModel.AdminDealorOrderModel;
import com.gahiragro.app.model.enquirydetails.EnquiryDetailModelM;
import com.gahiragro.app.model.enquirymodel.EnquiryModel;
import com.gahiragro.app.model.getOrderListMOdel.OrderListModel;
import com.gahiragro.app.model.orderDetailModel.OrderDetailModel;
import com.gahiragro.app.myModel.GetRegisterProductsModel.RegisterProductModel;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {

    //www.dharmani.com/gahir/api/GetAllProducts.php


    @FormUrlEncoded
    @POST("ForgotPassword.php")
    Call<ForgetPasswordModel> forgetPassword(@Field("email") String email);

    //    @FormUrlEncoded
//    @POST("DealerPhoneLogin.php")
//    Call<DealorPhoneLoginModel> dealorPhoneLOgin(@Field("phone") String phone,
//                                                 @Field("device_type") String deviceType,
//                                                 @Field("device_token") String deviceToken);
    @POST("PhoneLogin.php")
    @FormUrlEncoded
    Call<DealorPhoneLoginModel> PhoneLOgin(@FieldMap Map<String, String> params);

    @POST("DealerSignup.php")
    @FormUrlEncoded
    Call<DealorSignUpModel> dealorSignUp(@FieldMap Map<String, String> params);

    @POST("CustomerSignup.php")
    @FormUrlEncoded
    Call<DealorSignUpModel> customerSignUp(@FieldMap Map<String, String> params);

    @POST("FilterCategory.php")
    @FormUrlEncoded
    Call<FilterCategoryModel> filterCategory(@FieldMap Map<String, String> params);


    @POST("DealerLogin.php")
    @FormUrlEncoded
    Call<DealorLoginModel> dealorLogin(@FieldMap Map<String, String> params);

    @POST("EmailLogin.php")
    @FormUrlEncoded
    Call<EmailLoginModel> emailLogin(@FieldMap Map<String, String> params);

    @Multipart
    @POST("UpdateProfile.php")
    Call<UpdateProfileModel> updateProfile(
            @Part("access_token") RequestBody access_token,
            @Part("first_name") RequestBody first_name,
            @Part("bio") RequestBody bio,
            @Part MultipartBody.Part image);

//
//    @POST("UpdateProfile.php")
//    @FormUrlEncoded
//    Call<UpdateProfileModel> updateProfile(@FieldMap Map<String, String> params);

    @POST("ContactUs.php")
    @FormUrlEncoded
    Call<ContactUsModel> contactUs(@FieldMap Map<String, String> params);

    @POST("GetAllCategories.php")
    @FormUrlEncoded
    Call<AllCategoryModel> allCategory(@FieldMap Map<String, String> params);

    @POST("GetUserProfile.php")
    @FormUrlEncoded
    Call<UserDetailModel> userProfile(@FieldMap Map<String, String> params);

    @POST("FilterProduct.php")
    @FormUrlEncoded
    Call<FilterProductModel> filterProduct(@FieldMap Map<String, String> params);

    @POST("GetProductDetail.php")
    @FormUrlEncoded
    Call<ProductDetailModel> productDetailApi(@FieldMap Map<String, String> params);

    @POST("SearchProduct.php")
    @FormUrlEncoded
    Call<SearchModel> searchApi(@FieldMap Map<String, String> params);

    @POST("GetRecentSearch.php")
    @FormUrlEncoded
    Call<RecentSearchModel> recentSearchApi(@FieldMap Map<String, String> params);

    @POST("GetAllOrders.php")
    @FormUrlEncoded
    Call<AllOrderModel> getAllOrder(@FieldMap Map<String, String> params);

    @POST("AddEnquiry.php")
    @FormUrlEncoded
    Call<AddEnquiryModel> addEnquiry(@FieldMap Map<String, String> params);

    @POST("GetAllEnquiries.php")
    @FormUrlEncoded
    Call<EnquiryModel> getAllEnquiry(@FieldMap Map<String, String> params);

    @POST("GetAllNotifications.php")
    @FormUrlEncoded
    Call<NotificationModel> notificationApi(@FieldMap Map<String, String> params);

    @POST("GetEnquiryDetail.php")
    @FormUrlEncoded
    Call<EnquiryDetailModelM> enquiryDetailApi(@FieldMap Map<String, String> params);

    @POST("AddOrder.php")
    @FormUrlEncoded
    Call<AddOrderModel> addOrderApi(@FieldMap Map<String, String> params);

    @POST("GetAllOrders.php")
    @FormUrlEncoded
    Call<OrderListModel> getOrderApi(@FieldMap Map<String, String> params);

    @POST("ChangePassword.php")
    @FormUrlEncoded
    Call<ChangePasswordModel> changePassword(@FieldMap Map<String, String> params);

    //Admin
    @POST("AdminLogin.php")
    @FormUrlEncoded
    Call<AdminLoginModel> adminLogin(@FieldMap Map<String, String> params);

    @POST("GetDealerEnquiries.php")
    @FormUrlEncoded
    Call<DealorAdminEnquiry> dealorAdminEnquiry(@FieldMap Map<String, String> params);

    @POST("GetDealerOrders.php")
    @FormUrlEncoded
    Call<AdminDealorOrderModel> adminDealorOrder(@FieldMap Map<String, String> params);

    //Admin
    @POST("SalesLogin.php")
    @FormUrlEncoded
    Call<AdminLoginModel> salesExecutive(@FieldMap Map<String, String> params);

    //CUSTOMER

    @POST("CustomerPhoneLogin.php")
    @FormUrlEncoded
    Call<CustomerPhoneLogin> customerPhoneLogin(@FieldMap Map<String, String> params);

    @POST("VerifyDealer.php")
    @FormUrlEncoded
    Call<VerifyDealerModel> verifyDealerApi(@FieldMap Map<String, String> params);

    @POST("VerifyCustomer.php")
    @FormUrlEncoded
    Call<VerifyDealerModel> verifyCustomerApi(@FieldMap Map<String, String> params);

    @POST("UpdateLocation.php")
    @FormUrlEncoded
    Call<LocationModel> locationModel(@FieldMap Map<String, String> params);

    @POST("GetOrderDetail.php")
    @FormUrlEncoded
    Call<OrderDetailModel> orderDetailApi(@FieldMap Map<String, String> params);


    @Multipart
    @POST("RegisterProduct.php")
    Call<RegisterProductModel> registerProducts(
            @Part("access_token") RequestBody access_token,
            @Part("sr_no") RequestBody sr_no,
            @Part("product_id") RequestBody productId);


    @POST("GetRegisteredProducts.php")
    @FormUrlEncoded
    Call<GetRegisterProductModel> getRegisterProucts(@FieldMap Map<String, String> params);

    @POST("GetAllReviews.php")
    @FormUrlEncoded
    Call<FilterReviewModel> getAllReviews(@FieldMap Map<String, String> params);

    @GET("GetAllStates.php")
    Call<StateDistrictModel> getStateDistList();

    @POST("SearchDealer.php")
    @FormUrlEncoded
    Call<SearchDealerModel> getSearchDealer(@FieldMap Map<String, String> params);

    @POST("GetRegProductDetail.php")
    @FormUrlEncoded
    Call<RegProductDetailModel> regProductDetailApi(@FieldMap Map<String, String> params);


    @POST("GetAllProducts.php")
    @FormUrlEncoded
    Call<ALlProductsModel> getAllProduct(@FieldMap Map<String, String> params);

    @POST("CheckWarranty.php")
    @FormUrlEncoded
    Call<CheckWarrantyService> checkServiceWarrenty(@FieldMap Map<String, String> params);

    @POST("CheckWarrantyByProduct.php")
    @FormUrlEncoded
    Call<ChekWarrentyDetailModel> checkWarrantyApi(@FieldMap Map<String, String> params);

    @Multipart
    @POST("AddCustomerReview.php")
    Call<CustomerReviewModel> addCustomerReview(
            @Part("access_token") RequestBody access_token,
            @Part("message") RequestBody first_name,
            @Part("product_id") RequestBody bio,
            @Part MultipartBody.Part image);

    @POST("FilterReview.php")
    @FormUrlEncoded
    Call<FilterReviewModel> getFilterReview(@FieldMap Map<String, String> params);

    @POST("GetAllCustomerComplains.php")
    @FormUrlEncoded
    Call<CustomerComplaintModel> getCustomerComplaint(@FieldMap Map<String, String> params);


    @Multipart
    @POST("AddCustomerComplain.php")
    Call<CustomerComplainModel> addCustomerComplaint(
            @Part("access_token") RequestBody access_token,
            @Part("contact_no") RequestBody contactNo,
            @Part("product_id") RequestBody prodId,
            @Part("sr_no") RequestBody srNo,
            @Part("comp_reason") RequestBody compReason,
            @Part("reason_detail") RequestBody reasondetail,
            @Part MultipartBody.Part support_img,
            @Part MultipartBody.Part support_audio);


    @POST("GetAllServiceComplains.php")
    @FormUrlEncoded
    Call<ServiceComplaintModel> getAllServiceComplaint(@FieldMap Map<String, String> params);


    @Multipart
    @POST("ServiceComplain.php")
    Call<CustomerReviewModel> serviceComplain(
            @Part("access_token") RequestBody access_token,
            @Part("service_id") RequestBody serviceId,
            @Part("complain_id") RequestBody complainID,
            @Part("lubrication") RequestBody lubrication,
            @Part("part_change") RequestBody partChange,
            @Part("part_repair") RequestBody partRepair,
            @Part("machine_status") RequestBody machineStatus,
            @Part("hour_spent") RequestBody hourSpent,
            @Part("full_machine_check") RequestBody fullMacCheck,
            @Part("otp") RequestBody otp,
            @Part MultipartBody.Part selfie,
            @Part MultipartBody.Part parts_changed1,
            @Part MultipartBody.Part parts_changed2,
            @Part MultipartBody.Part parts_changed3,
            @Part MultipartBody.Part parts_repaired1,
            @Part MultipartBody.Part parts_repaired2,
            @Part MultipartBody.Part parts_repaired3);


    @POST("GetAllProductParts.php")
    @FormUrlEncoded
    Call<ProductPartListModel> getAllPrdParts(@FieldMap Map<String, String> params);


    @POST("AddToCart.php")
    Call<CartListModel> addToCartListApi(@Body JsonObject jsonObject);


    @POST("GetAllCartItems.php")
    @FormUrlEncoded
    Call<AllcartItemsModel> getAllCartItems(@FieldMap Map<String, String> params);


    @POST("AddPartEnquiry.php")
    @FormUrlEncoded
    Call<AddPartEnquiryModel> addPartEnquiry(@FieldMap Map<String, String> params);


    @POST("RemoveFromCart.php")
    Call<RemoveCartModel> removeCartItem(@Body Map<String, String> params);

    @POST("GetDealerNotifications.php")
    @FormUrlEncoded
    Call<MessageModel> messageApi(@FieldMap Map<String, String> params);


    @POST("GetDealerCustomerList.php")
    @FormUrlEncoded
    Call<CustomersListModel> customersListApi(@FieldMap Map<String, String> params);


    @POST("SendDealerMassPush.php")
    @FormUrlEncoded
    Call<SendMessageToAllCustomersModel> SendDealerMassPushApi(@FieldMap Map<String, String> params);

    @POST("SendDealerSpecificPush.php")
    @FormUrlEncoded
    Call<SendDealerSpecificPushModel> SendDealerSpecificPushApi(@FieldMap Map<String, String> params);


    @POST("GetAllDealerComplains.php")
    @FormUrlEncoded
    Call<GetAllDealerComplainsModel> GetAllDealerComplains(@FieldMap Map<String, String> params);


    @GET
    Call<SearchLocationModel> getAddressFromLatLngRequest(@Url String mUrl);
}