package com.gahiragro.app.RetrofitApi

import com.gahiragro.app.model.CustomerComplainModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ApiKotlinInterface {

//    @Multipart
//    @POST("AddCustomerComplain.php")
//    suspend fun addCustomerComplaint(
//        @Part("access_token") token: RequestBody? = null,
//        @Part("contact_no") contactNo: RequestBody? = null,
//        @Part("product_id") prodId: RequestBody? = null,
//        @Part("sr_no") sNo: RequestBody? = null,
//        @Part("comp_reason") compReason: RequestBody? = null,
//        @Part("reason_detail") reasondetail: RequestBody? = null,
//        @Part support_img1: MultipartBody.Part? = null,
//        @Part support_img2: MultipartBody.Part? = null,
//        @Part support_img3: MultipartBody.Part? = null,
//        @Part support_audio: MultipartBody.Part? = null
//    ):  Response<CustomerComplainModel>


    @Multipart
    @POST("AddCustomerComplain.php")
    fun addCustomerComplaint(
        @Part("access_token") token: RequestBody?,
        @Part("contact_no") contactNo: RequestBody?,
        @Part("product_id") prodId: RequestBody?,
        @Part("sr_no") sNo: RequestBody?,
        @Part("comp_reason") compReason: RequestBody?,
        @Part("reason_detail") reasondetail: RequestBody?,
        @Part support_img1: MultipartBody.Part?,
        @Part support_img2: MultipartBody.Part?,
        @Part support_img3: MultipartBody.Part?,
        @Part support_audio: MultipartBody.Part?,
        ): Call<CustomerComplainModel>
}