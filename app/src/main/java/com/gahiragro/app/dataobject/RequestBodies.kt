package com.icma.app.dataobject

import android.graphics.Bitmap
import okhttp3.MultipartBody
import okhttp3.RequestBody

object RequestBodies {

//
//    data class EditProfileBody(
//    mMultipartBodyChanged1: MultipartBody.Part?,
//    mMultipartBodyChanged2: MultipartBody.Part?,
//    mMultipartBodyChanged3: MultipartBody.Part?,
//    mMultipartBody1: MultipartBody.Part?,
//    accessToken: RequestBody,
//    contactNum: RequestBody,
//    prodId: RequestBody,
//    srNo: RequestBody,
//    compReason: RequestBody,
//    reasonDetail: RequestBody
//    )

    data class EditProfileBody(
        val mMultipartBodyChanged1: MultipartBody.Part?,
        val mMultipartBodyChanged2: MultipartBody.Part?,
        val mMultipartBodyChanged3: MultipartBody.Part?,
        val mMultipartBody1: MultipartBody.Part?,
        val accessToken: RequestBody,
        val   contactNum: RequestBody,
        val  prodId: RequestBody,
        val srNo: RequestBody,
        val  compReason: RequestBody,
        val reasonDetail: RequestBody
    )
}