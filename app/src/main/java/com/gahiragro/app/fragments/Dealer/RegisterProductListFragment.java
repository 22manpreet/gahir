package com.gahiragro.app.fragments.Dealer;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.Dealer.ProductAdapter;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.GetRegisterProductModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterProductListFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = RegisterProductListFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */

//    Unbinder unbinder;

//    @BindView(R.id.productRV)
    RecyclerView productRV;

//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;
//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

    int page_no = 1;
    String lastPage = "false";
    List<GetRegisterProductModel.AllProduct> mGetProductsList = new ArrayList<>();
    List<GetRegisterProductModel.AllProduct> mTemGetProductsAL = new ArrayList<>();
    ProductAdapter productAdapter;

    //List<MyModel> mlist = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_product_list_fragment, container, false);
//        unbinder = ButterKnife.bind(this, view);
        productRV=view.findViewById(R.id.productRV);
        noDataFoundTV=view.findViewById(R.id.noDataFoundTV);
        mainHeaderLL=view.findViewById(R.id.mainHeaderLL);
        callApi();
        return view;

    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    private void callApi() {
        if (isNetworkAvailable(getActivity())) {
            executeGetProductsApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetProductsApi() {

        if (page_no == 1) {

            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            // mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getRegisterProucts(mPram()).enqueue(new Callback<GetRegisterProductModel>() {
            @Override
            public void onResponse(Call<GetRegisterProductModel> call, Response<GetRegisterProductModel> response) {

                if (page_no == 1) {
                    dismissProgressDialog();
                }
                GetRegisterProductModel getRegisterProductsModel = response.body();
                if (getRegisterProductsModel.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    lastPage = getRegisterProductsModel.getProductList().getLastPage();

                    if (page_no == 1) {
                        mGetProductsList = getRegisterProductsModel.getProductList().getAllProducts();
                    } else if (page_no > 1) {
                        mTemGetProductsAL = getRegisterProductsModel.getProductList().getAllProducts();
                    }
                    if (mTemGetProductsAL.size() > 0) {
                        mGetProductsList.addAll(mTemGetProductsAL);
                    }
                    if (page_no == 1) {
                        initSetRV();
                    } else {
                        productAdapter.notifyDataSetChanged();
                    }

                }
                else if(getRegisterProductsModel.getStatus().equals("0")){
                    showAlertDialog(getActivity(),getRegisterProductsModel.getMessage());
                }
                else if (getRegisterProductsModel.getStatus().equals("100")) {
                    showToast(getActivity(), getRegisterProductsModel.getMessage());
                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(getRegisterProductsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetRegisterProductModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });

    }


    void initSetRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        productRV.setLayoutManager(layoutManager);
        productAdapter = new ProductAdapter(mGetProductsList, getActivity(), mInterfaceData);
        productRV.setAdapter(productAdapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();

    }

    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {
                if (lastPage.equals("FALSE")) {
                    //mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeGetProductsApi();
                        } else {
                            /*if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);*/
                        }
                    }
                }, 1500);
            }
        }
    };

}