package com.gahiragro.app.fragments;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;

import static com.gahiragro.app.activities.HomeActivity.downloadRL;


public class DealerDocumentFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealerDocumentFragment.this.getClass().getSimpleName();
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    /**
     * Widgets
     */

//    @BindView(R.id.messageTV)
    TextView messageTV;

//    @BindView(R.id.webView)
    WebView webView;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;
    String path = "";
    String img = "";
    //Initialize
//    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dealer_document, container, false);
//        unbinder = ButterKnife.bind(this, view);
        messageTV =view.findViewById(R.id.messageTV);
        webView =view.findViewById(R.id.webView);
        mainHeaderLL =view.findViewById(R.id.mainHeaderLL);

        PRDownloader.initialize(getActivity().getApplicationContext());
        // Enabling database for resume support even after the application is killed:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getActivity().getApplicationContext(), config);

// Setting timeout globally for the download network requests:
        PRDownloaderConfig configs = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getActivity(), configs);
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GAHIR/";
        Log.e(TAG, "pathname:" + path);
        File dir = new File(path);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);



      //  webView.loadUrl(GahirPreference.readString(getActivity(), GahirPreference.DEALER_DOC, ""));

    //    img = GahirPreference.readString(getActivity(), GahirPreference.DEALER_DOC, "");


//        webView.loadUrl(gson.fromJson(json,type));
//        img= gson.fromJson(json, type);
        if(img == "") {
            webView.setVisibility(View.GONE);
            messageTV.setVisibility(View.VISIBLE);
        }
        else{
            webView.setVisibility(View.VISIBLE);
            messageTV.setVisibility(View.GONE);
        }
        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (checkPermission()) {
                        setDownloadMethod();
                    } else {
                        requestPermission();
                    }
                }



        });
        return view;

    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    private void setDownloadMethod() {
        showProgressDialog(getActivity());
        DownloadRequest prDownloader = PRDownloader.download(img, path, "image1" + ".jpg")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                });

        prDownloader.start(new OnDownloadListener() {
            @Override
            public void onDownloadComplete() {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "saved Succesfully.", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
                Log.e(TAG, "VALUE::" + error);
                Toast.makeText(getActivity(), "error" + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getMD5EncryptedString(String encTarget) {
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while (md5.length() < 32) {
            md5 = "0" + md5;
        }
        return md5;
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(getActivity(), writeReadStorage);

        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{writeExternalStorage, writeReadStorage}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //    onSelectImageClick();
                    setDownloadMethod();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }
}