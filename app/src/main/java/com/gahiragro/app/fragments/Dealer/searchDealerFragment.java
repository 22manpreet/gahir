package com.gahiragro.app.fragments.Dealer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.SearchDealerListActivity;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.StateDistrictModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class searchDealerFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */


    String TAG = searchDealerFragment.this.getClass().getSimpleName();
//    Unbinder unbinder;


//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;


//    @BindView(R.id.stateSpinner)
    Spinner stateSpinner;

//    @BindView(R.id.districtSpinner)
    Spinner districtSpinner;

//    @BindView(R.id.btnSearchBT)
    Button btnSearchBT;

//    @BindView(R.id.stateRL)
    RelativeLayout stateRL;

//    @BindView(R.id.districtRL)
    RelativeLayout districtRL;

    List<StateDistrictModel.StateListItem> mStateList = new ArrayList<>();
    List<StateDistrictModel.AllDistrictsItem> mDistList = new ArrayList<>();

    String stateName = "", distName = "", stateId = "0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_dealer, container, false);
//        unbinder = ButterKnife.bind(this, view);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);
        stateSpinner = view.findViewById(R.id.stateSpinner);
        districtSpinner = view.findViewById(R.id.districtSpinner);
        btnSearchBT = view.findViewById(R.id.btnSearchBT);
        stateRL = view.findViewById(R.id.stateRL);
        districtRL = view.findViewById(R.id.districtRL);
        // execute state list
        executeSearchDealerList();

        Onclick();

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    void Onclick() {
        btnSearchBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidate()) {

                    if (isNetworkAvailable(getActivity())) {

                        Intent i = new Intent(getActivity(), SearchDealerListActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.DISTRICT_SEARCH, distName);
                        bundle.putString(Constants.STATE_SEARCH, stateName);
                        i.putExtras(bundle);
                        startActivity(i);

                    } else {
                        showToast(getActivity(), getString(R.string.internet_connection_error));
                    }
                }
            }
        });
    }


    private void executeSearchDealerList() {
        showProgressDialog(getActivity());

        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getStateDistList().enqueue(new Callback<StateDistrictModel>() {
            @Override
            public void onResponse(Call<StateDistrictModel> call, Response<StateDistrictModel> response) {
                dismissProgressDialog();

                StateDistrictModel stateDistrictModel = response.body();
                if (stateDistrictModel.getStatus().equals("1")) {

                    // set state list
                    mStateList.addAll(stateDistrictModel.getStateList());

                    //set state spinner
                    setStateSpinner();

                } else {

                    showAlertDialog(getActivity(), stateDistrictModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<StateDistrictModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }


    private void setStateSpinner() {

        StateDistrictModel.StateListItem mModel = new StateDistrictModel.StateListItem();
        mModel.setStateId("0");
        mModel.setStateTitle("Select State");
        mStateList.add(0, mModel);


        //mStateList.clear();
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                "Poppins-SemiBold.otf");

        ArrayAdapter<StateDistrictModel.StateListItem> mAdapter = new ArrayAdapter<StateDistrictModel.StateListItem>(getActivity(), android.R.layout.simple_spinner_item, mStateList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = (mStateList.get(position).getStateTitle());
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);

                stateRL.setBackgroundResource(R.drawable.bg_spinner_red);

                return v;
            }
        };

        stateSpinner.setAdapter(mAdapter);

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);

                if (position == stateSpinner.getSelectedItemPosition()) {
                    StateDistrictModel.StateListItem getStateItem = mStateList.get(position);
                    stateName = getStateItem.getStateTitle();
                    stateId = getStateItem.getStateId();
                    Log.e(TAG, "onItemSelected: " + stateId);

                    ((TextView) view).setText(stateName);
                    stateSpinner.setSelection(position);


                    mDistList.clear();
                    StateDistrictModel.AllDistrictsItem modelD = new StateDistrictModel.AllDistrictsItem();
                    modelD.setDistrictid("0");
                    modelD.setDistrictTitle("Select District");
                    mDistList.add(0, modelD);
//                    }

                    // loop of state id ,get district
                    for (int i = 0; i < mStateList.size(); i++) {
                        int id1 = Integer.parseInt(stateId);

                        if (id1 == Integer.parseInt(mStateList.get(i).getStateId())) {
                            //   mDistList.clear();

                            if (mStateList.get(i).getAllDistricts() != null)
                                mDistList.addAll(mStateList.get(i).getAllDistricts());
                        }
                    }

                    stateRL.setBackgroundResource(R.drawable.bg_spinner_red);

                    if (mDistList != null && mDistList.size() > 0)
                        setDistrictSpinner();

                } else {
                    districtRL.setBackgroundResource(R.drawable.bg_spinner);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                stateRL.setBackgroundResource(R.drawable.bg_spinner_red);

            }
        });

    }

    private void setDistrictSpinner() {

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                "Poppins-SemiBold.otf");

        ArrayAdapter<StateDistrictModel.AllDistrictsItem> mDisAdapter = new ArrayAdapter<StateDistrictModel.AllDistrictsItem>(getActivity(), android.R.layout.simple_spinner_item, mDistList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mDistList.get(position).getDistrictTitle();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);

                //   districtRL.setBackgroundResource(R.drawable.bg_spinner);
                return v;
            }
        };

        districtSpinner.setAdapter(mDisAdapter);
        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);

                if (position == districtSpinner.getSelectedItemPosition()) {

                    StateDistrictModel.AllDistrictsItem getDistlist = mDistList.get(position);
                    distName = getDistlist.getDistrictTitle();

                    ((TextView) view).setText(distName);
                    districtSpinner.setSelection(position);

                    districtRL.setBackgroundResource(R.drawable.bg_spinner_red);
                } else {
                    districtRL.setBackgroundResource(R.drawable.bg_spinner);
                }
                stateRL.setBackgroundResource(R.drawable.bg_spinner_red);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                districtRL.setBackgroundResource(R.drawable.bg_spinner);
            }
        });
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (stateName.equals("Select State")) {
            showAlertDialog(getActivity(), getString(R.string.please_select_state));
            flag = false;
        } else if (distName.equals("Select District")) {
            showAlertDialog(getActivity(), getString(R.string.please_select_district));
            flag = false;
        }

        return flag;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
    }
}