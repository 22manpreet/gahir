package com.gahiragro.app.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.adapters.DocumentAdapter;
import com.gahiragro.app.adapters.NotificationAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;

public class DocumentFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = DocumentFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.docRV)
    RecyclerView docRV;


//    @BindView(R.id.messageTV)
    TextView messageTV;

//    Unbinder unbinder;
    DocumentAdapter mDocumentAdapter;
    ArrayList<String> contactList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_document, container, false);
//        unbinder = ButterKnife.bind(this, view);

        docRV =view.findViewById(R.id.docRV);
        messageTV =view.findViewById(R.id.messageTV);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String json = prefs.getString("doc", null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();

      contactList = gson.fromJson(json, type);

      if(contactList == null || contactList.size()==0){
          messageTV.setVisibility(View.VISIBLE);
          docRV.setVisibility(View.GONE);
      }
      else{
          messageTV.setVisibility(View.GONE);
          docRV.setVisibility(View.VISIBLE);
          setAdapter();
      }

        return view;
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        docRV.setLayoutManager(layoutManager);
        mDocumentAdapter = new DocumentAdapter(getActivity(),contactList);
        docRV.setAdapter(mDocumentAdapter);
    }
}