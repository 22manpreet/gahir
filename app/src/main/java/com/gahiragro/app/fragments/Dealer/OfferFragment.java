package com.gahiragro.app.fragments.Dealer;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.fragments.BaseFragment;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;


public class OfferFragment extends BaseFragment {


//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

//    Unbinder unbinder;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

      View view = inflater.inflate(R.layout.fragment_offer, container, false);
//        unbinder =  ButterKnife.bind(this,view);
        mainHeaderLL=view.findViewById(R.id.mainHeaderLL);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();
    }
}