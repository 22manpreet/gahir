package com.gahiragro.app.fragments.Dealer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.CheckRewardsActivity;
import com.gahiragro.app.activities.Dealer.SearchDealerListActivity;
import com.gahiragro.app.fragments.BaseFragment;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;


public class RewardsFragment extends BaseFragment {
//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;
//    Unbinder unbinder;

//    @BindView(R.id.btnCheckRewardsBT)
    Button btnCheckRewardsBT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rewards, container, false);
//        unbinder = ButterKnife.bind(this, view);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);
        btnCheckRewardsBT = view.findViewById(R.id.btnCheckRewardsBT);
        Onclick();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    void Onclick() {
        btnCheckRewardsBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(), CheckRewardsActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
    }
}