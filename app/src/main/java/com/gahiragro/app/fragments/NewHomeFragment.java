package com.gahiragro.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.gahiragro.app.HttpsTrustManager;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.SplashActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.adapters.HomeNewTopFilterAdapter;
import com.gahiragro.app.adapters.HomeTopFilterAdapter;
import com.gahiragro.app.adapters.NotificationAdapter;
import com.gahiragro.app.adapters.TopCardCategoryAdapter;
import com.gahiragro.app.model.AllCategoryModel;
import com.gahiragro.app.model.ContactUsModel;
import com.gahiragro.app.model.DummyModel;
import com.gahiragro.app.utils.GahirPreference;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewHomeFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = NewHomeFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.categoryRV)
    RecyclerView categoryRV;


//    @BindView(R.id.indicator)
    SpringDotsIndicator indicator;
    ViewPager viewpager;

    //INitialize
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    HomeNewTopFilterAdapter mHomeNewTopFilterAdapter;
    TopCardCategoryAdapter mTopCardCategoryAdapter;
//    Unbinder unbinder;
    ArrayList<AllCategoryModel.AllCategory> mCategoryAL = new ArrayList<>();
    ArrayList<DummyModel> mDummyAL = new ArrayList<>();
    Integer img[] =
            {
                    R.drawable.ic_harvestor_new,
                    R.drawable.ic_leveller_new,
                    R.drawable.ic_spray_pump_new,
                    R.drawable.ic_straw_reaper_new,
                    R.drawable.ic_mud_loader_new,
                    R.drawable.ic_super_seeder_new,
                    R.drawable.ic_electronic_sys_new
            };

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_home, container, false);
//        unbinder = ButterKnife.bind(this, view);
        viewpager = (ViewPager) view.findViewById(R.id.pager);
        categoryRV =view.findViewById(R.id.categoryRV);
        indicator =view.findViewById(R.id.indicator);
        if (isNetworkAvailable(getActivity())) {
            executeGetAllCategoryApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
        // setTopCardAdapter();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(mHomeNewTopFilterAdapter!=null)
//        {
//            mHomeNewTopFilterAdapter.notifyDataSetChanged();
//        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) {
//            unbinder.unbind();
        }
    }

    /*
     * ContactUs APi
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetAllCategoryApi() {
        showProgressDialog(getActivity());

        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.allCategory(mParam()).enqueue(new Callback<AllCategoryModel>() {
            @Override
            public void onResponse(Call<AllCategoryModel> call, Response<AllCategoryModel> response) {
                dismissProgressDialog();
                AllCategoryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {

                    mCategoryAL.addAll(mModel.getCategoryList().getAllCategories());

                    for (int i = 0; i < mModel.getCategoryList().getAllCategories().size(); i++) {
                        DummyModel dummyModel = new DummyModel();
                        dummyModel.setName(mModel.getCategoryList().getAllCategories().get(i).getCatName());
                        dummyModel.setId(mModel.getCategoryList().getAllCategories().get(i).getId());
                        dummyModel.setImg(mModel.getCategoryList().getAllCategories().get(i).getCatImage());
                     //   dummyModel.setImg(img[i]);
                        mDummyAL.add(dummyModel);

                    }
                    //  Log.e(TAG, "*****size*****" + dummyModel);

                    setTopCardAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                }
            }

            @Override
            public void onFailure(Call<AllCategoryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    /*
     * Set Top Filter  Adapter
     * */
    private void setAdapter() {
        categoryRV.setNestedScrollingEnabled(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        categoryRV.setLayoutManager(layoutManager);
        categoryRV.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mHomeNewTopFilterAdapter = new HomeNewTopFilterAdapter(getActivity(), mDummyAL);
        categoryRV.setAdapter(mHomeNewTopFilterAdapter);
        categoryRV.setMotionEventSplittingEnabled(false);
    }

    private void setTopCardAdapter() {
        if (viewpager != null) {
            if (indicator != null) {
                indicator.setVisibility(View.VISIBLE);
            }
            if (indicator != null) {
                mTopCardCategoryAdapter = new TopCardCategoryAdapter(getActivity(), mCategoryAL);
                viewpager.setAdapter(mTopCardCategoryAdapter);
                indicator.setViewPager(viewpager);
                setAdapter();
                final Handler handler = new Handler();
                Timer timer = new Timer();
                final Runnable runnable = new Runnable() {
                    public void run() {
                        int numPages = viewpager.getAdapter().getCount();
                        currentPage = (currentPage + 1) % numPages;
                        viewpager.setCurrentItem(currentPage);

                    }
                };
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(runnable);
                    }
                }, 3000, 3000);

            }
            viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    currentPage = position;
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }


    }

}