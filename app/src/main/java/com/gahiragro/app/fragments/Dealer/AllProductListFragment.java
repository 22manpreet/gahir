package com.gahiragro.app.fragments.Dealer;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.Dealer.AllProductListAdapter;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.ALlProductsModel;
import com.gahiragro.app.model.FilterReviewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllProductListFragment extends BaseFragment {


//    @BindView(R.id.productListRV)
    RecyclerView productListRV;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

//    Unbinder unbinder;
    AllProductListAdapter mAllProductListAdapter;
    String lastPage = "false";
    List<ALlProductsModel.AllProduct> mGetProductsList = new ArrayList<>();
    List<ALlProductsModel.AllProduct> mTemGetProductsAL = new ArrayList<>();

    List<FilterReviewModel.AllReview> filterReviewList = new ArrayList<>();
    int page_no = 1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_all_product_list, container, false);
//        unbinder = ButterKnife.bind(this, view);
        productListRV=view.findViewById(R.id.productListRV);
        mainHeaderLL=view.findViewById(R.id.mainHeaderLL);
        mProgressBar=view.findViewById(R.id.mProgressBar);
        executeGetProductsApi();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();

    }

    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e("TAG", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetProductsApi() {

        if (page_no == 1) {

            showProgressDialog(getActivity());

        } else if (page_no > 1) {
             mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllProduct(mPram()).enqueue(new Callback<ALlProductsModel>() {
            @Override
            public void onResponse(Call<ALlProductsModel> call, Response<ALlProductsModel> response) {

                if (page_no == 1) {
                    dismissProgressDialog();
                }
                ALlProductsModel allProductsModel = response.body();
                if (allProductsModel.getStatus().equals("1")) {
                    lastPage = allProductsModel.getProductList().getLastPage();

                    if (page_no == 1) {
                        mGetProductsList = allProductsModel.getProductList().getAllProducts();
                    } else if (page_no > 1) {
                        mTemGetProductsAL = allProductsModel.getProductList().getAllProducts();
                    }
                    if (mTemGetProductsAL.size() > 0) {
                        mGetProductsList.addAll(mTemGetProductsAL);
                    }
                    if (page_no == 1) {
                        initRV();
                    } else {
                        mAllProductListAdapter.notifyDataSetChanged();
                    }

                } else if (allProductsModel.getStatus().equals("100")) {
                    showToast(getActivity(), allProductsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ALlProductsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("TAG", "**ERROR**" + t.toString());

            }
        });

    }


    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        productListRV.setLayoutManager(layoutManager);
        mAllProductListAdapter = new AllProductListAdapter(mGetProductsList, getActivity(),mInterfaceData);
        productListRV.setAdapter(mAllProductListAdapter);

    }
    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {

                if (lastPage.equals("FALSE")) {
                  //  mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeGetProductsApi();
                        } else {
                            if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };
}