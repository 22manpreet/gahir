package com.gahiragro.app.fragments;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.model.ContactUsModel;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = ContactUsFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.editNameET)
    EditText editNameET;
    //    @BindView(R.id.editEmailET)
    EditText editEmailET;
    //    @BindView(R.id.editMessageET)
    EditText editMessageET;
    //    @BindView(R.id.btnSignUpBT)
    Button btnSignUpBT;
    //    @BindView(R.id.nameRL)
    RelativeLayout nameRL;
    //    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
    //    @BindView(R.id.messageRL)
    RelativeLayout messageRL;

    //    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;


    //Initialize
//    Unbinder unbinder;
    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        initViews(view);


//        unbinder = ButterKnife.bind(this, view);
        editTextSelector(editNameET, nameRL, "");
        editTextSelector(editEmailET, emailRL, "");
        editTextSelector(editMessageET, messageRL, "");
        //To set Scroll Focus inside field
        editMessageET.setOnTouchListener((v, event) -> {
            if (editMessageET.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_SCROLL:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }
            }
            return false;
        });
        onViewClicked();
        return view;
    }

    private void initViews(View view) {
        editNameET = view.findViewById(R.id.editNameET);
        editEmailET = view.findViewById(R.id.editEmailET);
        editMessageET = view.findViewById(R.id.editMessageET);
        btnSignUpBT = view.findViewById(R.id.btnSignUpBT);
        nameRL = view.findViewById(R.id.nameRL);
        emailRL = view.findViewById(R.id.emailRL);
        messageRL = view.findViewById(R.id.messageRL);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

    /*
     * ContactUs APi
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("name", editNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("message", editMessageET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeContactApi() {

        showProgressDialog(getActivity());
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.contactUs(mParam()).enqueue(new Callback<ContactUsModel>() {
            @Override
            public void onResponse(Call<ContactUsModel> call, Response<ContactUsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**Response**" + response.body().toString());
                ContactUsModel mModel = response.body();
                if (mModel.getStatus() != null) {
                    if (mModel.getStatus().equals("1")) {
                        showAlertDialog(getActivity(), mModel.getMessage());
                        editNameET.setText("");
                        editEmailET.setText("");
                        editMessageET.setText("");
                    } else if (mModel.getStatus().equals("100")) {
                        showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                    }
                }


            }

            @Override
            public void onFailure(Call<ContactUsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private Boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(getActivity(), getString(R.string.please_enter_name));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(getActivity(), getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(getActivity(), getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editMessageET.getText().toString().trim().equals("")) {
            showAlertDialog(getActivity(), getString(R.string.please_enter_message));
            flag = false;
        }
        return flag;
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.btnSignUpBT})
    public void onViewClicked(/*View view*/) {
        btnSignUpBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (isNetworkAvailable(getActivity())) {
                        executeContactApi();
                    } else {
                        showToast(getActivity(), getString(R.string.internet_connection_error));
                    }
                }
            }
        });

        /*switch (view.getId()) {
            case R.id.btnSignUpBT:

                break;


        }*/
    }
}
