package com.gahiragro.app.fragments.Dealer;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.CheckWarrantyValidServiceActivity;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.CheckWarrantyService;
import com.lassi.domain.media.LassiOption;
import com.lassi.domain.media.MediaType;
import com.lassi.presentation.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class
CheckWarrantyFragment extends BaseFragment {

//    Unbinder unbinder;

//    @BindView(R.id.btnCheckBT)
    Button btnCheckBT;

//    @BindView(R.id.btnCheckBT)
    LinearLayout mainHeaderLL;
//    @BindView(R.id.enterSrET)
    EditText enterSrET;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_warranty, container, false);
//        unbinder = ButterKnife.bind(this, view);
        btnCheckBT = view.findViewById(R.id.btnCheckBT);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);
        enterSrET = view.findViewById(R.id.enterSrET);
        Onclick();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

    void Onclick() {
        btnCheckBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enterSrET.getText().toString().trim().equals("")) {
                    showToast(requireActivity(), "Please enter serial number.");
                } else {
                    executeCheckWarrentyApi();
                }

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
    }

    /*
     * Get Product Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("sr_no", enterSrET.getText().toString().trim());
        Log.e("TAG", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCheckWarrentyApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.checkServiceWarrenty(mDetailParam()).enqueue(new Callback<CheckWarrantyService>() {
            @Override
            public void onResponse(Call<CheckWarrantyService> call, Response<CheckWarrantyService> response) {
                dismissProgressDialog();
                CheckWarrantyService mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Intent intent = new Intent(getActivity(), CheckWarrantyValidServiceActivity.class);
                    intent.putExtra("warranty", mModel.getWarranty().getWarranty());
                    intent.putExtra("prodImage", mModel.getWarranty().getProductDetail().getProdImage());
                    intent.putExtra("sr_no", enterSrET.getText().toString().trim());
                    startActivity(intent);
                } else {
                    showAlertDialog(getActivity(), mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<CheckWarrantyService> call, Throwable t) {
                dismissProgressDialog();
                Log.e("TAG", "**ERROR**" + t.toString());
            }
        });

    }
}