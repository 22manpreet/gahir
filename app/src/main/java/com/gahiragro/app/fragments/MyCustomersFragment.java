package com.gahiragro.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.HomeActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.MyCustomersAdapter;
import com.gahiragro.app.adapters.NotificationAdapter;
import com.gahiragro.app.fragments.Dealer.MessageFragment;
import com.gahiragro.app.model.AllCustomersItem;
import com.gahiragro.app.model.CustomersListModel;
import com.gahiragro.app.model.NotificationModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCustomersFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = MyCustomersFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */

    /**
     * Widgets
     */
//    @BindView(R.id.customerRV)
    RecyclerView customerRV;

//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    int page_no = 1;
//    Unbinder unbinder;
    String lastPage = "FALSE";

    MyCustomersAdapter mMyCustomersAdapter;

    List<AllCustomersItem> mAllCustomers = new ArrayList<>();
    List<AllCustomersItem> mTempAL = new ArrayList<>();

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(getActivity());
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_customers, container, false);
//        unbinder = ButterKnife.bind(this, view);
        customerRV =view.findViewById(R.id.customerRV);
        mProgressBar =view.findViewById(R.id.mProgressBar);


        if (isNetworkAvailable(getActivity())) {
            if (mAllCustomers != null) {
                mAllCustomers.clear();
            }
            if (mTempAL != null) {
                mTempAL.clear();
            }
            executeCustomersApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }


        return view;
    }
    /*
     * MyCustomers APi
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeCustomersApi() {
        if (page_no == 1) {
            showProgressDialog(getActivity());
        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.customersListApi(mParam()).enqueue(new Callback<CustomersListModel>() {
            @Override
            public void onResponse(Call<CustomersListModel> call, Response<CustomersListModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                CustomersListModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    lastPage = mModel.getCustomerList().getLastPage();
                    if (page_no == 1) {
                        mAllCustomers = mModel.getCustomerList().getAllCustomers();
                    } else if (page_no > 1) {
                        mTempAL = mModel.getCustomerList().getAllCustomers();
                    }
                    if (mTempAL.size() > 0) {
                        mAllCustomers.addAll(mTempAL);
                    }
//                    for(int i=0;i<mAllCustomers.size();i++){
//                        System.out.println("notif_type : "+mAllCustomers.get(0).getNotifyType());
//                    }
                    if (page_no == 1) {
                        setMyCustomesListAdapter();
                    } else {
                        mMyCustomersAdapter.notifyDataSetChanged();
                    }
//                    mNotificationAL = mModel.getNotificationList().getAllNotifications();
//                    setNotificationListAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                }
                else if(mModel.getStatus().equals("0")){
                    showAlertDialog(getActivity(),mModel.getMessage());
//                    HomeActivity homeActivity = new HomeActivity();
//                    homeActivity.sendButtonVisibility();
                }
            }

            @Override
            public void onFailure(Call<CustomersListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private void setMyCustomesListAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        customerRV.setLayoutManager(layoutManager);
        mMyCustomersAdapter = new MyCustomersAdapter(getActivity(), mAllCustomers,mInterfaceData);
        customerRV.setAdapter(mMyCustomersAdapter);
    }

    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {

                if (lastPage.equals("FALSE")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeCustomersApi();
                        } else {
                            if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };
}
