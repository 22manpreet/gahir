package com.gahiragro.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;


public class PrivacyPolicyFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = PrivacyPolicyFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.webView)
    WebView webView;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;


//    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
//        unbinder = ButterKnife.bind(this, view);
        webView=view.findViewById(R.id.webView);
        mainHeaderLL=view.findViewById(R.id.mainHeaderLL);

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.loadUrl(Constants.PRIVACY_POLICY);
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

}