package com.gahiragro.app.fragments.Dealer;

public class MyModel {

    String name;
    int image;
    String nameTitle;


    public MyModel(String name, int image, String nameTitle) {
        this.name = name;
        this.image = image;
        this.nameTitle = nameTitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

}
