package com.gahiragro.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.SplashActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.activities.interfaces.NotificationClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.NotificationAdapter;
import com.gahiragro.app.fragments.Dealer.RegisterProductListFragment;
import com.gahiragro.app.model.NotificationModel;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = NotificationFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.mRecycleViewRV)
    RecyclerView mRecycleViewRV;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    //Initialize
    int page_no = 1;
//    Unbinder unbinder;
    String lastPage = "FALSE";
    NotificationAdapter mNotificationAdapter;
    List<NotificationModel.AllNotification> mNotificationAL = new ArrayList<>();
    List<NotificationModel.AllNotification> mTempAL = new ArrayList<>();

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
//        unbinder = ButterKnife.bind(this, view);
        mRecycleViewRV =view.findViewById(R.id.mRecycleViewRV);
        mainHeaderLL =view.findViewById(R.id.mainHeaderLL);
        mProgressBar =view.findViewById(R.id.mProgressBar);

        if (isNetworkAvailable(getActivity())) {
            if (mNotificationAL != null) {
                mNotificationAL.clear();
            }
            if (mTempAL != null) {
                mTempAL.clear();
            }
            executeNotificationApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        if (unbinder != null)
//            unbinder.unbind();
    }
    /*
     * Set Top Filter  Adapter
     * */

    private void setNotificationListAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        mRecycleViewRV.setLayoutManager(layoutManager);
        mNotificationAdapter = new NotificationAdapter(getActivity(), mNotificationAL, mInterfaceData);
        mRecycleViewRV.setAdapter(mNotificationAdapter);
    }

//    NotificationClickInterface mNotificationClickInterface = new NotificationClickInterface() {
//        @Override
//        public void onNotiClick(String notifyType) {
//            if(notifyType.equals("register_product")){
//                switchFragment(new RegisterProductListFragment(), "", false, null);
//            }
//        }
//    };
//    public void switchFragment( Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
//        Fragment newFragment = fragment;
//        if(fragment != null) {
//            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//            transaction.replace(R.id.containerRL, newFragment, Tag);
//            if (addToStack)
//                transaction.addToBackStack(Tag);
//            if (bundle != null)
//                fragment.setArguments(bundle);
//            transaction.commit();
//        }

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        if (fragment != null) {
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.containerRL, fragment, Tag);
//            if (addToStack)
//                fragmentTransaction.addToBackStack(Tag);
//            if (bundle != null)
//                fragment.setArguments(bundle);
//            fragmentTransaction.commit();
//            fragmentManager.executePendingTransactions();
//        }
  //  }


    /*
     * ContactUs APi
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    private void executeNotificationApi() {
        if (page_no == 1) {

            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.notificationApi(mParam()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                NotificationModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    lastPage = mModel.getNotificationList().getLastPage();
                    if (page_no == 1) {
                        mNotificationAL = mModel.getNotificationList().getAllNotifications();
                    } else if (page_no > 1) {
                        mTempAL = mModel.getNotificationList().getAllNotifications();
                    }
                    if (mTempAL.size() > 0) {
                        mNotificationAL.addAll(mTempAL);
                    }
                    for(int i=0;i<mNotificationAL.size();i++){
                        System.out.println("notif_type : "+mNotificationAL.get(0).getNotifyType());
                    }
                    if (page_no == 1) {
                        setNotificationListAdapter();
                    } else {
                        mNotificationAdapter.notifyDataSetChanged();
                    }


//                    mNotificationAL = mModel.getNotificationList().getAllNotifications();
//                    setNotificationListAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                }
                else if(mModel.getStatus().equals("0")){
                    showAlertDialog(getActivity(), mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {

                if (lastPage.equals("FALSE")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }

                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeNotificationApi();
                        } else {
                            if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };
}
