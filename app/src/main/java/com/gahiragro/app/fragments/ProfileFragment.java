package com.gahiragro.app.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;


public class ProfileFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProfileFragment.this.getClass().getSimpleName();


//    @BindView(R.id.mainHeaderRL)
    RelativeLayout mainHeaderRL;

    //Initialize
//    Unbinder unbinder;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
//        unbinder = ButterKnife.bind(this,view);
        mainHeaderRL = view.findViewById(R.id.mainHeaderRL);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressRClick(mainHeaderRL);
    }
}
