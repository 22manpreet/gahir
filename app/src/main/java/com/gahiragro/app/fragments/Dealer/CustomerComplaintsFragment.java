package com.gahiragro.app.fragments.Dealer;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.DealerPlayAudioActivity;
import com.gahiragro.app.activities.PlayAudioActivity;
import com.gahiragro.app.activities.interfaces.AudioClickInterface;
import com.gahiragro.app.activities.interfaces.DealerAudioClickInterface;
import com.gahiragro.app.adapters.Dealer.ComplaintsAdapter;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.AllComplainsItem;
import com.gahiragro.app.model.CustomerComplaintModel;
import com.gahiragro.app.model.GetAllDealerComplainsModel;
import com.gahiragro.app.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerComplaintsFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = CustomerComplaintsFragment.this.getClass().getSimpleName();
//    Unbinder unbinder;

//    @BindView(R.id.complaintsRV)
    RecyclerView complaintsRV;

//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;


//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

    int page_no = 1;
    ProgressBar progress_bar;
    String lastPage = "false";

    DealerComplaintsAdapter dealerComplaintsAdapter;
    List<AllComplainsItem> mlist = new ArrayList<>();
    List<AllComplainsItem> mTemplist = new ArrayList<>();

    DealerAudioClickInterface mDealerAudioClickInterface = new DealerAudioClickInterface() {
        @Override
        public void onAudioClick(int position, AllComplainsItem mModel) {
            Intent intent = new Intent(getActivity(), DealerPlayAudioActivity.class);
            intent.putExtra(Constants.deal_model,mModel);
            startActivity(intent);
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_customer_complaints, container, false);
//        unbinder = ButterKnife.bind(this, view);
        progress_bar = view.findViewById(R.id.progress_bar);
        complaintsRV = view.findViewById(R.id.complaintsRV);
        noDataFoundTV = view.findViewById(R.id.noDataFoundTV);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);

        if (isNetworkAvailable(getActivity())) {
            if (mlist != null) {
                mlist.clear();
            }
            if (mTemplist != null) {
                mTemplist.clear();
            }
            page_no = 1;
            executeGetAllComplaintsApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();

    }
    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetAllComplaintsApi() {
        if (page_no > 1) {
            progress_bar.setVisibility(View.VISIBLE);

        } else {
            showProgressDialog(getActivity());
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.GetAllDealerComplains(mPram()).enqueue(new Callback<GetAllDealerComplainsModel>() {
            @Override
            public void onResponse(Call<GetAllDealerComplainsModel> call, Response<GetAllDealerComplainsModel> response) {
                dismissProgressDialog();

                GetAllDealerComplainsModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    lastPage = mModel.getComplainList().getLastPage();

                    if (page_no == 1) {
                        mlist = mModel.getComplainList().getAllComplains();
                        Log.e(TAG, "onResponse: " + mlist);

                    } else if (page_no > 1) {
                        mTemplist = mModel.getComplainList().getAllComplains();
                        progress_bar.setVisibility(View.GONE);

                    }
                    if (mTemplist.size() > 0) {
                        mlist.addAll(mTemplist);
                    }

                    if (page_no == 1) {
                        initRV();
                    } else {
                        dealerComplaintsAdapter.notifyDataSetChanged();
                    }

                } else {
                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(mModel.getMessage());
                    // showAlertDialog(getActivity(), mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<GetAllDealerComplainsModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });

    }

    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        complaintsRV.setLayoutManager(layoutManager);
        dealerComplaintsAdapter = new DealerComplaintsAdapter(getActivity(), mlist, mDealerAudioClickInterface);
        complaintsRV.setAdapter(dealerComplaintsAdapter);
    }
}