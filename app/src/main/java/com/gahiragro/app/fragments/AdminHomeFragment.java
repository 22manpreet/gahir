package com.gahiragro.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.SplashActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.DealorOrderListAdapter;
import com.gahiragro.app.adapters.adminAdapters.AdminDealerOrderAdapter;
import com.gahiragro.app.model.adminDealorOrderModel.AdminDealorOrderModel;
import com.gahiragro.app.model.adminDealorOrderModel.AllOrdersItem;
import com.gahiragro.app.model.getOrderListMOdel.EnquiryDetail;
import com.gahiragro.app.model.getOrderListMOdel.OrderListModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminHomeFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = AdminHomeFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.mOrderRV)
    RecyclerView mOrderRV;

//    @BindView(R.id.dummyTV)
    TextView dummyTV;


//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    //Initialize
//    Unbinder unbinder;
    int page_no = 1;
    String lastPage = "false";
    AdminDealerOrderAdapter mAdminDealerOrderAdapter;
    DealorOrderListAdapter mDealorOrderListAdapter;
    ArrayList<EnquiryDetail> mEnquiryAL = new ArrayList<>();
    ArrayList<EnquiryDetail> mTempEnquiryAL = new ArrayList<>();
    List<AllOrdersItem> mAdminOrderAL = new ArrayList<>();
    List<AllOrdersItem> mTempAdminOrderAL = new ArrayList<>();

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
           setLogoutClick(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin_home, container, false);
//        unbinder = ButterKnife.bind(this, view);
        dummyTV = view.findViewById(R.id.dummyTV);
        mOrderRV = view.findViewById(R.id.mOrderRV);
        mProgressBar = view.findViewById(R.id.mProgressBar);
        if (isNetworkAvailable(getActivity())) {
            if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                if (mAdminOrderAL != null || mTempAdminOrderAL != null) {
                    mAdminOrderAL.clear();
                    mTempAdminOrderAL.clear();
                }
                executeAdminOrderListApi();
            } else {
                if (mEnquiryAL != null || mTempEnquiryAL != null) {
                    mEnquiryAL.clear();
                    mTempEnquiryAL.clear();
                }

                executeOrderListApi();
            }
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();
    }


    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeOrderListApi() {
        if (page_no == 1) {
            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        // showProgressDialog(getActivity());
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getOrderApi(mParam()).enqueue(new Callback<OrderListModel>() {
            @Override
            public void onResponse(Call<OrderListModel> call, Response<OrderListModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                OrderListModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    dummyTV.setVisibility(View.GONE);
                    lastPage = mModel.getOrderList().getLastPage();
                    for (int i = 0; i < mModel.getOrderList().getAllOrders().size(); i++) {
                        if (page_no == 1) {
                            mEnquiryAL.add(mModel.getOrderList().getAllOrders().get(i).getEnquiryDetail());
                        } else if (page_no > 1) {
                            mTempEnquiryAL.add(mModel.getOrderList().getAllOrders().get(i).getEnquiryDetail());
                        }
                        if (mTempEnquiryAL.size() > 0) {
                            mEnquiryAL.addAll(mTempEnquiryAL);
                        }
                        if (page_no == 1) {
                            setDealorOrderListAdapter();
                        } else {
                            mDealorOrderListAdapter.notifyDataSetChanged();
                        }
                    }
                    Log.e(TAG, "size::" + mEnquiryAL.size());
                }
                else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                }
                else {

                    if (mEnquiryAL.size() == 0) {
                        dummyTV.setVisibility(View.VISIBLE);

                    }
                }
            }

            @Override
            public void onFailure(Call<OrderListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }


    private void executeAdminOrderListApi() {
        if (page_no == 1) {
            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.adminDealorOrder(mParam()).enqueue(new Callback<AdminDealorOrderModel>() {
            @Override
            public void onResponse(Call<AdminDealorOrderModel> call, Response<AdminDealorOrderModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                AdminDealorOrderModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    dummyTV.setVisibility(View.GONE);
                    lastPage = mModel.getOrderList().getLastPage();

                    if (page_no == 1) {
                        mAdminOrderAL = mModel.getOrderList().getAllOrders();
                    } else if (page_no > 1) {
                        mTempAdminOrderAL = mModel.getOrderList().getAllOrders();

                    }
                    if (mTempAdminOrderAL.size() > 0) {
                        mAdminOrderAL.addAll(mTempAdminOrderAL);
                    }

                    if (page_no == 1) {
                        setAdminOrderListAdapter();
                    } else {
                        mAdminDealerOrderAdapter.notifyDataSetChanged();
                    }

                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                } else {
                    if (mAdminOrderAL.size() == 0) {
                        dummyTV.setVisibility(View.VISIBLE);

                    }
                }

            }

            @Override
            public void onFailure(Call<AdminDealorOrderModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private void setDealorOrderListAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mOrderRV.setLayoutManager(layoutManager);
        mDealorOrderListAdapter = new DealorOrderListAdapter(getActivity(), mEnquiryAL, mInterfaceData);
        mOrderRV.setAdapter(mDealorOrderListAdapter);
    }

    private void setAdminOrderListAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mOrderRV.setLayoutManager(layoutManager);
        mAdminDealerOrderAdapter = new AdminDealerOrderAdapter(getActivity(), mAdminOrderAL, mInterfaceData);
        mOrderRV.setAdapter(mAdminDealerOrderAdapter);
    }


    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {

                if (lastPage.equals("FALSE")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }

                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                                executeAdminOrderListApi();
                            } else {
                                executeOrderListApi();
                            }

                        } else {
                            if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };

}

