package com.gahiragro.app.fragments.Dealer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.AddProductsActivity;
import com.gahiragro.app.activities.interfaces.FilterReviewListInterface;
import com.gahiragro.app.activities.interfaces.FilterReviewStateInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.Dealer.CustomerAdapter;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.FilterReviewModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerReviewFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = CustomerReviewFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;

//    @BindView(R.id.mainHeaderRL)
    RelativeLayout mainHeaderRL;

    RecyclerView customerReviewRV;
    CustomerAdapter customerAdapter;
    FloatingActionButton floatingBt;
    List<FilterReviewModel.AllReview> mlist = new ArrayList<>();
    List<FilterReviewModel.AllReview> mTemplist = new ArrayList<>();
    ProgressBar progress_bar;

    int page_no = 1;
    String lastPage = "false";
    String stateIds = "", productIds = "";
    String productStateIds = " ";
    String type = "";
//    Unbinder unbinder;
    int regProdCount;

    FilterReviewListInterface filterInterface = new FilterReviewListInterface() {
        @Override
        public void onFilterReview(int position, List<String> mNewList) {

            Log.e(TAG, "**ERROR**" + mNewList.toString());

            productIds = TextUtils.join(", ", mNewList);
            GahirPreference.writeString(getActivity(), Constants.FILTER_TYPE_ID, productIds);
//            showToast(mActivity, productIds);
        }
    };

    FilterReviewStateInterface filterReviewStateInterface = new FilterReviewStateInterface() {
        @Override
        public void onFilterReview(int position, List<String> mNewList, String stateIds) {
            Log.e(TAG, "**ERROR**" + mNewList.toString());

            stateIds = TextUtils.join(", ", mNewList);
            GahirPreference.writeString(getActivity(), Constants.FILTER_TYPE_ID, stateIds);
        }
    };

/*    FilterReviewStateInterface stateFilterInterface = new FilterReviewStateInterface() {
        @Override
        public void onFilterReview(int position, List<String> mNewList) {

            Log.e(TAG, "**ERROR**" + mNewList.toString());

            stateIds = TextUtils.join(", ", mNewList);
            GahirPreference.writeString(getActivity(), Constants.FILTER_TYPE_ID, stateIds);

        }
    };*/

    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {
                if (lastPage.equals("FALSE")) {
                    //mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {

                            if (Constants.FILTER_BACK_PRESSED) {
                                executeFilterReview();
                            } else {
                                executeGetAllReviewApi();
                            }

                        } else {
                            /*if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);*/
                        }
                    }
                }, 1000);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_review, container, false);
//        unbinder = ButterKnife.bind(this, view);
        customerReviewRV = view.findViewById(R.id.customerReviewRV);
        noDataFoundTV = view.findViewById(R.id.noDataFoundTV);
        mainHeaderRL = view.findViewById(R.id.mainHeaderRL);
        floatingBt = view.findViewById(R.id.floatingBt);
        progress_bar = view.findViewById(R.id.progress_bar);
        OnClickBt();
        /* clear all filter values */
        Constants.FILTER_BACK_PRESSED = false;
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (unbinder != null) {
//            unbinder.unbind();
//        }

    }

//    @OnClick({R.id.floatingBt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.floatingBt:
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //You need to add the following line for this solution to work; thanks skayred
        backPressRClick(mainHeaderRL);
//        if (customerAdapter != null) {
//            customerAdapter.notifyDataSetChanged();
//        }
        callApi();
    }

    private void callApi() {

        if (isNetworkAvailable(getActivity())) {
            if (mlist != null) {
                mlist.clear();
            }
            if (mTemplist != null) {
                mTemplist.clear();
            }
            page_no = 1;

            type = GahirPreference.readString(getActivity(), Constants.FILTER_TYPE, "");
            productStateIds = GahirPreference.readString(getActivity(), Constants.FILTER_TYPE_ID, "");

            if (Constants.FILTER_BACK_PRESSED == true) {

                if (type.equals("product")) {
                    productIds = GahirPreference.readString(getActivity(), Constants.FILTER_TYPE_ID, "");

                } else if (type.equals("state")) {
                    stateIds = GahirPreference.readString(getActivity(), Constants.FILTER_TYPE_ID, "");
                }

                executeFilterReview();
            } else {
                executeGetAllReviewApi();
            }
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    private Map<String, String> mPramFilter() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        mMap.put("type_id", productStateIds);
        mMap.put("type", (type));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeFilterReview() {
        if (page_no > 1) {
          //  progress_bar.setVisibility(View.VISIBLE);
        } else {
            showProgressDialog(getActivity());
        }

        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getFilterReview(mPramFilter()).enqueue(new Callback<FilterReviewModel>() {
            @Override
            public void onResponse(Call<FilterReviewModel> call, Response<FilterReviewModel> response) {

                dismissProgressDialog();

                /* after filter condition false */
                Constants.FILTER_BACK_PRESSED = false;

                FilterReviewModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    lastPage = mModel.getReviewList().getLastPage();
                    if (page_no == 1) {
                        mlist = mModel.getReviewList().getAllReviews();
                        Log.e(TAG, "onResponse: " + mlist);

                    } else if (page_no > 1) {
                        mTemplist = mModel.getReviewList().getAllReviews();
                        progress_bar.setVisibility(View.GONE);

                    }
                    if (mTemplist.size() > 0) {
                        mlist.addAll(mTemplist);
                    }

                    if (page_no == 1) {
                        initSetRV();
                    } else {
                        customerAdapter.notifyDataSetChanged();
                    }

                } else {
                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(mModel.getMessage());
                    //showAlertDialog(getActivity(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<FilterReviewModel> call, Throwable t) {
                /* after filter condition false */

                Constants.FILTER_BACK_PRESSED = false;
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });
    }

    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetAllReviewApi() {

        if (page_no > 1) {
          //  progress_bar.setVisibility(View.VISIBLE);
        } else {
            showProgressDialog(getActivity());
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllReviews(mPram()).enqueue(new Callback<FilterReviewModel>() {
            @Override
            public void onResponse(Call<FilterReviewModel> call, Response<FilterReviewModel> response) {
                dismissProgressDialog();
                FilterReviewModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    regProdCount = mModel.getRegisterCount();
                    noDataFoundTV.setVisibility(View.GONE);
                    lastPage = mModel.getReviewList().getLastPage();

                    if (page_no == 1) {
                        mlist = mModel.getReviewList().getAllReviews();
                        Log.e(TAG, "onResponse: " + mlist);

                    } else if (page_no > 1) {
                        mTemplist = mModel.getReviewList().getAllReviews();
                        progress_bar.setVisibility(View.GONE);

                    }
                    if (mTemplist.size() > 0) {
                        mlist.addAll(mTemplist);
                    }

                    if (page_no == 1) {
                        initSetRV();
                    } else {
                        customerAdapter.notifyDataSetChanged();
                    }

                }
                else {
                    regProdCount = mModel.getRegisterCount();
                    if(mlist.size()==0)
                    {
                        noDataFoundTV.setVisibility(View.VISIBLE);
                        noDataFoundTV.setText(mModel.getMessage());
                    }

                    // showAlertDialog(getActivity(), mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<FilterReviewModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });

    }

    private void initSetRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        customerReviewRV.setLayoutManager(layoutManager);
        customerAdapter = new CustomerAdapter(mlist, getContext(), mInterfaceData);
        customerReviewRV.setAdapter(customerAdapter);
        customerReviewRV.setMotionEventSplittingEnabled(false);
        //addListData();
    }

    void OnClickBt() {
        floatingBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (regProdCount <= 0) {
                    showAlertDialog(getActivity(), getString(R.string.please_add_product_first));
                } else {
                    Intent intent = new Intent(getActivity(), AddProductsActivity.class);
                    startActivity(intent);
                }

            }
        });

    }


}