package com.gahiragro.app.fragments.Dealer;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.adapters.Dealer.ProductServicesAdapter;
import com.gahiragro.app.adapters.Dealer.ServiceManualAdapter;
import com.gahiragro.app.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;


public class ServiceManualFragment extends BaseFragment {

//    Unbinder unbinder;
//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;
//    @BindView(R.id.serviceManualRV)
    RecyclerView serviceManualRV;

    List<MyModel> mlist = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view = inflater.inflate(R.layout.fragment_service_manual, container, false);
//        unbinder = ButterKnife.bind(this, view);
        mainHeaderLL =view.findViewById(R.id.mainHeaderLL);
        serviceManualRV =view.findViewById(R.id.serviceManualRV);
        // recycler list data
        initRV();

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    void initRV() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        serviceManualRV.setLayoutManager(layoutManager);
        ServiceManualAdapter serviceManualAdapter = new ServiceManualAdapter(mlist, getActivity());
        serviceManualRV.setAdapter(serviceManualAdapter);

        addListData();
    }

    void addListData() {

        mlist.add(new MyModel("Laser leveller",R.drawable.ic_leveller_new,"SUPER-484"));
        mlist.add(new MyModel("Spray Pump",R.drawable.ic_spray_pump_new,"R-30"));
        mlist.add(new MyModel("Mud Loader",R.drawable.ic_mud_loader_new,"SUPER-555"));
        mlist.add(new MyModel("Harvester",R.drawable.ic_harvestor_new,"SUPER-653"));
        mlist.add(new MyModel("Super Seeder",R.drawable.ic_super_seeder_new ,"SUPER-321"));
        mlist.add(new MyModel("Straw Reaper",R.drawable.ic_straw_reaper_new,"SUPER-453"));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();

    }
}