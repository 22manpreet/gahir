package com.gahiragro.app.fragments.Dealer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.PlayServiceAudioActivity;
import com.gahiragro.app.activities.PlayAudioActivity;
import com.gahiragro.app.activities.interfaces.AudioClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.Dealer.ComplaintsServiceAdapter;
import com.gahiragro.app.fragments.BaseFragment;
import com.gahiragro.app.model.CustomerComplaintModel;
import com.gahiragro.app.model.ServiceComplaintModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ComplaintsServiceFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = ComplaintsServiceFragment.this.getClass().getSimpleName();
//    Unbinder unbinder;

//    @BindView(R.id.complaintsServiceRV)
    RecyclerView complaintsServiceRV;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;

//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;


    int page_no = 1;
    ProgressBar progress_bar;
    String lastPage = "false";
    ComplaintsServiceAdapter mComplaintsServiceAdapter;
    List<ServiceComplaintModel.ComplainList.AllComplain> mlist = new ArrayList<>();
    List<ServiceComplaintModel.ComplainList.AllComplain> mTemplist = new ArrayList<>();


    AudioClickInterface mAudioClickInterface = new AudioClickInterface() {
        @Override
        public void onAudioClick(int position, CustomerComplaintModel.ComplainList.AllComplain mModel) {

        }

        @Override
        public void onServiceAudioClick(int position, ServiceComplaintModel.ComplainList.AllComplain mModel) {
            Intent intent = new Intent(getActivity(), PlayServiceAudioActivity.class);
            intent.putExtra(Constants.model,mModel);
            startActivity(intent);
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complaints_service, container, false);
//        unbinder = ButterKnife.bind(this, view);
        progress_bar = view.findViewById(R.id.progress_bar);
        complaintsServiceRV = view.findViewById(R.id.complaintsServiceRV);
        mainHeaderLL = view.findViewById(R.id.mainHeaderLL);
        noDataFoundTV = view.findViewById(R.id.noDataFoundTV);
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
        if (isNetworkAvailable(getActivity())) {
            if (mlist != null) {
                mlist.clear();
            }
            if (mTemplist != null) {
                mTemplist.clear();
            }
            page_no = 1;
            executeGetAllServiceComplaintsApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();

    }


    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetAllServiceComplaintsApi() {

//        if (page_no > 1) {
//            progress_bar.setVisibility(View.VISIBLE);
//
//        } else {
//            showProgressDialog(getActivity());
//        }
        if (page_no == 1) {

            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            // mProgressBar.setVisibility(View.GONE);
        }

        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllServiceComplaint(mPram()).enqueue(new Callback<ServiceComplaintModel>() {
            @Override
            public void onResponse(Call<ServiceComplaintModel> call, Response<ServiceComplaintModel> response) {
//                dismissProgressDialog();
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                ServiceComplaintModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    lastPage = mModel.getComplainList().getLastPage();

                    if (page_no == 1) {
                        mlist = mModel.getComplainList().getAllComplains();
                        Log.e(TAG, "onResponse: " + mlist);

                    } else if (page_no > 1) {
                        mTemplist = mModel.getComplainList().getAllComplains();
                        progress_bar.setVisibility(View.GONE);

                    }
                    if (mTemplist.size() > 0) {
                        mlist.addAll(mTemplist);
                    }

                    if (page_no == 1) {
                        initRV();
                    } else {
                        mComplaintsServiceAdapter.notifyDataSetChanged();
                    }

                } else {

                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(mModel.getMessage());
                    // showAlertDialog(getActivity(), mModel.getMessage());
                }
            }
            @Override
            public void onFailure(Call<ServiceComplaintModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });
    }
    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        complaintsServiceRV.setLayoutManager(layoutManager);
        mComplaintsServiceAdapter = new ComplaintsServiceAdapter(getActivity(), mlist,mAudioClickInterface,mInterfaceData);
        complaintsServiceRV.setAdapter(mComplaintsServiceAdapter);

    }
    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {

                if (lastPage.equals("FALSE")) {
                    //  mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeGetAllServiceComplaintsApi();
                        } else {
//                            if (mProgressBar.isShown())
//                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };
}