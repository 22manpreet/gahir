package com.gahiragro.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.SplashActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.DealorOrderListAdapter;
import com.gahiragro.app.adapters.adminAdapters.AdminDealorEnquiryAdapter;
import com.gahiragro.app.adapters.DealorEnquiryAdapter;
import com.gahiragro.app.model.adminDealorEnquiry.DealorAdminEnquiry;
import com.gahiragro.app.model.enquirymodel.AllEnquiriesItem;
import com.gahiragro.app.model.enquirymodel.EnquiryModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DealorEnquiryListFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealorEnquiryListFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.mEnquiryListRV)
    RecyclerView mEnquiryListRV;

//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

//    @BindView(R.id.dummyTV)
    TextView dummyTV;

//    @BindView(R.id.mainHeaderRL)
    RelativeLayout mainHeaderRL;

    //Initialize
//    Unbinder unbinder;
    int page_no = 1;
    String lastPage = "false";
    String strEventType = "";
    DealorEnquiryAdapter mDealorOrderAdapter;
    AdminDealorEnquiryAdapter mAdminDealorEnquiryAdapter;
    List<AllEnquiriesItem> mEnquiryAL = new ArrayList<>();
    List<AllEnquiriesItem> mTempEnquiryAL = new ArrayList<>();
    List<com.gahiragro.app.model.adminDealorEnquiry.AllEnquiriesItem> mAdminEnquiryAL = new ArrayList<>();
    List<com.gahiragro.app.model.adminDealorEnquiry.AllEnquiriesItem> mTempAdminEnquiryAL = new ArrayList<>();
    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
         setLogoutClick(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dealor_enquiry_list, container, false);
//        unbinder = ButterKnife.bind(this, view);
        mEnquiryListRV =view.findViewById(R.id.mEnquiryListRV);
        mProgressBar =view.findViewById(R.id.mProgressBar);
        dummyTV =view.findViewById(R.id.dummyTV);
        mainHeaderRL =view.findViewById(R.id.mainHeaderRL);


        if (isNetworkAvailable(getActivity())) {
            if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                strEventType = "admin/sales";
                if (mAdminEnquiryAL != null || mTempAdminEnquiryAL != null) {

                    mAdminEnquiryAL.clear();
                    mTempAdminEnquiryAL.clear();
                }
                executeGetAdminEnquiryApi();
            } else {
                if (mEnquiryAL != null || mTempEnquiryAL != null) {
                    mEnquiryAL.clear();
                    mTempEnquiryAL.clear();
                }
                executeGetEnquiryApi();
            }
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        backPressRClick(mainHeaderRL);
    }

    /*
     * Add Enquiry  Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetEnquiryApi() {
        if (page_no == 1) {

            showProgressDialog(getActivity());

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllEnquiry(mParam()).enqueue(new Callback<EnquiryModel>() {
            @Override
            public void onResponse(Call<EnquiryModel> call, Response<EnquiryModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                EnquiryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    dummyTV.setVisibility(View.GONE);
                    lastPage = mModel.getEnquiryList().getLastPage();
                    if (page_no == 1) {
                        mEnquiryAL = mModel.getEnquiryList().getAllEnquiries();
                    } else if (page_no > 1) {
                        mTempEnquiryAL = mModel.getEnquiryList().getAllEnquiries();
                    }
                    if (mTempEnquiryAL.size() > 0) {
                        mEnquiryAL.addAll(mTempEnquiryAL);
                    }
//                    if (mEnquiryAL.size() > 0) {
//                        setEnquiryAdapter();
//                    } else {
//                        mDealorOrderAdapter.notifyDataSetChanged();
//                    }

                    if (page_no == 1) {
                        setEnquiryAdapter();
                } else {
                        mDealorOrderAdapter.notifyDataSetChanged();
                }


                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                } else {
                    if (mEnquiryAL.size() == 0) {
                        dummyTV.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<EnquiryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    /*
     * Dealor Admin Enquiry  Api
     * */
    private void executeGetAdminEnquiryApi() {
        if (page_no == 1) {
            showProgressDialog(getActivity());
        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.dealorAdminEnquiry(mParam()).enqueue(new Callback<DealorAdminEnquiry>() {
            @Override
            public void onResponse(Call<DealorAdminEnquiry> call, Response<DealorAdminEnquiry> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                DealorAdminEnquiry mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    dummyTV.setVisibility(View.GONE);
                    lastPage = mModel.getEnquiryList().getLastPage();
                    if (page_no == 1) {
                        mAdminEnquiryAL = mModel.getEnquiryList().getAllEnquiries();
                    } else if (page_no > 1) {
                        mTempAdminEnquiryAL = mModel.getEnquiryList().getAllEnquiries();
                    }
                    if (mTempAdminEnquiryAL.size() > 0) {
                        mAdminEnquiryAL.addAll(mTempAdminEnquiryAL);
                    }

                    if (page_no == 1) {
                        setAdminEnquiryAdapter();
                    } else {
                        mAdminDealorEnquiryAdapter.notifyDataSetChanged();
                    }

                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(getActivity(), mModel.getMessage(), mLogoutInterface);
                } else {
                    if (mAdminEnquiryAL.size() == 0) {
                        dummyTV.setVisibility(View.VISIBLE);
                    }
                    //showToast(getActivity(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DealorAdminEnquiry> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private void setEnquiryAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mEnquiryListRV.setLayoutManager(layoutManager);
        mDealorOrderAdapter = new DealorEnquiryAdapter(getActivity(), mEnquiryAL, mInterfaceData);
        mEnquiryListRV.setAdapter(mDealorOrderAdapter);
    }

    private void setAdminEnquiryAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mEnquiryListRV.setLayoutManager(layoutManager);
        mAdminDealorEnquiryAdapter = new AdminDealorEnquiryAdapter(getActivity(), mAdminEnquiryAL, mInterfaceData, strEventType);
        mEnquiryListRV.setAdapter(mAdminDealorEnquiryAdapter);
    }

    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {
                if (lastPage.equals("FALSE")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                                executeGetAdminEnquiryApi();
                            } else {
                                executeGetEnquiryApi();
                            }

                        } else {
                            if (mProgressBar.isShown())
                                mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1500);
            }
        }
    };

}