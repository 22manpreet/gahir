package com.gahiragro.app.model.enquirymodel;

import com.google.gson.annotations.SerializedName;

public class SystemDetail{

	@SerializedName("disable")
	private String disable;

	@SerializedName("trac_name")
	private String tracName;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setTracName(String tracName){
		this.tracName = tracName;
	}

	public String getTracName(){
		return tracName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	@Override
 	public String toString(){
		return 
			"SystemDetail{" + 
			"disable = '" + disable + '\'' + 
			",trac_name = '" + tracName + '\'' + 
			",id = '" + id + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			"}";
		}
}