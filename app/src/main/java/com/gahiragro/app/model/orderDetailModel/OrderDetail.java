package com.gahiragro.app.model.orderDetailModel;

import com.google.gson.annotations.SerializedName;

public class OrderDetail{

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("dealer_code")
	private String dealerCode;

	@SerializedName("amount")
	private String amount;

	public String getDisplay_date() {
		return display_date;
	}

	public void setDisplay_date(String display_date) {
		this.display_date = display_date;
	}

	@SerializedName("display_date")
	private String display_date;

	@SerializedName("utr_no")
	private String utrNo;

	@SerializedName("enquiry_detail")
	private EnquiryDetail enquiryDetail;

	@SerializedName("dispatch_date")
	private String dispatchDate;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("status")
	private String status;

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setDealerCode(String dealerCode){
		this.dealerCode = dealerCode;
	}

	public String getDealerCode(){
		return dealerCode;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setUtrNo(String utrNo){
		this.utrNo = utrNo;
	}

	public String getUtrNo(){
		return utrNo;
	}

	public void setEnquiryDetail(EnquiryDetail enquiryDetail){
		this.enquiryDetail = enquiryDetail;
	}

	public EnquiryDetail getEnquiryDetail(){
		return enquiryDetail;
	}

	public void setDispatchDate(String dispatchDate){
		this.dispatchDate = dispatchDate;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setEnquiryId(String enquiryId){
		this.enquiryId = enquiryId;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public void setSerialNo(String serialNo){
		this.serialNo = serialNo;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetail{" + 
			"booking_id = '" + bookingId + '\'' + 
			",dealer_code = '" + dealerCode + '\'' + 
			",amount = '" + amount + '\'' + 
			",utr_no = '" + utrNo + '\'' + 
			",enquiry_detail = '" + enquiryDetail + '\'' + 
			",dispatch_date = '" + dispatchDate + '\'' + 
			",id = '" + id + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",enquiry_id = '" + enquiryId + '\'' + 
			",serial_no = '" + serialNo + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}