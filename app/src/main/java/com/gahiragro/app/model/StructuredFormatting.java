package com.gahiragro.app.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class StructuredFormatting implements Serializable {

	@SerializedName("main_text")
	private String mainText;

	@SerializedName("main_text_matched_substrings")
	private List<MainTextMatchedSubstringsItem> mainTextMatchedSubstrings;

	@SerializedName("secondary_text")
	private String secondaryText;

	@SerializedName("secondary_text_matched_substrings")
	private List<SecondaryTextMatchedSubstringsItem> secondaryTextMatchedSubstrings;

	public void setMainText(String mainText){
		this.mainText = mainText;
	}

	public String getMainText(){
		return mainText;
	}

	public void setMainTextMatchedSubstrings(List<MainTextMatchedSubstringsItem> mainTextMatchedSubstrings){
		this.mainTextMatchedSubstrings = mainTextMatchedSubstrings;
	}

	public List<MainTextMatchedSubstringsItem> getMainTextMatchedSubstrings(){
		return mainTextMatchedSubstrings;
	}

	public void setSecondaryText(String secondaryText){
		this.secondaryText = secondaryText;
	}

	public String getSecondaryText(){
		return secondaryText;
	}

	public void setSecondaryTextMatchedSubstrings(List<SecondaryTextMatchedSubstringsItem> secondaryTextMatchedSubstrings){
		this.secondaryTextMatchedSubstrings = secondaryTextMatchedSubstrings;
	}

	public List<SecondaryTextMatchedSubstringsItem> getSecondaryTextMatchedSubstrings(){
		return secondaryTextMatchedSubstrings;
	}

	@Override
 	public String toString(){
		return 
			"StructuredFormatting{" + 
			"main_text = '" + mainText + '\'' + 
			",main_text_matched_substrings = '" + mainTextMatchedSubstrings + '\'' + 
			",secondary_text = '" + secondaryText + '\'' + 
			",secondary_text_matched_substrings = '" + secondaryTextMatchedSubstrings + '\'' + 
			"}";
		}
}