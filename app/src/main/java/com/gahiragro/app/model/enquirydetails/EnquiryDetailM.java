package com.gahiragro.app.model.enquirydetails;

import com.google.gson.annotations.SerializedName;

public class EnquiryDetailM {

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("total")
	private String total;

	@SerializedName("status_text")
	private String status_text;

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public String getDispatch_day() {
		return dispatch_day;
	}

	public void setDispatch_day(String dispatch_day) {
		this.dispatch_day = dispatch_day;
	}

	@SerializedName("order_time")
	private String order_time;

	@SerializedName("dispatch_day")
	private String dispatch_day;


	public String getStatus_text() {
		return status_text;
	}

	public void setStatus_text(String status_text) {
		this.status_text = status_text;
	}

	@SerializedName("dealer_code")
	private String dealer_code;

	public String getDealer_code() {
		return dealer_code;
	}

	public void setDealer_code(String dealer_code) {
		this.dealer_code = dealer_code;
	}

	@SerializedName("system")
	private String system;

	@SerializedName("order_status")
	private Boolean order_status;

	public Boolean getOrder_status() {
		return order_status;
	}

	public void setOrder_status(Boolean order_status) {
		this.order_status = order_status;
	}

	@SerializedName("user_id")
	private String userId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("id")
	private String id;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private String status;

	public void setProductDetail(ProductDetail productDetail){
		this.productDetail = productDetail;
	}

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public void setAccessories(Accessories accessories){
		this.accessories = accessories;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setProdName(String prodName){
		this.prodName = prodName;
	}

	public String getProdName(){
		return prodName;
	}

	public void setAccName(String accName){
		this.accName = accName;
	}

	public String getAccName(){
		return accName;
	}

	public void setSystemDetail(SystemDetail systemDetail){
		this.systemDetail = systemDetail;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setSystem(String system){
		this.system = system;
	}

	public String getSystem(){
		return system;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAccessory(String accessory){
		this.accessory = accessory;
	}

	public String getAccessory(){
		return accessory;
	}

	public void setEnquiryId(String enquiryId){
		this.enquiryId = enquiryId;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"EnquiryDetail{" + 
			"product_detail = '" + productDetail + '\'' + 
			",accessories = '" + accessories + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",type = '" + type + '\'' + 
			",prod_name = '" + prodName + '\'' + 
			",acc_name = '" + accName + '\'' + 
			",system_detail = '" + systemDetail + '\'' + 
			",total = '" + total + '\'' + 
			",system = '" + system + '\'' + 
			",user_id = '" + userId + '\'' + 
			",product_id = '" + productId + '\'' + 
			",qty = '" + qty + '\'' + 
			",id = '" + id + '\'' + 
			",accessory = '" + accessory + '\'' + 
			",enquiry_id = '" + enquiryId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}