package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceComplaintModel implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("complain_list")
    @Expose
    private ComplainList complainList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ComplainList getComplainList() {
        return complainList;
    }

    public void setComplainList(ComplainList complainList) {
        this.complainList = complainList;
    }

    public class ComplainList implements Serializable{

        @SerializedName("all_complains")
        @Expose
        private List<AllComplain> allComplains = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllComplain> getAllComplains() {
            return allComplains;
        }

        public void setAllComplains(List<AllComplain> allComplains) {
            this.allComplains = allComplains;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

        public class AllComplain  implements  Serializable{
            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            @SerializedName("product_name")
            @Expose
            private String product_name;
            @SerializedName("service_id")
            @Expose
            private String serviceId;
            @SerializedName("complain_id")
            @Expose
            private String complainId;
            @SerializedName("user_id")
            @Expose
            private String userId;
            @SerializedName("lubrication")
            @Expose
            private String lubrication;
            @SerializedName("part_change")
            @Expose
            private String partChange;
            @SerializedName("part_repair")
            @Expose
            private String partRepair;
            @SerializedName("machine_status")
            @Expose
            private String machineStatus;
            @SerializedName("hour_spent")
            @Expose
            private String hourSpent;
            @SerializedName("full_machine_check")
            @Expose
            private String fullMachineCheck;
            @SerializedName("creation_date")
            @Expose
            private String creationDate;
            @SerializedName("part_change_list")
            @Expose
            private List<Object> partChangeList = null;
            @SerializedName("user_detail")
            @Expose
            private UserDetail userDetail;
            @SerializedName("complain_detail")
            @Expose
            private ComplainDetail complainDetail;
            @SerializedName("status")
            @Expose
            private String status;

            public String getServiceId() {
                return serviceId;
            }

            public void setServiceId(String serviceId) {
                this.serviceId = serviceId;
            }

            public String getComplainId() {
                return complainId;
            }

            public void setComplainId(String complainId) {
                this.complainId = complainId;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getLubrication() {
                return lubrication;
            }

            public void setLubrication(String lubrication) {
                this.lubrication = lubrication;
            }

            public String getPartChange() {
                return partChange;
            }

            public void setPartChange(String partChange) {
                this.partChange = partChange;
            }

            public String getPartRepair() {
                return partRepair;
            }

            public void setPartRepair(String partRepair) {
                this.partRepair = partRepair;
            }

            public String getMachineStatus() {
                return machineStatus;
            }

            public void setMachineStatus(String machineStatus) {
                this.machineStatus = machineStatus;
            }

            public String getHourSpent() {
                return hourSpent;
            }

            public void setHourSpent(String hourSpent) {
                this.hourSpent = hourSpent;
            }

            public String getFullMachineCheck() {
                return fullMachineCheck;
            }

            public void setFullMachineCheck(String fullMachineCheck) {
                this.fullMachineCheck = fullMachineCheck;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public List<Object> getPartChangeList() {
                return partChangeList;
            }

            public void setPartChangeList(List<Object> partChangeList) {
                this.partChangeList = partChangeList;
            }

            public UserDetail getUserDetail() {
                return userDetail;
            }

            public void setUserDetail(UserDetail userDetail) {
                this.userDetail = userDetail;
            }

            public ComplainDetail getComplainDetail() {
                return complainDetail;
            }

            public void setComplainDetail(ComplainDetail complainDetail) {
                this.complainDetail = complainDetail;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public class ComplainDetail implements Serializable{

                @SerializedName("comp_id")
                @Expose
                private String compId;
                @SerializedName("user_id")
                @Expose
                private String userId;
                @SerializedName("reg_prod_id")
                @Expose
                private String regProdId;
                @SerializedName("contact_no")
                @Expose
                private String contactNo;
                @SerializedName("prod_sr_no")
                @Expose
                private String prodSrNo;
                @SerializedName("comp_reason")
                @Expose
                private String compReason;
                @SerializedName("reason_detail")
                @Expose
                private String reasonDetail;
                @SerializedName("support_img")
                @Expose
                private ArrayList<String> supportImg;
                @SerializedName("support_audio")
                @Expose
                private String supportAudio;
                @SerializedName("assigned_to")
                @Expose
                private String assignedTo;
                @SerializedName("comp_status")
                @Expose
                private String compStatus;
                @SerializedName("creation_date")
                @Expose
                private String creationDate;

                public String getCompId() {
                    return compId;
                }

                public void setCompId(String compId) {
                    this.compId = compId;
                }

                public String getUserId() {
                    return userId;
                }

                public void setUserId(String userId) {
                    this.userId = userId;
                }

                public String getRegProdId() {
                    return regProdId;
                }

                public void setRegProdId(String regProdId) {
                    this.regProdId = regProdId;
                }

                public String getContactNo() {
                    return contactNo;
                }

                public void setContactNo(String contactNo) {
                    this.contactNo = contactNo;
                }

                public String getProdSrNo() {
                    return prodSrNo;
                }

                public void setProdSrNo(String prodSrNo) {
                    this.prodSrNo = prodSrNo;
                }

                public String getCompReason() {
                    return compReason;
                }

                public void setCompReason(String compReason) {
                    this.compReason = compReason;
                }

                public String getReasonDetail() {
                    return reasonDetail;
                }

                public void setReasonDetail(String reasonDetail) {
                    this.reasonDetail = reasonDetail;
                }

                public ArrayList<String> getSupportImg() {
                    return supportImg;
                }

                public void setSupportImg(ArrayList<String> supportImg) {
                    this.supportImg = supportImg;
                }

                public String getSupportAudio() {
                    return supportAudio;
                }

                public void setSupportAudio(String supportAudio) {
                    this.supportAudio = supportAudio;
                }

                public String getAssignedTo() {
                    return assignedTo;
                }

                public void setAssignedTo(String assignedTo) {
                    this.assignedTo = assignedTo;
                }

                public String getCompStatus() {
                    return compStatus;
                }

                public void setCompStatus(String compStatus) {
                    this.compStatus = compStatus;
                }

                public String getCreationDate() {
                    return creationDate;
                }

                public void setCreationDate(String creationDate) {
                    this.creationDate = creationDate;
                }

            }

            public class UserDetail implements Serializable {

                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("first_name")
                @Expose
                private String firstName;
                @SerializedName("last_name")
                @Expose
                private String lastName;
                @SerializedName("username")
                @Expose
                private String username;
                @SerializedName("firm_name")
                @Expose
                private String firmName;
                @SerializedName("phone_no")
                @Expose
                private String phoneNo;
                @SerializedName("image")
                @Expose
                private String image;
                @SerializedName("role")
                @Expose
                private String role;
                @SerializedName("password")
                @Expose
                private String password;
                @SerializedName("auth_key")
                @Expose
                private String authKey;
                @SerializedName("device_type")
                @Expose
                private String deviceType;
                @SerializedName("device_token")
                @Expose
                private String deviceToken;
                @SerializedName("user_lat")
                @Expose
                private String userLat;
                @SerializedName("app_signup")
                @Expose
                private String appSignup;
                @SerializedName("admin_signup")
                @Expose
                private String adminSignup;
                @SerializedName("user_long")
                @Expose
                private String userLong;
                @SerializedName("bio")
                @Expose
                private String bio;
                @SerializedName("dealer_code")
                @Expose
                private String dealerCode;
                @SerializedName("serial_no")
                @Expose
                private String serialNo;
                @SerializedName("address")
                @Expose
                private String address;
                @SerializedName("created_on")
                @Expose
                private String createdOn;
                @SerializedName("disable")
                @Expose
                private String disable;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getFirmName() {
                    return firmName;
                }

                public void setFirmName(String firmName) {
                    this.firmName = firmName;
                }

                public String getPhoneNo() {
                    return phoneNo;
                }

                public void setPhoneNo(String phoneNo) {
                    this.phoneNo = phoneNo;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getRole() {
                    return role;
                }

                public void setRole(String role) {
                    this.role = role;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public String getAuthKey() {
                    return authKey;
                }

                public void setAuthKey(String authKey) {
                    this.authKey = authKey;
                }

                public String getDeviceType() {
                    return deviceType;
                }

                public void setDeviceType(String deviceType) {
                    this.deviceType = deviceType;
                }

                public String getDeviceToken() {
                    return deviceToken;
                }

                public void setDeviceToken(String deviceToken) {
                    this.deviceToken = deviceToken;
                }

                public String getUserLat() {
                    return userLat;
                }

                public void setUserLat(String userLat) {
                    this.userLat = userLat;
                }

                public String getAppSignup() {
                    return appSignup;
                }

                public void setAppSignup(String appSignup) {
                    this.appSignup = appSignup;
                }

                public String getAdminSignup() {
                    return adminSignup;
                }

                public void setAdminSignup(String adminSignup) {
                    this.adminSignup = adminSignup;
                }

                public String getUserLong() {
                    return userLong;
                }

                public void setUserLong(String userLong) {
                    this.userLong = userLong;
                }

                public String getBio() {
                    return bio;
                }

                public void setBio(String bio) {
                    this.bio = bio;
                }

                public String getDealerCode() {
                    return dealerCode;
                }

                public void setDealerCode(String dealerCode) {
                    this.dealerCode = dealerCode;
                }

                public String getSerialNo() {
                    return serialNo;
                }

                public void setSerialNo(String serialNo) {
                    this.serialNo = serialNo;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getCreatedOn() {
                    return createdOn;
                }

                public void setCreatedOn(String createdOn) {
                    this.createdOn = createdOn;
                }

                public String getDisable() {
                    return disable;
                }

                public void setDisable(String disable) {
                    this.disable = disable;
                }

            }
        }
    }
}
