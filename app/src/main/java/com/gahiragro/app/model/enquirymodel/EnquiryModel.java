package com.gahiragro.app.model.enquirymodel;

import com.google.gson.annotations.SerializedName;

public class EnquiryModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("enquiry_list")
	private EnquiryList enquiryList;

	@SerializedName("status")
	private String status;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setEnquiryList(EnquiryList enquiryList){
		this.enquiryList = enquiryList;
	}

	public EnquiryList getEnquiryList(){
		return enquiryList;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"EnquiryModel{" + 
			"access_token = '" + accessToken + '\'' + 
			",message = '" + message + '\'' + 
			",enquiry_list = '" + enquiryList + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}