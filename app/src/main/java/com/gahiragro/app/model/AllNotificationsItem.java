package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class AllNotificationsItem{

	@SerializedName("notify_type")
	private String notifyType;

	@SerializedName("notify_title")
	private String notifyTitle;

	@SerializedName("display_date")
	private String displayDate;

	@SerializedName("notify_cat")
	private String notifyCat;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("notified_user")
	private String notifiedUser;

	@SerializedName("notify_id")
	private String notifyId;

	@SerializedName("sender_id")
	private String senderId;

	@SerializedName("notify_message")
	private String notifyMessage;

	public String getNotifyType(){
		return notifyType;
	}

	public String getNotifyTitle(){
		return notifyTitle;
	}

	public String getDisplayDate(){
		return displayDate;
	}

	public String getNotifyCat(){
		return notifyCat;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getNotifiedUser(){
		return notifiedUser;
	}

	public String getNotifyId(){
		return notifyId;
	}

	public String getSenderId(){
		return senderId;
	}

	public String getNotifyMessage(){
		return notifyMessage;
	}
}