package com.gahiragro.app.model.orderDetailModel;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetail{

	@SerializedName("prod_desc")
	private List<String> prodDesc;

	@SerializedName("prod_type")
	private String prodType;

	@SerializedName("accessories")
	private List<AccessoriesItem> accessories;

	@SerializedName("prod_pdf")
	private List<ProdPdf> prodPdf;

	@SerializedName("prod_price")
	private String prodPrice;

	@SerializedName("prod_video")
	private List<ProdVideo> prodVideo;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_qty")
	private String prodQty;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("prod_sno")
	private String prodSno;

	@SerializedName("prod_image")
	private String prodImage;

	@SerializedName("systems")
	private List<SystemsItem> systems;

	@SerializedName("disable")
	private String disable;

	@SerializedName("price")
	private String price;

	@SerializedName("prod_acc")
	private String prodAcc;

	@SerializedName("id")
	private String id;

	@SerializedName("prod_model")
	private String prodModel;

	public void setProdDesc(List<String> prodDesc){
		this.prodDesc = prodDesc;
	}

	public List<String> getProdDesc(){
		return prodDesc;
	}

	public void setProdType(String prodType){
		this.prodType = prodType;
	}

	public String getProdType(){
		return prodType;
	}

	public void setAccessories(List<AccessoriesItem> accessories){
		this.accessories = accessories;
	}

	public List<AccessoriesItem> getAccessories(){
		return accessories;
	}

	public void setProdPdf(List<ProdPdf> prodPdf){
		this.prodPdf = prodPdf;
	}

	public List<ProdPdf> getProdPdf(){
		return prodPdf;
	}

	public void setProdPrice(String prodPrice){
		this.prodPrice = prodPrice;
	}

	public String getProdPrice(){
		return prodPrice;
	}

	public void setProdVideo(List<ProdVideo> prodVideo){
		this.prodVideo = prodVideo;
	}

	public List<ProdVideo> getProdVideo(){
		return prodVideo;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setProdQty(String prodQty){
		this.prodQty = prodQty;
	}

	public String getProdQty(){
		return prodQty;
	}

	public void setProdName(String prodName){
		this.prodName = prodName;
	}

	public String getProdName(){
		return prodName;
	}

	public void setProdSno(String prodSno){
		this.prodSno = prodSno;
	}

	public String getProdSno(){
		return prodSno;
	}

	public void setProdImage(String prodImage){
		this.prodImage = prodImage;
	}

	public String getProdImage(){
		return prodImage;
	}

	public void setSystems(List<SystemsItem> systems){
		this.systems = systems;
	}

	public List<SystemsItem> getSystems(){
		return systems;
	}

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setProdAcc(String prodAcc){
		this.prodAcc = prodAcc;
	}

	public String getProdAcc(){
		return prodAcc;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setProdModel(String prodModel){
		this.prodModel = prodModel;
	}

	public String getProdModel(){
		return prodModel;
	}

	@Override
 	public String toString(){
		return 
			"ProductDetail{" + 
			"prod_desc = '" + prodDesc + '\'' + 
			",prod_type = '" + prodType + '\'' + 
			",accessories = '" + accessories + '\'' + 
			",prod_pdf = '" + prodPdf + '\'' + 
			",prod_price = '" + prodPrice + '\'' + 
			",prod_video = '" + prodVideo + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",prod_qty = '" + prodQty + '\'' + 
			",prod_name = '" + prodName + '\'' + 
			",prod_sno = '" + prodSno + '\'' + 
			",prod_image = '" + prodImage + '\'' + 
			",systems = '" + systems + '\'' + 
			",disable = '" + disable + '\'' + 
			",price = '" + price + '\'' + 
			",prod_acc = '" + prodAcc + '\'' + 
			",id = '" + id + '\'' + 
			",prod_model = '" + prodModel + '\'' + 
			"}";
		}
	public class ProdPdf  {

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	public class ProdVideo{

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}