package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class SubPartsItem{

	@SerializedName("psp_name")
	private String pspName;

	@SerializedName("psp_key")
	private String pspKey;

	@SerializedName("pp_id")
	private String ppId;

	@SerializedName("disable")
	private String disable;

	@SerializedName("psp_id")
	private String pspId;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_id")
	private String prodId;

	public String getPspName(){
		return pspName;
	}

	public String getPspKey(){
		return pspKey;
	}

	public String getPpId(){
		return ppId;
	}

	public String getDisable(){
		return disable;
	}

	public String getPspId(){
		return pspId;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getProdId(){
		return prodId;
	}
}