package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class CustomersListModel{

	@SerializedName("customer_list")
	private CustomerList customerList;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public CustomerList getCustomerList(){
		return customerList;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}