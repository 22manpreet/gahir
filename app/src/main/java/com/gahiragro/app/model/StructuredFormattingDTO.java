package com.gahiragro.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


public class StructuredFormattingDTO implements Serializable {

	@SerializedName("main_text")
	private String mainText;

	@SerializedName("main_text_matched_substrings")
	private List<MainTextMatchedSubstringsDTO> mainTextMatchedSubstrings;

	@SerializedName("secondary_text")
	private String secondaryText;

	@SerializedName("secondary_text_matched_substrings")
	private List<SecondaryTextMatchedSubstringsDTO> secondaryTextMatchedSubstrings;

	public void setMainText(String mainText){
		this.mainText = mainText;
	}

	public String getMainText(){
		return mainText;
	}

	public void setMainTextMatchedSubstrings(List<MainTextMatchedSubstringsDTO> mainTextMatchedSubstrings){
		this.mainTextMatchedSubstrings = mainTextMatchedSubstrings;
	}

	public List<MainTextMatchedSubstringsDTO> getMainTextMatchedSubstrings(){
		return mainTextMatchedSubstrings;
	}

	public void setSecondaryText(String secondaryText){
		this.secondaryText = secondaryText;
	}

	public String getSecondaryText(){
		return secondaryText;
	}

	public void setSecondaryTextMatchedSubstrings(List<SecondaryTextMatchedSubstringsDTO> secondaryTextMatchedSubstrings){
		this.secondaryTextMatchedSubstrings = secondaryTextMatchedSubstrings;
	}

	public List<SecondaryTextMatchedSubstringsDTO> getSecondaryTextMatchedSubstrings(){
		return secondaryTextMatchedSubstrings;
	}

	@Override
 	public String toString(){
		return 
			"StructuredFormattingDTO{" + 
			"main_text = '" + mainText + '\'' + 
			",main_text_matched_substrings = '" + mainTextMatchedSubstrings + '\'' + 
			",secondary_text = '" + secondaryText + '\'' + 
			",secondary_text_matched_substrings = '" + secondaryTextMatchedSubstrings + '\'' + 
			"}";
		}
}