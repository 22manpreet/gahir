package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckWarrantyService {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("warranty")
    @Expose
    private Warranty warranty;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Warranty getWarranty() {
        return warranty;
    }

    public void setWarranty(Warranty warranty) {
        this.warranty = warranty;
    }

    public class Warranty {

        @SerializedName("product_detail")
        @Expose
        private ProductDetail productDetail;
        @SerializedName("warranty")
        @Expose
        private String warranty;

        public ProductDetail getProductDetail() {
            return productDetail;
        }

        public void setProductDetail(ProductDetail productDetail) {
            this.productDetail = productDetail;
        }

        public String getWarranty() {
            return warranty;
        }

        public void setWarranty(String warranty) {
            this.warranty = warranty;
        }
    }

public class ProductDetail {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("prod_name")
    @Expose
    private String prodName;
    @SerializedName("prod_model")
    @Expose
    private String prodModel;
    @SerializedName("prod_cat")
    @Expose
    private String prodCat;
    @SerializedName("prod_type")
    @Expose
    private String prodType;
    @SerializedName("prod_image")
    @Expose
    private String prodImage;
    @SerializedName("prod_price")
    @Expose
    private String prodPrice;
    @SerializedName("prod_sno")
    @Expose
    private String prodSno;
    @SerializedName("prod_desc")
    @Expose
    private List<String> prodDesc = null;
    @SerializedName("prod_qty")
    @Expose
    private String prodQty;
    @SerializedName("prod_video")
    @Expose
    private List<Object> prodVideo = null;
    @SerializedName("prod_pdf")
    @Expose
    private List<Object> prodPdf = null;
    @SerializedName("prod_acc")
    @Expose
    private String prodAcc;
    @SerializedName("creation_date")
    @Expose
    private String creationDate;
    @SerializedName("disable")
    @Expose
    private String disable;
    @SerializedName("accessories")
    @Expose
    private List<Accessory> accessories = null;
    @SerializedName("systems")
    @Expose
    private List<System> systems = null;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("part_list")
    @Expose
    private List<Object> partList = null;
    @SerializedName("op_manual")
    @Expose
    private String opManual;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdModel() {
        return prodModel;
    }

    public void setProdModel(String prodModel) {
        this.prodModel = prodModel;
    }

    public String getProdCat() {
        return prodCat;
    }

    public void setProdCat(String prodCat) {
        this.prodCat = prodCat;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }

    public String getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(String prodPrice) {
        this.prodPrice = prodPrice;
    }

    public String getProdSno() {
        return prodSno;
    }

    public void setProdSno(String prodSno) {
        this.prodSno = prodSno;
    }

    public List<String> getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(List<String> prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getProdQty() {
        return prodQty;
    }

    public void setProdQty(String prodQty) {
        this.prodQty = prodQty;
    }

    public List<Object> getProdVideo() {
        return prodVideo;
    }

    public void setProdVideo(List<Object> prodVideo) {
        this.prodVideo = prodVideo;
    }

    public List<Object> getProdPdf() {
        return prodPdf;
    }

    public void setProdPdf(List<Object> prodPdf) {
        this.prodPdf = prodPdf;
    }

    public String getProdAcc() {
        return prodAcc;
    }

    public void setProdAcc(String prodAcc) {
        this.prodAcc = prodAcc;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public List<System> getSystems() {
        return systems;
    }

    public void setSystems(List<System> systems) {
        this.systems = systems;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Object> getPartList() {
        return partList;
    }

    public void setPartList(List<Object> partList) {
        this.partList = partList;
    }

    public String getOpManual() {
        return opManual;
    }

    public void setOpManual(String opManual) {
        this.opManual = opManual;
    }

    public class System {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("trac_name")
        @Expose
        private String tracName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTracName() {
            return tracName;
        }

        public void setTracName(String tracName) {
            this.tracName = tracName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }


    }
    public class Accessory {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("acc_name")
        @Expose
        private String accName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAccName() {
            return accName;
        }

        public void setAccName(String accName) {
            this.accName = accName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }
}

}
