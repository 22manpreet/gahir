package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecentSearchModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("search_list")
    @Expose
    private SearchList searchList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public SearchList getSearchList() {
        return searchList;
    }

    public void setSearchList(SearchList searchList) {
        this.searchList = searchList;
    }

    public class SearchList {

        @SerializedName("search_list")
        @Expose
        private List<String> searchList = null;

        public List<String> getSearchList() {
            return searchList;
        }

        public void setSearchList(List<String> searchList) {
            this.searchList = searchList;
        }

    }
}
