package com.gahiragro.app.model;

import java.io.Serializable;

public class DummyModel implements Serializable {
    String name;
    String id;
    String img;
    String banner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner() {return banner;}

    public void setBanner(String banner) {this.banner = banner;}


}
