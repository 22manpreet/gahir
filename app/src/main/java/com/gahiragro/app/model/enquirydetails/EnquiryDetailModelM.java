package com.gahiragro.app.model.enquirydetails;

import com.google.gson.annotations.SerializedName;

public class EnquiryDetailModelM {

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("enquiry_detail")
	private EnquiryDetailM enquiryDetail;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setEnquiryDetail(EnquiryDetailM enquiryDetail){
		this.enquiryDetail = enquiryDetail;
	}

	public EnquiryDetailM getEnquiryDetail(){
		return enquiryDetail;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"EnquiryDetailModel{" + 
			"access_token = '" + accessToken + '\'' + 
			",enquiry_detail = '" + enquiryDetail + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}