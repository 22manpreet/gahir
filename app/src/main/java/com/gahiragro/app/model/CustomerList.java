package com.gahiragro.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CustomerList{

	@SerializedName("all_customers")
	private List<AllCustomersItem> allCustomers;

	@SerializedName("last_page")
	private String lastPage;

	public List<AllCustomersItem> getAllCustomers(){
		return allCustomers;
	}

	public String getLastPage(){
		return lastPage;
	}
}