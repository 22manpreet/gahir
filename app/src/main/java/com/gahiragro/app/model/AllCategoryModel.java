package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllCategoryModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("category_list")
    @Expose
    private CategoryList categoryList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public CategoryList getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(CategoryList categoryList) {
        this.categoryList = categoryList;
    }

    public class CategoryList {

        @SerializedName("all_categories")
        @Expose
        private List<AllCategory> allCategories = null;

        public List<AllCategory> getAllCategories() {
            return allCategories;
        }

        public void setAllCategories(List<AllCategory> allCategories) {
            this.allCategories = allCategories;
        }

    }
    public class AllCategory {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("cat_name")
        @Expose
        private String catName;
        @SerializedName("cat_type")
        @Expose
        private String catType;
        @SerializedName("cat_image")
        @Expose
        private String catImage;
        @SerializedName("cat_banner")
        @Expose
        private String catBanner;
        @SerializedName("feature_product")
        @Expose
        private String featureProduct;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatType() {
            return catType;
        }

        public void setCatType(String catType) {
            this.catType = catType;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public String getCatBanner() {
            return catBanner;
        }

        public void setCatBanner(String catBanner) {
            this.catBanner = catBanner;
        }


        public String getFeatureProduct() {
            return featureProduct;
        }

        public void setFeatureProduct(String featureProduct) {
            this.featureProduct = featureProduct;
        }

    }
}
