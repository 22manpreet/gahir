package com.gahiragro.app.model.adminDealorOrderModel;

import com.google.gson.annotations.SerializedName;

public class AllOrdersItem{

	@SerializedName("deposit_amount")
	private String depositAmount;

	@SerializedName("amount")
	private String amount;

	@SerializedName("remark")
	private String remark;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("dealer_code")
	private String dealerCode;

	@SerializedName("utr_no")
	private String utrNo;

	@SerializedName("enquiry_detail")
	private EnquiryDetail enquiryDetail;

	@SerializedName("dispatch_date")
	private String dispatchDate;

	@SerializedName("id")
	private String id;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("status")
	private String status;

	@SerializedName("dealer_detail")
	private DealerDetail dealerDetail;

	public String getDepositAmount(){
		return depositAmount;
	}

	public String getAmount(){
		return amount;
	}

	public String getRemark(){
		return remark;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getBookingId(){
		return bookingId;
	}

	public String getDealerCode(){
		return dealerCode;
	}

	public String getUtrNo(){
		return utrNo;
	}

	public EnquiryDetail getEnquiryDetail(){
		return enquiryDetail;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public String getId(){
		return id;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public String getStatus(){
		return status;
	}

	public DealerDetail getDealerDetail(){
		return dealerDetail;
	}
}