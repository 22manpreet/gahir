package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class AllCustomersItem{

	@SerializedName("phone_no")
	private String phoneNo;

	@SerializedName("image")
	private String image;

	@SerializedName("role")
	private String role;

	@SerializedName("address")
	private String address;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("user_lat")
	private String userLat;

	@SerializedName("user_long")
	private String userLong;

	@SerializedName("bio")
	private String bio;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("admin_signup")
	private String adminSignup;

	@SerializedName("auth_key")
	private String authKey;

	@SerializedName("dealer_code")
	private String dealerCode;

	@SerializedName("password")
	private String password;

	@SerializedName("created_on")
	private String createdOn;

	@SerializedName("disable")
	private String disable;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("firm_name")
	private String firmName;

	@SerializedName("id")
	private String id;

	@SerializedName("app_signup")
	private String appSignup;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("username")
	private String username;

	public String getPhoneNo(){
		return phoneNo;
	}

	public String getImage(){
		return image;
	}

	public String getRole(){
		return role;
	}

	public String getAddress(){
		return address;
	}

	public String getLastName(){
		return lastName;
	}

	public String getUserLat(){
		return userLat;
	}

	public String getUserLong(){
		return userLong;
	}

	public String getBio(){
		return bio;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public String getAdminSignup(){
		return adminSignup;
	}

	public String getAuthKey(){
		return authKey;
	}

	public String getDealerCode(){
		return dealerCode;
	}

	public String getPassword(){
		return password;
	}

	public String getCreatedOn(){
		return createdOn;
	}

	public String getDisable(){
		return disable;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getFirmName(){
		return firmName;
	}

	public String getId(){
		return id;
	}

	public String getAppSignup(){
		return appSignup;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public String getUsername(){
		return username;
	}
}