package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SearchDealerModel implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_list")
    @Expose
    private UserList userList;
    @SerializedName("helpline")
    @Expose
    private String helpline;
    private final static long serialVersionUID = -9184020977866855481L;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }

    public String getHelpline() {
        return helpline;
    }

    public void setHelpline(String helpline) {
        this.helpline = helpline;
    }


    public class UserList implements Serializable {

        @SerializedName("all_users")
        @Expose
        private List<AllUser> allUsers = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;
        private final static long serialVersionUID = 9128819293895158140L;

        public List<AllUser> getAllUsers() {
            return allUsers;
        }

        public void setAllUsers(List<AllUser> allUsers) {
            this.allUsers = allUsers;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

    }

    public class AllUser implements Serializable
    {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("district")
        @Expose
        private String district;
        @SerializedName("state")
        @Expose
        private String state;
        private final static long serialVersionUID = 2945469374286455515L;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }

}