package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllcartItemsModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cart_items")
    @Expose
    private ArrayList<CartItem> cartItems = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(ArrayList<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public class CartItem {

        @SerializedName("cart_id")
        @Expose
        private String cartId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("psp_id")
        @Expose
        private String pspId;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("enq_add")
        @Expose
        private String enqAdd;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("psp_detail")
        @Expose
        private PspDetail pspDetail;

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getPspId() {
            return pspId;
        }

        public void setPspId(String pspId) {
            this.pspId = pspId;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getEnqAdd() {
            return enqAdd;
        }

        public void setEnqAdd(String enqAdd) {
            this.enqAdd = enqAdd;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public PspDetail getPspDetail() {
            return pspDetail;
        }

        public void setPspDetail(PspDetail pspDetail) {
            this.pspDetail = pspDetail;
        }

        public class PspDetail {

            @SerializedName("psp_id")
            @Expose
            private String pspId;
            @SerializedName("pp_id")
            @Expose
            private String ppId;
            @SerializedName("prod_id")
            @Expose
            private String prodId;
            @SerializedName("psp_name")
            @Expose
            private String pspName;
            @SerializedName("psp_key")
            @Expose
            private String pspKey;
            @SerializedName("creation_date")
            @Expose
            private String creationDate;
            @SerializedName("disable")
            @Expose
            private String disable;

            public String getPspId() {
                return pspId;
            }

            public void setPspId(String pspId) {
                this.pspId = pspId;
            }

            public String getPpId() {
                return ppId;
            }

            public void setPpId(String ppId) {
                this.ppId = ppId;
            }

            public String getProdId() {
                return prodId;
            }

            public void setProdId(String prodId) {
                this.prodId = prodId;
            }

            public String getPspName() {
                return pspName;
            }

            public void setPspName(String pspName) {
                this.pspName = pspName;
            }

            public String getPspKey() {
                return pspKey;
            }

            public void setPspKey(String pspKey) {
                this.pspKey = pspKey;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public String getDisable() {
                return disable;
            }

            public void setDisable(String disable) {
                this.disable = disable;
            }
        }
    }
}
