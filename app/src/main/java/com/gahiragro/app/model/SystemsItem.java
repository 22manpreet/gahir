package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SystemsItem implements Serializable {

	@SerializedName("disable")
	private String disable;

	@SerializedName("trac_name")
	private String tracName;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("sys_name")
	private String sysName;

	@SerializedName("pump_type")
	private String pumpType;

	@SerializedName("piston")
	private String piston;

	@SerializedName("suction")
	private String suction;

	@SerializedName("weight")
	private String weight;

	@SerializedName("pressure")
	private String pressure;

	@SerializedName("power")
	private String power;

	@SerializedName("brand")
	private String brand;

	@SerializedName("rpm")
	private String rpm;

	public String getDisable(){
		return disable;
	}

	public String getTracName(){
		return tracName;
	}

	public String getId(){
		return id;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getSysName(){
		return sysName;
	}

	public String getPumpType(){
		return pumpType;
	}

	public String getPiston(){
		return piston;
	}

	public String getSuction(){
		return suction;
	}

	public String getWeight(){
		return weight;
	}

	public String getPressure(){
		return pressure;
	}

	public String getPower(){
		return power;
	}

	public String getBrand(){
		return brand;
	}

	public String getRpm(){
		return rpm;
	}
}