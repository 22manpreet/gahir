package com.gahiragro.app.model.adminDealorEnquiry;

import com.google.gson.annotations.SerializedName;

public class SystemDetail{

	@SerializedName("pump_type")
	private String pumpType;

	@SerializedName("piston")
	private String piston;

	@SerializedName("disable")
	private String disable;

	@SerializedName("suction")
	private String suction;

	@SerializedName("weight")
	private String weight;

	@SerializedName("id")
	private String id;

	@SerializedName("pressure")
	private String pressure;

	@SerializedName("power")
	private String power;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("brand")
	private String brand;

	@SerializedName("rpm")
	private String rpm;

	@SerializedName("sys_name")
	private String sysName;

	@SerializedName("trac_name")
	private String tracName;

	public String getPumpType(){
		return pumpType;
	}

	public String getPiston(){
		return piston;
	}

	public String getDisable(){
		return disable;
	}

	public String getSuction(){
		return suction;
	}

	public String getWeight(){
		return weight;
	}

	public String getId(){
		return id;
	}

	public String getPressure(){
		return pressure;
	}

	public String getPower(){
		return power;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getBrand(){
		return brand;
	}

	public String getRpm(){
		return rpm;
	}

	public String getSysName(){
		return sysName;
	}

	public String getTracName(){
		return tracName;
	}
}