package com.gahiragro.app.model.getOrderListMOdel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EnquiryDetail implements Serializable {

	@SerializedName("approval_date")
	private String approvalDate;

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("enq_status")
	private String enqStatus;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("order_status")
	private boolean orderStatus;


	@SerializedName("dispatch_day")
	private String dispatchDay;


	@SerializedName("total")
	private String total;

	@SerializedName("system")
	private String system;

	@SerializedName("order_id")
	private String order_id;

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	@SerializedName("user_id")
	private String userId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("id")
	private String id;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private Object status;

	public String getApprovalDate(){
		return approvalDate;
	}

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public String getEnqStatus(){
		return enqStatus;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getType(){
		return type;
	}

	public String getProdName(){
		return prodName;
	}

	public String getAccName(){
		return accName;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public boolean isOrderStatus(){
		return orderStatus;
	}
	public String getDispatchDay(){
		return dispatchDay;
	}

	public String getTotal(){
		return total;
	}

	public String getSystem(){
		return system;
	}

	public String getUserId(){
		return userId;
	}

	public String getProductId(){
		return productId;
	}

	public String getQty(){
		return qty;
	}

	public String getId(){
		return id;
	}

	public String getAccessory(){
		return accessory;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public Object getStatus(){
		return status;
	}
}