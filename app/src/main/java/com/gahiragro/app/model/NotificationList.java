package com.gahiragro.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NotificationList{

	@SerializedName("all_notifications")
	private List<AllNotificationsItem> allNotifications;

	@SerializedName("last_page")
	private String lastPage;

	public List<AllNotificationsItem> getAllNotifications(){
		return allNotifications;
	}

	public String getLastPage(){
		return lastPage;
	}
}