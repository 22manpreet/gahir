package com.gahiragro.app.model.getOrderListMOdel;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetail implements Serializable {

	@SerializedName("prod_desc")
	private List<String> prodDesc;

	@SerializedName("prod_type")
	private String prodType;

	@SerializedName("accessories")
	private List<AccessoriesItem> accessories;

	@SerializedName("prod_price")
	private String prodPrice;

	@SerializedName("prod_pdf")
	private List<ProdPdf> prodPdf;

	@SerializedName("prod_video")
	private List<ProdVideo> prodVideo;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_qty")
	private String prodQty;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("prod_sno")
	private String prodSno;

	@SerializedName("prod_image")
	private String prodImage;

	@SerializedName("systems")
	private List<SystemsItem> systems;

	@SerializedName("disable")
	private String disable;

	@SerializedName("price")
	private String price;

	@SerializedName("prod_acc")
	private String prodAcc;

	@SerializedName("id")
	private String id;

	@SerializedName("prod_model")
	private String prodModel;

	public List<String> getProdDesc(){
		return prodDesc;
	}

	public String getProdType(){
		return prodType;
	}

	public List<AccessoriesItem> getAccessories(){
		return accessories;
	}

	public List<ProdPdf> getProdPdf(){
		return prodPdf;
	}

	public String getProdPrice(){
		return prodPrice;
	}

	public List<ProdVideo> getProdVideo(){
		return prodVideo;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getProdQty(){
		return prodQty;
	}

	public String getProdName(){
		return prodName;
	}

	public String getProdSno(){
		return prodSno;
	}

	public String getProdImage(){
		return prodImage;
	}

	public List<SystemsItem> getSystems(){
		return systems;
	}

	public String getDisable(){
		return disable;
	}

	public String getPrice(){
		return price;
	}

	public String getProdAcc(){
		return prodAcc;
	}

	public String getId(){
		return id;
	}

	public String getProdModel(){
		return prodModel;
	}

	public class ProdPdf implements Serializable {

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	public class ProdVideo implements Serializable{

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}
