package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class EmailLoginModel{
	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("user_detail")
	private UserDetail userDetail;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public UserDetail getUserDetail(){
		return userDetail;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}
