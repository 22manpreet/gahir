package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateDistrictModel {

	@SerializedName("state_list")
	private List<StateListItem> stateList;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setStateList(List<StateListItem> stateList){
		this.stateList = stateList;
	}

	public List<StateListItem> getStateList(){
		return stateList;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}


	public static class StateListItem{

		@SerializedName("state_description")
		private String stateDescription;

		@SerializedName("state_title")
		private String stateTitle;

		@SerializedName("all_districts")
		private List<AllDistrictsItem> allDistricts;

		@SerializedName("state_id")
		private String stateId;

		@SerializedName("status")
		private String status;

		public void setStateDescription(String stateDescription){
			this.stateDescription = stateDescription;
		}

		public String getStateDescription(){
			return stateDescription;
		}

		public void setStateTitle(String stateTitle){
			this.stateTitle = stateTitle;
		}

		public String getStateTitle(){
			return stateTitle;
		}

		public void setAllDistricts(List<AllDistrictsItem> allDistricts){
			this.allDistricts = allDistricts;
		}

		public List<AllDistrictsItem> getAllDistricts(){
			return allDistricts;
		}

		public void setStateId(String stateId){
			this.stateId = stateId;
		}

		public String getStateId(){
			return stateId;
		}

		public void setStatus(String status){
			this.status = status;
		}

		public String getStatus(){
			return status;
		}

		@Override
		public String toString() {
			return stateTitle;
		}
	}

	public static class AllDistrictsItem{

		@SerializedName("districtid")
		private String districtid;

		@SerializedName("district_title")
		private String districtTitle;

		@SerializedName("state_id")
		private String stateId;

		@SerializedName("district_description")
		private String districtDescription;

		@SerializedName("district_status")
		private String districtStatus;

		public void setDistrictid(String districtid){
			this.districtid = districtid;
		}

		public String getDistrictid(){
			return districtid;
		}

		public void setDistrictTitle(String districtTitle){
			this.districtTitle = districtTitle;
		}

		public String getDistrictTitle(){
			return districtTitle;
		}

		public void setStateId(String stateId){
			this.stateId = stateId;
		}

		public String getStateId(){
			return stateId;
		}

		public void setDistrictDescription(String districtDescription){
			this.districtDescription = districtDescription;
		}

		public String getDistrictDescription(){
			return districtDescription;
		}

		public void setDistrictStatus(String districtStatus){
			this.districtStatus = districtStatus;
		}

		public String getDistrictStatus(){
			return districtStatus;
		}
	}
}