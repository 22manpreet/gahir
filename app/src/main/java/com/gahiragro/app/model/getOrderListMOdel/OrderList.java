package com.gahiragro.app.model.getOrderListMOdel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderList implements Serializable {

	@SerializedName("all_orders")
	private List<AllOrdersItem> allOrders;

	@SerializedName("last_page")
	private String lastPage;

	public List<AllOrdersItem> getAllOrders(){
		return allOrders;
	}

	public String getLastPage(){
		return lastPage;
	}
}