package com.gahiragro.app.model.adminDealorEnquiry;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnquiryList{

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_enquiries")
	private ArrayList<AllEnquiriesItem> allEnquiries;

	public String getLastPage(){
		return lastPage;
	}

	public List<AllEnquiriesItem> getAllEnquiries(){
		return allEnquiries;
	}
	public void setAllEnquiries(ArrayList<AllEnquiriesItem> allEnquiries){
		this.allEnquiries = allEnquiries;
	}

}