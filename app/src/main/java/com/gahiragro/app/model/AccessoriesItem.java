package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccessoriesItem implements Serializable {

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("disable")
	private String disable;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	public String getAccName(){
		return accName;
	}

	public String getDisable(){
		return disable;
	}

	public String getId(){
		return id;
	}

	public String getCreationDate(){
		return creationDate;
	}
}