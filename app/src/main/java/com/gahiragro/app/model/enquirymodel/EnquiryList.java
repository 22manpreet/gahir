package com.gahiragro.app.model.enquirymodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnquiryList{

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_enquiries")
	private ArrayList<AllEnquiriesItem> allEnquiries;

	public void setLastPage(String lastPage){
		this.lastPage = lastPage;
	}

	public String getLastPage(){
		return lastPage;
	}

	public void setAllEnquiries(ArrayList<AllEnquiriesItem> allEnquiries){
		this.allEnquiries = allEnquiries;
	}

	public List<AllEnquiriesItem> getAllEnquiries(){
		return allEnquiries;
	}

	@Override
 	public String toString(){
		return 
			"EnquiryList{" + 
			"last_page = '" + lastPage + '\'' + 
			",all_enquiries = '" + allEnquiries + '\'' + 
			"}";
		}
}