package com.gahiragro.app.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ComplainList implements Serializable {

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_complains")
	private List<AllComplainsItem> allComplains;

	public String getLastPage(){
		return lastPage;
	}

	public List<AllComplainsItem> getAllComplains(){
		return allComplains;
	}
}