package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class SendMessageToAllCustomersModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}