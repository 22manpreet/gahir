package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterProductModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("product_list")
    @Expose
    private ProductList productList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    public class ProductList {

        @SerializedName("all_products")
        @Expose
        private List<AllProduct> allProducts = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllProduct> getAllProducts() {
            return allProducts;
        }

        public void setAllProducts(List<AllProduct> allProducts) {
            this.allProducts = allProducts;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }
    }
    public class ProdPdf  {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }
    public class ProdVideo {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }
        public class AllProduct {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("prod_name")
            @Expose
            private String prodName;
            @SerializedName("prod_model")
            @Expose
            private String prodModel;
            @SerializedName("prod_type")
            @Expose
            private String prodType;
            @SerializedName("prod_image")
            @Expose
            private String prodImage;
            @SerializedName("prod_price")
            @Expose
            private String prodPrice;
            @SerializedName("prod_sno")
            @Expose
            private String prodSno;
            @SerializedName("prod_desc")
            @Expose
            private String prodDesc;
            @SerializedName("prod_qty")
            @Expose
            private String prodQty;
            @SerializedName("prod_video")
            @Expose
            private List<ProdVideo> prodVideo = null;
            @SerializedName("prod_pdf")
            @Expose
            private List<ProdPdf> prodPdf = null;
            @SerializedName("prod_acc")
            @Expose
            private String prodAcc;
            @SerializedName("creation_date")
            @Expose
            private String creationDate;
            @SerializedName("disable")
            @Expose
            private String disable;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProdName() {
                return prodName;
            }

            public void setProdName(String prodName) {
                this.prodName = prodName;
            }

            public String getProdModel() {
                return prodModel;
            }

            public void setProdModel(String prodModel) {
                this.prodModel = prodModel;
            }

            public String getProdType() {
                return prodType;
            }

            public void setProdType(String prodType) {
                this.prodType = prodType;
            }

            public String getProdImage() {
                return prodImage;
            }

            public void setProdImage(String prodImage) {
                this.prodImage = prodImage;
            }

            public String getProdPrice() {
                return prodPrice;
            }

            public void setProdPrice(String prodPrice) {
                this.prodPrice = prodPrice;
            }

            public String getProdSno() {
                return prodSno;
            }

            public void setProdSno(String prodSno) {
                this.prodSno = prodSno;
            }

            public String getProdDesc() {
                return prodDesc;
            }

            public void setProdDesc(String prodDesc) {
                this.prodDesc = prodDesc;
            }

            public String getProdQty() {
                return prodQty;
            }

            public void setProdQty(String prodQty) {
                this.prodQty = prodQty;
            }

            public List<ProdVideo> getProdVideo() {
                return prodVideo;
            }

            public void setProdVideo(List<ProdVideo> prodVideo) {
                this.prodVideo = prodVideo;
            }

            public List<ProdPdf> getProdPdf() {
                return prodPdf;
            }

            public void setProdPdf(List<ProdPdf> prodPdf) {
                this.prodPdf = prodPdf;
            }

            public String getProdAcc() {
                return prodAcc;
            }

            public void setProdAcc(String prodAcc) {
                this.prodAcc = prodAcc;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public String getDisable() {
                return disable;
            }

            public void setDisable(String disable) {
                this.disable = disable;

        }
    }
  }