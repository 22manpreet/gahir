package com.gahiragro.app.model.adminDealorOrderModel;

import com.google.gson.annotations.SerializedName;

public class EnquiryDetail{

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("remark")
	private String remark;

	@SerializedName("order_time")
	private String orderTime;

	@SerializedName("type")
	private String type;

	@SerializedName("dispatch_day")
	private String dispatchDay;

	@SerializedName("dealer_code")
	private String dealerCode;

	@SerializedName("order_status")
	private boolean orderStatus;

	@SerializedName("total")
	private String total;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("id")
	private String id;

	@SerializedName("lat")
	private String lat;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("approval_date")
	private String approvalDate;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("enq_status")
	private String enqStatus;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("system")
	private String system;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("dealer_loc")
	private String dealerLoc;

	@SerializedName("qty")
	private String qty;

	@SerializedName("status_text")
	private String statusText;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private Object status;

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public String getRemark(){
		return remark;
	}

	public String getOrderTime(){
		return orderTime;
	}

	public String getType(){
		return type;
	}

	public String getDispatchDay(){
		return dispatchDay;
	}

	public String getDealerCode(){
		return dealerCode;
	}

	public boolean isOrderStatus(){
		return orderStatus;
	}

	public String getTotal(){
		return total;
	}

	public String getProductId(){
		return productId;
	}

	public String getId(){
		return id;
	}

	public String getLat(){
		return lat;
	}

	public String getLongitude(){
		return longitude;
	}

	public String getApprovalDate(){
		return approvalDate;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public String getEnqStatus(){
		return enqStatus;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getProdName(){
		return prodName;
	}

	public String getAccName(){
		return accName;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public String getSystem(){
		return system;
	}

	public String getUserId(){
		return userId;
	}

	public String getDealerLoc(){
		return dealerLoc;
	}

	public String getQty(){
		return qty;
	}

	public String getStatusText(){
		return statusText;
	}

	public String getAccessory(){
		return accessory;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public Object getStatus(){
		return status;
	}
}