package com.gahiragro.app.model.adminDealorEnquiry;

import com.google.gson.annotations.SerializedName;

public class DealorAdminEnquiry{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("enquiry_list")
	private EnquiryList enquiryList;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public String getMessage(){
		return message;
	}

	public EnquiryList getEnquiryList(){
		return enquiryList;
	}

	public String getStatus(){
		return status;
	}
}