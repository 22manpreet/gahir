package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("notification_list")
    @Expose
    private NotificationList notificationList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public NotificationList getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(NotificationList notificationList) {
        this.notificationList = notificationList;
    }

    public class NotificationList {

        @SerializedName("all_notifications")
        @Expose
        private List<AllNotification> allNotifications = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllNotification> getAllNotifications() {
            return allNotifications;
        }

        public void setAllNotifications(List<AllNotification> allNotifications) {
            this.allNotifications = allNotifications;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

    }
    public class AllNotification {

        @SerializedName("notify_id")
        @Expose
        private String notifyId;
        @SerializedName("notify_type")
        @Expose
        private String notifyType;
        @SerializedName("notify_title")
        @Expose
        private String notifyTitle;
        @SerializedName("notify_message")
        @Expose
        private String notifyMessage;

        public String getDisplay_date() {
            return display_date;
        }

        public void setDisplay_date(String display_date) {
            this.display_date = display_date;
        }

        @SerializedName("creation_date")
        @Expose
        private String creationDate;

        @SerializedName("display_date")
        @Expose
        private String display_date;




        public String getNotifyId() {
            return notifyId;
        }

        public void setNotifyId(String notifyId) {
            this.notifyId = notifyId;
        }

        public String getNotifyType() {
            return notifyType;
        }

        public void setNotifyType(String notifyType) {
            this.notifyType = notifyType;
        }

        public String getNotifyTitle() {
            return notifyTitle;
        }

        public void setNotifyTitle(String notifyTitle) {
            this.notifyTitle = notifyTitle;
        }

        public String getNotifyMessage() {
            return notifyMessage;
        }

        public void setNotifyMessage(String notifyMessage) {
            this.notifyMessage = notifyMessage;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

    }
}
