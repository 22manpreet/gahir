package com.gahiragro.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PredictionsItem implements Serializable {

	@SerializedName("description")
	private String description;

	private String showCityState = "";
	private String showActualCityState = "";

	@SerializedName("matched_substrings")
	private List<MatchedSubstringsItem> matchedSubstrings;

	@SerializedName("place_id")
	private String placeId;

	@SerializedName("reference")
	private String reference;

	@SerializedName("structured_formatting")
	private StructuredFormatting structuredFormatting;

	@SerializedName("terms")
	private List<TermsItem> terms;

	@SerializedName("types")
	private List<String> types;

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public String getShowCityState() {
		return showCityState;
	}

	public String getShowActualCityState() {
		return showActualCityState;
	}

	public void setShowActualCityState(String showActualCityState) {
		this.showActualCityState = showActualCityState;
	}

	public void setShowCityState(String showCityState) {
		this.showCityState = showCityState;
	}


	public void setMatchedSubstrings(List<MatchedSubstringsItem> matchedSubstrings){
		this.matchedSubstrings = matchedSubstrings;
	}

	public List<MatchedSubstringsItem> getMatchedSubstrings(){
		return matchedSubstrings;
	}

	public void setPlaceId(String placeId){
		this.placeId = placeId;
	}

	public String getPlaceId(){
		return placeId;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setStructuredFormatting(StructuredFormatting structuredFormatting){
		this.structuredFormatting = structuredFormatting;
	}

	public StructuredFormatting getStructuredFormatting(){
		return structuredFormatting;
	}

	public void setTerms(List<TermsItem> terms){
		this.terms = terms;
	}

	public List<TermsItem> getTerms(){
		return terms;
	}

	public void setTypes(List<String> types){
		this.types = types;
	}

	public List<String> getTypes(){
		return types;
	}

	@Override
 	public String toString(){
		return 
			"PredictionsItem{" + 
			"description = '" + description + '\'' + 
			",matched_substrings = '" + matchedSubstrings + '\'' + 
			",place_id = '" + placeId + '\'' + 
			",reference = '" + reference + '\'' + 
			",structured_formatting = '" + structuredFormatting + '\'' + 
			",terms = '" + terms + '\'' + 
			",types = '" + types + '\'' + 
			"}";
		}
}