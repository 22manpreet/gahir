package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


public class MainTextMatchedSubstringsDTO implements Serializable {

	@SerializedName("length")
	private int length;

	@SerializedName("offset")
	private int offset;

	public void setLength(int length){
		this.length = length;
	}

	public int getLength(){
		return length;
	}

	public void setOffset(int offset){
		this.offset = offset;
	}

	public int getOffset(){
		return offset;
	}

	@Override
 	public String toString(){
		return 
			"MainTextMatchedSubstringsDTO{" + 
			"length = '" + length + '\'' + 
			",offset = '" + offset + '\'' + 
			"}";
		}
}