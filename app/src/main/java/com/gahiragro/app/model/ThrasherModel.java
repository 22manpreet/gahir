package com.gahiragro.app.model;

public class ThrasherModel {
    String subPartId;
    int subPartCount;

    public String getSubPartId() {
        return subPartId;
    }

    public int getSubPartCount() {
        return subPartCount;
    }

    public void setSubPartCount(int subPartCount) {
        this.subPartCount = subPartCount;
    }

    public void setSubPartId(String subPartId) {
        this.subPartId = subPartId;
    }


}
