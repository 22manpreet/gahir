package com.gahiragro.app.model.orderDetailModel;

import com.google.gson.annotations.SerializedName;

public class OrderDetailModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("order_detail")
	private OrderDetail orderDetail;

	@SerializedName("status")
	private String status;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setOrderDetail(OrderDetail orderDetail){
		this.orderDetail = orderDetail;
	}

	public OrderDetail getOrderDetail(){
		return orderDetail;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetailModel{" + 
			"access_token = '" + accessToken + '\'' + 
			",message = '" + message + '\'' + 
			",order_detail = '" + orderDetail + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}