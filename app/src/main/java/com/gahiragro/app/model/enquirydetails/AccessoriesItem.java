package com.gahiragro.app.model.enquirydetails;

import com.google.gson.annotations.SerializedName;

public class AccessoriesItem{

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("disable")
	private String disable;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	public void setAccName(String accName){
		this.accName = accName;
	}

	public String getAccName(){
		return accName;
	}

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	@Override
 	public String toString(){
		return 
			"AccessoriesItem{" + 
			"acc_name = '" + accName + '\'' + 
			",disable = '" + disable + '\'' + 
			",id = '" + id + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			"}";
		}
}