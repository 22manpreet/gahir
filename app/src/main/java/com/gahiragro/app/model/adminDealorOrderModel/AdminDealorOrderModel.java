package com.gahiragro.app.model.adminDealorOrderModel;

import com.google.gson.annotations.SerializedName;

public class AdminDealorOrderModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("order_list")
	private OrderList orderList;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public String getMessage(){
		return message;
	}

	public OrderList getOrderList(){
		return orderList;
	}

	public String getStatus(){
		return status;
	}
}