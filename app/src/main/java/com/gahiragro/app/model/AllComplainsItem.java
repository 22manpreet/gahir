package com.gahiragro.app.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllComplainsItem implements Serializable {

	@SerializedName("user_detail")
	private UserDetail userDetail;

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("prod_sr_no")
	private String prodSrNo;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("comp_id")
	private String compId;

	@SerializedName("support_audio")
	private String supportAudio;

	@SerializedName("reg_prod_id")
	private String regProdId;

	@SerializedName("comp_status")
	private String compStatus;

	@SerializedName("reason_detail")
	private String reasonDetail;

	@SerializedName("comp_reason")
	private String compReason;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("comp_no")
	private String compNo;

	@SerializedName("contact_no")
	private String contactNo;

	@SerializedName("support_img")
	private List<String> supportImg;

	@SerializedName("assigned_to")
	private String assignedTo;

	public UserDetail getUserDetail(){
		return userDetail;
	}

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public String getProdSrNo(){
		return prodSrNo;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getCompId(){
		return compId;
	}

	public String getSupportAudio(){
		return supportAudio;
	}

	public String getRegProdId(){
		return regProdId;
	}

	public String getCompStatus(){
		return compStatus;
	}

	public String getReasonDetail(){
		return reasonDetail;
	}

	public String getCompReason(){
		return compReason;
	}

	public String getUserId(){
		return userId;
	}

	public String getCompNo(){
		return compNo;
	}

	public String getContactNo(){
		return contactNo;
	}

	public List<String> getSupportImg(){
		return supportImg;
	}

	public String getAssignedTo(){
		return assignedTo;
	}
}