package com.gahiragro.app.model.getOrderListMOdel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AllOrdersItem implements Serializable {

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("dealer_code")
	private String dealerCode;

	@SerializedName("amount")
	private String amount;

	@SerializedName("utr_no")
	private String utrNo;

	@SerializedName("enquiry_detail")
	private EnquiryDetail enquiryDetail;

	@SerializedName("dispatch_date")
	private String dispatchDate;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("status")
	private String status;

	public String getBookingId(){
		return bookingId;
	}

	public String getDealerCode(){
		return dealerCode;
	}

	public String getAmount(){
		return amount;
	}

	public String getUtrNo(){
		return utrNo;
	}

	public EnquiryDetail getEnquiryDetail(){
		return enquiryDetail;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public String getId(){
		return id;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public String getStatus(){
		return status;
	}
}