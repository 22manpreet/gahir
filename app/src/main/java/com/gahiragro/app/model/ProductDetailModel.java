package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("product_detail")
    @Expose
    private ProductDetail productDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }
    public class ProdVideo {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    public class ProductDetail {

        @SerializedName("troubleshooting")
        @Expose
        private String troubleshooting;

        public String getTroubleshooting() {
            return troubleshooting;
        }

        public void setTroubleshooting(String troubleshooting) {
            this.troubleshooting = troubleshooting;
        }

        public String getService_manual() {
            return service_manual;
        }

        public void setService_manual(String service_manual) {
            this.service_manual = service_manual;
        }

        @SerializedName("service_manual")
        @Expose
        private String service_manual;

        public List<String> getAll_images() {
            return all_images;
        }

        public void setAll_images(List<String> all_images) {
            this.all_images = all_images;
        }

        @SerializedName("all_images")
        @Expose
        private List<String> all_images = null;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("prod_name")
        @Expose
        private String prodName;
        @SerializedName("prod_model")
        @Expose
        private String prodModel;
        @SerializedName("prod_type")
        @Expose
        private String prodType;
        @SerializedName("prod_image")
        @Expose
        private String prodImage;
        @SerializedName("prod_price")
        @Expose
        private String prodPrice;
        @SerializedName("prod_sno")
        @Expose
        private String prodSno;
        @SerializedName("prod_desc")
        @Expose
        private List<String> prodDesc = null;
        @SerializedName("prod_qty")
        @Expose
        private String prodQty;
        @SerializedName("prod_video")
        @Expose
        private List<ProdVideo> prodVideo = null;
        @SerializedName("prod_pdf")
        @Expose
        private List<ProdPdf> prodPdf = null;
        @SerializedName("prod_acc")
        @Expose
        private String prodAcc;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("accessories")
        @Expose
        private ArrayList<Accessory> accessories = null;

        @SerializedName("systems")
        @Expose
        private ArrayList<System> systems = null;
        @SerializedName("price")
        @Expose
        private String price;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProdName() {
            return prodName;
        }

        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        public String getProdModel() {
            return prodModel;
        }

        public void setProdModel(String prodModel) {
            this.prodModel = prodModel;
        }

        public String getProdType() {
            return prodType;
        }

        public void setProdType(String prodType) {
            this.prodType = prodType;
        }

        public String getProdImage() {
            return prodImage;
        }

        public void setProdImage(String prodImage) {
            this.prodImage = prodImage;
        }

        public String getProdPrice() {
            return prodPrice;
        }

        public void setProdPrice(String prodPrice) {
            this.prodPrice = prodPrice;
        }

        public String getProdSno() {
            return prodSno;
        }

        public void setProdSno(String prodSno) {
            this.prodSno = prodSno;
        }

        public List<String> getProdDesc() {
            return prodDesc;
        }

        public void setProdDesc(List<String> prodDesc) {
            this.prodDesc = prodDesc;
        }

        public String getProdQty() {
            return prodQty;
        }

        public void setProdQty(String prodQty) {
            this.prodQty = prodQty;
        }

        public List<ProdVideo> getProdVideo() {
            return prodVideo;
        }

        public void setProdVideo(List<ProdVideo> prodVideo) {
            this.prodVideo = prodVideo;
        }

        public List<ProdPdf> getProdPdf() {
            return prodPdf;
        }

        public void setProdPdf(List<ProdPdf> prodPdf) {
            this.prodPdf = prodPdf;
        }

        public String getProdAcc() {
            return prodAcc;
        }

        public void setProdAcc(String prodAcc) {
            this.prodAcc = prodAcc;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<Accessory> getAccessories() {
            return accessories;
        }

        public void setAccessories(ArrayList<Accessory> accessories) {
            this.accessories = accessories;
        }

        public List<System> getSystems() {
            return systems;
        }

        public void setSystems(ArrayList<System> systems) {
            this.systems = systems;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
    public class ProdPdf {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    public class Accessory {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("acc_name")
        @Expose
        private String accName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAccName() {
            return accName;
        }

        public void setAccName(String accName) {
            this.accName = accName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        @Override
        public String toString() {
            return accName;
        }

    }

    public class System {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("trac_name")
        @Expose
        public String tracName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTracName() {
            return tracName;
        }

        public void setTracName(String tracName) {
            this.tracName = tracName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        @Override
        public String toString() {
            return tracName;
        }

    }
}
