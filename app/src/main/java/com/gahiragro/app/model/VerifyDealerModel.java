package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyDealerModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("dealer_detail")
    @Expose
    private DealerDetail dealerDetail;
    @SerializedName("app_signup")
    @Expose
    private Integer appSignup;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public DealerDetail getDealerDetail() {
        return dealerDetail;
    }

    public void setDealerDetail(DealerDetail dealerDetail) {
        this.dealerDetail = dealerDetail;
    }

    public Integer getAppSignup() {
        return appSignup;
    }

    public void setAppSignup(Integer appSignup) {
        this.appSignup = appSignup;
    }
    public class DealerDetail {
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        @SerializedName("city")
        @Expose
        private String city;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("dealer_doc")
        @Expose
        private String dealerDoc;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("district")
        @Expose
        private String district;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getDealerDoc() {
            return dealerDoc;
        }

        public void setDealerDoc(String dealerDoc) {
            this.dealerDoc = dealerDoc;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

    }


}
