package com.gahiragro.app.model;


import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


public class TermsItem implements Serializable {

	@SerializedName("offset")
	private int offset;

	@SerializedName("value")
	private String value;

	public void setOffset(int offset){
		this.offset = offset;
	}

	public int getOffset(){
		return offset;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"TermsItem{" + 
			"offset = '" + offset + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}