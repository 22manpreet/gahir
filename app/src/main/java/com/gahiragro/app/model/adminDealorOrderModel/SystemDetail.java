package com.gahiragro.app.model.adminDealorOrderModel;

import com.google.gson.annotations.SerializedName;

public class SystemDetail{

	@SerializedName("disable")
	private String disable;

	@SerializedName("trac_name")
	private String tracName;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("sys_name")
	private String sysName;

	public String getDisable(){
		return disable;
	}

	public String getTracName(){
		return tracName;
	}

	public String getId(){
		return id;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getSysName(){
		return sysName;
	}
}