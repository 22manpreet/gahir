package com.gahiragro.app.model.enquirymodel;

import com.google.gson.annotations.SerializedName;

public class AllEnquiriesItem{

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("total")
	private String total;

	@SerializedName("system")
	private String system;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("order_status")
	private Boolean orderStatus;

	@SerializedName("dispatch_day")
	private String dispatchDay;

	public void setDispatchDay(String dispatchDay){
		this.dispatchDay = dispatchDay;
	}

	public String getDispatchDay(){
		return dispatchDay;
	}


	public Boolean getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Boolean orderStatus) {
		this.orderStatus = orderStatus;
	}

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("id")
	private String id;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private String status;

	public void setProductDetail(ProductDetail productDetail){
		this.productDetail = productDetail;
	}

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public void setAccessories(Accessories accessories){
		this.accessories = accessories;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setSystemDetail(SystemDetail systemDetail){
		this.systemDetail = systemDetail;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setSystem(String system){
		this.system = system;
	}

	public String getSystem(){
		return system;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAccessory(String accessory){
		this.accessory = accessory;
	}

	public String getAccessory(){
		return accessory;
	}

	public void setEnquiryId(String enquiryId){
		this.enquiryId = enquiryId;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AllEnquiriesItem{" + 
			"product_detail = '" + productDetail + '\'' + 
			",accessories = '" + accessories + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",type = '" + type + '\'' + 
			",system_detail = '" + systemDetail + '\'' + 
			",total = '" + total + '\'' + 
			",system = '" + system + '\'' + 
			",user_id = '" + userId + '\'' + 
			",product_id = '" + productId + '\'' + 
			",qty = '" + qty + '\'' + 
			",id = '" + id + '\'' + 
			",accessory = '" + accessory + '\'' + 
			",enquiry_id = '" + enquiryId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}