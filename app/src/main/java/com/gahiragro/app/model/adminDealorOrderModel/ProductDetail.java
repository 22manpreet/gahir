package com.gahiragro.app.model.adminDealorOrderModel;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetail{

	@SerializedName("prod_desc")
	private List<String> prodDesc;

	@SerializedName("prod_type")
	private String prodType;

	@SerializedName("part_list")
	private List<Object> partList;

	@SerializedName("accessories")
	private List<AccessoriesItem> accessories;

	@SerializedName("prod_pdf")
	private List<ProdPdf> prodPdf;

	@SerializedName("prod_price")
	private String prodPrice;

	@SerializedName("all_images")
	private List<String> allImages;

	@SerializedName("prod_video")
	private List<ProdPdf> prodVideo;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_qty")
	private String prodQty;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("prod_sno")
	private String prodSno;

	@SerializedName("op_manual")
	private String opManual;

	@SerializedName("prod_cat")
	private String prodCat;

	@SerializedName("prod_image")
	private String prodImage;

	@SerializedName("systems")
	private List<SystemsItem> systems;

	@SerializedName("disable")
	private String disable;

	@SerializedName("price")
	private String price;

	@SerializedName("prod_acc")
	private String prodAcc;

	@SerializedName("warranty")
	private String warranty;

	@SerializedName("id")
	private String id;

	@SerializedName("prod_model")
	private String prodModel;

	public List<String> getProdDesc(){
		return prodDesc;
	}

	public String getProdType(){
		return prodType;
	}

	public List<Object> getPartList(){
		return partList;
	}

	public List<AccessoriesItem> getAccessories(){
		return accessories;
	}

	public List<ProdPdf> getProdPdf(){
		return prodPdf;
	}

	public String getProdPrice(){
		return prodPrice;
	}

	public List<String> getAllImages(){
		return allImages;
	}

	public List<ProdPdf> getProdVideo(){
		return prodVideo;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getProdQty(){
		return prodQty;
	}

	public String getProdName(){
		return prodName;
	}

	public String getProdSno(){
		return prodSno;
	}

	public String getOpManual(){
		return opManual;
	}

	public String getProdCat(){
		return prodCat;
	}

	public String getProdImage(){
		return prodImage;
	}

	public List<SystemsItem> getSystems(){
		return systems;
	}

	public String getDisable(){
		return disable;
	}

	public String getPrice(){
		return price;
	}

	public String getProdAcc(){
		return prodAcc;
	}

	public String getWarranty(){
		return warranty;
	}

	public String getId(){
		return id;
	}

	public String getProdModel(){
		return prodModel;
	}

	public class ProdPdf{

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	public class ProdVideo{

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}