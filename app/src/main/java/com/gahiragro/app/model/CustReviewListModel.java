package com.gahiragro.app.model;

import com.gahiragro.app.model.adminDealorEnquiry.ProductDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustReviewListModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("review_list")
    @Expose
    private ReviewList reviewList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReviewList getReviewList() {
        return reviewList;
    }

    public void setReviewList(ReviewList reviewList) {
        this.reviewList = reviewList;
    }

    public class ReviewList {

        @SerializedName("all_reviews")
        @Expose
        private List<AllReview> allReviews = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllReview> getAllReviews() {
            return allReviews;
        }

        public void setAllReviews(List<AllReview> allReviews) {
            this.allReviews = allReviews;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

    }


    public class AllReview {

        @SerializedName("review_id")
        @Expose
        private String reviewId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("review_text")
        @Expose
        private String reviewText;
        @SerializedName("review_img")
        @Expose
        private String reviewImg;
        @SerializedName("approval_status")
        @Expose
        private String approvalStatus;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("user_detail")
        @Expose
        private UserDetail userDetail;
        @SerializedName("product_detail")
        @Expose
        private ProductDetail productDetail;

        public String getReviewId() {
            return reviewId;
        }

        public void setReviewId(String reviewId) {
            this.reviewId = reviewId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getReviewText() {
            return reviewText;
        }

        public void setReviewText(String reviewText) {
            this.reviewText = reviewText;
        }

        public String getReviewImg() {
            return reviewImg;
        }

        public void setReviewImg(String reviewImg) {
            this.reviewImg = reviewImg;
        }

        public String getApprovalStatus() {
            return approvalStatus;
        }

        public void setApprovalStatus(String approvalStatus) {
            this.approvalStatus = approvalStatus;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public CustReviewListModel.UserDetail getUserDetail() {
            return userDetail;
        }

        public void setUserDetail(CustReviewListModel.UserDetail userDetail) {
            this.userDetail = userDetail;
        }

        public ProductDetail getProductDetail() {
            return productDetail;
        }

        public void setProductDetail(ProductDetail productDetail) {
            this.productDetail = productDetail;
        }
    }

    public class UserDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("firm_name")
        @Expose
        private String firmName;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("auth_key")
        @Expose
        private String authKey;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("user_lat")
        @Expose
        private String userLat;
        @SerializedName("app_signup")
        @Expose
        private String appSignup;
        @SerializedName("admin_signup")
        @Expose
        private String adminSignup;
        @SerializedName("user_long")
        @Expose
        private String userLong;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("serial_no")
        @Expose
        private String serialNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("dealer_doc")
        @Expose
        private List<String> dealerDoc = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirmName() {
            return firmName;
        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAuthKey() {
            return authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getUserLat() {
            return userLat;
        }

        public void setUserLat(String userLat) {
            this.userLat = userLat;
        }

        public String getAppSignup() {
            return appSignup;
        }

        public void setAppSignup(String appSignup) {
            this.appSignup = appSignup;
        }

        public String getAdminSignup() {
            return adminSignup;
        }

        public void setAdminSignup(String adminSignup) {
            this.adminSignup = adminSignup;
        }

        public String getUserLong() {
            return userLong;
        }

        public void setUserLong(String userLong) {
            this.userLong = userLong;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<String> getDealerDoc() {
            return dealerDoc;
        }

        public void setDealerDoc(List<String> dealerDoc) {
            this.dealerDoc = dealerDoc;
        }

    }

    public class ProductDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("prod_name")
        @Expose
        private String prodName;
        @SerializedName("prod_model")
        @Expose
        private String prodModel;
        @SerializedName("prod_cat")
        @Expose
        private String prodCat;
        @SerializedName("prod_type")
        @Expose
        private String prodType;
        @SerializedName("prod_image")
        @Expose
        private String prodImage;
        @SerializedName("prod_price")
        @Expose
        private String prodPrice;
        @SerializedName("prod_sno")
        @Expose
        private String prodSno;
        @SerializedName("prod_desc")
        @Expose
        private List<String> prodDesc = null;
        @SerializedName("prod_qty")
        @Expose
        private String prodQty;
        @SerializedName("prod_video")
        @Expose
        private List<Object> prodVideo = null;
        @SerializedName("prod_pdf")
        @Expose
        private List<Object> prodPdf = null;
        @SerializedName("prod_acc")
        @Expose
        private String prodAcc;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("accessories")
        @Expose
        private List<ProductDetailModel.Accessory> accessories = null;
        @SerializedName("systems")
        @Expose
        private List<System> systems = null;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("part_list")
        @Expose
        private List<Object> partList = null;
        @SerializedName("doc_list")
        @Expose
        private List<Object> docList = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProdName() {
            return prodName;
        }

        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        public String getProdModel() {
            return prodModel;
        }

        public void setProdModel(String prodModel) {
            this.prodModel = prodModel;
        }

        public String getProdCat() {
            return prodCat;
        }

        public void setProdCat(String prodCat) {
            this.prodCat = prodCat;
        }

        public String getProdType() {
            return prodType;
        }

        public void setProdType(String prodType) {
            this.prodType = prodType;
        }

        public String getProdImage() {
            return prodImage;
        }

        public void setProdImage(String prodImage) {
            this.prodImage = prodImage;
        }

        public String getProdPrice() {
            return prodPrice;
        }

        public void setProdPrice(String prodPrice) {
            this.prodPrice = prodPrice;
        }

        public String getProdSno() {
            return prodSno;
        }

        public void setProdSno(String prodSno) {
            this.prodSno = prodSno;
        }

        public List<String> getProdDesc() {
            return prodDesc;
        }

        public void setProdDesc(List<String> prodDesc) {
            this.prodDesc = prodDesc;
        }

        public String getProdQty() {
            return prodQty;
        }

        public void setProdQty(String prodQty) {
            this.prodQty = prodQty;
        }

        public List<Object> getProdVideo() {
            return prodVideo;
        }

        public void setProdVideo(List<Object> prodVideo) {
            this.prodVideo = prodVideo;
        }

        public List<Object> getProdPdf() {
            return prodPdf;
        }

        public void setProdPdf(List<Object> prodPdf) {
            this.prodPdf = prodPdf;
        }

        public String getProdAcc() {
            return prodAcc;
        }

        public void setProdAcc(String prodAcc) {
            this.prodAcc = prodAcc;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<ProductDetailModel.Accessory> getAccessories() {
            return accessories;
        }

        public void setAccessories(List<ProductDetailModel.Accessory> accessories) {
            this.accessories = accessories;
        }

        public List<System> getSystems() {
            return systems;
        }

        public void setSystems(List<System> systems) {
            this.systems = systems;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public List<Object> getPartList() {
            return partList;
        }

        public void setPartList(List<Object> partList) {
            this.partList = partList;
        }

        public List<Object> getDocList() {
            return docList;
        }

        public void setDocList(List<Object> docList) {
            this.docList = docList;
        }

    }

    public class Accessory {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("acc_name")
        @Expose
        private String accName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAccName() {
            return accName;
        }

        public void setAccName(String accName) {
            this.accName = accName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }

    public  class System
    {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("trac_name")
        @Expose
        private String trac_name;
        @SerializedName("creation_date")
        @Expose
        private String creation_date;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTrac_name() {
            return trac_name;
        }

        public void setTrac_name(String trac_name) {
            this.trac_name = trac_name;
        }

        public String getCreation_date() {
            return creation_date;
        }

        public void setCreation_date(String creation_date) {
            this.creation_date = creation_date;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }
    }

   }