package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductPartListModel implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("part_list")
    @Expose
    private ArrayList<Part> partList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Part> getPartList() {
        return partList;
    }

    public void setPartList(ArrayList<Part> partList) {
        this.partList = partList;
    }

    public class Part implements Serializable {

        @SerializedName("pp_id")
        @Expose
        private String ppId;
        @SerializedName("prod_id")
        @Expose
        private String prodId;
        @SerializedName("part_pos")
        @Expose
        private String partPos;
        @SerializedName("part_name")
        @Expose
        private String partName;
        @SerializedName("part_key")
        @Expose
        private String partKey;
        @SerializedName("part_img")
        @Expose
        private String partImg;
        @SerializedName("part_catalog_img")
        @Expose
        private String partCatalogImg;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("sub_parts")
        @Expose
        private ArrayList<SubPart> subParts = null;

        public String getPpId() {
            return ppId;
        }

        public void setPpId(String ppId) {
            this.ppId = ppId;
        }

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getPartPos() {
            return partPos;
        }

        public void setPartPos(String partPos) {
            this.partPos = partPos;
        }

        public String getPartName() {
            return partName;
        }

        public void setPartName(String partName) {
            this.partName = partName;
        }

        public String getPartKey() {
            return partKey;
        }

        public void setPartKey(String partKey) {
            this.partKey = partKey;
        }

        public String getPartImg() {
            return partImg;
        }

        public void setPartImg(String partImg) {
            this.partImg = partImg;
        }

        public String getPartCatalogImg() {
            return partCatalogImg;
        }

        public void setPartCatalogImg(String partCatalogImg) {
            this.partCatalogImg = partCatalogImg;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public ArrayList<SubPart> getSubParts() {
            return subParts;
        }

        public void setSubParts(ArrayList<SubPart> subParts) {
            this.subParts = subParts;
        }

        public class SubPart implements Serializable {
            int count = 1;

            public String getSubPartId() {
                return subPartId;
            }

            public void setSubPartId(String subPartId) {
                this.subPartId = subPartId;
            }

            String subPartId;
            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            @SerializedName("psp_id")
            @Expose
            private String pspId;
            @SerializedName("pp_id")
            @Expose
            private String ppId;
            @SerializedName("prod_id")
            @Expose
            private String prodId;
            @SerializedName("psp_name")
            @Expose
            private String pspName;
            @SerializedName("psp_key")
            @Expose
            private String pspKey;
            @SerializedName("creation_date")
            @Expose
            private String creationDate;
            @SerializedName("disable")
            @Expose
            private String disable;
            @SerializedName("psp_no")
            @Expose
            private String pspNo ;

            public String getPspId() {
                return pspId;
            }

            public void setPspId(String pspId) {
                this.pspId = pspId;
            }

            public String getPspNo() {
                return pspNo;
            }

            public void setPspNo(String pspNo) {
                this.pspNo = pspNo;
            }

            public String getPpId() {
                return ppId;
            }

            public void setPpId(String ppId) {
                this.ppId = ppId;
            }

            public String getProdId() {
                return prodId;
            }

            public void setProdId(String prodId) {
                this.prodId = prodId;
            }

            public String getPspName() {
                return pspName;
            }

            public void setPspName(String pspName) {
                this.pspName = pspName;
            }

            public String getPspKey() {
                return pspKey;
            }

            public void setPspKey(String pspKey) {
                this.pspKey = pspKey;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public String getDisable() {
                return disable;
            }

            public void setDisable(String disable) {
                this.disable = disable;
            }

        }
    }
}
