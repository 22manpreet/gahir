package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RegProductDetailModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product_detail")
    @Expose
    private ProductDetail productDetail;
    @SerializedName("user_detail")
    @Expose
    private RegProductDetailModel.UserDetail userDetail;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public RegProductDetailModel.UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(RegProductDetailModel.UserDetail userDetail) {
        this.userDetail = userDetail;
    } public class UserDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("firm_name")
        @Expose
        private String firmName;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("auth_key")
        @Expose
        private String authKey;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("user_lat")
        @Expose
        private String userLat;
        @SerializedName("app_signup")
        @Expose
        private String appSignup;
        @SerializedName("admin_signup")
        @Expose
        private String adminSignup;
        @SerializedName("user_long")
        @Expose
        private String userLong;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("serial_no")
        @Expose
        private String serialNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirmName() {
            return firmName;
        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAuthKey() {
            return authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getUserLat() {
            return userLat;
        }

        public void setUserLat(String userLat) {
            this.userLat = userLat;
        }

        public String getAppSignup() {
            return appSignup;
        }

        public void setAppSignup(String appSignup) {
            this.appSignup = appSignup;
        }

        public String getAdminSignup() {
            return adminSignup;
        }

        public void setAdminSignup(String adminSignup) {
            this.adminSignup = adminSignup;
        }

        public String getUserLong() {
            return userLong;
        }

        public void setUserLong(String userLong) {
            this.userLong = userLong;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }
    public class ProdPdf implements Serializable {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }
    public class ProdVideo  implements Serializable{

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }
    public class ProductDetail implements Serializable {


        @SerializedName("Left")
        @Expose
        private String Left;

        @SerializedName("Right")
        @Expose
        private String Right;

        public String getLeft() {
            return Left;
        }

        public void setLeft(String left) {
            Left = left;
        }

        public String getRight() {
            return Right;
        }

        public void setRight(String right) {
            Right = right;
        }

        public String getFront() {
            return Front;
        }

        public void setFront(String front) {
            Front = front;
        }

        public String getBack() {
            return Back;
        }

        public void setBack(String back) {
            Back = back;
        }

        @SerializedName("Front")
        @Expose
        private String Front;


        @SerializedName("Back")
        @Expose
        private String Back;

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("prod_name")
        @Expose
        private String prodName;
        @SerializedName("prod_model")
        @Expose
        private String prodModel;
        @SerializedName("prod_cat")
        @Expose
        private String prodCat;
        @SerializedName("prod_type")
        @Expose
        private String prodType;
        @SerializedName("prod_image")
        @Expose
        private String prodImage;
        @SerializedName("prod_price")
        @Expose
        private String prodPrice;
        @SerializedName("prod_sno")
        @Expose
        private String prodSno;
        @SerializedName("prod_desc")
        @Expose
        private List<String> prodDesc = null;
        @SerializedName("prod_qty")
        @Expose
        private String prodQty;
        @SerializedName("prod_video")
        @Expose
        private List<ProdVideo> prodVideo = null;
        @SerializedName("prod_pdf")
        @Expose
        private List<ProdPdf> prodPdf = null;
        @SerializedName("prod_acc")
        @Expose
        private String prodAcc;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("accessories")
        @Expose
        private List<Accessory> accessories = null;
        @SerializedName("systems")
        @Expose
        private List<System> systems = null;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("part_list")
        @Expose
        private List<Part> partList = null;
        @SerializedName("op_manual")
        @Expose
        private String opManual;
        @SerializedName("3d_image")
        @Expose
        private String _3dImage;
        @SerializedName("serial_no")
        @Expose
        private String serialNo;
        @SerializedName("system_detail")
        @Expose
        private String systemDetail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProdName() {
            return prodName;
        }

        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        public String getProdModel() {
            return prodModel;
        }

        public void setProdModel(String prodModel) {
            this.prodModel = prodModel;
        }

        public String getProdCat() {
            return prodCat;
        }

        public void setProdCat(String prodCat) {
            this.prodCat = prodCat;
        }

        public String getProdType() {
            return prodType;
        }

        public void setProdType(String prodType) {
            this.prodType = prodType;
        }

        public String getProdImage() {
            return prodImage;
        }

        public void setProdImage(String prodImage) {
            this.prodImage = prodImage;
        }

        public String getProdPrice() {
            return prodPrice;
        }

        public void setProdPrice(String prodPrice) {
            this.prodPrice = prodPrice;
        }

        public String getProdSno() {
            return prodSno;
        }

        public void setProdSno(String prodSno) {
            this.prodSno = prodSno;
        }

        public List<String> getProdDesc() {
            return prodDesc;
        }

        public void setProdDesc(List<String> prodDesc) {
            this.prodDesc = prodDesc;
        }

        public String getProdQty() {
            return prodQty;
        }

        public void setProdQty(String prodQty) {
            this.prodQty = prodQty;
        }

        public List<ProdVideo> getProdVideo() {
            return prodVideo;
        }

        public void setProdVideo(List<ProdVideo> prodVideo) {
            this.prodVideo = prodVideo;
        }

        public List<ProdPdf> getProdPdf() {
            return prodPdf;
        }

        public void setProdPdf(List<ProdPdf> prodPdf) {
            this.prodPdf = prodPdf;
        }

        public String getProdAcc() {
            return prodAcc;
        }

        public void setProdAcc(String prodAcc) {
            this.prodAcc = prodAcc;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<Accessory> getAccessories() {
            return accessories;
        }

        public void setAccessories(List<Accessory> accessories) {
            this.accessories = accessories;
        }

        public List<System> getSystems() {
            return systems;
        }

        public void setSystems(List<System> systems) {
            this.systems = systems;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public List<Part> getPartList() {
            return partList;
        }

        public void setPartList(List<Part> partList) {
            this.partList = partList;
        }

        public String getOpManual() {
            return opManual;
        }

        public void setOpManual(String opManual) {
            this.opManual = opManual;
        }

        public String get3dImage() {
            return _3dImage;
        }

        public void set3dImage(String _3dImage) {
            this._3dImage = _3dImage;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public String getSystemDetail() {
            return systemDetail;
        }

        public void setSystemDetail(String systemDetail) {
            this.systemDetail = systemDetail;
        }

    }

    public class SubPart implements Serializable {

        @SerializedName("psp_id")
        @Expose
        private String pspId;
        @SerializedName("pp_id")
        @Expose
        private String ppId;
        @SerializedName("prod_id")
        @Expose
        private String prodId;
        @SerializedName("psp_name")
        @Expose
        private String pspName;
        @SerializedName("psp_key")
        @Expose
        private String pspKey;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getPspId() {
            return pspId;
        }

        public void setPspId(String pspId) {
            this.pspId = pspId;
        }

        public String getPpId() {
            return ppId;
        }

        public void setPpId(String ppId) {
            this.ppId = ppId;
        }

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getPspName() {
            return pspName;
        }

        public void setPspName(String pspName) {
            this.pspName = pspName;
        }

        public String getPspKey() {
            return pspKey;
        }

        public void setPspKey(String pspKey) {
            this.pspKey = pspKey;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }

    public class System implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("pump_type")
        @Expose
        private String pumpType;
        @SerializedName("piston")
        @Expose
        private String piston;
        @SerializedName("rpm")
        @Expose
        private String rpm;
        @SerializedName("suction")
        @Expose
        private String suction;
        @SerializedName("pressure")
        @Expose
        private String pressure;
        @SerializedName("power")
        @Expose
        private String power;
        @SerializedName("weight")
        @Expose
        private String weight;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("trac_name")
        @Expose
        private String tracName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getPumpType() {
            return pumpType;
        }

        public void setPumpType(String pumpType) {
            this.pumpType = pumpType;
        }

        public String getPiston() {
            return piston;
        }

        public void setPiston(String piston) {
            this.piston = piston;
        }

        public String getRpm() {
            return rpm;
        }

        public void setRpm(String rpm) {
            this.rpm = rpm;
        }

        public String getSuction() {
            return suction;
        }

        public void setSuction(String suction) {
            this.suction = suction;
        }

        public String getPressure() {
            return pressure;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        public String getPower() {
            return power;
        }

        public void setPower(String power) {
            this.power = power;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getTracName() {
            return tracName;
        }

        public void setTracName(String tracName) {
            this.tracName = tracName;
        }

    }

    public class Part implements Serializable {
        int countInc ;
        @SerializedName("pp_id")
        @Expose
        private String ppId;
        @SerializedName("prod_id")
        @Expose
        private String prodId;
        @SerializedName("part_name")
        @Expose
        private String partName;
        @SerializedName("part_key")
        @Expose
        private String partKey;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("sub_parts")
        @Expose
        private List<SubPart> subParts = null;

        public int getCountInc() {
            return countInc;
        }

        public void setCountInc(int countInc) {
            this.countInc = countInc;
        }

        public String getPpId() {
            return ppId;
        }

        public void setPpId(String ppId) {
            this.ppId = ppId;
        }

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getPartName() {
            return partName;
        }

        public void setPartName(String partName) {
            this.partName = partName;
        }

        public String getPartKey() {
            return partKey;
        }

        public void setPartKey(String partKey) {
            this.partKey = partKey;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<SubPart> getSubParts() {
            return subParts;
        }

        public void setSubParts(List<SubPart> subParts) {
            this.subParts = subParts;
        }

    }

    public class Accessory implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("acc_name")
        @Expose
        private String accName;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAccName() {
            return accName;
        }

        public void setAccName(String accName) {
            this.accName = accName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }
}