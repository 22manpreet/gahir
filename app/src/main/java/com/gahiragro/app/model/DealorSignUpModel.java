package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DealorSignUpModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("user_detail")
    @Expose
    private UserDetail userDetail;
    @SerializedName("app_signup")
    @Expose
    private Integer appSignup;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public Integer getAppSignup() {
        return appSignup;
    }

    public void setAppSignup(Integer appSignup) {
        this.appSignup = appSignup;
    }

    public class UserDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private Object lastName;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("firm_name")
        @Expose
        private String firmName;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("auth_key")
        @Expose
        private String authKey;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("user_lat")
        @Expose
        private String userLat;
        @SerializedName("app_signup")
        @Expose
        private String appSignup;
        @SerializedName("admin_signup")
        @Expose
        private String adminSignup;
        @SerializedName("user_long")
        @Expose
        private String userLong;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("serial_no")
        @Expose
        private String serialNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("dealer_doc")
        @Expose
        private List<Object> dealerDoc = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirmName() {
            return firmName;
        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAuthKey() {
            return authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getUserLat() {
            return userLat;
        }

        public void setUserLat(String userLat) {
            this.userLat = userLat;
        }

        public String getAppSignup() {
            return appSignup;
        }

        public void setAppSignup(String appSignup) {
            this.appSignup = appSignup;
        }

        public String getAdminSignup() {
            return adminSignup;
        }

        public void setAdminSignup(String adminSignup) {
            this.adminSignup = adminSignup;
        }

        public String getUserLong() {
            return userLong;
        }

        public void setUserLong(String userLong) {
            this.userLong = userLong;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public List<Object> getDealerDoc() {
            return dealerDoc;
        }

        public void setDealerDoc(List<Object> dealerDoc) {
            this.dealerDoc = dealerDoc;
        }

    }
}
