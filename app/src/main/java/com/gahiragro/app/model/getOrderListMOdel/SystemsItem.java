package com.gahiragro.app.model.getOrderListMOdel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SystemsItem implements Serializable {

	@SerializedName("disable")
	private String disable;

	@SerializedName("trac_name")
	private String tracName;

	@SerializedName("id")
	private String id;

	@SerializedName("creation_date")
	private String creationDate;

	public String getDisable(){
		return disable;
	}

	public String getTracName(){
		return tracName;
	}

	public String getId(){
		return id;
	}

	public String getCreationDate(){
		return creationDate;
	}
}