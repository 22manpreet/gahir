package com.gahiragro.app.model.adminDealorEnquiry;

import com.google.gson.annotations.SerializedName;

public class DealerDetail{

	@SerializedName("phone_no")
	private String phoneNo;

	@SerializedName("user_lat")
	private String user_lat;

	@SerializedName("user_long")
	private String user_long;

	public String getUser_lat() {
		return user_lat;
	}

	public void setUser_lat(String user_lat) {
		this.user_lat = user_lat;
	}

	public String getUser_long() {
		return user_long;
	}

	public void setUser_long(String user_long) {
		this.user_long = user_long;
	}

	@SerializedName("image")
	private String image;

	@SerializedName("country")
	private String country;

	@SerializedName("role")
	private String role;

	@SerializedName("flag_image")
	private String flagImage;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("bio")
	private String bio;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("admin_signup")
	private String adminSignup;

	@SerializedName("auth_key")
	private String authKey;

	@SerializedName("password")
	private String password;

	@SerializedName("created_on")
	private String createdOn;

	@SerializedName("disable")
	private String disable;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("id")
	private String id;

	@SerializedName("app_signup")
	private String appSignup;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("username")
	private String username;

	public String getPhoneNo(){
		return phoneNo;
	}

	public String getImage(){
		return image;
	}

	public String getCountry(){
		return country;
	}

	public String getRole(){
		return role;
	}

	public String getFlagImage(){
		return flagImage;
	}

	public String getLastName(){
		return lastName;
	}

	public String getBio(){
		return bio;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public String getAdminSignup(){
		return adminSignup;
	}

	public String getAuthKey(){
		return authKey;
	}

	public String getPassword(){
		return password;
	}

	public String getCreatedOn(){
		return createdOn;
	}

	public String getDisable(){
		return disable;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getId(){
		return id;
	}

	public String getAppSignup(){
		return appSignup;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getUsername(){
		return username;
	}
}