package com.gahiragro.app.model.orderDetailModel;

import com.google.gson.annotations.SerializedName;

public class EnquiryDetail{

	@SerializedName("approval_date")
	private String approvalDate;

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("remark")
	private String remark;

	@SerializedName("enq_status")
	private String enqStatus;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("prod_name")
	private String prodName;

	@SerializedName("acc_name")
	private String accName;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("order_status")
	private boolean orderStatus;

	@SerializedName("dispatch_day")
	private String dispatchDay;

	@SerializedName("total")
	private String total;

	@SerializedName("system")
	private String system;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("id")
	private String id;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private Object status;

	public void setApprovalDate(String approvalDate){
		this.approvalDate = approvalDate;
	}

	public String getApprovalDate(){
		return approvalDate;
	}

	public void setProductDetail(ProductDetail productDetail){
		this.productDetail = productDetail;
	}

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public void setAccessories(Accessories accessories){
		this.accessories = accessories;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setEnqStatus(String enqStatus){
		this.enqStatus = enqStatus;
	}

	public String getEnqStatus(){
		return enqStatus;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setProdName(String prodName){
		this.prodName = prodName;
	}

	public String getProdName(){
		return prodName;
	}

	public void setAccName(String accName){
		this.accName = accName;
	}

	public String getAccName(){
		return accName;
	}

	public void setSystemDetail(SystemDetail systemDetail){
		this.systemDetail = systemDetail;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public void setOrderStatus(boolean orderStatus){
		this.orderStatus = orderStatus;
	}

	public boolean isOrderStatus(){
		return orderStatus;
	}

	public void setDispatchDay(String dispatchDay){
		this.total = dispatchDay;
	}

	public String getDispatchDay(){
		return dispatchDay;
	}


	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setSystem(String system){
		this.system = system;
	}

	public String getSystem(){
		return system;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAccessory(String accessory){
		this.accessory = accessory;
	}

	public String getAccessory(){
		return accessory;
	}

	public void setEnquiryId(String enquiryId){
		this.enquiryId = enquiryId;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public void setStatus(Object status){
		this.status = status;
	}

	public Object getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"EnquiryDetail{" + 
			"approval_date = '" + approvalDate + '\'' + 
			",product_detail = '" + productDetail + '\'' + 
			",accessories = '" + accessories + '\'' + 
			",remark = '" + remark + '\'' + 
			",enq_status = '" + enqStatus + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",type = '" + type + '\'' + 
			",prod_name = '" + prodName + '\'' + 
			",acc_name = '" + accName + '\'' + 
			",system_detail = '" + systemDetail + '\'' + 
			",order_status = '" + orderStatus + '\'' + 
			",total = '" + total + '\'' + 
			",system = '" + system + '\'' + 
			",user_id = '" + userId + '\'' + 
			",product_id = '" + productId + '\'' + 
			",qty = '" + qty + '\'' + 
			",id = '" + id + '\'' + 
			",accessory = '" + accessory + '\'' + 
			",enquiry_id = '" + enquiryId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}