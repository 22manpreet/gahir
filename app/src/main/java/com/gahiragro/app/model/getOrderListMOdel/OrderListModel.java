package com.gahiragro.app.model.getOrderListMOdel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderListModel implements Serializable {

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("order_list")
	private OrderList orderList;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public String getMessage(){
		return message;
	}

	public OrderList getOrderList(){
		return orderList;
	}

	public String getStatus(){
		return status;
	}
}