package com.gahiragro.app.model.adminDealorEnquiry;

import com.google.gson.annotations.SerializedName;

public class AllEnquiriesItem{

	@SerializedName("product_detail")
	private ProductDetail productDetail;

	@SerializedName("accessories")
	private Accessories accessories;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("system_detail")
	private SystemDetail systemDetail;

	@SerializedName("total")
	private String total;

	@SerializedName("system")
	private String system;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("id")
	private String id;

	@SerializedName("accessory")
	private String accessory;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("status")
	private String status;

	@SerializedName("dealer_detail")
	private DealerDetail dealerDetail;

	public ProductDetail getProductDetail(){
		return productDetail;
	}

	public Accessories getAccessories(){
		return accessories;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getType(){
		return type;
	}

	public SystemDetail getSystemDetail(){
		return systemDetail;
	}

	public String getTotal(){
		return total;
	}

	public String getSystem(){
		return system;
	}

	public String getUserId(){
		return userId;
	}

	public String getProductId(){
		return productId;
	}

	public String getQty(){
		return qty;
	}

	public String getId(){
		return id;
	}

	public String getAccessory(){
		return accessory;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public String getStatus(){
		return status;
	}

	public DealerDetail getDealerDetail(){
		return dealerDetail;
	}
}