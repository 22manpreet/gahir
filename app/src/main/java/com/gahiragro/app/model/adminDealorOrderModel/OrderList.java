package com.gahiragro.app.model.adminDealorOrderModel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderList{

	@SerializedName("all_orders")
	private List<AllOrdersItem> allOrders;

	@SerializedName("last_page")
	private String lastPage;

	public List<AllOrdersItem> getAllOrders(){
		return allOrders;
	}

	public String getLastPage(){
		return lastPage;
	}
}