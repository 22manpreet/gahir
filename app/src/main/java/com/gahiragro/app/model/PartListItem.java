package com.gahiragro.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PartListItem{

	@SerializedName("part_pos")
	private String partPos;

	@SerializedName("part_img")
	private String partImg;

	@SerializedName("pp_id")
	private String ppId;

	@SerializedName("disable")
	private String disable;

	@SerializedName("part_key")
	private String partKey;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("prod_id")
	private String prodId;

	@SerializedName("part_name")
	private String partName;

	@SerializedName("part_catalog_img")
	private String partCatalogImg;

	@SerializedName("sub_parts")
	private List<SubPartsItem> subParts;

	public String getPartPos(){
		return partPos;
	}

	public String getPartImg(){
		return partImg;
	}

	public String getPpId(){
		return ppId;
	}

	public String getDisable(){
		return disable;
	}

	public String getPartKey(){
		return partKey;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getProdId(){
		return prodId;
	}

	public String getPartName(){
		return partName;
	}

	public String getPartCatalogImg(){
		return partCatalogImg;
	}

	public List<SubPartsItem> getSubParts(){
		return subParts;
	}
}