package com.gahiragro.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class FilterReviewModel implements Serializable {
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("review_list")
	@Expose
	private ReviewList reviewList;
	@SerializedName("register_count")
	@Expose
	private Integer registerCount;
	private final static long serialVersionUID = 3438680848965470903L;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ReviewList getReviewList() {
		return reviewList;
	}

	public void setReviewList(ReviewList reviewList) {
		this.reviewList = reviewList;
	}

	public Integer getRegisterCount() {
		return registerCount;
	}

	public void setRegisterCount(Integer registerCount) {
		this.registerCount = registerCount;
	}

	public class AllReview implements Serializable
	{

		@SerializedName("review_id")
		@Expose
		private String reviewId;
		@SerializedName("user_id")
		@Expose
		private String userId;
		@SerializedName("product_id")
		@Expose
		private String productId;
		@SerializedName("review_text")
		@Expose
		private String reviewText;
		@SerializedName("review_img")
		@Expose
		private String reviewImg;
		@SerializedName("approval_status")
		@Expose
		private String approvalStatus;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("user_detail")
		@Expose
		private UserDetail userDetail;
		@SerializedName("product_detail")
		@Expose
		private ProductDetail productDetail;
		private final static long serialVersionUID = -289016467522724566L;

		public String getReviewId() {
			return reviewId;
		}

		public void setReviewId(String reviewId) {
			this.reviewId = reviewId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public String getReviewText() {
			return reviewText;
		}

		public void setReviewText(String reviewText) {
			this.reviewText = reviewText;
		}

		public String getReviewImg() {
			return reviewImg;
		}

		public void setReviewImg(String reviewImg) {
			this.reviewImg = reviewImg;
		}

		public String getApprovalStatus() {
			return approvalStatus;
		}

		public void setApprovalStatus(String approvalStatus) {
			this.approvalStatus = approvalStatus;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public UserDetail getUserDetail() {
			return userDetail;
		}

		public void setUserDetail(UserDetail userDetail) {
			this.userDetail = userDetail;
		}

		public ProductDetail getProductDetail() {
			return productDetail;
		}

		public void setProductDetail(ProductDetail productDetail) {
			this.productDetail = productDetail;
		}

	}
	public class Accessory implements Serializable
	{

		@SerializedName("id")
		@Expose
		private String id;
		@SerializedName("acc_name")
		@Expose
		private String accName;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("disable")
		@Expose
		private String disable;
		private final static long serialVersionUID = -2799711547697193975L;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getAccName() {
			return accName;
		}

		public void setAccName(String accName) {
			this.accName = accName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

	}
	public class Part implements Serializable
	{

		@SerializedName("pp_id")
		@Expose
		private String ppId;
		@SerializedName("prod_id")
		@Expose
		private String prodId;
		@SerializedName("part_pos")
		@Expose
		private String partPos;
		@SerializedName("part_name")
		@Expose
		private String partName;
		@SerializedName("part_key")
		@Expose
		private String partKey;
		@SerializedName("part_img")
		@Expose
		private String partImg;
		@SerializedName("part_catalog_img")
		@Expose
		private String partCatalogImg;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("disable")
		@Expose
		private String disable;
		@SerializedName("sub_parts")
		@Expose
		private List<SubPart> subParts = null;
		private final static long serialVersionUID = 5608517847620938182L;

		public String getPpId() {
			return ppId;
		}

		public void setPpId(String ppId) {
			this.ppId = ppId;
		}

		public String getProdId() {
			return prodId;
		}

		public void setProdId(String prodId) {
			this.prodId = prodId;
		}

		public String getPartPos() {
			return partPos;
		}

		public void setPartPos(String partPos) {
			this.partPos = partPos;
		}

		public String getPartName() {
			return partName;
		}

		public void setPartName(String partName) {
			this.partName = partName;
		}

		public String getPartKey() {
			return partKey;
		}

		public void setPartKey(String partKey) {
			this.partKey = partKey;
		}

		public String getPartImg() {
			return partImg;
		}

		public void setPartImg(String partImg) {
			this.partImg = partImg;
		}

		public String getPartCatalogImg() {
			return partCatalogImg;
		}

		public void setPartCatalogImg(String partCatalogImg) {
			this.partCatalogImg = partCatalogImg;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

		public List<SubPart> getSubParts() {
			return subParts;
		}

		public void setSubParts(List<SubPart> subParts) {
			this.subParts = subParts;
		}

	}
	public class ProductDetail implements Serializable
	{

		@SerializedName("id")
		@Expose
		private String id;
		@SerializedName("prod_name")
		@Expose
		private String prodName;
		@SerializedName("prod_model")
		@Expose
		private String prodModel;
		@SerializedName("prod_cat")
		@Expose
		private String prodCat;
		@SerializedName("prod_type")
		@Expose
		private String prodType;
		@SerializedName("prod_image")
		@Expose
		private String prodImage;
		@SerializedName("prod_price")
		@Expose
		private String prodPrice;
		@SerializedName("prod_sno")
		@Expose
		private String prodSno;
		@SerializedName("prod_desc")
		@Expose
		private List<String> prodDesc = null;
		@SerializedName("prod_qty")
		@Expose
		private String prodQty;
		@SerializedName("prod_video")
		@Expose
		private List<ProdVideo> prodVideo = null;
		@SerializedName("prod_pdf")
		@Expose
		private List<ProdPdf> prodPdf = null;
		@SerializedName("prod_acc")
		@Expose
		private String prodAcc;
		@SerializedName("warranty")
		@Expose
		private String warranty;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("disable")
		@Expose
		private String disable;
		@SerializedName("all_images")
		@Expose
		private List<String> allImages = null;
		@SerializedName("accessories")
		@Expose
		private List<Accessory> accessories = null;
		@SerializedName("systems")
		@Expose
		private List<System> systems = null;
		@SerializedName("price")
		@Expose
		private String price;
		@SerializedName("part_list")
		@Expose
		private List<Part> partList = null;
		@SerializedName("op_manual")
		@Expose
		private String opManual;
		@SerializedName("3d_image")
		@Expose
		private String _3dImage;
		@SerializedName("service_manual")
		@Expose
		private String serviceManual;
		@SerializedName("troubleshooting")
		@Expose
		private String troubleshooting;
		@SerializedName("Left")
		@Expose
		private String left;
		@SerializedName("Right")
		@Expose
		private String right;
		@SerializedName("Front")
		@Expose
		private String front;
		@SerializedName("Back")
		@Expose
		private String back;
		private final static long serialVersionUID = -7321615159237416633L;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getProdName() {
			return prodName;
		}

		public void setProdName(String prodName) {
			this.prodName = prodName;
		}

		public String getProdModel() {
			return prodModel;
		}

		public void setProdModel(String prodModel) {
			this.prodModel = prodModel;
		}

		public String getProdCat() {
			return prodCat;
		}

		public void setProdCat(String prodCat) {
			this.prodCat = prodCat;
		}

		public String getProdType() {
			return prodType;
		}

		public void setProdType(String prodType) {
			this.prodType = prodType;
		}

		public String getProdImage() {
			return prodImage;
		}

		public void setProdImage(String prodImage) {
			this.prodImage = prodImage;
		}

		public String getProdPrice() {
			return prodPrice;
		}

		public void setProdPrice(String prodPrice) {
			this.prodPrice = prodPrice;
		}

		public String getProdSno() {
			return prodSno;
		}

		public void setProdSno(String prodSno) {
			this.prodSno = prodSno;
		}

		public List<String> getProdDesc() {
			return prodDesc;
		}

		public void setProdDesc(List<String> prodDesc) {
			this.prodDesc = prodDesc;
		}

		public String getProdQty() {
			return prodQty;
		}

		public void setProdQty(String prodQty) {
			this.prodQty = prodQty;
		}

		public List<ProdVideo> getProdVideo() {
			return prodVideo;
		}

		public void setProdVideo(List<ProdVideo> prodVideo) {
			this.prodVideo = prodVideo;
		}

		public List<ProdPdf> getProdPdf() {
			return prodPdf;
		}

		public void setProdPdf(List<ProdPdf> prodPdf) {
			this.prodPdf = prodPdf;
		}

		public String getProdAcc() {
			return prodAcc;
		}

		public void setProdAcc(String prodAcc) {
			this.prodAcc = prodAcc;
		}

		public String getWarranty() {
			return warranty;
		}

		public void setWarranty(String warranty) {
			this.warranty = warranty;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

		public List<String> getAllImages() {
			return allImages;
		}

		public void setAllImages(List<String> allImages) {
			this.allImages = allImages;
		}

		public List<Accessory> getAccessories() {
			return accessories;
		}

		public void setAccessories(List<Accessory> accessories) {
			this.accessories = accessories;
		}

		public List<System> getSystems() {
			return systems;
		}

		public void setSystems(List<System> systems) {
			this.systems = systems;
		}

		public String getPrice() {
			return price;
		}

		public void setPrice(String price) {
			this.price = price;
		}

		public List<Part> getPartList() {
			return partList;
		}

		public void setPartList(List<Part> partList) {
			this.partList = partList;
		}

		public String getOpManual() {
			return opManual;
		}

		public void setOpManual(String opManual) {
			this.opManual = opManual;
		}

		public String get3dImage() {
			return _3dImage;
		}

		public void set3dImage(String _3dImage) {
			this._3dImage = _3dImage;
		}

		public String getServiceManual() {
			return serviceManual;
		}

		public void setServiceManual(String serviceManual) {
			this.serviceManual = serviceManual;
		}

		public String getTroubleshooting() {
			return troubleshooting;
		}

		public void setTroubleshooting(String troubleshooting) {
			this.troubleshooting = troubleshooting;
		}

		public String getLeft() {
			return left;
		}

		public void setLeft(String left) {
			this.left = left;
		}

		public String getRight() {
			return right;
		}

		public void setRight(String right) {
			this.right = right;
		}

		public String getFront() {
			return front;
		}

		public void setFront(String front) {
			this.front = front;
		}

		public String getBack() {
			return back;
		}

		public void setBack(String back) {
			this.back = back;
		}

	}
	public class ReviewList implements Serializable
	{

		@SerializedName("all_reviews")
		@Expose
		private List<AllReview> allReviews = null;
		@SerializedName("last_page")
		@Expose
		private String lastPage;
		private final static long serialVersionUID = -2210283830166036777L;

		public List<AllReview> getAllReviews() {
			return allReviews;
		}

		public void setAllReviews(List<AllReview> allReviews) {
			this.allReviews = allReviews;
		}

		public String getLastPage() {
			return lastPage;
		}

		public void setLastPage(String lastPage) {
			this.lastPage = lastPage;
		}

	}

	public class SubPart implements Serializable
	{

		@SerializedName("psp_id")
		@Expose
		private String pspId;
		@SerializedName("pp_id")
		@Expose
		private String ppId;
		@SerializedName("prod_id")
		@Expose
		private String prodId;
		@SerializedName("psp_name")
		@Expose
		private String pspName;
		@SerializedName("psp_key")
		@Expose
		private String pspKey;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("disable")
		@Expose
		private String disable;
		private final static long serialVersionUID = 2416475894124908285L;

		public String getPspId() {
			return pspId;
		}

		public void setPspId(String pspId) {
			this.pspId = pspId;
		}

		public String getPpId() {
			return ppId;
		}

		public void setPpId(String ppId) {
			this.ppId = ppId;
		}

		public String getProdId() {
			return prodId;
		}

		public void setProdId(String prodId) {
			this.prodId = prodId;
		}

		public String getPspName() {
			return pspName;
		}

		public void setPspName(String pspName) {
			this.pspName = pspName;
		}

		public String getPspKey() {
			return pspKey;
		}

		public void setPspKey(String pspKey) {
			this.pspKey = pspKey;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

	}
	public class ProdPdf implements Serializable {

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	public class ProdVideo implements Serializable {

		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	public class System implements Serializable
	{

		@SerializedName("id")
		@Expose
		private String id;
		@SerializedName("sys_name")
		@Expose
		private String sysName;
		@SerializedName("creation_date")
		@Expose
		private String creationDate;
		@SerializedName("disable")
		@Expose
		private String disable;
		@SerializedName("trac_name")
		@Expose
		private String tracName;
		private final static long serialVersionUID = 5737772319894948305L;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSysName() {
			return sysName;
		}

		public void setSysName(String sysName) {
			this.sysName = sysName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

		public String getTracName() {
			return tracName;
		}

		public void setTracName(String tracName) {
			this.tracName = tracName;
		}

	}
	public class UserDetail implements Serializable
	{

		@SerializedName("id")
		@Expose
		private String id;
		@SerializedName("first_name")
		@Expose
		private String firstName;
		@SerializedName("last_name")
		@Expose
		private String lastName;
		@SerializedName("username")
		@Expose
		private String username;
		@SerializedName("firm_name")
		@Expose
		private String firmName;
		@SerializedName("phone_no")
		@Expose
		private String phoneNo;
		@SerializedName("image")
		@Expose
		private String image;
		@SerializedName("role")
		@Expose
		private String role;
		@SerializedName("password")
		@Expose
		private String password;
		@SerializedName("auth_key")
		@Expose
		private String authKey;
		@SerializedName("device_type")
		@Expose
		private String deviceType;
		@SerializedName("device_token")
		@Expose
		private String deviceToken;
		@SerializedName("user_lat")
		@Expose
		private String userLat;
		@SerializedName("app_signup")
		@Expose
		private String appSignup;
		@SerializedName("admin_signup")
		@Expose
		private String adminSignup;
		@SerializedName("user_long")
		@Expose
		private String userLong;
		@SerializedName("bio")
		@Expose
		private String bio;
		@SerializedName("dealer_code")
		@Expose
		private String dealerCode;
		@SerializedName("serial_no")
		@Expose
		private String serialNo;
		@SerializedName("address")
		@Expose
		private String address;
		@SerializedName("created_on")
		@Expose
		private String createdOn;
		@SerializedName("disable")
		@Expose
		private String disable;
		@SerializedName("dealer_doc")
		@Expose
		private List<String> dealerDoc = null;
		private final static long serialVersionUID = 518255464696049750L;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getFirmName() {
			return firmName;
		}

		public void setFirmName(String firmName) {
			this.firmName = firmName;
		}

		public String getPhoneNo() {
			return phoneNo;
		}

		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getAuthKey() {
			return authKey;
		}

		public void setAuthKey(String authKey) {
			this.authKey = authKey;
		}

		public String getDeviceType() {
			return deviceType;
		}

		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}

		public String getDeviceToken() {
			return deviceToken;
		}

		public void setDeviceToken(String deviceToken) {
			this.deviceToken = deviceToken;
		}

		public String getUserLat() {
			return userLat;
		}

		public void setUserLat(String userLat) {
			this.userLat = userLat;
		}

		public String getAppSignup() {
			return appSignup;
		}

		public void setAppSignup(String appSignup) {
			this.appSignup = appSignup;
		}

		public String getAdminSignup() {
			return adminSignup;
		}

		public void setAdminSignup(String adminSignup) {
			this.adminSignup = adminSignup;
		}

		public String getUserLong() {
			return userLong;
		}

		public void setUserLong(String userLong) {
			this.userLong = userLong;
		}

		public String getBio() {
			return bio;
		}

		public void setBio(String bio) {
			this.bio = bio;
		}

		public String getDealerCode() {
			return dealerCode;
		}

		public void setDealerCode(String dealerCode) {
			this.dealerCode = dealerCode;
		}

		public String getSerialNo() {
			return serialNo;
		}

		public void setSerialNo(String serialNo) {
			this.serialNo = serialNo;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getDisable() {
			return disable;
		}

		public void setDisable(String disable) {
			this.disable = disable;
		}

		public List<String> getDealerDoc() {
			return dealerDoc;
		}

		public void setDealerDoc(List<String> dealerDoc) {
			this.dealerDoc = dealerDoc;
		}

	}
//	@SerializedName("status")
//	@Expose
//	private String status;
//	@SerializedName("message")
//	@Expose
//	private String message;
//	@SerializedName("register_count")
//	@Expose
//	private int register_count;
//
//	public int getRegister_count() {
//		return register_count;
//	}
//
//	public void setRegister_count(int register_count) {
//		this.register_count = register_count;
//	}
//
//	@SerializedName("review_list")
//	@Expose
//	private ReviewList reviewList;
//	private final static long serialVersionUID = -1661894321328203327L;
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getMessage() {
//		return message;
//	}
//
//	public void setMessage(String message) {
//		this.message = message;
//	}
//
//	public ReviewList getReviewList() {
//		return reviewList;
//	}
//
//	public void setReviewList(ReviewList reviewList) {
//		this.reviewList = reviewList;
//	}
//
//
//	public class ReviewList implements Serializable {
//
//		@SerializedName("all_reviews")
//		@Expose
//		private List<AllReview> allReviews = null;
//		@SerializedName("last_page")
//		@Expose
//		private String lastPage;
//		private final static long serialVersionUID = -2210283830166036777L;
//
//		public List<AllReview> getAllReviews() {
//			return allReviews;
//		}
//
//		public void setAllReviews(List<AllReview> allReviews) {
//			this.allReviews = allReviews;
//		}
//
//		public String getLastPage() {
//			return lastPage;
//		}
//
//		public void setLastPage(String lastPage) {
//			this.lastPage = lastPage;
//		}
//
//	}
//
//	public class AllReview implements Serializable {
//		String isSelected = "";
//
//		public String getIsSelected() {
//			return isSelected;
//		}
//
//		public void setIsSelected(String isSelected) {
//			this.isSelected = isSelected;
//		}
//
//		@SerializedName("review_id")
//		@Expose
//		private String reviewId;
//		@SerializedName("user_id")
//		@Expose
//		private String userId;
//		@SerializedName("product_id")
//		@Expose
//		private String productId;
//		@SerializedName("review_text")
//		@Expose
//		private String reviewText;
//		@SerializedName("review_img")
//		@Expose
//		private String reviewImg;
//		@SerializedName("approval_status")
//		@Expose
//		private String approvalStatus;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		@SerializedName("user_detail")
//		@Expose
//		private UserDetail userDetail;
//		@SerializedName("product_detail")
//		@Expose
//		private ProductDetail productDetail;
//		private final static long serialVersionUID = -289016467522724566L;
//
//		public String getReviewId() {
//			return reviewId;
//		}
//
//		public void setReviewId(String reviewId) {
//			this.reviewId = reviewId;
//		}
//
//		public String getUserId() {
//			return userId;
//		}
//
//		public void setUserId(String userId) {
//			this.userId = userId;
//		}
//
//		public String getProductId() {
//			return productId;
//		}
//
//		public void setProductId(String productId) {
//			this.productId = productId;
//		}
//
//		public String getReviewText() {
//			return reviewText;
//		}
//
//		public void setReviewText(String reviewText) {
//			this.reviewText = reviewText;
//		}
//
//		public String getReviewImg() {
//			return reviewImg;
//		}
//
//		public void setReviewImg(String reviewImg) {
//			this.reviewImg = reviewImg;
//		}
//
//		public String getApprovalStatus() {
//			return approvalStatus;
//		}
//
//		public void setApprovalStatus(String approvalStatus) {
//			this.approvalStatus = approvalStatus;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//		public UserDetail getUserDetail() {
//			return userDetail;
//		}
//
//		public void setUserDetail(UserDetail userDetail) {
//			this.userDetail = userDetail;
//		}
//
//		public ProductDetail getProductDetail() {
//			return productDetail;
//		}
//
//		public void setProductDetail(ProductDetail productDetail) {
//			this.productDetail = productDetail;
//		}
//
//	}
//
//	public class UserDetail implements Serializable {
//
//		@SerializedName("id")
//		@Expose
//		private String id;
//		@SerializedName("first_name")
//		@Expose
//		private String firstName;
//		@SerializedName("last_name")
//		@Expose
//		private String lastName;
//		@SerializedName("username")
//		@Expose
//		private String username;
//		@SerializedName("firm_name")
//		@Expose
//		private String firmName;
//		@SerializedName("phone_no")
//		@Expose
//		private String phoneNo;
//		@SerializedName("image")
//		@Expose
//		private String image;
//		@SerializedName("role")
//		@Expose
//		private String role;
//		@SerializedName("password")
//		@Expose
//		private String password;
//		@SerializedName("auth_key")
//		@Expose
//		private String authKey;
//		@SerializedName("device_type")
//		@Expose
//		private String deviceType;
//		@SerializedName("device_token")
//		@Expose
//		private String deviceToken;
//		@SerializedName("user_lat")
//		@Expose
//		private String userLat;
//		@SerializedName("app_signup")
//		@Expose
//		private String appSignup;
//		@SerializedName("admin_signup")
//		@Expose
//		private String adminSignup;
//		@SerializedName("user_long")
//		@Expose
//		private String userLong;
//		@SerializedName("bio")
//		@Expose
//		private String bio;
//		@SerializedName("dealer_code")
//		@Expose
//		private String dealerCode;
//		@SerializedName("serial_no")
//		@Expose
//		private String serialNo;
//		@SerializedName("address")
//		@Expose
//		private String address;
//		@SerializedName("created_on")
//		@Expose
//		private String createdOn;
//		@SerializedName("disable")
//		@Expose
//		private String disable;
//		@SerializedName("dealer_doc")
//		@Expose
//		private List<String> dealerDoc = null;
//		private final static long serialVersionUID = -5507138413772463099L;
//
//		public String getId() {
//			return id;
//		}
//
//		public void setId(String id) {
//			this.id = id;
//		}
//
//		public String getFirstName() {
//			return firstName;
//		}
//
//		public void setFirstName(String firstName) {
//			this.firstName = firstName;
//		}
//
//		public String getLastName() {
//			return lastName;
//		}
//
//		public void setLastName(String lastName) {
//			this.lastName = lastName;
//		}
//
//		public String getUsername() {
//			return username;
//		}
//
//		public void setUsername(String username) {
//			this.username = username;
//		}
//
//		public String getFirmName() {
//			return firmName;
//		}
//
//		public void setFirmName(String firmName) {
//			this.firmName = firmName;
//		}
//
//		public String getPhoneNo() {
//			return phoneNo;
//		}
//
//		public void setPhoneNo(String phoneNo) {
//			this.phoneNo = phoneNo;
//		}
//
//		public String getImage() {
//			return image;
//		}
//
//		public void setImage(String image) {
//			this.image = image;
//		}
//
//		public String getRole() {
//			return role;
//		}
//
//		public void setRole(String role) {
//			this.role = role;
//		}
//
//		public String getPassword() {
//			return password;
//		}
//
//		public void setPassword(String password) {
//			this.password = password;
//		}
//
//		public String getAuthKey() {
//			return authKey;
//		}
//
//		public void setAuthKey(String authKey) {
//			this.authKey = authKey;
//		}
//
//		public String getDeviceType() {
//			return deviceType;
//		}
//
//		public void setDeviceType(String deviceType) {
//			this.deviceType = deviceType;
//		}
//
//		public String getDeviceToken() {
//			return deviceToken;
//		}
//
//		public void setDeviceToken(String deviceToken) {
//			this.deviceToken = deviceToken;
//		}
//
//		public String getUserLat() {
//			return userLat;
//		}
//
//		public void setUserLat(String userLat) {
//			this.userLat = userLat;
//		}
//
//		public String getAppSignup() {
//			return appSignup;
//		}
//
//		public void setAppSignup(String appSignup) {
//			this.appSignup = appSignup;
//		}
//
//		public String getAdminSignup() {
//			return adminSignup;
//		}
//
//		public void setAdminSignup(String adminSignup) {
//			this.adminSignup = adminSignup;
//		}
//
//		public String getUserLong() {
//			return userLong;
//		}
//
//		public void setUserLong(String userLong) {
//			this.userLong = userLong;
//		}
//
//		public String getBio() {
//			return bio;
//		}
//
//		public void setBio(String bio) {
//			this.bio = bio;
//		}
//
//		public String getDealerCode() {
//			return dealerCode;
//		}
//
//		public void setDealerCode(String dealerCode) {
//			this.dealerCode = dealerCode;
//		}
//
//		public String getSerialNo() {
//			return serialNo;
//		}
//
//		public void setSerialNo(String serialNo) {
//			this.serialNo = serialNo;
//		}
//
//		public String getAddress() {
//			return address;
//		}
//
//		public void setAddress(String address) {
//			this.address = address;
//		}
//
//		public String getCreatedOn() {
//			return createdOn;
//		}
//
//		public void setCreatedOn(String createdOn) {
//			this.createdOn = createdOn;
//		}
//
//		public String getDisable() {
//			return disable;
//		}
//
//		public void setDisable(String disable) {
//			this.disable = disable;
//		}
//
//		public List<String> getDealerDoc() {
//			return dealerDoc;
//		}
//
//		public void setDealerDoc(List<String> dealerDoc) {
//			this.dealerDoc = dealerDoc;
//		}
//
//	}
//
//
//	public class ProductDetail implements Serializable {
//
//		@SerializedName("id")
//		@Expose
//		private String id;
//		@SerializedName("prod_name")
//		@Expose
//		private String prodName;
//		@SerializedName("prod_model")
//		@Expose
//		private String prodModel;
//		@SerializedName("prod_cat")
//		@Expose
//		private String prodCat;
//		@SerializedName("prod_type")
//		@Expose
//		private String prodType;
//		@SerializedName("prod_image")
//		@Expose
//		private String prodImage;
//		@SerializedName("prod_price")
//		@Expose
//		private String prodPrice;
//		@SerializedName("prod_sno")
//		@Expose
//		private String prodSno;
//		@SerializedName("prod_desc")
//		@Expose
//		private List<String> prodDesc = null;
//		@SerializedName("prod_qty")
//		@Expose
//		private String prodQty;
//		@SerializedName("prod_video")
//		@Expose
//		private List<String> prodVideo = null;
//		@SerializedName("prod_pdf")
//		@Expose
//		private List<String> prodPdf = null;
//		@SerializedName("prod_acc")
//		@Expose
//		private String prodAcc;
//		@SerializedName("warranty")
//		@Expose
//		private String warranty;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		@SerializedName("disable")
//		@Expose
//		private String disable;
//		@SerializedName("all_images")
//		@Expose
//		private List<String> allImages = null;
//		@SerializedName("accessories")
//		@Expose
//		private List<Accessory> accessories = null;
//		@SerializedName("systems")
//		@Expose
//		private List<System> systems = null;
//		@SerializedName("price")
//		@Expose
//		private String price;
//		@SerializedName("part_list")
//		@Expose
//		private List<Part> partList = null;
//		@SerializedName("op_manual")
//		@Expose
//		private String opManual;
//		@SerializedName("3d_image")
//		@Expose
//		private String _3dImage;
//		@SerializedName("service_manual")
//		@Expose
//		private String serviceManual;
//		@SerializedName("troubleshooting")
//		@Expose
//		private String troubleshooting;
//		@SerializedName("Left")
//		@Expose
//		private String left;
//		@SerializedName("Right")
//		@Expose
//		private String right;
//		@SerializedName("Front")
//		@Expose
//		private String front;
//		@SerializedName("Back")
//		@Expose
//		private String back;
//
//		@SerializedName("doc_list")
//		@Expose
//		private List<Doc> docList = null;
//
//		private final static long serialVersionUID = -1810052883720516472L;
//
//		public String getId() {
//			return id;
//		}
//
//		public void setId(String id) {
//			this.id = id;
//		}
//
//		public String getProdName() {
//			return prodName;
//		}
//
//		public void setProdName(String prodName) {
//			this.prodName = prodName;
//		}
//
//		public String getProdModel() {
//			return prodModel;
//		}
//
//		public void setProdModel(String prodModel) {
//			this.prodModel = prodModel;
//		}
//
//		public String getProdCat() {
//			return prodCat;
//		}
//
//		public void setProdCat(String prodCat) {
//			this.prodCat = prodCat;
//		}
//
//		public String getWarranty() {
//			return warranty;
//		}
//
//		public void setWarranty(String warranty) {
//			this.warranty = warranty;
//		}
//		public String getProdType() {
//			return prodType;
//		}
//
//		public void setProdType(String prodType) {
//			this.prodType = prodType;
//		}
//
//		public String getProdImage() {
//			return prodImage;
//		}
//
//		public void setProdImage(String prodImage) {
//			this.prodImage = prodImage;
//		}
//
//		public String getProdPrice() {
//			return prodPrice;
//		}
//
//		public void setProdPrice(String prodPrice) {
//			this.prodPrice = prodPrice;
//		}
//
//		public String getProdSno() {
//			return prodSno;
//		}
//
//		public void setProdSno(String prodSno) {
//			this.prodSno = prodSno;
//		}
//		public List<String> getAllImages() {
//			return allImages;
//		}
//
//		public void setAllImages(List<String> allImages) {
//			this.allImages = allImages;
//		}
//
//		public List<String> getProdDesc() {
//			return prodDesc;
//		}
//
//		public void setProdDesc(List<String> prodDesc) {
//			this.prodDesc = prodDesc;
//		}
//
//		public String getProdQty() {
//			return prodQty;
//		}
//
//		public void setProdQty(String prodQty) {
//			this.prodQty = prodQty;
//		}
//
//		public List<String> getProdVideo() {
//			return prodVideo;
//		}
//
//		public void setProdVideo(List<String> prodVideo) {
//			this.prodVideo = prodVideo;
//		}
//
//		public List<String> getProdPdf() {
//			return prodPdf;
//		}
//
//		public void setProdPdf(List<String> prodPdf) {
//			this.prodPdf = prodPdf;
//		}
//
//		public String getProdAcc() {
//			return prodAcc;
//		}
//
//		public void setProdAcc(String prodAcc) {
//			this.prodAcc = prodAcc;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//		public String getDisable() {
//			return disable;
//		}
//
//		public void setDisable(String disable) {
//			this.disable = disable;
//		}
//
//		public List<Accessory> getAccessories() {
//			return accessories;
//		}
//
//		public void setAccessories(List<Accessory> accessories) {
//			this.accessories = accessories;
//		}
//
//		public List<System> getSystems() {
//			return systems;
//		}
//
//		public void setSystems(List<System> systems) {
//			this.systems = systems;
//		}
//
//		public String getPrice() {
//			return price;
//		}
//
//		public void setPrice(String price) {
//			this.price = price;
//		}
//
//		public List<Part> getPartList() {
//			return partList;
//		}
//
//		public void setPartList(List<Part> partList) {
//			this.partList = partList;
//		}
//
//		public List<Doc> getDocList() {
//			return docList;
//		}
//
//		public void setDocList(List<Doc> docList) {
//			this.docList = docList;
//		}
//		public String getOpManual() {
//			return opManual;
//		}
//
//		public void setOpManual(String opManual) {
//			this.opManual = opManual;
//		}
//
//		public String get3dImage() {
//			return _3dImage;
//		}
//
//		public void set3dImage(String _3dImage) {
//			this._3dImage = _3dImage;
//		}
//
//		public String getServiceManual() {
//			return serviceManual;
//		}
//
//		public void setServiceManual(String serviceManual) {
//			this.serviceManual = serviceManual;
//		}
//
//		public String getTroubleshooting() {
//			return troubleshooting;
//		}
//
//		public void setTroubleshooting(String troubleshooting) {
//			this.troubleshooting = troubleshooting;
//		}
//
//		public String getLeft() {
//			return left;
//		}
//
//		public void setLeft(String left) {
//			this.left = left;
//		}
//
//		public String getRight() {
//			return right;
//		}
//
//		public void setRight(String right) {
//			this.right = right;
//		}
//
//		public String getFront() {
//			return front;
//		}
//
//		public void setFront(String front) {
//			this.front = front;
//		}
//
//		public String getBack() {
//			return back;
//		}
//
//		public void setBack(String back) {
//			this.back = back;
//		}
//
//
//	}
//	public class SubPart implements Serializable
//	{
//
//		@SerializedName("psp_id")
//		@Expose
//		private String pspId;
//		@SerializedName("pp_id")
//		@Expose
//		private String ppId;
//		@SerializedName("prod_id")
//		@Expose
//		private String prodId;
//		@SerializedName("psp_name")
//		@Expose
//		private String pspName;
//		@SerializedName("psp_key")
//		@Expose
//		private String pspKey;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		@SerializedName("disable")
//		@Expose
//		private String disable;
//		private final static long serialVersionUID = 2416475894124908285L;
//
//		public String getPspId() {
//			return pspId;
//		}
//
//		public void setPspId(String pspId) {
//			this.pspId = pspId;
//		}
//
//		public String getPpId() {
//			return ppId;
//		}
//
//		public void setPpId(String ppId) {
//			this.ppId = ppId;
//		}
//
//		public String getProdId() {
//			return prodId;
//		}
//
//		public void setProdId(String prodId) {
//			this.prodId = prodId;
//		}
//
//		public String getPspName() {
//			return pspName;
//		}
//
//		public void setPspName(String pspName) {
//			this.pspName = pspName;
//		}
//
//		public String getPspKey() {
//			return pspKey;
//		}
//
//		public void setPspKey(String pspKey) {
//			this.pspKey = pspKey;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//		public String getDisable() {
//			return disable;
//		}
//
//		public void setDisable(String disable) {
//			this.disable = disable;
//		}
//
//	}
//	public class Part implements Serializable
//	{
//
//		@SerializedName("pp_id")
//		@Expose
//		private String ppId;
//		@SerializedName("prod_id")
//		@Expose
//		private String prodId;
//		@SerializedName("part_pos")
//		@Expose
//		private String partPos;
//		@SerializedName("part_name")
//		@Expose
//		private String partName;
//		@SerializedName("part_key")
//		@Expose
//		private String partKey;
//		@SerializedName("part_img")
//		@Expose
//		private String partImg;
//		@SerializedName("part_catalog_img")
//		@Expose
//		private String partCatalogImg;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		@SerializedName("disable")
//		@Expose
//		private String disable;
//		@SerializedName("sub_parts")
//		@Expose
//		private List<SubPart> subParts = null;
//		private final static long serialVersionUID = 5608517847620938182L;
//
//		public String getPpId() {
//			return ppId;
//		}
//
//		public void setPpId(String ppId) {
//			this.ppId = ppId;
//		}
//
//		public String getProdId() {
//			return prodId;
//		}
//
//		public void setProdId(String prodId) {
//			this.prodId = prodId;
//		}
//
//		public String getPartPos() {
//			return partPos;
//		}
//
//		public void setPartPos(String partPos) {
//			this.partPos = partPos;
//		}
//
//		public String getPartName() {
//			return partName;
//		}
//
//		public void setPartName(String partName) {
//			this.partName = partName;
//		}
//
//		public String getPartKey() {
//			return partKey;
//		}
//
//		public void setPartKey(String partKey) {
//			this.partKey = partKey;
//		}
//
//		public String getPartImg() {
//			return partImg;
//		}
//
//		public void setPartImg(String partImg) {
//			this.partImg = partImg;
//		}
//
//		public String getPartCatalogImg() {
//			return partCatalogImg;
//		}
//
//		public void setPartCatalogImg(String partCatalogImg) {
//			this.partCatalogImg = partCatalogImg;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//		public String getDisable() {
//			return disable;
//		}
//
//		public void setDisable(String disable) {
//			this.disable = disable;
//		}
//
//		public List<SubPart> getSubParts() {
//			return subParts;
//		}
//
//		public void setSubParts(List<SubPart> subParts) {
//			this.subParts = subParts;
//		}
//
//	}
//
//	public class Doc implements Serializable {
//
//		@SerializedName("pd_id")
//		@Expose
//		private String pdId;
//		@SerializedName("pd_type")
//		@Expose
//		private String pdType;
//		@SerializedName("prod_id")
//		@Expose
//		private String prodId;
//		@SerializedName("pd_name")
//		@Expose
//		private String pdName;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		private final static long serialVersionUID = 6274558895885321797L;
//
//		public String getPdId() {
//			return pdId;
//		}
//
//		public void setPdId(String pdId) {
//			this.pdId = pdId;
//		}
//
//		public String getPdType() {
//			return pdType;
//		}
//
//		public void setPdType(String pdType) {
//			this.pdType = pdType;
//		}
//
//		public String getProdId() {
//			return prodId;
//		}
//
//		public void setProdId(String prodId) {
//			this.prodId = prodId;
//		}
//
//		public String getPdName() {
//			return pdName;
//		}
//
//		public void setPdName(String pdName) {
//			this.pdName = pdName;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//	}
//
//	public class Accessory implements Serializable {
//
//		@SerializedName("id")
//		@Expose
//		private String id;
//		@SerializedName("acc_name")
//		@Expose
//		private String accName;
//		@SerializedName("creation_date")
//		@Expose
//		private String creationDate;
//		@SerializedName("disable")
//		@Expose
//		private String disable;
//		private final static long serialVersionUID = -2799711547697193975L;
//
//		public String getId() {
//			return id;
//		}
//
//		public void setId(String id) {
//			this.id = id;
//		}
//
//		public String getAccName() {
//			return accName;
//		}
//
//		public void setAccName(String accName) {
//			this.accName = accName;
//		}
//
//		public String getCreationDate() {
//			return creationDate;
//		}
//
//		public void setCreationDate(String creationDate) {
//			this.creationDate = creationDate;
//		}
//
//		public String getDisable() {
//			return disable;
//		}
//
//		public void setDisable(String disable) {
//			this.disable = disable;
//		}
//
//	}

}