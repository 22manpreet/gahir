package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

public class MessageModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("notification_list")
	private NotificationList notificationList;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public String getAccessToken(){
		return accessToken;
	}

	public NotificationList getNotificationList(){
		return notificationList;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}