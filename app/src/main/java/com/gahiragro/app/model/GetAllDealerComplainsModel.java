package com.gahiragro.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetAllDealerComplainsModel implements Serializable {

	@SerializedName("complain_list")
	private ComplainList complainList;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public ComplainList getComplainList(){
		return complainList;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}