package com.gahiragro.app.utils;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;

public class GahirAgroApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
    }

}
