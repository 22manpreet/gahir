package com.gahiragro.app.utils

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class GahirApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(applicationContext)
    }
}