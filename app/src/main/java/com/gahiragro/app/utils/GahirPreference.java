package com.gahiragro.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import java.util.List;

public class GahirPreference {
    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    public static final String IS_LOGIN = "is_login";
    public static final String USER_ID = "user_id";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String AUTHENTICATE_TOKEN = "authenticate_token";
    public static final String TICKET_ID = "ticket_id";
    public static final String EVENT_ID = "event_id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "lngtitude";
    public static final String FACE_LOCK = "face_lock";
    public static final String FINGER_LOCK = "finger_lock";
    public static final String CITY = "city";
    public static final String FOVORITE_TYPE = "fav_type";
    public static final String STATUS_TYPE = "status_type";
    public static final String INVITE_ID = "invite_id";
    public static final String FINGER_LOGIN_DONE = "finger_login_done";
    public static final String LANGUAGE = "LANGUAGE_";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String SIGNUP_STATUS = "signUpStatus";
    public static final String USER_NAME = "user_name";
    public static final String USER_PROFILE_PIC = "user_profile_pic";
    public static final String LOGIN_TYPE = "login_type";
    public static final String BOOKING_ID = "BOOKING_ID";
    public static final String SERIAL_NO = "serialno";
    public static final String DEALER_CODE = "dealercode";
    public static final String PHONE_NO = "phoneno";
    public static final String DEALER_DOC = "DEALER_DOC";
    public static final String GUEST_LOGIN = "guestLogin";
    public static final String ADDRESS="address";
    public static final String SELECTED_LOCATION = "selected_location";
    public static final String FULL_SELECTED_LOCATION = "selected_full_location";

    /*
     * Keys
     * */
//    public static final String USER_ID = "user_id";
//    public static final String FIRST_NAME = "firstname";
//    public static final String LAST_NAME = "lastname";
//    public static final String EMAIL = "email";


    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        if(context!=null)
        {
            getEditor(context).putString(key, value).apply();
        }


    }





    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        if(context!=null)
        {
            return getPreferences(context).getString(key, defValue);
        }
        return null;
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        if(context!=null)
        {
            return context.getSharedPreferences(PREF_NAME, MODE);
        }
        return null;
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        if(context!=null)
        {
            return getPreferences(context).edit();
        }
        return null;
    }

}



