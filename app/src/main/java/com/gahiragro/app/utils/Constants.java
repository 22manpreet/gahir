package com.gahiragro.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;

import com.gahiragro.app.R;

public class Constants {
    static Dialog progressDialog;
    //public static final String BASE_URL = "https://www.dharmani.com/gahir/api/";
    //public static final String BASE_URL = "https://gahiragro.in/api/";
  //  public static final String BASE_URL = "https://gahiragro.in/Staging/api/";

    // Live Base url
public static final String BASE_URL = "https://gahiragro.in/api/";


    public static final String GOOGLE_PLACES_SEARCH = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    public static final String GOOGLE_PLACES_LAT_LONG = "https://maps.googleapis.com/maps/api/place/details/json?place_id=";


    public static final String EMAIL = "";
    public static final String PASSWORD = "";
    public static final String PHONE_NUMBER = "";
    public static final String DEVICE_TYPE = "Android";
    public static final String ID = "id";
    public static final String ENQUIRY_ID = "enquiry_id";
    public static final String ORDER_LIST_MODEL = "order_list_model";
    public static final String LOGIN_NUMBER = "login_number";
    public static final String DEALER_CODE = "dealer_code";
    public static final String SERIAL_NO = "serial_no";
    public static final String APP_SIGNUP = "app_signup";
    public static final String class_type = "class_type";
    public static final String URL = "";
    public static final String ADMIN = "admin";
    public static final String DEALER = "Dealer";
    public static final String CUSTOMER = "Customer";
    public static final String SALES_EXECUTIVE = "Sales";
    public static final String SERVICE = "Service";
    public static final String NOTIFICATION_MASS = "mass";
    public static final String NOTIFICATION_REGISTER_PRODUCT = "register_product";
    public static final String NOTIFICATION_REVIEW ="review";
    public static final String NOTIFICATION_PART_ENQUIRY = "part_enquiry";
    public static final String NOTIFICATION_COMPLAIN = "complain";
    public static final String NOTIFICATION_COSTUM="custom";
    public static final String ORDER_ID = "order_id";
    public static final String TYPE = "type";
    public static final String PRIVACY_POLICY = BASE_URL+"PrivacyPolicy.html";
    public static final String TERMS_CONDITION = BASE_URL+"terms_of_service.html";
    public static final String CATEGORY_ID = "categoryId";
    public static final String IMAGE = "image";
    public static final String FRAGMENT = "FRAGMENT";
    public static final String ADDRESS = "adreesss";
    public static final String STREET = "street";
    public static final String DISTRICT = "district";
    public static final String STATE = "state";
    public static final String PUSH = "push";
    public static final String SEL_PRODUCT_ID = "clickd_id";
    public static final String SEL_ID="id";
    public static final String SEL_PRODUCT_IMAGE = "prod_image";
    public static final String SEL_PRODUCT_SNO = "sr_no";
    public static final String SEL_PRODUCT_NAME = "selProductName";
    public static final String model = "model";
    public static final String deal_model = "dealer_model";
    public static final String TAG = "tag";
    public static final String PASSWORD_VALUE = "password_value";


    public static final String DISTRICT_SEARCH = "district_search";
    public static final String STATE_SEARCH = "state_search";
    public static final String LUBRICATION_STATUS = "lub_value";
    public static final String COMPLAIN_ID = "complin_id";
    public static final String PART_CHANGE = "part_changed";
    public static final String PART_REPAIR = "part_repair";
    public static final String MACHINE_STATUS = "machine_status";
    public static final String HOUR_SPENT = "hour_spent";
    public static final String FULL_MACHINE_CHECK = "full_machine_check";
    public static final String PARTS_CHANGED = "parts_changed";
    public static final String PARTS_REPAIR = "parts_repair";
    public static final String SERVICE_ID = "SERVICE_ID";
    public static final String PARTS_CHANGED_IMAGES = "change_images";
    public static final String PARTS_REPAIR_IMAGES = "repair_images";
    public static final String OTP = "otp";
    public static final String TROUBLESHOOTING = "troubleshooting";
    public static final String SERVICE_MANUAL = "service_manual";
    public static final String LEFT = "left_image";
    public static final String RIGHT = "right_image";
    public static final String BACK = "back_image";
    public static final String FRONT = "front_image";

    /* filter customer review */
    public static Boolean FILTER_BACK_PRESSED = false;
    public static Boolean Review_BACK_PRESSED = false;
    public static final String FILTER_TYPE_ID = "filter_type_id";
    public static final String FILTER_TYPE = "filter_type";
    public static final String LIST = "list";
    public static final String D3_IMAGE = "3dImage";
    public static final String AUDIO_PATH = "audioPTH";
    public static String VIDEO_PATH = "video_path";
    public static String VIDEO_URI = "video_uri";
    public static String DEALER_NAME = "dealer_name";
    public static String CITY_NAME = "city_name";

    /*
     * Check Internet Connections
     * */
    public static boolean isNetworkAvailable(GahirApplication application) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Show Progress Dialog
     * */
    public static void showProgressDialog(Context mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*
     * Hide Progress Dialog
     * */
    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (RuntimeException ex) {

            }
        }
    }

    /*
     * USA States Abbreviations
     * */
    public static final String USA_STATES = "[\n" +
            "    {\n" +
            "        \"name\": \"Alabama\",\n" +
            "        \"abbreviation\": \"AL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Alaska\",\n" +
            "        \"abbreviation\": \"AK\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"American Samoa\",\n" +
            "        \"abbreviation\": \"AS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Arizona\",\n" +
            "        \"abbreviation\": \"AZ\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Arkansas\",\n" +
            "        \"abbreviation\": \"AR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"California\",\n" +
            "        \"abbreviation\": \"CA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Colorado\",\n" +
            "        \"abbreviation\": \"CO\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Connecticut\",\n" +
            "        \"abbreviation\": \"CT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Delaware\",\n" +
            "        \"abbreviation\": \"DE\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"District Of Columbia\",\n" +
            "        \"abbreviation\": \"DC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Federated States Of Micronesia\",\n" +
            "        \"abbreviation\": \"FM\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Florida\",\n" +
            "        \"abbreviation\": \"FL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Georgia\",\n" +
            "        \"abbreviation\": \"GA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Guam Gu\",\n" +
            "        \"abbreviation\": \"GU\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Hawaii\",\n" +
            "        \"abbreviation\": \"HI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Idaho\",\n" +
            "        \"abbreviation\": \"ID\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Illinois\",\n" +
            "        \"abbreviation\": \"IL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Indiana\",\n" +
            "        \"abbreviation\": \"IN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Iowa\",\n" +
            "        \"abbreviation\": \"IA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Kansas\",\n" +
            "        \"abbreviation\": \"KS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Kentucky\",\n" +
            "        \"abbreviation\": \"KY\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Louisiana\",\n" +
            "        \"abbreviation\": \"LA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Maine\",\n" +
            "        \"abbreviation\": \"ME\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Marshall Islands\",\n" +
            "        \"abbreviation\": \"MH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Maryland\",\n" +
            "        \"abbreviation\": \"MD\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Massachusetts\",\n" +
            "        \"abbreviation\": \"MA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Michigan\",\n" +
            "        \"abbreviation\": \"MI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Minnesota\",\n" +
            "        \"abbreviation\": \"MN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Mississippi\",\n" +
            "        \"abbreviation\": \"MS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Missouri\",\n" +
            "        \"abbreviation\": \"MO\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Montana\",\n" +
            "        \"abbreviation\": \"MT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Nebraska\",\n" +
            "        \"abbreviation\": \"NE\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Nevada\",\n" +
            "        \"abbreviation\": \"NV\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Hampshire\",\n" +
            "        \"abbreviation\": \"NH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Jersey\",\n" +
            "        \"abbreviation\": \"NJ\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Mexico\",\n" +
            "        \"abbreviation\": \"NM\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New York\",\n" +
            "        \"abbreviation\": \"NY\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"North Carolina\",\n" +
            "        \"abbreviation\": \"NC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"North Dakota\",\n" +
            "        \"abbreviation\": \"ND\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Northern Mariana Islands\",\n" +
            "        \"abbreviation\": \"MP\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Ohio\",\n" +
            "        \"abbreviation\": \"OH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Oklahoma\",\n" +
            "        \"abbreviation\": \"OK\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Oregon\",\n" +
            "        \"abbreviation\": \"OR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Palau\",\n" +
            "        \"abbreviation\": \"PW\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Pennsylvania\",\n" +
            "        \"abbreviation\": \"PA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Puerto Rico\",\n" +
            "        \"abbreviation\": \"PR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Rhode Island\",\n" +
            "        \"abbreviation\": \"RI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"South Carolina\",\n" +
            "        \"abbreviation\": \"SC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"South Dakota\",\n" +
            "        \"abbreviation\": \"SD\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Tennessee\",\n" +
            "        \"abbreviation\": \"TN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Texas\",\n" +
            "        \"abbreviation\": \"TX\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Utah\",\n" +
            "        \"abbreviation\": \"UT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Vermont\",\n" +
            "        \"abbreviation\": \"VT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Virgin Islands\",\n" +
            "        \"abbreviation\": \"VI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Virginia\",\n" +
            "        \"abbreviation\": \"VA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Washington\",\n" +
            "        \"abbreviation\": \"WA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"West Virginia\",\n" +
            "        \"abbreviation\": \"WV\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Wisconsin\",\n" +
            "        \"abbreviation\": \"WI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Wyoming\",\n" +
            "        \"abbreviation\": \"WY\"\n" +
            "    }\n" +
            "]";
}
