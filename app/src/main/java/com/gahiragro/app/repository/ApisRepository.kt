package com.coach.app.repository

import android.app.Activity
import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gahiragro.app.R
import com.gahiragro.app.RetrofitApi.ApiKotlinInterface
import com.gahiragro.app.RetrofitApi.RestClientKotlin
import com.gahiragro.app.model.CustomerComplainModel
import com.gahiragro.app.utils.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.IOException


class ApisRepository(
    private val apiInterface: ApiKotlinInterface,
    val mActivity: Activity,
   val app: Application
) {

        fun ComplaintDataRequest(
            accessToken: RequestBody,
            contactNum: RequestBody,
            prodIdd: RequestBody,
            srNo: RequestBody,
            compReason: RequestBody,
            reasonDetail: RequestBody,
            support_img1: MultipartBody.Part?,
            support_img2: MultipartBody.Part?,
            support_img3: MultipartBody.Part?,
            support_audios: MultipartBody.Part?) = RestClientKotlin.appApi?.addCustomerComplaint(
            accessToken,
            contactNum,
            prodIdd,
            srNo,
            compReason,
            reasonDetail,
            support_img1,
            support_img2,
            support_img3,
            support_audios
        )



}
