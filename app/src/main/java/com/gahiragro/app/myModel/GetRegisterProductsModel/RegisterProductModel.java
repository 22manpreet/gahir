package com.gahiragro.app.myModel.GetRegisterProductsModel;

import com.google.gson.annotations.SerializedName;

public class RegisterProductModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}