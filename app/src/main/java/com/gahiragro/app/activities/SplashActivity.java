package com.gahiragro.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.UserDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SplashActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;

    //    @BindView(R.id.emailRL)
    RelativeLayout emailRL;

    //    @BindView(R.id.phoneRL)
    RelativeLayout phoneRL;

    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
    //    @BindView(R.id.headerRL)
    RelativeLayout headerRL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        ButterKnife.bind(this);

        txtSignInTV = findViewById(R.id.txtSignInTV);
        emailRL = findViewById(R.id.emailRL);
        phoneRL = findViewById(R.id.phoneRL);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        headerRL = findViewById(R.id.headerRL);

        setStatusBar(mActivity, Color.BLACK);
        if (GahirPreference.readString(mActivity, GahirPreference.GUEST_LOGIN, "").equals("true")) {
            headerRL.setVisibility(View.VISIBLE);
        } else {
            headerRL.setVisibility(View.GONE);
        }

        onViewClicked();

    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.txtSignInTV, R.id.emailRL, R.id.phoneRL, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        txtSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignUpClick();
            }
        });
        emailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignInClick();
            }
        });
        phoneRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performPhoneLoginClick();
            }
        });
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performBackClick();
            }
        });

        /*switch (view.getId()) {
            case R.id.txtSignInTV:
                performSignUpClick();
                break;
            case R.id.emailRL:
                performSignInClick();
                break;
            case R.id.phoneRL:
                performPhoneLoginClick();
                break;
            case R.id.imgMenuRL:
                performBackClick();
                break;
        }*/
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performSignUpClick() {
        Intent intent = new Intent(mActivity, RoleSelectActivity.class);
        startActivity(intent);

    }

    private void performPhoneLoginClick() {
        Intent intent = new Intent(mActivity, PhoneLoginActivity.class);
        startActivity(intent);
    }

    private void performSignInClick() {
        Intent i = new Intent(mActivity, SignInActivity.class);
        startActivity(i);
        //finish();
    }
}
