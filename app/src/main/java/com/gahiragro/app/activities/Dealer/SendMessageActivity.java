package com.gahiragro.app.activities.Dealer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.model.NotificationModel;
import com.gahiragro.app.model.SendDealerSpecificPushModel;
import com.gahiragro.app.model.SendMessageToAllCustomersModel;

import java.util.HashMap;
import java.util.Map;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SendMessageActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SendMessageActivity.this;
    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    //    @BindView(R.id.editTitleET)
    EditText editTitleET;

    //    @BindView(R.id.editMessageET)
    EditText editMessageET;

    //    @BindView(R.id.btnSendB)
    Button btnSendB;

    String customer_id = null;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        editTitleET = findViewById(R.id.editTitleET);
        editMessageET = findViewById(R.id.editMessageET);
        btnSendB = findViewById(R.id.btnSendB);

        intent = getIntent();
        customer_id = intent.getStringExtra("Customer_id");
        OnClick();
    }

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(mActivity);
        }
    };

    private void OnClick() {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSendB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customer_id != null) {
                    if (isNetworkAvailable(mActivity)) {
                        executeSendParticularMessageApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                } else {
                    if (isNetworkAvailable(mActivity)) {
                        executeSendMessageApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }

            }
        });
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("message", editMessageET.getText().toString().trim());
        mMap.put("title", editTitleET.getText().toString().trim());
        mMap.put("customer_id", customer_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSendParticularMessageApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.SendDealerSpecificPushApi(mParams()).enqueue(new Callback<SendDealerSpecificPushModel>() {
            @Override
            public void onResponse(Call<SendDealerSpecificPushModel> call, Response<SendDealerSpecificPushModel> response) {
                dismissProgressDialog();
                SendDealerSpecificPushModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showDialog(mActivity, mModel.getMessage());
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                }
            }

            @Override
            public void onFailure(Call<SendDealerSpecificPushModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("message", editMessageET.getText().toString().trim());
        mMap.put("title", editTitleET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSendMessageApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.SendDealerMassPushApi(mParam()).enqueue(new Callback<SendMessageToAllCustomersModel>() {
            @Override
            public void onResponse(Call<SendMessageToAllCustomersModel> call, Response<SendMessageToAllCustomersModel> response) {
                dismissProgressDialog();
                SendMessageToAllCustomersModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showDialog(mActivity, mModel.getMessage());
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                }
            }

            @Override
            public void onFailure(Call<SendMessageToAllCustomersModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    public void showDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                editTitleET.setText("");
                editMessageET.setText("");
                onBackPressed();
                finish();
            }
        });
        alertDialog.show();
    }
}