package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class TroubleshootingDetailsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TroubleshootingDetailsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TroubleshootingDetailsActivity.this;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troubleshooting_details);
        setStatusBar(mActivity, Color.WHITE);
        imgMenuRL =findViewById(R.id.imgMenuRL);
//        ButterKnife.bind(this);
        OnClick();
    }

    private void OnClick() {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}