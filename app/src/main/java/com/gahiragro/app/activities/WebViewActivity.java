package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class WebViewActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = WebViewActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */

    Activity mActivity = WebViewActivity.this;

//    @BindView(R.id.titleTV)
    TextView titleTV;
//    @BindView(R.id.backIV)
    ImageView backIV;

    String troubleshoot = "";
    String serviceManual = "";
    String classType = "";
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
//        ButterKnife.bind(this);
        titleTV=findViewById(R.id.titleTV);
        backIV=findViewById(R.id.backIV);
        setStatusBar(mActivity, Color.WHITE);
        webView = findViewById(R.id.webview);
        if (getIntent() != null) {
            classType = getIntent().getExtras().getString(Constants.TYPE);
            troubleshoot = getIntent().getExtras().getString(Constants.TROUBLESHOOTING);
            serviceManual = getIntent().getExtras().getString(Constants.SERVICE_MANUAL);
            showProgressDialog(mActivity);
           showPdfFile();
        }

        onViewClicked();
    }
    private void showPdfFile() {

        webView.invalidate();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            boolean checkOnPageStartedCalled = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                checkOnPageStartedCalled = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (checkOnPageStartedCalled) {
                    dismissProgressDialog();
                } else {
                    showPdfFile();
                }
            }
        });

        if (classType.equals("trobleShoot")) {
            titleTV.setText(getString(R.string.troubleshooting));
            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + troubleshoot);
        } else if (classType.equals("serviceManual")) {
            titleTV.setText(getString(R.string.service_manual));
            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + serviceManual);
        }

    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.backIV})
    public void onViewClicked(/*View view*/) {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

       /* switch (view.getId()) {
            case R.id.backIV:
                setBackClick();
                break;

        }*/
    }
    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}