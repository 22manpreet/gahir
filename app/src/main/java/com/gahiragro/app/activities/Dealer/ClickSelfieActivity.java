package com.gahiragro.app.activities.Dealer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.model.CustomerComplainModel;
import com.gahiragro.app.model.CustomerReviewModel;
import com.gahiragro.app.model.ImagesModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.gson.Gson;
import com.lassi.data.media.MiMedia;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClickSelfieActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ClickSelfieActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ClickSelfieActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.ClickRL)
    RelativeLayout ClickRL;

//    @BindView(R.id.btnClickBT)
    TextView btnClickBT;

//    @BindView(R.id.clickImageIV)
    ImageView clickImageIV;

//    @BindView(R.id.sumbitRl)
    RelativeLayout sumbitRl;

    Bitmap selfieImage = null;
    int CAMERA_PIC_REQUEST = 123;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    String serviceId = "";
    String complainId = "";
    String strLubrication = "";
    String strPartsChange = "";
    String strPartsRepaired = "";
    String strMachineStatus = "";
    String strHourSpent = "";
    String strFullMachineCheck = "";
    String strOtp = "";
    ArrayList<MiMedia> imagesChanges;
    ArrayList<MiMedia> imagesRepair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_selfie);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        ClickRL=findViewById(R.id.ClickRL);
        btnClickBT=findViewById(R.id.btnClickBT);
        clickImageIV=findViewById(R.id.clickImageIV);
        sumbitRl=findViewById(R.id.sumbitRl);

        getIntentData();
        Onclick();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            strOtp = getIntent().getStringExtra(Constants.OTP);
            imagesChanges = (ArrayList<MiMedia>) getIntent().getSerializableExtra(Constants.PARTS_CHANGED_IMAGES);
            imagesRepair = (ArrayList<MiMedia>) getIntent().getSerializableExtra(Constants.PARTS_REPAIR_IMAGES);
            serviceId = GahirPreference.readString(mActivity, Constants.SERVICE_ID, "");
            complainId = GahirPreference.readString(mActivity, Constants.COMPLAIN_ID, "");
            strLubrication = GahirPreference.readString(mActivity, Constants.LUBRICATION_STATUS, "");
            strPartsChange = GahirPreference.readString(mActivity, Constants.PARTS_CHANGED, "");
            strPartsRepaired = GahirPreference.readString(mActivity, Constants.PARTS_REPAIR, "");
            strMachineStatus = GahirPreference.readString(mActivity, Constants.MACHINE_STATUS, "");
            strHourSpent = GahirPreference.readString(mActivity, Constants.HOUR_SPENT, "");
            strFullMachineCheck = GahirPreference.readString(mActivity, Constants.FULL_MACHINE_CHECK, "");

        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, writeCamera}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        }
    }

    private void performProfileClick() {
        if (checkPermission()) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
        } else {
            requestPermission();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    void Onclick() {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }

        });


        sumbitRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventMultipleClick();
                setServiceAttentComplaintApi();
            }
        });

        ClickRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                performProfileClick();


            }

        });
    }


    @SuppressLint("MissingSuperCall")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST) {
            selfieImage = (Bitmap) data.getExtras().get("data");
            if (selfieImage != null) {
                sumbitRl.setVisibility(View.VISIBLE);
            } else {
                sumbitRl.setVisibility(View.GONE);
            }
            Glide.with(mActivity).load(selfieImage).into(clickImageIV);
        }
    }


    private void setServiceAttentComplaintApi() {
//        if (isNetworkAvailable(mActivity)) {
//           executeServiceComplaintApi();
//        } else {
//            showToast(mActivity, getString(R.string.internet_connection_error));
//        }


        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                new AsyncTaskExample().execute();
            }
        });

    }


    private void executeServiceComplaintApi() {

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBodyChanged1 = null;
        MultipartBody.Part mMultipartBodyChanged2 = null;
        MultipartBody.Part mMultipartBodyChanged3 = null;
        MultipartBody.Part mMultipartBodyChanged4 = null;
        MultipartBody.Part mMultipartBodyChanged5 = null;
        MultipartBody.Part mMultipartBodyRepair1 = null;
        MultipartBody.Part mMultipartBodyRepair2 = null;
        MultipartBody.Part mMultipartBodyRepair3 = null;
        MultipartBody.Part mMultipartBodyRepair4 = null;
        MultipartBody.Part mMultipartBodyRepair5 = null;

        RequestBody requestFile = null;
        RequestBody requestFileC1 = null;
        RequestBody requestFileC2 = null;
        RequestBody requestFileC3 = null;
        RequestBody requestFileC4 = null;
        RequestBody requestFileC5 = null;
        RequestBody requestFileR1 = null;
        RequestBody requestFileR2 = null;
        RequestBody requestFileR3 = null;
        RequestBody requestFileR4 = null;
        RequestBody requestFileR5 = null;

        if (selfieImage != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(selfieImage));
            mMultipartBody1 = MultipartBody.Part.createFormData("selfie", getAlphaNumericString() + ".jpg", requestFile);
        }
        for (int i = 0; i < imagesChanges.size(); i++) {
            if(imagesChanges.size()>0) {
                if (imagesChanges.get(0) != null) {
                    Bitmap bitmap1 = BitmapFactory.decodeFile(imagesChanges.get(0).getPath());
                    requestFileC1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap1));
                    mMultipartBodyChanged1 = MultipartBody.Part.createFormData("parts_changed[0]", getAlphaNumericString() + ".jpg", requestFileC1);
                }
            }
            if(imagesChanges.size()>1) {
                if (imagesChanges.get(1) != null) {
                    Bitmap bitmap2 = BitmapFactory.decodeFile(imagesChanges.get(1).getPath());
                    requestFileC2 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap2));
                    mMultipartBodyChanged2 = MultipartBody.Part.createFormData("parts_changed[1]", getAlphaNumericString() + ".jpg", requestFileC2);
                }
            }
            if(imagesChanges.size()>2) {
                if (imagesChanges.get(2) != null) {
                    Bitmap bitmap3 = BitmapFactory.decodeFile(imagesChanges.get(2).getPath());
                    requestFileC3 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap3));
                    mMultipartBodyChanged3 = MultipartBody.Part.createFormData("parts_changed[2]", getAlphaNumericString() + ".jpg", requestFileC3);
                }
            }
//            if (imagesChanges.get(i) != null) {
//                Bitmap bitmap4 = BitmapFactory.decodeFile(imagesChanges.get(i).getFimeNmae());
//                requestFileC4 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap4));
//                mMultipartBodyChanged4 = MultipartBody.Part.createFormData("parts_changed[3] ", getAlphaNumericString() + ".jpg", requestFileC4);
//            }
//            if (imagesChanges.get(i) != null) {
//                Bitmap bitmap5 = BitmapFactory.decodeFile(imagesChanges.get(i).getFimeNmae());
//                requestFileC5 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap5));
//                mMultipartBodyChanged5 = MultipartBody.Part.createFormData("parts_changed[4] ", getAlphaNumericString() + ".jpg", requestFileC5);
//            }
        }

        for (int j = 0; j < imagesRepair.size(); j++) {
            if(imagesRepair.size()>0) {
                if (imagesRepair.get(0) != null) {
                    Bitmap bitmap6 = BitmapFactory.decodeFile(imagesRepair.get(0).getPath());
                    requestFileR1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap6));
                    mMultipartBodyRepair1 = MultipartBody.Part.createFormData("parts_repaired[0]", getAlphaNumericString() + ".jpg", requestFileR1);
                }
            }
            if(imagesRepair.size()>1) {
                if (imagesRepair.get(1) != null) {
                    Bitmap bitmap7 = BitmapFactory.decodeFile(imagesRepair.get(1).getPath());
                    requestFileR2 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap7));
                    mMultipartBodyRepair2 = MultipartBody.Part.createFormData("parts_repaired[1]", getAlphaNumericString() + ".jpg", requestFileR2);
                }
            }
            if(imagesRepair.size()>2) {
                if (imagesRepair.get(2) != null) {
                    Bitmap bitmap8 = BitmapFactory.decodeFile(imagesRepair.get(2).getPath());
                    requestFileR3 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap8));
                    mMultipartBodyRepair3 = MultipartBody.Part.createFormData("parts_repaired[2]", getAlphaNumericString() + ".jpg", requestFileR3);
                }
            }
//            if (imagesRepair.get(j) != null) {
//                Bitmap bitmap9 = BitmapFactory.decodeFile(imagesRepair.get(j).getFimeNmae());
//                requestFileR4 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap9));
//                mMultipartBodyRepair4 = MultipartBody.Part.createFormData("parts_repaired[3]", getAlphaNumericString() + ".jpg", requestFileR4);
//            }
//            if (imagesRepair.get(j) != null) {
//                Bitmap bitmap10 = BitmapFactory.decodeFile(imagesRepair.get(j).getFimeNmae());
//                requestFileR5 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap10));
//                mMultipartBodyRepair5 = MultipartBody.Part.createFormData("parts_repaired[4]", getAlphaNumericString() + ".jpg", requestFileR5);
//            }
        }
        RequestBody access_token = RequestBody.create(MediaType.parse("multipart/form-data"), getAccessToken());
        RequestBody serviceID = RequestBody.create(MediaType.parse("multipart/form-data"), serviceId);
        RequestBody complainID = RequestBody.create(MediaType.parse("multipart/form-data"), complainId);
        RequestBody lubrication = RequestBody.create(MediaType.parse("multipart/form-data"), strLubrication);
        RequestBody partChanged = RequestBody.create(MediaType.parse("multipart/form-data"), strPartsChange);
        RequestBody partRepair = RequestBody.create(MediaType.parse("multipart/form-data"), strPartsRepaired);
        RequestBody machineStatus = RequestBody.create(MediaType.parse("multipart/form-data"), strMachineStatus);
        RequestBody hourSpentt = RequestBody.create(MediaType.parse("multipart/form-data"), strHourSpent);
        RequestBody fullMachineChcek = RequestBody.create(MediaType.parse("multipart/form-data"), strFullMachineCheck);
        RequestBody otp = RequestBody.create(MediaType.parse("multipart/form-data"), "123456");
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.serviceComplain(access_token, serviceID, complainID, lubrication, partChanged, partRepair, machineStatus, hourSpentt, fullMachineChcek, otp, mMultipartBody1, mMultipartBodyChanged1, mMultipartBodyChanged2, mMultipartBodyChanged3, mMultipartBodyRepair1, mMultipartBodyRepair2, mMultipartBodyRepair3).enqueue(new Callback<CustomerReviewModel>() {
            @Override
            public void onResponse(Call<CustomerReviewModel> call, Response<CustomerReviewModel> response) {
                dismissProgressDialog();
                CustomerReviewModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CustomerReviewModel> call, Throwable t) {
                showAlertDialog(mActivity, t.toString());
                dismissProgressDialog();
            }
        });
    }


    private class AsyncTaskExample extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(String... strings) {
            if (isNetworkAvailable(mActivity)) {
                executeServiceComplaintApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void bitmap) {
            super.onPostExecute(bitmap);

        }
    }
}

