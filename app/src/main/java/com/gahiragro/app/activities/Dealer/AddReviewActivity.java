package com.gahiragro.app.activities.Dealer;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.VideoTrimActivity;
import com.gahiragro.app.model.CustomerReviewModel;
import com.gahiragro.app.model.GetRegisterProductModel;
import com.gahiragro.app.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReviewActivity extends BaseActivity {

    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    public int STATIC_INTEGER_VALUE = 12;
    /**
     * Getting the Current Class Name
     */
    String TAG = AddReviewActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddReviewActivity.this;

//    @BindView(R.id.imgCrossRL)
    RelativeLayout imgCrossRL;

//    @BindView(R.id.surfaceView1)
    VideoView surfaceView1;

//    @BindView(R.id.btnPostBT)
    Button btnPostBT;

//    @BindView(R.id.btnPostSelBT)
    Button btnPostSelBT;

//    @BindView(R.id.addedImageIV)
    ImageView addedImageIV;

//    @BindView(R.id.attahmentRL)
    RelativeLayout attahmentRL;

//    @BindView(R.id.messageET)
    EditText messageET;

//    @BindView(R.id.userNameTV)
    TextView userNameTV;

//    @BindView(R.id.imageprofileIV)
    ImageView imageprofileIV;

//    @BindView(R.id.modelNameTV)
    TextView modelNameTV;

//    @BindView(R.id.prodNameTV)
    TextView prodNameTV;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.playIV)
    ImageView playIV;

    String currentPhotoPath = "";
    String clickType = "";
    String fileName;
    String productId = "";
    String imageType = "";
    String selectedVideo = "";

    Bitmap selectedImage;
    Bitmap rotatedBitmap = null;
    BottomSheetDialog bottomSheetDialog;

    // url = file path or whatever suitable URL you want.
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);

        initViews();
        getIntentData();
        //  apply click listener
        OnClick();
     onViewClicked();
    }

    private void initViews() {
        imgCrossRL =findViewById(R.id.imgCrossRL);
        addedImageIV =findViewById(R.id.addedImageIV);
        attahmentRL =findViewById(R.id.attahmentRL);
        surfaceView1 =findViewById(R.id.surfaceView1);
        btnPostBT =findViewById(R.id.btnPostBT);
        btnPostSelBT =findViewById(R.id.btnPostSelBT);
        playIV =findViewById(R.id.playIV);
        productIV =findViewById(R.id.productIV);
        prodNameTV =findViewById(R.id.prodNameTV);
        modelNameTV =findViewById(R.id.modelNameTV);
        imageprofileIV =findViewById(R.id.imageprofileIV);
        userNameTV =findViewById(R.id.userNameTV);
        messageET =findViewById(R.id.messageET);
    }

    private void getIntentData() {
        userNameTV.setText(getUserName());
        Glide.with(mActivity).load(getUserProfilePic()).
                placeholder(R.drawable.gahir_logo_dummy).
                into(imageprofileIV);

        if (getIntent() != null) {
            GetRegisterProductModel.AllProduct myModel = (GetRegisterProductModel.AllProduct) getIntent().getSerializableExtra(Constants.model);
            productId = myModel.getId();
            prodNameTV.setText(myModel.getProdName());
            modelNameTV.setText(myModel.getProdModel());
            Glide.with(mActivity).load(myModel.getProdImage()).
                    placeholder(R.drawable.gahir_logo_dummy).
                    into(productIV);

        }

    }

//    @OnClick({R.id.btnPostSelBT, R.id.attahmentRL})
    public void onViewClicked(/*View view*/) {
        attahmentRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet();
            }
        });

        btnPostSelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventMultipleClick();
                setPostClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.attahmentRL:
                showBottomSheet();
                break;
            case R.id.btnPostSelBT:
                preventMultipleClick();
                setPostClick();
                break;
        }*/
    }

    private void setPostClick() {
        if (isValidate()) {
            mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    new AsyncTaskExample().execute();
                }
            });
        }
    }

    private void showBottomSheet() {
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.bottom_header);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView imgClearIV = bottomSheetDialog.findViewById(R.id.imgClearIV);
        LinearLayout camLL = bottomSheetDialog.findViewById(R.id.camLL);
        LinearLayout photoLL = bottomSheetDialog.findViewById(R.id.photoLL);
        imgClearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
        camLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                surfaceView1.setVisibility(View.GONE);
                addedImageIV.setVisibility(View.GONE);
                playIV.setVisibility(View.GONE);
                clickType = "camera";
                onCameraClick();
                bottomSheetDialog.dismiss();
            }
        });
        photoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                surfaceView1.setVisibility(View.GONE);
                addedImageIV.setVisibility(View.GONE);
                playIV.setVisibility(View.GONE);
                clickType = "video";
                onCameraClick();
                bottomSheetDialog.dismiss();
            }
        });
        bottomSheetDialog.show();

    }

    private void OnClick() {


        imgCrossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void onCameraClick() {
        if (checkPermission()) {
            if (clickType.equals("camera")) {
                openCamera();
            } else {
                openGallery();
            }

        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, writeCamera}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    // onSelectImageClick();
                    if (clickType.equals("camera")) {
                        openCamera();
                    } else {
                        openGallery();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getImageFile(); // 1
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
            uri = FileProvider.getUriForFile(mActivity, "com.gahiragro.app", file);
        else
            uri = Uri.fromFile(file); // 3
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
        if (pictureIntent != null) {
            startActivityForResult(pictureIntent, 0);

        }
    }

    private File getImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        File file = null;
        try {
            file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file != null && file.getAbsolutePath() != null) {
            currentPhotoPath = "file:" + file.getAbsolutePath();
        }
        return file;
    }

    private void openGallery() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/* video/*");
        //  pictureIntent.setAction(Intent.ACTION_PICK);

        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);  // 2
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png", "video/mp4"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        if (pictureIntent != null) {
            startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), 1);  // 4
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(currentPhotoPath);
            if (uri != null) {
                openCropActivity(uri, uri);
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = UCrop.getOutput(data);
            showImage(uri);
        } else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Uri sourceUri = data.getData(); // 1
            File file = getImageFile(); // 2
            Uri destinationUri = Uri.fromFile(file);  // 3
            if (data != null) {
                fileName = getRealPathFromURI_API19(mActivity, sourceUri);
                String strFileName = getMimeType(fileName);
                if (strFileName != null) {
                    if (strFileName.equals("video/mp4")) {
                        Intent intent = new Intent(this, VideoTrimActivity.class);
                        intent.putExtra(Constants.VIDEO_URI, fileName);
                        startActivityForResult(intent, 890);
                    } else {
                        //showImage(sourceUri);
                        openCropActivity(sourceUri, destinationUri);
                    }
                }

            }
        } else if (requestCode == 890 && data != null) {
            //handle video
            imageType = "video";
            addedImageIV.setVisibility(View.VISIBLE);
            playIV.setVisibility(View.VISIBLE);

            btnPostSelBT.setVisibility(View.VISIBLE);
            btnPostBT.setVisibility(View.GONE);
            String mUri = data.getStringExtra(Constants.VIDEO_URI);
            fileName = mUri.toString();
            //Creating MediaController
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(surfaceView1);
            playIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    surfaceView1.setVisibility(View.VISIBLE);
                    addedImageIV.setVisibility(View.GONE);
                    playIV.setVisibility(View.GONE);
                    //Setting MediaController and URI, then starting the videoView
                    surfaceView1.setMediaController(mediaController);
                    surfaceView1.setVideoURI(Uri.parse(mUri));
                    surfaceView1.requestFocus();
                    surfaceView1.start();
                }
            });
            Bitmap videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(mUri, MediaStore.Video.Thumbnails.MINI_KIND);
            addedImageIV.setImageBitmap(videoThumbnail_bitmap);
        }
    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(1920, 1080)
                .withOptions(options)
                .start(mActivity);
    }

    private void showImage(Uri imageUri) {
        addedImageIV.setVisibility(View.VISIBLE);
        Log.e(TAG, "IMAGEVALUE::" + imageUri);
        fileName = getRealPathFromURI_API19(mActivity, imageUri);
        Log.e(TAG, "PATH::" + fileName);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "EXCEPTION::" + e.toString());
            e.printStackTrace();
        }

        String hu = getMimeType(fileName);
        if (hu.equals("video/mp4")) {
            imageType = "video";
            playIV.setVisibility(View.VISIBLE);
        } else {
            imageType = "image";
            playIV.setVisibility(View.GONE);
        }
        if (inputStream != null) {

            btnPostSelBT.setVisibility(View.VISIBLE);
            btnPostBT.setVisibility(View.GONE);

            selectedImage = BitmapFactory.decodeStream(inputStream);
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:

                    rotatedBitmap = rotateImage(selectedImage, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(selectedImage, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(selectedImage, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = selectedImage;
            }

            if (hu.equals("video/mp4")) {
            } else {
                addedImageIV.setImageBitmap(rotatedBitmap);
            }
        }

    }

    private void executeAddPostApi() {
        // showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        RequestBody requestFile = null;
        if (rotatedBitmap != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(rotatedBitmap));
        }
        if (fileName != null && !fileName.equals("")) {
            File propertyImageFile = new File(fileName);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), propertyImageFile);
            if (imageType.equals("video")) {
                mMultipartBody1 = MultipartBody.Part.createFormData("image", propertyImageFile.getName(), videoBody);
            } else {
                mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpg", requestFile);
            }
        }

        RequestBody access_token = RequestBody.create(MediaType.parse("multipart/form-data"), getAccessToken());
        RequestBody messageType = RequestBody.create(MediaType.parse("multipart/form-data"), messageET.getText().toString().trim());
        RequestBody prodId = RequestBody.create(MediaType.parse("multipart/form-data"), productId);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        Log.e(TAG, "**PARAMS**" + "ACCESS_TOKEN=" + getAccessToken() + "MESSAGE=" + messageET.getText().toString().trim() + "PRODUCT_ID=" + productId);
        mApiInterface.addCustomerReview(access_token, messageType, prodId, mMultipartBody1).enqueue(new Callback<CustomerReviewModel>() {

            @Override
            public void onResponse(Call<CustomerReviewModel> call, Response<CustomerReviewModel> response) {
                dismissProgressDialog();
                CustomerReviewModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Constants.FILTER_BACK_PRESSED = true;
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CustomerReviewModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (messageET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_message));
            flag = false;
        }
        return flag;
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }


    private class AsyncTaskExample extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(String... strings) {
            if (isNetworkAvailable(mActivity)) {
                executeAddPostApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void bitmap) {
            super.onPostExecute(bitmap);

        }
    }

    private String getRealPathFromURI(Activity context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e(TAG, "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}