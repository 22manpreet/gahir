package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.DeleteItemInterface;
import com.gahiragro.app.activities.interfaces.DeletePrtInterface;
import com.gahiragro.app.adapters.Dealer.SelectPartsAdapter;
import com.gahiragro.app.model.AddPartEnquiryModel;
import com.gahiragro.app.model.AllcartItemsModel;
import com.gahiragro.app.model.RemoveCartModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectedPartsActivity extends BaseActivity implements DeletePrtInterface, DeleteItemInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = SelectedPartsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SelectedPartsActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.selectedPartsRV)
    RecyclerView selectedPartsRV;

//    @BindView(R.id.btnEnquiryBT)
    Button btnEnquiryBT;

//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;
    SelectPartsAdapter selectPartsAdapter;
    List<AllcartItemsModel.CartItem> mlist = new ArrayList<>();
    private long mLastClickTab1 = 0;
    DeletePrtInterface mDeletePrtInterface;
    DeleteItemInterface mDeleteItemInterface;
    String strCartId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_parts);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL =findViewById(R.id.imgMenuRL);
        selectedPartsRV =findViewById(R.id.selectedPartsRV);
        btnEnquiryBT =findViewById(R.id.btnEnquiryBT);
        noDataFoundTV =findViewById(R.id.noDataFoundTV);

        mDeletePrtInterface = this;
        mDeleteItemInterface = this;
        if (isNetworkAvailable(mActivity)) {
            executeGetCartItemsApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
        Onclick();

    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnEnquiryBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
                    return;
                }
                mLastClickTab1 = SystemClock.elapsedRealtime();
                executeAddPartEnquiryApi();

            }
        });

    }


    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetCartItemsApi() {
        mlist.clear();
        showProgressDialog(mActivity);
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllCartItems(mPram()).enqueue(new Callback<AllcartItemsModel>() {
            @Override
            public void onResponse(Call<AllcartItemsModel> call, Response<AllcartItemsModel> response) {
                AllcartItemsModel mModel = response.body();
                dismissProgressDialog();
                if (mModel.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    btnEnquiryBT.setVisibility(View.VISIBLE);
                    mlist.addAll(mModel.getCartItems());
                    initRV();
                } else {
                    btnEnquiryBT.setVisibility(View.GONE);
                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AllcartItemsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        selectedPartsRV.setLayoutManager(layoutManager);
        selectPartsAdapter = new SelectPartsAdapter(mlist, this, mDeletePrtInterface);
        selectedPartsRV.setAdapter(selectPartsAdapter);
    }

    private Map<String, String> mPrams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddPartEnquiryApi() {
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.addPartEnquiry(mPrams()).enqueue(new Callback<AddPartEnquiryModel>() {
            @Override
            public void onResponse(Call<AddPartEnquiryModel> call, Response<AddPartEnquiryModel> response) {
                AddPartEnquiryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showFinishAlertDialog(mActivity, mModel.getMessage());
                } else {
                }
            }

            @Override
            public void onFailure(Call<AddPartEnquiryModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    @Override
    public void onDelete(int position, String cartId) {
        strCartId = cartId;
        mDeleteItemInterface.onConfirm();

    }

    private Map<String, String> mDeletePram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("items", strCartId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDeleteCartItemsApi() {
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.removeCartItem(mDeletePram()).enqueue(new Callback<RemoveCartModel>() {
            @Override
            public void onResponse(Call<RemoveCartModel> call, Response<RemoveCartModel> response) {
                RemoveCartModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    executeGetCartItemsApi();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RemoveCartModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    @Override
    public void onConfirm() {
        executeDeleteCartItemsApi();
    }
}