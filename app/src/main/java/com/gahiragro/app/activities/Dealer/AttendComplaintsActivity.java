package com.gahiragro.app.activities.Dealer;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.RemoveImageInterface;
import com.gahiragro.app.adapters.Dealer.UploadAdapter;
import com.gahiragro.app.adapters.Dealer.UploadRepairedAdapter;
import com.gahiragro.app.model.ImagesModel;
import com.gahiragro.app.model.ServiceComplaintModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.gson.Gson;
import com.lassi.common.utils.KeyUtils;
import com.lassi.data.media.MiMedia;
import com.lassi.domain.media.LassiOption;
import com.lassi.domain.media.MediaType;
import com.lassi.presentation.builder.Lassi;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class AttendComplaintsActivity extends BaseActivity {
    private static final int REQUEST_CODE = 732;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    UploadAdapter uploadAdapter;
    UploadRepairedAdapter uploadRepairedAdapter;
    /**
     * Getting the Current Class Name
     */
    String TAG = AttendComplaintsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AttendComplaintsActivity.this;

    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    //    @BindView(R.id.houtET)
    EditText houtET;

    //    @BindView(R.id.complaintsTypeRL)
    RelativeLayout complaintsTypeRL;

    //    @BindView(R.id.complaintsTypeSpinner)
    Spinner complaintsTypeSpinner;

    //    @BindView(R.id.lubricationSpinner)
    Spinner lubricationSpinner;

    //    @BindView(R.id.machineRunningSpinner)
    Spinner machineRunningSpinner;

    //    @BindView(R.id.btnApprovalBT)
    Button btnApprovalBT;

    //    @BindView(R.id.hourSpinner)
    Spinner hourSpinner;

    //    @BindView(R.id.radioGroupPC)
    RadioGroup radioGroupPC;


    //    @BindView(R.id.radioGroupPR)
    RadioGroup radioGroupPR;

    //    @BindView(R.id.yesRadioBt)
    RadioButton yesRadioBt;

    //    @BindView(R.id.noRadioBt)
    RadioButton noRadioBt;

    //    @BindView(R.id.yesPartRadioBt)
    RadioButton yesPartRadioBt;

    //    @BindView(R.id.noPartRadioBt)
    RadioButton noPartRadioBt;

    //    @BindView(R.id.customerMobileNumET)
    EditText customerMobileNumET;

    //    @BindView(R.id.productET)
    EditText productET;

    //    @BindView(R.id.uploadRV)
    RecyclerView uploadRV;

    //    @BindView(R.id.srNumET)
    EditText srNumET;

    //    @BindView(R.id.complaintTypeET)
    EditText complaintTypeET;

    //    @BindView(R.id.uplaodImageIV)
    ImageView uplaodImageIV;

    //    @BindView(R.id.uploadingRV)
    RecyclerView uploadingRV;

    //    @BindView(R.id.uploadRepairedIV)
    ImageView uploadRepairedIV;

    //    @BindView(R.id.fullMachineCheckIV)
    ImageView fullMachineCheckIV;

    String firstClick = "";
    String currentPhotoPath = "";
    ArrayList<MiMedia> mPartsChangedImagesAL = new ArrayList<>();
    ArrayList<MiMedia> mPartsRepairedImagesAL = new ArrayList<>();
    Bitmap selectedImage;
    String clickType = "";
    String partsChanged = "1";
    String partsRepair = "1";
    String strLubricationValue = "Yes";
    String strMachineStatusValue = "1";
    String strFullMachineStatus = "";
    private ArrayList<String> mArrayList = new ArrayList<>();
    ArrayList<String> mlistLub = new ArrayList<String>();
    ServiceComplaintModel.ComplainList.AllComplain mModel;


    RemoveImageInterface mRemoveImageInterface = new RemoveImageInterface() {
        @Override
        public void onRemoveClick(int pos, String type) {
            if (type.equals("repairedImages")) {
                mPartsRepairedImagesAL.remove(pos);
                uploadRepairedAdapter.notifyDataSetChanged();
            } else {
                mPartsChangedImagesAL.remove(pos);
                uploadAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend_complaints);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);

        initViews();
        Onclick();
        getIntentData();
        bgChecked();
        //spinner select dropdown list show
        selectLubricationSpinner();
        selectMachineRunningSpinner();
        selectHoursSpinner();
    }


    private void initViews() {
        productET = findViewById(R.id.productET);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        fullMachineCheckIV = findViewById(R.id.fullMachineCheckIV);
        uploadRepairedIV = findViewById(R.id.uploadRepairedIV);
        uploadingRV = findViewById(R.id.uploadingRV);
        uplaodImageIV = findViewById(R.id.uplaodImageIV);
        complaintTypeET = findViewById(R.id.complaintTypeET);
        srNumET = findViewById(R.id.srNumET);
        uploadRV = findViewById(R.id.uploadRV);
        customerMobileNumET = findViewById(R.id.customerMobileNumET);
        noPartRadioBt = findViewById(R.id.noPartRadioBt);
        yesPartRadioBt = findViewById(R.id.yesPartRadioBt);
        noRadioBt = findViewById(R.id.noRadioBt);
        yesRadioBt = findViewById(R.id.yesRadioBt);
        radioGroupPR = findViewById(R.id.radioGroupPR);
        radioGroupPC = findViewById(R.id.radioGroupPC);
        hourSpinner = findViewById(R.id.hourSpinner);
        btnApprovalBT = findViewById(R.id.btnApprovalBT);
        machineRunningSpinner = findViewById(R.id.machineRunningSpinner);
        lubricationSpinner = findViewById(R.id.lubricationSpinner);
        complaintsTypeSpinner = findViewById(R.id.complaintsTypeSpinner);
        complaintsTypeRL = findViewById(R.id.complaintsTypeRL);
        houtET = findViewById(R.id.houtET);
    }


    private void getIntentData() {
        if (getIntent() != null) {
            mModel = (ServiceComplaintModel.ComplainList.AllComplain) getIntent().getSerializableExtra(Constants.model);
            customerMobileNumET.setText(mModel.getComplainDetail().getContactNo());
            productET.setText(mModel.getProduct_name());
            srNumET.setText(mModel.getComplainDetail().getProdSrNo());
            complaintTypeET.setText(mModel.getComplainDetail().getCompReason());
        }
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnApprovalBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    Intent intent = new Intent(AttendComplaintsActivity.this, OtpVerficationActivity.class);
                    intent.putExtra(Constants.PHONE_NUMBER, customerMobileNumET.getText().toString().trim());
                    intent.putExtra(Constants.PARTS_CHANGED_IMAGES, mPartsChangedImagesAL);
                    intent.putExtra(Constants.PARTS_REPAIR_IMAGES, mPartsRepairedImagesAL);
                    GahirPreference.writeString(mActivity, Constants.PHONE_NUMBER, customerMobileNumET.getText().toString().trim());
                    GahirPreference.writeString(mActivity, Constants.LUBRICATION_STATUS, strLubricationValue);
                    GahirPreference.writeString(mActivity, Constants.COMPLAIN_ID, mModel.getComplainId());
                    GahirPreference.writeString(mActivity, Constants.SERVICE_ID, mModel.getServiceId());
                    GahirPreference.writeString(mActivity, Constants.PARTS_CHANGED, partsChanged);
                    GahirPreference.writeString(mActivity, Constants.PARTS_REPAIR, partsRepair);
                    GahirPreference.writeString(mActivity, Constants.MACHINE_STATUS, strMachineStatusValue);
                    GahirPreference.writeString(mActivity, Constants.HOUR_SPENT, houtET.getText().toString().trim());
                    GahirPreference.writeString(mActivity, Constants.FULL_MACHINE_CHECK, strFullMachineStatus);
                    startActivity(intent);
                    finish();
                }
            }
        });

//        radioGroupPC.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!false) {
//                    yesRadioBt.setChecked(true);
//                    noRadioBt.setChecked(false);
//                    partsChanged = "1";     //yes
//                } else {
//                    noRadioBt.setChecked(true);
//                    yesRadioBt.setChecked(false);
//                    partsChanged = "0";      //no
//                }
//            }
//        });
        yesRadioBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yesRadioBt.setChecked(true);
                noRadioBt.setChecked(false);
                partsChanged = "1";
            }
        });
        noRadioBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noRadioBt.setChecked(true);
                yesRadioBt.setChecked(false);
                partsChanged = "0";
                if (mPartsChangedImagesAL.size() > 0) {
                    mPartsChangedImagesAL.clear();
                    uploadAdapter.notifyDataSetChanged();
                }

            }
        });
//        radioGroupPR.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (!false) {
//                    yesPartRadioBt.setChecked(true);
//                    noPartRadioBt.setChecked(false);
//                    partsRepair = "1";       //yes
//                } else {
//                    noPartRadioBt.setChecked(true);
//                    yesPartRadioBt.setChecked(false);
//                    partsRepair = "0";       //no
//
//                }
//            }
//        });
        yesPartRadioBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yesPartRadioBt.setChecked(true);
                noPartRadioBt.setChecked(false);
                partsRepair = "1";
            }
        });
        noPartRadioBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noPartRadioBt.setChecked(true);
                yesPartRadioBt.setChecked(false);
                partsRepair = "0";
                if (mPartsRepairedImagesAL.size() > 0) {
                    mPartsRepairedImagesAL.clear();
                    uploadRepairedAdapter.notifyDataSetChanged();

                }

            }
        });
        uplaodImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPartsChangedImagesAL.size() < 3) {
                    clickType = "uploadImage";
                    if (yesRadioBt.isChecked()) {
                        onCameraClick();
                    }
                }
            }
        });
        uploadRepairedIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPartsRepairedImagesAL.size() < 3) {
                    clickType = "repairImage";
                    if (yesPartRadioBt.isChecked()) {
                        onCameraClick();
                    }
                }

            }
        });
        fullMachineCheckIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstClick.equals("true")) {
                    firstClick = "false";
                    fullMachineCheckIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_green));
                    strFullMachineStatus = "1";
                } else {
                    firstClick = "true";
                    fullMachineCheckIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck_green));
                    strFullMachineStatus = "0";
                }
            }
        });
    }

    private void onCameraClick() {

        if (checkPermission()) {
            //  onSelectImageClick();
            openCameraGalleryDialog();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        } else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, writeCamera}, 369);
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        }
    }

    private void openCameraGalleryDialog() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    openCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    performMultiImgClick();
                    //openGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void performMultiImgClick() {


        Intent intent = new Lassi(AttendComplaintsActivity.this)
                .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA, GALLERY or CAMERA_AND_GALLERY
                .setMaxCount(3)
                .setGridSize(3)
                .setMediaType(MediaType.IMAGE) // MediaType : VIDEO IMAGE, AUDIO OR DOC
                .build();
        if (clickType.equals("uploadImage")) {
            if (mPartsChangedImagesAL.size() == 0) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3);
            } else if (mPartsChangedImagesAL.size() == 1) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2);
            } else if (mPartsChangedImagesAL.size() == 2) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
            }

        } else if (clickType.equals("repairImage")) {
            if (mPartsRepairedImagesAL.size() == 0) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3);
            } else if (mPartsRepairedImagesAL.size() == 1) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2);
            } else if (mPartsRepairedImagesAL.size() == 2) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
            }
        }

        receiveData.launch(intent);


       /* // start multiple photos selector
        Intent intent = new Intent(AttendComplaintsActivity.this, ImagesSelectorActivity.class);
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
        // max number of images to be selected
        if (clickType.equals("uploadImage")) {
            if (mPartsChangedImagesAL.size() == 0) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3);
            } else if (mPartsChangedImagesAL.size() == 1) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2);
            } else if (mPartsChangedImagesAL.size() == 2) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
            }

        } else if (clickType.equals("repairImage")) {
            if (mPartsRepairedImagesAL.size() == 0) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3);
            } else if (mPartsRepairedImagesAL.size() == 1) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2);
            } else if (mPartsRepairedImagesAL.size() == 2) {
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
            }
        }

        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100);
        // pass current selected images as the initial value
        // intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mArrayList);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE);*/
    }

    private ArrayList<MiMedia> imageListInUri = new ArrayList<>();
    int imageCount = 3;

    private ActivityResultLauncher<Intent> receiveData = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK) {
            Intent data = result.getData();
            if (data != null) {
                if (clickType.equals("uploadImage")) {
                    ArrayList<MiMedia> selectedMedia = (ArrayList<MiMedia>) data.getSerializableExtra(KeyUtils.SELECTED_MEDIA);
                    if (selectedMedia != null && !selectedMedia.isEmpty()) {
                        mPartsChangedImagesAL.addAll(selectedMedia);
                        imageCount -= selectedMedia.size();
                        initRV();
                    }
                } else {
                    ArrayList<MiMedia> selectedMedia = (ArrayList<MiMedia>) data.getSerializableExtra(KeyUtils.SELECTED_MEDIA);
                    if (selectedMedia != null && !selectedMedia.isEmpty()) {
                        mPartsRepairedImagesAL.addAll(selectedMedia);
                        imageCount -= selectedMedia.size();
                        initRVTwo();
                    }
                }
            }
        }
    });


    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getImageFile(); // 1
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
            uri = FileProvider.getUriForFile(mActivity, "com.gahiragro.app", file);
        else
            uri = Uri.fromFile(file); // 3
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
        if (pictureIntent != null) {
            startActivityForResult(pictureIntent, 0);
        }
    }

    private File getImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File file = null;
        try {
            file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }

    private void openGallery() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");  // 1
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);  // 2
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};  // 3
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        if (pictureIntent != null) {
            startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), 1);  // 4
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(currentPhotoPath);
            if (uri != null) {
                showImage(uri);
            }
        } else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Uri sourceUri = data.getData(); // 1
            if (data != null) {
                showImage(sourceUri);
            }
        } /*else if (requestCode == REQUEST_CODE) {
            if (data != null) {
                mArrayList = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mArrayList != null;
                if (clickType.equals("uploadImage")) {
                    for (int i = 0; i < mArrayList.size(); i++) {
                        MiMedia mModel = new MiMedia();
//                        mModel.setFimeNmae(mArrayList.get(i));
                        mPartsChangedImagesAL.add(mModel);
                    }
                    initRV();
                } else {
                    for (int i = 0; i < mArrayList.size(); i++) {
                        ImagesModel mModel = new ImagesModel();
                        mModel.setFimeNmae(mArrayList.get(i));
                        mPartsRepairedImagesAL.add(mModel);
                    }
                    initRVTwo();
                }
            }
        }*/
    }

    private void showImage(Uri imageUri) {

        Log.e(TAG, "IMAGEVALUE::" + imageUri);
        String fileName = getRealPathFromURI_API19(mActivity, imageUri);
        Log.e(TAG, "PATH::" + fileName);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "EXCEPTION::" + e.toString());
            e.printStackTrace();
        }

        if (inputStream != null) {
            selectedImage = BitmapFactory.decodeStream(inputStream);
            if (clickType.equals("uploadImage")) {
                MiMedia mImagesModel = new MiMedia();
                mImagesModel.setPath(fileName);
//                mPartsChangedImagesAL.add(mImagesModel);
                initRV();
            } else {
                MiMedia mImagesModel = new MiMedia();
                mImagesModel.setPath(fileName);
                mPartsRepairedImagesAL.add(mImagesModel);
                initRVTwo();
            }


        }
    }

    void bgChecked() {
        if (customerMobileNumET.getText().toString().trim().length() < 0) {
            customerMobileNumET.setBackground(getResources().getDrawable(R.drawable.bg_open_red));
            productET.requestFocus();

        } else if (productET.getText().toString().trim().length() < 0) {
            productET.setBackground(getResources().getDrawable(R.drawable.bg_open_red));
        }
    }

    private void selectLubricationSpinner() {
        mlistLub.clear();
        addLubriData();
        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "Poppins-Regular.otf");


        ArrayAdapter<String> mColorAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mlistLub) {
//            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
//                View v = convertView;
//                if (v == null) {
//                    Context mContext = this.getContext();
//                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    assert vi != null;
//                    v = vi.inflate(R.layout.item_spinner, null);
//                }
//
//                TextView itemTextView = v.findViewById(R.id.itemTV);
////                strLubricationValue = mlistLub.get(position);
////                itemTextView.setText(strLubricationValue);
//                itemTextView.setTypeface(font);
//
//                return v;
//            }
        };
        mColorAd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        lubricationSpinner.setAdapter(mColorAd);
        lubricationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);
                strLubricationValue = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    void addLubriData() {
        mlistLub.add("yes");
        mlistLub.add("no");
    }

    private void selectMachineRunningSpinner() {
        mlistLub.clear();

        addLubriData();

        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "Poppins-Regular.otf");

        ArrayAdapter<String> mColorAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mlistLub) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTV);
                strMachineStatusValue = mlistLub.get(position);
                itemTextView.setText(strMachineStatusValue);
                itemTextView.setTypeface(font);

                return v;
            }
        };

        machineRunningSpinner.setAdapter(mColorAd);
        machineRunningSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void selectHoursSpinner() {
        mlistLub.clear();

        addHourData();

        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "Poppins-Regular.otf");


        ArrayAdapter<String> mColorAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mlistLub) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mlistLub.get(position);
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);

                return v;
            }
        };

        hourSpinner.setAdapter(mColorAd);
        hourSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        uploadRV.setLayoutManager(layoutManager);
        uploadAdapter = new UploadAdapter(mPartsChangedImagesAL, this, mRemoveImageInterface);
        uploadRV.setAdapter(uploadAdapter);
    }

    void initRVTwo() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        uploadingRV.setLayoutManager(layoutManager);
        uploadRepairedAdapter = new UploadRepairedAdapter(mPartsRepairedImagesAL, this, mRemoveImageInterface);
        uploadingRV.setAdapter(uploadRepairedAdapter);
    }

    void addHourData() {
        mlistLub.add("yes");
        mlistLub.add("no");
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (houtET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_hours));
            flag = false;
        }
        return flag;
    }
}