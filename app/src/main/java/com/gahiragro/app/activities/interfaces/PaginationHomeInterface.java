package com.gahiragro.app.activities.interfaces;

public interface PaginationHomeInterface {
    public void mHomePagination(boolean isLastScrolled);
}
