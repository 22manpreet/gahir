package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.HomeActivity;
import com.gahiragro.app.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class RegisterCongratulationActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RegisterCongratulationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = RegisterCongratulationActivity.this;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.titleNameTV)
    TextView titleNameTV;


//    @BindView(R.id.productIV)
    ImageView productIV;


    String prodImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_congratulation);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        titleNameTV=findViewById(R.id.titleNameTV);
        productIV=findViewById(R.id.productIV);

        OnClick();
        titleNameTV.setText('"' + "Congratulation" + '"');
        getIntentData();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.putExtra(Constants.class_type, "RegisterCongratulationScreen");
                startActivity(intent);
            }
        }, 2000);

    }

    private void getIntentData() {
        if (getIntent() != null) {
            prodImage = getIntent().getStringExtra(Constants.SEL_PRODUCT_IMAGE);
            Glide.with(mActivity).load(prodImage).into(productIV);

        }
    }

    private void OnClick() {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


}