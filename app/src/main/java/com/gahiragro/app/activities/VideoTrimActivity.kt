package com.gahiragro.app.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.gahiragro.app.R
import com.gahiragro.app.utils.Constants
import life.knowledge4.videotrimmer.K4LVideoTrimmer
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener
import java.io.File

class VideoTrimActivity : BaseActivity() {
    var timeLine: K4LVideoTrimmer? = null
    var butonSave: Button? = null

    /**
     * set Activity
     */
    var mActivity: Activity = this

    /**
     * set Activity TAG
     */
    var TAG: String = this.javaClass.getSimpleName()

    companion object {
        public lateinit var newTrimActivity: VideoTrimActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_trim)

        newTrimActivity = this;
        setStatusBar(mActivity,Color.WHITE)
        setUpViews()
    }

    private fun setUpViews() {
        timeLine = findViewById<View>(R.id.timeLine) as K4LVideoTrimmer
        timeLine!!.setMaxDuration(30)
//        timeLine!!.setDestinationPath("/storage/emulated/0/DCIM/CameraCustom/");

        /*Retrive Data from Privious Activity*/
        if (intent != null) {
            if (intent.getStringExtra(Constants.VIDEO_URI) != null) {
                val strFilePath = intent.getStringExtra(Constants.VIDEO_URI)
                if (timeLine != null) {
                    timeLine!!.setVideoURI(Uri.parse(strFilePath))
                }
            }
        }
        val outDir = File(externalCacheDir, "")
        timeLine!!.setDestinationPath(outDir.toString())
        timeLine!!.setOnTrimVideoListener(object : OnTrimVideoListener {
            override fun getResult(uri: Uri) {
                val strURI = uri.toString()
                Log.e("test", "URITEST====$strURI")
                //var returnIntent = Intent(this, VideoReviewActivity::class.java)
                setData(uri.toString())
                /* setResult(201, returnIntent)
                 finish()*/
            }

            override fun cancelAction() {
                finish()
            }
        })
    }

    fun setData(uri: String) {
        val intent = Intent()
        intent.putExtra(Constants.VIDEO_URI, uri.toString())
        setResult(890, intent)
        finish()
    }

    override fun onBackPressed() {
        finish()
    }
}
