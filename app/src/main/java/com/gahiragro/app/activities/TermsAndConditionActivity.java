package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.fragments.PrivacyPolicyFragment;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class TermsAndConditionActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TermsAndConditionActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
//    @BindView(R.id.webView)
    WebView webView;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
//        ButterKnife.bind(this);
        webView=findViewById(R.id.webView);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.loadUrl(Constants.TERMS_CONDITION);

        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

       /* switch (view.getId()) {

            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
}
