package com.gahiragro.app.activities.interfaces;

public interface GuestLoginInterface {
    public void onGuestLogin();
}
