package com.gahiragro.app.activities.interfaces;

import java.util.List;

public interface FilterReviewStateInterface {

    public  void onFilterReview(int position, List<String> mNewList,String stateIds);

}
