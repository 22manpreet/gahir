package com.gahiragro.app.activities;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.AdminLoginModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminSignInActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AdminSignInActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AdminSignInActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.editEmailET)
    EditText editEmailET;
//    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
//    @BindView(R.id.txtForgotPwdTV)
    TextView txtForgotPwdTV;
//    @BindView(R.id.btnLoginB)
    Button btnLoginB;
//    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
//    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
//    @BindView(R.id.passwordRL)
    RelativeLayout passwordRL;
    //INitialize
    String strPushToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_sign_in);
//        ButterKnife.bind(this);
        initView();
        editTextSelector(editEmailET, emailRL, "");
        editTextSelector(editPasswordET, passwordRL, "");
        getPushToken();

        onViewClicked();
    }

    private void initView() {
        editEmailET =findViewById(R.id.editEmailET);
        editPasswordET =findViewById(R.id.editPasswordET);
        txtForgotPwdTV =findViewById(R.id.txtForgotPwdTV);
        btnLoginB =findViewById(R.id.btnLoginB);
        txtSignUpTV =findViewById(R.id.txtSignUpTV);
        emailRL =findViewById(R.id.emailRL);
        passwordRL =findViewById(R.id.passwordRL);
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.txtForgotPwdTV, R.id.txtSignUpTV, R.id.btnLoginB})
    public void onViewClicked(/*View view*/) {
        txtSignUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignUpClick();
            }
        });

        btnLoginB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getLoginRoleType().equals("Sales Executive")) {
                    performSalesExecutiveClick();
                } else {
                    performLoginClick();
                }
            }
        });

        txtForgotPwdTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performForgotPwdClick();
            }
        });


      /*  switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.btnLoginB:
                if (getLoginRoleType().equals("Sales Executive")) {
                    performSalesExecutiveClick();
                } else {
                    performLoginClick();
                }
                break;
            case R.id.txtForgotPwdTV:
                performForgotPwdClick();
                break;
        }*/
    }
//great
    private void performSalesExecutiveClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity)) {
                executeSalesLoginApi();
            } else
                showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void performSignUpClick() {
        Intent i = new Intent(mActivity, SignUpFirstScreenActivity.class);
        startActivity(i);
        //finish();
    }


    private void performLoginClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity)) {
                executeAdminLoginApi();
            } else
                showToast(mActivity, getString(R.string.internet_connection_error));

    }

    private void performForgotPwdClick() {
        Intent i = new Intent(mActivity, ForgotPasswordActivity.class);
        startActivity(i);
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }

        return flag;
    }

    /*
     * Phone Login Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAdminLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.adminLogin(mParam()).enqueue(new Callback<AdminLoginModel>() {
            @Override
            public void onResponse(Call<AdminLoginModel> call, Response<AdminLoginModel> response) {
                dismissProgressDialog();
                AdminLoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Intent i = new Intent(mActivity, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finishAffinity();
                    GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
                    GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
                    GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                    GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AdminLoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }

    private void executeSalesLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.salesExecutive(mParam()).enqueue(new Callback<AdminLoginModel>() {
            @Override
            public void onResponse(Call<AdminLoginModel> call, Response<AdminLoginModel> response) {
                dismissProgressDialog();
                AdminLoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, getString(R.string.coming_soon));
//                    Intent i = new Intent(mActivity, HomeActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(i);
//                    finishAffinity();
//                    GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
//                    GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
//                    GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
//                    GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                } else {
                    //showAlertDialog(mActivity, mModel.getMessage());
                    showToast(mActivity, getString(R.string.coming_soon));
                }
            }

            @Override
            public void onFailure(Call<AdminLoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });
    }
}