package com.gahiragro.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.GuestLoginInterface;
import com.gahiragro.app.activities.interfaces.ProdSelImageInterface;
import com.gahiragro.app.adapters.ProdImagesAdapter;
import com.gahiragro.app.adapters.SpecificationAdapter;
import com.gahiragro.app.model.ProductDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealorProductDetailActivity extends BaseActivity implements ProdSelImageInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealorProductDetailActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DealorProductDetailActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.btnEnquiry)
    Button btnEnquiry;
    //    @BindView(R.id.widthTV)
    //    TextView widthTV;
//    @BindView(R.id.specificationRV)
    RecyclerView specificationRV;
//    @BindView(R.id.modelNameTV)
    TextView modelNameTV;
//    @BindView(R.id.productImgIV)
    ImageView productImgIV;
//    @BindView(R.id.productModelTV)
    TextView productModelTV;


//    @BindView(R.id.btnVideoPdf)
    Button btnVideoPdf;

//    @BindView(R.id.serviceManualBT)
    Button serviceManualBT;

//    @BindView(R.id.troubleShootingBT)
    Button troubleShootingBT;

//    @BindView(R.id.prodImagesRV)
    RecyclerView prodImagesRV;


    //INITAILIZE
    String id = "";
    ProductDetailModel mModel;
    ArrayList<ProductDetailModel.ProductDetail> mProductDetailAL = new ArrayList();
    ArrayList<String> mProdImagesAL = new ArrayList();
    ProdSelImageInterface mProdSelImageInterface;
    List<String> mList = new ArrayList();

    String productImage;
    GuestLoginInterface mGuestLoginInterface = new GuestLoginInterface() {
        @Override
        public void onGuestLogin() {
            GahirPreference.writeString(mActivity,GahirPreference.GUEST_LOGIN,"true");
            Intent i = new Intent(mActivity, SplashActivity.class);
            startActivity(i);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealor_product_detail);
//        ButterKnife.bind(this);
        initViews();
        setStatusBar(mActivity, Color.WHITE);
        mProdSelImageInterface = this;
        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ID);
            if (isNetworkAvailable(mActivity)) {
                executeProductDetailApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
        setRoleSelectionData();

        productImgIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity,FullImageActivity.class);
                intent.putExtra(Constants.IMAGE,productImage);
                startActivity(intent);
            }
        });
    }

    private void initViews() {
        imgMenuRL =findViewById(R.id.imgMenuRL);
        btnEnquiry =findViewById(R.id.btnEnquiry);
        specificationRV =findViewById(R.id.specificationRV);
        modelNameTV =findViewById(R.id.modelNameTV);
        productImgIV =findViewById(R.id.productImgIV);
        productModelTV =findViewById(R.id.productModelTV);
        btnVideoPdf =findViewById(R.id.btnVideoPdf);
        serviceManualBT =findViewById(R.id.serviceManualBT);
        troubleShootingBT =findViewById(R.id.troubleShootingBT);
        prodImagesRV =findViewById(R.id.prodImagesRV);

        onViewClicked();
    }

    private void setRoleSelectionData() {
        if (getLoginRoleType().equals(Constants.CUSTOMER)) {
            btnVideoPdf.setVisibility(View.VISIBLE);
            btnEnquiry.setVisibility(View.GONE);
            serviceManualBT.setVisibility(View.GONE);
            troubleShootingBT.setVisibility(View.GONE);
        }
        else if(getLoginRoleType().equals(Constants.SERVICE))
        {
            btnVideoPdf.setVisibility(View.GONE);
            btnEnquiry.setVisibility(View.GONE);
            serviceManualBT.setVisibility(View.VISIBLE);
            troubleShootingBT.setVisibility(View.VISIBLE);
        }
        else if(getLoginRoleType().equals(Constants.SALES_EXECUTIVE)){
            btnVideoPdf.setVisibility(View.VISIBLE);
            btnEnquiry.setVisibility(View.GONE);
            serviceManualBT.setVisibility(View.VISIBLE);
            troubleShootingBT.setVisibility(View.VISIBLE);
        }

        else {

        }
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL, R.id.troubleShootingBT,R.id.serviceManualBT,R.id.btnEnquiry, R.id.btnVideoPdf})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

        btnEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnquiryClick();
            }
        });
        btnVideoPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVideosPdfClick();
            }
        });

        troubleShootingBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mModel.getProductDetail().getService_manual()!=null && !mModel.getProductDetail().getTroubleshooting().equals("") )
                {
                    setIntentClick("trobleShoot");
                }
            }
        });


        serviceManualBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mModel.getProductDetail().getService_manual()!=null && !mModel.getProductDetail().getService_manual().equals("") )
                {
                    setIntentClick("serviceManual");
                }
            }
        });



        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.btnEnquiry:
                setEnquiryClick();
                break;
            case R.id.btnVideoPdf:
                setVideosPdfClick();
                break;
            case R.id.troubleShootingBT:
                if(mModel.getProductDetail().getService_manual()!=null && !mModel.getProductDetail().getTroubleshooting().equals("") )
                {
                    setIntentClick("trobleShoot");
                }
                break;
            case R.id.serviceManualBT:
                if(mModel.getProductDetail().getService_manual()!=null && !mModel.getProductDetail().getService_manual().equals("") )
                {
                    setIntentClick("serviceManual");
                }
                break;


        }*/
    }

    private void setIntentClick(String type) {
        Intent intent = new Intent(mActivity, WebViewActivity.class);
        intent.putExtra(Constants.TROUBLESHOOTING, mModel.getProductDetail().getTroubleshooting());
        intent.putExtra(Constants.SERVICE_MANUAL, mModel.getProductDetail().getService_manual());
        intent.putExtra(Constants.TYPE, type);
        startActivity(intent);
    }

    private void setVideosPdfClick() {
        Intent intent = new Intent(mActivity, VideosPdfLIstingActivity.class);
        intent.putExtra(Constants.ID, id);
        startActivity(intent);
    }

    private void setEnquiryClick() {
        if (IsLogin()) {
            Intent intent = new Intent(mActivity, DealorEnquiryActivity.class);
            intent.putExtra(Constants.ID, id);
            startActivity(intent);
        } else {
            showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
        }

    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    /*
     * Get User Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProductDetailApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.productDetailApi(mDetailParam()).enqueue(new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                dismissProgressDialog();
                 mModel = response.body();

                mProductDetailAL.add(mModel.getProductDetail());
                modelNameTV.setText(mModel.getProductDetail().getProdName());
                Glide.with(mActivity).
                        load(mModel.getProductDetail().getProdImage())
                        .placeholder(R.drawable.gahir_logo_dummy)
                        .into(productImgIV);
                productImage=mModel.getProductDetail().getProdImage();
                productModelTV.setText(mModel.getProductDetail().getProdModel());
                mProdImagesAL.addAll(mModel.getProductDetail().getAll_images());
                Log.e(TAG,"SIZELIST::"+mProdImagesAL.size());

                //mModel.getProductDetail().getP

                if (mModel.getProductDetail().getProdDesc() != null)
                    mList = mModel.getProductDetail().getProdDesc();
                    setProdImagesAdapter();
                    setAdapter();
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private void setAdapter() {
        specificationRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        specificationRV.setLayoutManager(layoutManager);
        SpecificationAdapter mAdapter = new SpecificationAdapter(mActivity, mList);
        specificationRV.setAdapter(mAdapter);
    }

    private void setProdImagesAdapter() {
        prodImagesRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false);
        prodImagesRV.setLayoutManager(layoutManager);
        ProdImagesAdapter mAdapter = new ProdImagesAdapter(mActivity,mProdImagesAL,mProdSelImageInterface);
        prodImagesRV.setAdapter(mAdapter);
    }

    @Override
    public void onImageClick(String image) {
        Glide.with(mActivity).
                load(image)
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(productImgIV);
        productImage=image;
    }
}