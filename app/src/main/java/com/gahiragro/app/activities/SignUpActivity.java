package com.gahiragro.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.EasyLocationProvider;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.fonts.TextViewMedium;
import com.gahiragro.app.model.DealorSignUpModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class SignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;


    //INitialize
    String strPushToken = "";
    String serialDealerNo = "";
    String dealerNo = "";
    String street = "";
    String state = "";
    String district = "";
    String address = "";
    String dealerName = "";
    String cityName = "";
    EasyLocationProvider easyLocationProvider;

    /**
     * Widgets
     */
//    @BindView(R.id.editNameET)
    EditText editNameET;
    //    @BindView(R.id.editEmailET)
    EditText editEmailET;
    //    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    //    @BindView(R.id.btnSignUpBT)
    Button btnSignUpBT;
    //    @BindView(R.id.passwordRL)
    RelativeLayout passwordRL;
    //    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
    //    @BindView(R.id.nameRL)
    RelativeLayout nameRL;

    //    @BindView(R.id.firmRL)
    RelativeLayout firmRL;
    //    @BindView(R.id.termsConditionTV)
    TextView termsConditionTV;

    String strRadioValue = "";
    //    @BindView(R.id.firmnameET)
    EditText firmnameET;
    //    @BindView(R.id.firmTextTV)
    TextView firmTextTV;
    //    @BindView(R.id.radioIV)
    ImageView radioIV;
    //    @BindView(R.id.addressRL)
    RelativeLayout addressRL;
    //    @BindView(R.id.addressET)
    EditText addressET;
    //    @BindView(R.id.streetRL)
    RelativeLayout streetRL;
    //    @BindView(R.id.streetET)
    EditText streetET;
    //    @BindView(R.id.districtRL)
    RelativeLayout districtRL;
    //    @BindView(R.id.districtET)
    EditText districtET;
    //    @BindView(R.id.stateRL)
    RelativeLayout stateRL;
    //    @BindView(R.id.stateET)
    EditText stateET;
    //    @BindView(R.id.fullAddressLL)
    LinearLayout fullAddressLL;
    //    @BindView(R.id.cityET)
    EditText cityET;
    //    @BindView(R.id.cityRL)
    RelativeLayout cityRL;
    TextViewMedium txtSignInTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
//        ButterKnife.bind(this);
        editNameET = findViewById(R.id.editNameET);
        editEmailET = findViewById(R.id.editEmailET);
        editPasswordET = findViewById(R.id.editPasswordET);
        cityRL = findViewById(R.id.cityRL);
        cityET = findViewById(R.id.cityET);
        fullAddressLL = findViewById(R.id.fullAddressLL);
        stateET = findViewById(R.id.stateET);
        stateRL = findViewById(R.id.stateRL);
        districtET = findViewById(R.id.districtET);
        districtRL = findViewById(R.id.districtRL);
        streetET = findViewById(R.id.streetET);
        streetRL = findViewById(R.id.streetRL);
        addressET = findViewById(R.id.addressET);
        addressRL = findViewById(R.id.addressRL);
        radioIV = findViewById(R.id.radioIV);
        firmTextTV = findViewById(R.id.firmTextTV);
        firmnameET = findViewById(R.id.firmnameET);
        termsConditionTV = findViewById(R.id.termsConditionTV);
        firmRL = findViewById(R.id.firmRL);
        nameRL = findViewById(R.id.nameRL);
        emailRL = findViewById(R.id.emailRL);
        passwordRL = findViewById(R.id.passwordRL);
        btnSignUpBT = findViewById(R.id.btnSignUpBT);
        txtSignInTV = findViewById(R.id.txtSignInTV);
        editPasswordET = findViewById(R.id.editPasswordET);

        editTextSelector(editEmailET, emailRL, "");
        editTextSelector(editPasswordET, passwordRL, "");
        editTextSelector(editNameET, nameRL, "");
        editTextSelector(firmnameET, firmRL, "");
        editTextSelector(addressET, addressRL, "");
        editTextSelector(streetET, streetRL, "");
        editTextSelector(stateET, stateRL, "");
        editTextSelector(cityET, cityRL, "");
        editTextSelector(districtET, districtRL, "");
        //Get Device Token
        getPushToken();
        if (getLoginRoleType().equals(Constants.DEALER)) {
            firmTextTV.setVisibility(View.VISIBLE);
            firmRL.setVisibility(View.VISIBLE);
            fullAddressLL.setVisibility(View.VISIBLE);
        } else {
            firmTextTV.setVisibility(View.GONE);
            firmRL.setVisibility(View.GONE);
            fullAddressLL.setVisibility(View.GONE);
        }

        getIntentData();

        onViewClicked();
    }

    private void getIntentData() {
        address = getIntent().getExtras().getString(Constants.ADDRESS);
        street = getIntent().getExtras().getString(Constants.STREET);
        state = getIntent().getExtras().getString(Constants.STATE);
        district = getIntent().getExtras().getString(Constants.DISTRICT);
        dealerName = getIntent().getExtras().getString(Constants.DEALER_NAME);
        cityName = getIntent().getExtras().getString(Constants.CITY_NAME);
        addressET.setText(address);
        streetET.setText(street);
        stateET.setText(state);
        districtET.setText(district);
        editNameET.setText(dealerName);
        cityET.setText(cityName);

        dealerNo = GahirPreference.readString(mActivity, GahirPreference.DEALER_CODE, "");
        serialDealerNo = GahirPreference.readString(mActivity, GahirPreference.SERIAL_NO, "");
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.btnSignUpBT, R.id.txtSignInTV, R.id.termsConditionTV, R.id.radioIV})
    public void onViewClicked(/*View view*/) {
        btnSignUpBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignUpClick();
            }
        });

        txtSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignInClick();
            }
        });
        termsConditionTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performTermsConditionClick();
            }
        });
        radioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTermsClick();
            }
        });


    /*    switch (view.getId()) {
            case R.id.btnSignUpBT:
                performSignUpClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.termsConditionTV:
                performTermsConditionClick();
                break;
            case R.id.radioIV:
                setTermsClick();
                break;
        }*/
    }

    private void setTermsClick() {
        if (strRadioType == 0) {
            strRadioValue = "agree";
            radioIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
            strRadioType++;
        } else {
            strRadioValue = "not_agree";
            radioIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck));
            strRadioType = 0;
        }
    }


    private void performTermsConditionClick() {
        Intent intent = new Intent(this, TermsAndConditionActivity.class);
        startActivity(intent);
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });
    }

    private void performSignInClick() {
        Intent i = new Intent(mActivity, SignInActivity.class);
        startActivity(i);
        finish();
    }

    private void performSignUpClick() {

        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSignUpApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().length() < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit));
            flag = false;
        } else if (strRadioValue.equals("not_agree") || strRadioValue.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_accept_terms_condition));
            flag = false;
        }
        return flag;
    }

    /*
     * DealorSignUp APi
     * */
    private Map<String, String> mCustomerParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", editEmailET.getText().toString().trim());
        mMap.put("first_name", editNameET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
//        mMap.put("serial_no", serialDealerNo);
        mMap.put("phone", GahirPreference.readString(mActivity, GahirPreference.PHONE_NO, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private Map<String, String> mDealerParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", editEmailET.getText().toString().trim());
        mMap.put("first_name", editNameET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
        mMap.put("dealer_code", dealerNo);
        mMap.put("firm_name", firmnameET.getText().toString().trim());
        mMap.put("address", addressET.getText().toString().trim());
        mMap.put("street", streetET.getText().toString().trim());
        mMap.put("district", districtET.getText().toString().trim());
        mMap.put("city", cityET.getText().toString().trim());
        mMap.put("state", stateET.getText().toString().trim());
        mMap.put("phone", GahirPreference.readString(mActivity, GahirPreference.PHONE_NO, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        if (getLoginRoleType().equals("Dealer")) {
            mApiInterface.dealorSignUp(mDealerParam()).enqueue(new Callback<DealorSignUpModel>() {
                @Override
                public void onResponse(Call<DealorSignUpModel> call, Response<DealorSignUpModel> response) {
                    dismissProgressDialog();
                    DealorSignUpModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        Intent i = new Intent(mActivity, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finishAffinity();
                        GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                        GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                        GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
                        GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
                        // GahirPreference.writeString(mActivity, GahirPreference.DEALER_DOC, mModel.getUserDetail().getDealer_doc());
                        GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, mModel.getUserDetail().getRole());
                        GahirPreference.writeString(mActivity, Constants.PASSWORD_VALUE, editPasswordET.getText().toString().trim());

                    } else {
                        showAlertDialog(mActivity, mModel.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<DealorSignUpModel> call, Throwable t) {
                    dismissProgressDialog();
                    Log.e(TAG, "**ERROR**" + t.toString());
                }
            });
        } else if (getLoginRoleType().equals("Customer")) {
            mApiInterface.customerSignUp(mCustomerParam()).enqueue(new Callback<DealorSignUpModel>() {
                @Override
                public void onResponse(Call<DealorSignUpModel> call, Response<DealorSignUpModel> response) {
                    dismissProgressDialog();
                    DealorSignUpModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        Intent i = new Intent(mActivity, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finishAffinity();
                        GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                        GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                        GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
                        GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
                    } else {
                        showAlertDialog(mActivity, mModel.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<DealorSignUpModel> call, Throwable t) {
                    dismissProgressDialog();
                    Log.e(TAG, "**ERROR**" + t.toString());
                }
            });

        }


    }


}
