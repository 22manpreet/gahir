package com.gahiragro.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.SearchItemClickInterface;
import com.gahiragro.app.adapters.RecentSearchAdapter;
import com.gahiragro.app.adapters.SearchAdapter;
import com.gahiragro.app.model.RecentSearchModel;
import com.gahiragro.app.model.SearchModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SearchActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SearchActivity.this;
//    @BindView(R.id.cancelTV)
    TextView cancelTV;

//    @BindView(R.id.editSearchET)
    TextView editSearchET;

//    @BindView(R.id.mSearchRV)
    RecyclerView mSearchRV;

//    @BindView(R.id.mRecentRV)
    RecyclerView mRecentRV;

//    @BindView(R.id.recentSearchLL)
    LinearLayout recentSearchLL;


    //INitialize
    ArrayList mRecentItemsList;
    RecentSearchAdapter mRecentSearchAdapter;
    SearchAdapter mSearchAdapter;
    ArrayList<SearchModel.AllProduct> mAllProductAL = new ArrayList<>();
    ArrayList<String> mRecentSearchAL = new ArrayList<String>();
    SearchItemClickInterface mSearchItemClickInterface = new SearchItemClickInterface() {
        @Override
        public void onClick(String item) {
            String clickedItem = item;
            editSearchET.setText(clickedItem);
            recentSearchLL.setVisibility(View.GONE);

            executeSearchApi();


        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkAvailable(mActivity)) {
            executeRecentSearchApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        cancelTV =findViewById(R.id.cancelTV);
        editSearchET =findViewById(R.id.editSearchET);
        mSearchRV =findViewById(R.id.mSearchRV);
        mRecentRV =findViewById(R.id.mRecentRV);
        recentSearchLL =findViewById(R.id.recentSearchLL);
        TextWatchForEditText();
        editSearchET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //do here your stuff f

                    recentSearchLL.setVisibility(View.GONE);

                    if (isValidate())
                        if (isNetworkAvailable(mActivity)) {
                            executeSearchApi();
                        } else {
                            showToast(mActivity, getString(R.string.internet_connection_error));
                        }


                    hideKeyBoard(mActivity, getCurrentFocus());
                    return true;
                }
                return false;
            }
        });

        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.cancelTV, R.id.editSearchET})
    public void onViewClicked(/*View view*/) {

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCancelClick();
            }
        });
        editSearchET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCancelClick();
            }
        });

      /*switch (view.getId()) {
            case R.id.cancelTV:
                setCancelClick();
                break;
            case R.id.editSearchET:

                break;
        }*/
    }


    private void TextWatchForEditText() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mAllProductAL != null) {
                    if (mAllProductAL.size() >= 0) {
                        if (editable.length() == 0) {

                            //    mRecentSearchAL.clear();
                            if (mAllProductAL.size()>0) {
                                mAllProductAL.clear();
                                mSearchAdapter.notifyDataSetChanged();
                            }

//                            mRecentSearchAdapter.notifyDataSetChanged();
                            recentSearchLL.setVisibility(View.VISIBLE);
                        }
                    }
                }

            }
        });
    }

    private void setCancelClick() {
        finish();
    }


    /*
     * Search Api
     * */
    private Map<String, String> mSearchParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", "1");
        mMap.put("search", editSearchET.getText().toString().trim());
        Log.e(TAG, "**PARAMSearch**" + mMap.toString());
        return mMap;
    }

    private void executeSearchApi() {
        mAllProductAL.clear();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.searchApi(mSearchParam()).enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                dismissProgressDialog();
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mAllProductAL.addAll(mModel.getProductList().getAllProducts());
                    setSearchAdapter();
                } else {
                    showToast(mActivity, mModel.getMessage());
                    recentSearchLL.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    /*
     * Recent Search Api
     * */
    private Map<String, String> mRecentParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeRecentSearchApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recentSearchApi(mRecentParam()).enqueue(new Callback<RecentSearchModel>() {
            @Override
            public void onResponse(Call<RecentSearchModel> call, Response<RecentSearchModel> response) {
                dismissProgressDialog();
                RecentSearchModel mModel = response.body();

                for (int i = 0; i < mModel.getSearchList().getSearchList().size(); i++) {
                    if (i < 3) {
                        mRecentSearchAL.add(mModel.getSearchList().getSearchList().get(i));
                    }
                }
                setRecentSearchAdapter();
            }

            @Override
            public void onFailure(Call<RecentSearchModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private Boolean isValidate() {
        boolean flag = true;
        if (editSearchET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_something));
            flag = false;
        }
        return flag;
    }

    /*
     * Set Search Adapter
     * */

    private void setSearchAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mSearchRV.setLayoutManager(layoutManager);
        mSearchAdapter = new SearchAdapter(mActivity, mAllProductAL);
        mSearchRV.setAdapter(mSearchAdapter);
    }

    /*
     * Set Recent Search Adapter
     * */

    private void setRecentSearchAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mRecentRV.setLayoutManager(layoutManager);
        mRecentSearchAdapter = new RecentSearchAdapter(mActivity, mRecentSearchAL, mSearchItemClickInterface);
        mRecentRV.setAdapter(mRecentSearchAdapter);
    }


}