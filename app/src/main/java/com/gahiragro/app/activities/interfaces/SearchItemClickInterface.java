package com.gahiragro.app.activities.interfaces;

public interface SearchItemClickInterface {
    public  void  onClick(String item);
}
