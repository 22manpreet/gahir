package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class OperatorManualActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = OperatorManualActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = OperatorManualActivity.this;

//    @BindView(R.id.opManualIV)
//    ImageView opManualIV;

//    @BindView(R.id.opPdfTV)
    TextView opPdfTV;

//    @BindView(R.id.webView)
    WebView mWebViewWV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_manual);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        opPdfTV=findViewById(R.id.opPdfTV);
        mWebViewWV=findViewById(R.id.webView);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        OnClick();
        getIntentData();
        // callApi();

    }

    private void OnClick() {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getIntentData() {

        if (getIntent() != null) {

            String prodImg = getIntent().getStringExtra("OP_Image");
           // Glide.with(mActivity).load(prodImg).into(opManualIV);

            String pdf = getIntent().getStringExtra("PD_Name");

//            webView.getSettings().setJavaScriptEnabled(true);
////            webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url="+pdf);
//            webView.loadUrl(pdf);

            pdfOpen(pdf);
        }
    }

/*    private void loadUrlInWebView(String strLinkUrl) {
        mWebViewWV.setWebViewClient(new MyWebClient());
        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.loadUrl(strLinkUrl);
    }*/

    private void pdfOpen(String fileUrl) {

        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.getSettings().setPluginState(WebSettings.PluginState.ON);
      //  mWebViewWV.getSettings().setUseWideViewPort(true);
        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
 if(fileUrl.endsWith(".pdf")){
                 mWebViewWV.setWebViewClient(new Callback());

                  mWebViewWV.loadUrl("http://docs.google.com/gview?embedded=true&url=" + fileUrl);
        }
  else  {
            fileUrl = fileUrl.replaceAll(" ", "%20");
            String newUA = "Chrome/43.0.2357.65 ";
            mWebViewWV.getSettings().setUserAgentString(newUA);
            System.out.println("https://view.officeapps.live.com/op/view.aspx?src=" + fileUrl);
            mWebViewWV.loadUrl("https://view.officeapps.live.com/op/view.aspx?src=" + fileUrl);
        }
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

  /*  public class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
//            mProgressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
//            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }
    }*/
}