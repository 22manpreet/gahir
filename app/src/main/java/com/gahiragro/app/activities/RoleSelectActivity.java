package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.utils.GahirPreference;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class RoleSelectActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RoleSelectActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = RoleSelectActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.dealorRL)
    RelativeLayout dealorRL;

    //    @BindView(R.id.customerRL)
    RelativeLayout customerRL;

    //Initialize
    String role = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role_select);
//        ButterKnife.bind(this);
        dealorRL = findViewById(R.id.dealorRL);
        customerRL = findViewById(R.id.customerRL);

        onViewClicked();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.dealorRL, R.id.customerRL,})
    public void onViewClicked(/*View view*/) {

        dealorRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performDealorClick();
            }
        });
        customerRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performCustomerClick();
            }
        });

        /*switch (view.getId()) {
            case R.id.dealorRL:
                performDealorClick();
                break;
            case R.id.customerRL:
                performCustomerClick();
                break;
        }*/
    }

    private void performDealorClick() {
        role = "Dealer";
        // GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, "Dealer");
        dealorRL.setBackground(getDrawable(R.drawable.bg_role_field_s));
        customerRL.setBackground(getDrawable(R.drawable.bg_role_field_us));
        performNextDealorClick();
    }

    private void performCustomerClick() {
        role = "Customer";
        // GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, "Customer");
        dealorRL.setBackground(getDrawable(R.drawable.bg_role_field_us));
        customerRL.setBackground(getDrawable(R.drawable.bg_role_field_s));
        performNextDealorClick();
    }

    private void performNextDealorClick() {
        Intent intent = new Intent(mActivity, SignUpFirstScreenActivity.class);
        intent.putExtra("selRole", role);
        startActivity(intent);
    }


}