package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.enquirydetails.EnquiryDetailModelM;
import com.gahiragro.app.utils.Constants;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadyToDispatchActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ReadyToDispatchActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ReadyToDispatchActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.btnBookOrder)
    Button btnBookOrder;

//    @BindView(R.id.readyToDispatchTV)
    TextView readyToDispatchTV;

//    @BindView(R.id.productDescriptionTV)
//    TextView productDescriptionTV;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.modelNameTV)
    TextView modelNameTV;

//    @BindView(R.id.quantityTV)
    TextView quantityTV;

//    @BindView(R.id.noteRL)
    RelativeLayout noteRL;
    RelativeLayout imgMenuRL;

//    @BindView(R.id.modelTV)
    TextView modelTV;

//    @BindView(R.id.headingTV)
    TextView headingTV;

//    @BindView(R.id.dispatchdateTV)
    TextView dispatchdateTV;

//    @BindView(R.id.orderDateTV)
    TextView orderDateTV;

//    @BindView(R.id.quantityValueTV)
    TextView quantityValueTV;

//    @BindView(R.id.detailCV)
    CardView detailCV;

//    @BindView(R.id.cancelOrderCV)
    CardView cancelOrderCV;

//    @BindView(R.id.orderStatusTV)
    TextView orderStatusTV;


    String id = "";
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready_to_dispatch);
//        ButterKnife.bind(this);
        setStatusBar(mActivity, Color.WHITE);
        initViews();

        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ENQUIRY_ID);
            if (getIntent().getExtras().getString(Constants.TYPE) != null) {
                type = getIntent().getExtras().getString(Constants.TYPE);
            }
            if (getIntent().getExtras().getString(Constants.PUSH) != null) {
                if (getIntent().getExtras().getString(Constants.PUSH).equals("PUSH")) {
                    btnBookOrder.setVisibility(View.GONE);
                } else {
                    btnBookOrder.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    private void initViews() {
        btnBookOrder = findViewById(R.id.btnBookOrder);
        readyToDispatchTV = findViewById(R.id.readyToDispatchTV);
        orderStatusTV = findViewById(R.id.orderStatusTV);
        cancelOrderCV = findViewById(R.id.cancelOrderCV);
        detailCV = findViewById(R.id.detailCV);
        quantityValueTV = findViewById(R.id.quantityValueTV);
        orderDateTV = findViewById(R.id.orderDateTV);
        dispatchdateTV = findViewById(R.id.dispatchdateTV);
        headingTV = findViewById(R.id.headingTV);
        modelTV = findViewById(R.id.modelTV);
        noteRL = findViewById(R.id.noteRL);
        quantityTV = findViewById(R.id.quantityTV);
        modelNameTV = findViewById(R.id.modelNameTV);
        productIV = findViewById(R.id.productIV);
        imgMenuRL = findViewById(R.id.imgMenuRL);

        onViewClicked();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isNetworkAvailable(mActivity)) {
            executeGetEnquiryDetailApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.btnBookOrder, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {

        btnBookOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performBookOderClick();
            }
        });
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });


       /* switch (view.getId()) {
            case R.id.btnBookOrder:
                performBookOderClick();
                break;
            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performBookOderClick() {
        Intent intent = new Intent(mActivity, DealorBookOrderActivity.class);
        intent.putExtra(Constants.ID, id);
        startActivity(intent);
    }

    private Map<String, String> mEnquiryParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetEnquiryDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.enquiryDetailApi(mEnquiryParam()).enqueue(new Callback<EnquiryDetailModelM>() {
            @Override
            public void onResponse(Call<EnquiryDetailModelM> call, Response<EnquiryDetailModelM> response) {
                dismissProgressDialog();
                EnquiryDetailModelM mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    modelNameTV.setText(mModel.getEnquiryDetail().getProdName());
                    modelTV.setText(mModel.getEnquiryDetail().getProductDetail().getProdModel());
                    Glide.with(mActivity)
                            .load(mModel.getEnquiryDetail().getProductDetail().getProdImage())
                            .placeholder(R.drawable.gahir_logo_dummy)// Uri of the picture
                            .into(productIV);

                    if (type.equals("adminDealerAdapter")) {
                        headingTV.setText(getString(R.string.enquiries_detail));
                        btnBookOrder.setVisibility(View.VISIBLE);
                        dispatchdateTV.setVisibility(View.VISIBLE);
                        quantityValueTV.setVisibility(View.VISIBLE);
                        orderDateTV.setVisibility(View.VISIBLE);
                        dispatchdateTV.setText(mModel.getEnquiryDetail().getDispatch_day());
                        orderDateTV.setText(mModel.getEnquiryDetail().getOrder_time());
                        quantityValueTV.setText(mModel.getEnquiryDetail().getQty());
                    } else {
                        headingTV.setText(getString(R.string.my_enquiries_detail));
                        if (mModel.getEnquiryDetail().getStatus().equals("Not Responded") || mModel.getEnquiryDetail().getStatus().equals("Cancelled")
                                || (mModel.getEnquiryDetail().getStatus().equals("represenative will contact"))) {
                            btnBookOrder.setVisibility(View.GONE);
                            cancelOrderCV.setVisibility(View.VISIBLE);
                            detailCV.setVisibility(View.GONE);
                            orderStatusTV.setText(mModel.getEnquiryDetail().getStatus_text());
                            dispatchdateTV.setText(mModel.getEnquiryDetail().getDispatch_day());
                            orderDateTV.setText(mModel.getEnquiryDetail().getOrder_time());
                            quantityValueTV.setText(mModel.getEnquiryDetail().getQty());

                        } else {
                            if (!mModel.getEnquiryDetail().getOrder_status()) {
                                cancelOrderCV.setVisibility(View.GONE);
                                detailCV.setVisibility(View.VISIBLE);
                                btnBookOrder.setVisibility(View.VISIBLE);
                                dispatchdateTV.setText(mModel.getEnquiryDetail().getDispatch_day());
                                orderDateTV.setText(mModel.getEnquiryDetail().getOrder_time());
                                quantityValueTV.setText(mModel.getEnquiryDetail().getQty());
                            } else {
                                cancelOrderCV.setVisibility(View.GONE);
                                detailCV.setVisibility(View.VISIBLE);
                                btnBookOrder.setVisibility(View.GONE);
                                dispatchdateTV.setText(mModel.getEnquiryDetail().getDispatch_day());
                                orderDateTV.setText(mModel.getEnquiryDetail().getOrder_time());
                                quantityValueTV.setText(mModel.getEnquiryDetail().getQty());

                            }

                        }
//                            if (!mModel.getEnquiryDetail().getOrder_status()) {
//                                detailCV.setVisibility(View.GONE);
//                                cancelOrderCV.setVisibility(View.VISIBLE);
//                               // noteRL.setVisibility(View.GONE);
//                                //btnBookOrder.setVisibility(View.GONE);
//                               // readyToDispatchTV.setText(getString(R.string.not_responded));
//                                //productDescriptionTV.setText(getString(R.string.order_canceled_description));
//                               // productDescriptionTV.setText(mModel.getEnquiryDetail().getStatus_text());
//                            } else {
//                                detailCV.setVisibility(View.VISIBLE);
//                                cancelOrderCV.setVisibility(View.GONE);
//                                btnBookOrder.setVisibility(View.VISIBLE);
//                                dispatchdateTV.setText(mModel.getEnquiryDetail().getDispatch_day());
//                                orderDateTV.setText(mModel.getEnquiryDetail().getQty());
//                                quantityTV.setText(mModel.getEnquiryDetail().getOrder_time());
//                               // noteRL.setVisibility(View.VISIBLE);
//                              //  btnBookOrder.setVisibility(View.VISIBLE);
//                               // readyToDispatchTV.setText(mModel.getEnquiryDetail().getStatus());
//                                //productDescriptionTV.setText(getString(R.string.product_is_held_kindly_make_payment));
//                               // productDescriptionTV.setText(mModel.getEnquiryDetail().getStatus_text());
//                            }

                    }
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<EnquiryDetailModelM> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }
}