package com.gahiragro.app.activities.interfaces;

import java.util.List;

public interface FilterReviewListInterface {
    public  void onFilterReview(int position, List<String> mNewList);
}
