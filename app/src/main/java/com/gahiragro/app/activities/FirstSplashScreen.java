package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

//import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.fragments.Dealer.RegisterProductListFragment;
import com.gahiragro.app.model.UserDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.HashMap;
import java.util.Map;

public class FirstSplashScreen extends BaseActivity {
    public final int SPLASH_TIME_OUT = 2200;
    /**
     * Getting the Current Class Name
     */
    String TAG = FirstSplashScreen.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = FirstSplashScreen.this;

    String title = "", message = "", enquiryId = "", notificationType = "", orderId;
    Intent intent;
    boolean isNotification = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_splash_screen);
        setStatusBar(mActivity, Color.BLACK);
        if (getIntent().getExtras() != null) {
            enquiryId = getIntent().getExtras().getString("enquiry_id");
            orderId = getIntent().getExtras().getString("order_id");
            notificationType = getIntent().getExtras().getString("notification_type");
        }
        setNotificationClicks();
    }

    private void setNotificationClicks() {
        if (notificationType != null && !notificationType.equals("")) {
            isNotification = true;
            if (notificationType.equals("enquiry")) {
                intent = new Intent(this, ReadyToDispatchActivity.class);
                intent.putExtra(Constants.ENQUIRY_ID, enquiryId);
                intent.putExtra(Constants.TYPE, "adminDealerAdapter");
                intent.putExtra(Constants.PUSH, "PUSH");
            } else if (notificationType.equals("mass")) {
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_MASS, "mass_push");
            }
            else if (notificationType.contains("order")) {
                intent = new Intent(this, BookingDetailActivity.class);
                intent.putExtra(Constants.ORDER_ID, orderId);
            }
            else if(notificationType.equals("register_product")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_REGISTER_PRODUCT, "register_product");
            }
            else if(notificationType.equals("review")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_REVIEW, "review");
            }
            else if(notificationType.equals("part_enquiry")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_PART_ENQUIRY, "part_enquiry");
            }
            else if(notificationType.equals("complain")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_COMPLAIN, "complain");
            }
            else if(notificationType.equals("custom")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_COSTUM, "custom");
            }
            else {
                intent = new Intent(this, HomeActivity.class);
            }
            startActivity(intent);
            finish();
            if (!isNotification) {
                setSplash();
            }
        } else {
            setSplash();
        }
    }

    private void setSplash() {
        Thread background = new Thread() {
            public void run() {
                try {
                    if (IsLogin()) {
                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        // executeUserDetailApi();
                    } else {
                        // Thread will sleep for 5 seconds
                        sleep(SPLASH_TIME_OUT);
//                        Intent i = new Intent(getBaseContext(), SplashActivity.class);
//                        startActivity(i);
//                        finish();
                        Intent i = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (Exception e) {
                }
            }
        };
        // start thread
        background.start();
    }
}
