package com.gahiragro.app.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.ExifInterface
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
//import butterknife.BindView
//import butterknife.ButterKnife
//import butterknife.OnClick
import com.coach.app.activities.viewModel.MainViewModel
import com.coach.app.repository.ApisRepository
import com.gahiragro.app.R
import com.gahiragro.app.RetrofitApi.ApiKotlinInterface
import com.gahiragro.app.RetrofitApi.RestClient
import com.gahiragro.app.activities.Dealer.RecordAudioActivity
import com.gahiragro.app.activities.interfaces.RemoveImageInterface
import com.gahiragro.app.adapters.Dealer.UploadAdapter
import com.gahiragro.app.databinding.ActivityDemoComplaintBinding
import com.gahiragro.app.model.ImagesModel
import com.gahiragro.app.utils.Constants
import com.gahiragro.app.utils.Resource
import com.gahiragro.app.viewModel.MainViewModelFactory
import com.icma.app.dataobject.RequestBodies
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import com.zfdang.multiple_images_selector.SelectorSettings
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*


class DemoComplaintActivity : BaseActivity() {

    /**
     * Getting the Current Class Name
     */
    var TAG = this@DemoComplaintActivity.javaClass.simpleName
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var readMediaImages = Manifest.permission.READ_MEDIA_IMAGES
    private val REQUEST_CODE = 732
    var imageListInUri: java.util.ArrayList<MiMedia>? = java.util.ArrayList()
    /*
     * Current Activity Instance
     */
    var mActivity: Activity = this@DemoComplaintActivity

//    @BindView(R.id.imgMenuRL)
//    var imgMenuRL: RelativeLayout? = null


//    @BindView(R.id.selectProductRL)
//    var selectProductRL: RelativeLayout? = null

//    @BindView(R.id.btnSubmitBT)
//    var btnSubmitBT: Button? = null

//    @BindView(R.id.selectProductSpinner)
//    var selectProductSpinner: Spinner? = null

//    @BindView(R.id.audioPlayPauseClickRL)
//    var audioPlayPauseClickRL: RelativeLayout? = null

//    @BindView(R.id.audioIV)
//    var audioIV: ImageView? = null

//    @BindView(R.id.audioSeekBar)
//    var audioSeekBar: SeekBar? = null

//    @BindView(R.id.audioPlayingDurationTV)
//    var audioPlayingDurationTV: TextView? = null

//    @BindView(R.id.addPhotoLL)
//    var addPhotoLL: LinearLayout? = null

//    @BindView(R.id.imageTV)
//    var imageTV: TextView? = null


//    @BindView(R.id.customerNumET)
//    var customerNumET: EditText? = null

//    @BindView(R.id.fullReasonET)
//    var fullReasonET: EditText? = null

//    @BindView(R.id.proSerialNumtV)
//    var proSerialNumtV: TextView? = null

//    @BindView(R.id.productNameTV)
//    var productNameTV: TextView? = null

//    @BindView(R.id.reasonSecondLL)
//    var reasonSecondLL: LinearLayout? = null

//    @BindView(R.id.reasonFirstLL)
//    var reasonFirstLL: LinearLayout? = null

//    @BindView(R.id.imgMachineIV)
//    var imgMachineIV: ImageView? = null

//    @BindView(R.id.uplaodImageIV)
//    var uplaodImageIV: ImageView? = null

//    @BindView(R.id.imgbearkIV)
//    var imgbearkIV: ImageView? = null

    // - - Initialize Objects
    lateinit var binding: ActivityDemoComplaintBinding
    private var mArrayList = ArrayList<String>()

    var selectedImage: Bitmap? = null
    var mlist = ArrayList<String>()
    var audioPath: String? = ""
    var mPlayer: MediaPlayer? = null
    var currentPhotoPath = ""
    var fileName: String = ""
    var rotatedBitmap: Bitmap? = null
    private val mHandler: Handler? = null
    private var mRunnable: Runnable? = null
    private val mPass: TextView? = null
    private val mDuration: TextView? = null
    private val mDue: TextView? = null
    var clickType = "false"
    var clickType2 = "false"
    var reason1 = ""
    var product_id = ""
    var prod_srNo: String? = ""
    var prod_name: String? = ""
    var phoneNumber: String? = ""
    var mainViewModel: MainViewModel? = null

    var mMultileImagesAL = ArrayList<ImagesModel>()
    lateinit var uploadAdapter: UploadAdapter

    var mRemoveImageInterface =
        RemoveImageInterface { pos, type ->
            imageListInUri!!.removeAt(pos)
            imageCount = 5 - imageListInUri!!.size
            uploadAdapter.notifyItemRemoved(pos)
            uploadAdapter.notifyItemChanged(pos)
            uploadAdapter.notifyItemRangeChanged(pos, imageListInUri!!.size)
        }
    var imageCount = 3
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_demo_complaint)
        binding = ActivityDemoComplaintBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.WHITE)
//        ButterKnife.bind(this)
        setInit()
        //intentData
        getIntentData()

        //MVVM
        val mApiInterface = RestClient.getApiClient().create(
            ApiKotlinInterface::class.java
        )
        val repository = ApisRepository(mApiInterface, mActivity, application)
        mainViewModel = ViewModelProvider(
            this,
            MainViewModelFactory(repository, application)
        ).get(MainViewModel::class.java)
        // Initialize the handler
        val mHandler = Handler()

        // Click listener for playing button
        binding.audioIV?.setOnClickListener { // If media player another instance already running then stop it first
            stopPlaying()

            // Initialize media player
            //   mPlayer = MediaPlayer.create(mActivity,Uri.fromFile(new File(audioPath)));
            val mediaPath =
                Uri.parse("android.resource://$packageName/$audioPath")
            try {
                mPlayer?.setDataSource(applicationContext, mediaPath)
                mPlayer?.prepare()
                mPlayer?.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            // Start the media player
            //   mPlayer.start();
            Toast.makeText(mActivity, "Media Player is playing.", Toast.LENGTH_SHORT).show()

            // Get the current audio stats
            audioStats
            // Initialize the seek bar
            initializeSeekBar()
        }

        /*
            SeekBar.OnSeekBarChangeListener
                A callback that notifies clients when the progress level has been changed. This
                includes changes that were initiated by the user through a touch gesture or
                arrow key/trackball as well as changes that were initiated programmatically.
        */

        // Set a change listener for seek bar
        binding.audioSeekBar?.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            /*
                void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser)
                    Notification that the progress level has changed. Clients can use the fromUser
                    parameter to distinguish user-initiated changes from those that occurred programmatically.

                Parameters
                    seekBar SeekBar : The SeekBar whose progress has changed
                    progress int : The current progress level. This will be in the range min..max
                                   where min and max were set by setMin(int) and setMax(int),
                                   respectively. (The default values for min is 0 and max is 100.)
                    fromUser boolean : True if the progress change was initiated by the user.
            */
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (mPlayer != null && b) {
                    /*
                        void seekTo (int msec)
                            Seeks to specified time position. Same as seekTo(long, int)
                            with mode = SEEK_PREVIOUS_SYNC.

                        Parameters
                            msec int: the offset in milliseconds from the start to seek to

                        Throws
                            IllegalStateException : if the internal player engine has not been initialized
                    */
                    mPlayer?.seekTo(i * 1000)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        selectProductSpinner()
    }

    private fun setInit() {
        binding.uplaodImageIV.setOnClickListener()
        {
            if (imageListInUri!!.size < 3) {
                onCameraClick()
            }
        }

        binding.btnSubmitBT.setOnClickListener {
            preventMultipleClick()
            if (isValidate) {
                Constants.showProgressDialog(mActivity)
                Handler(Looper.getMainLooper()).post {
                    executeAddComplaintApi()
                }
            }

        }
        binding.reasonFirstLL.setOnClickListener()
        {
            onReasonFirstClick()
        }
        binding.reasonSecondLL.setOnClickListener()
        {
            onReasonSecondClick()
        }
        binding.imgAudioIV.setOnClickListener()
        {
            setAudioButtonClick()
        }
        binding.microPhoneLL.setOnClickListener()
        {
            val intent = Intent(this@DemoComplaintActivity, RecordAudioActivity::class.java)
            startActivityForResult(intent, 112)
        }
        binding.imgMenuRL.setOnClickListener()
        {
            onBackPressed()
        }
    }


    private fun getIntentData() {
        if (intent != null) {
            product_id = intent.getStringExtra(Constants.SEL_PRODUCT_ID)!!
            prod_srNo = intent.getStringExtra(Constants.SEL_PRODUCT_SNO)
            prod_name = intent.getStringExtra(Constants.SEL_PRODUCT_NAME)
            phoneNumber = intent.getStringExtra(Constants.PHONE_NUMBER)
            binding.productNameTV.text = prod_name
            binding.proSerialNumtV.text = prod_srNo
            binding.customerNumET.setText(phoneNumber)
//            if(phoneNumber=="" || phoneNumber== null){
//                binding.customerNumET.isEnabled=true
//            }
//            else {
//                binding.customerNumET.isEnabled=false
//            }
        }
    }

    private val inentData: Unit
        private get() {
            if (intent != null) {
                product_id = intent.getStringExtra(Constants.SEL_PRODUCT_ID)!!
                prod_srNo = intent.getStringExtra(Constants.SEL_PRODUCT_SNO)
                prod_name = intent.getStringExtra(Constants.SEL_PRODUCT_NAME)
                phoneNumber = intent.getStringExtra(Constants.PHONE_NUMBER)
                binding.productNameTV?.text = prod_name
                binding.proSerialNumtV!!.text = prod_srNo
                binding.customerNumET!!.setText(phoneNumber)
            }
        }

    protected fun initializeSeekBar() {
        binding.audioSeekBar?.max = mPlayer!!.duration / 1000
        mRunnable = Runnable {
            if (mPlayer != null) {
                val mCurrentPosition = mPlayer!!.currentPosition / 1000 // In milliseconds
                binding.audioSeekBar!!.progress = mCurrentPosition
                audioStats
            }
            mHandler!!.postDelayed(mRunnable!!, 1000)
        }
        mHandler!!.postDelayed(mRunnable!!, 1000)
    }

    // In milliseconds
    protected val audioStats: Unit
        protected get() {
            val duration = mPlayer!!.duration / 1000 // In milliseconds
            val due = (mPlayer!!.duration - mPlayer!!.currentPosition) / 1000
            val pass = duration - due
            mPass!!.text = "$pass seconds"
            mDuration!!.text = "$duration seconds"
            mDue!!.text = "$due seconds"
        }

    protected fun stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer?.stop()
            mPlayer?.release()
            mPlayer = null
            Toast.makeText(mActivity, "Stop playing.", Toast.LENGTH_SHORT).show()
            mHandler?.removeCallbacks(mRunnable!!)
        }
    }

//    @OnClick(
//        R.id.audioPlayPauseClickRL,
//        R.id.imgAudioIV,
//        R.id.imgMenuRL,
//        R.id.btnSubmitBT,
//        R.id.reasonFirstLL,
//        R.id.reasonSecondLL,
//        R.id.addPhotoLL,
//        R.id.microPhoneLL
//    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.audioPlayPauseClickRL -> {}
            R.id.addPhotoLL -> onCameraClick()
            R.id.btnSubmitBT -> {
                preventMultipleClick()
                setSubmitClick()
            }
            R.id.imgMenuRL -> onBackPressed()
            R.id.imgAudioIV -> setAudioButtonClick()
            R.id.microPhoneLL -> {
                val intent = Intent(this@DemoComplaintActivity, RecordAudioActivity::class.java)
                startActivityForResult(intent, 112)
            }
            R.id.reasonFirstLL -> onReasonFirstClick()
            R.id.reasonSecondLL -> onReasonSecondClick()
        }
    }

    fun setSubmitClick() {
        if (isValidate) {
//            mActivity.runOnUiThread(new Runnable() {
//                public void run() {
//                    new AsyncTaskExample().execute();
//                }
//            });
            executeAddComplaintApi()
        }
    }

    fun setAudioButtonClick() {
        val intent = Intent(this@DemoComplaintActivity, RecordAudioActivity::class.java)
        startActivityForResult(intent, 112)
    }

    private fun onReasonSecondClick() {
        binding.imgMachineIV.setImageDrawable(resources.getDrawable(R.drawable.ic_checkbox_unfill))
        clickType = "false"
        if (clickType2 == "false") {
            binding.imgbearkIV.setImageDrawable(resources.getDrawable(R.drawable.ic_chechkbox_fill))
            reason1 = getString(R.string.breakdown_in_machine)
            clickType2 = "true"
        } else {
            binding.imgbearkIV.setImageDrawable(resources.getDrawable(R.drawable.ic_checkbox_unfill))
            reason1 = ""
            clickType2 = "false"
        }
    }

    private fun onReasonFirstClick() {
        binding.imgbearkIV.setImageDrawable(resources.getDrawable(R.drawable.ic_checkbox_unfill))
        clickType2 = "false"
        if (clickType == "false") {
            binding.imgMachineIV.setImageDrawable(resources.getDrawable(R.drawable.ic_chechkbox_fill))
            reason1 = getString(R.string.machine_not_working)
            clickType = "true"
        } else {
            binding.imgMachineIV.setImageDrawable(resources.getDrawable(R.drawable.ic_checkbox_unfill))
            reason1 = ""
            clickType = "false"
        }
    }


    private fun selectProductSpinner() {
        mlist.clear()
        addListData()
        val mColorAd: ArrayAdapter<String?> =
            object : ArrayAdapter<String?>(
                this, android.R.layout.simple_spinner_item,
                mlist as List<String?>
            ) {
                override fun getDropDownView(
                    position: Int,
                    convertView: View,
                    parent: ViewGroup
                ): View {
                    var v = convertView
                    if (v == null) {
                        val mContext = this.context
                        val vi =
                            (mContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        v = vi.inflate(R.layout.item_spinner, null)
                    }
                    val itemTextView = v.findViewById<TextView>(R.id.itemTV)
                    val strName = mlist[position]
                    itemTextView.text = strName
                    return v
                }
            }
        binding.selectProductSpinner?.adapter = mColorAd
        binding.selectProductSpinner?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun addListData() {
        mlist.add("Laser Leveler")
        mlist.add("Spray Pump")
        mlist.add("Mud Loader")
    }

    private fun onCameraClick() {
        if (checkPermission()) {
            openCameraGalleryDialog()
        } else {
            requestPermission()
        }
    }

//    private fun checkPermission(): Boolean {
//        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
//        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
//        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
//        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
//    }
//
//    private fun requestPermission() {
//        ActivityCompat.requestPermissions(
//            mActivity,
//            arrayOf(writeExternalStorage, writeReadStorage, writeCamera),
//            369
//        )
//    }

    /*
     * Update Profile Picture
     * */
    private  fun checkPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
            val readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages)
            readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
        } else {
            val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
            val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
            val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
            write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, arrayOf(readMediaImages, writeCamera), 369)
        } else {
            ActivityCompat.requestPermissions(
                mActivity,
                arrayOf(writeExternalStorage, writeReadStorage, writeCamera),
                369
            )
        }
    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String>,
//        grantResults: IntArray
//    ) {
//        when (requestCode) {
//            369 ->                 // If request is cancelled, the result arrays are empty.
//                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    // onSelectImageClick();
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Log.e(TAG, "**Permission Denied**")
//                }
//        }
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    /* ImageType = 0 is for camera and ImageType = 0 is for photo & video library */
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e("TAG", "**Permission Denied**")
                }
        }
    }


    private fun openCameraGalleryDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder = AlertDialog.Builder(mActivity)
        builder.setTitle("Choose your profile picture")
        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {
                openCamera()
            } else if (options[item] == "Choose from Gallery") {
                performMultiImgClick()
                //  openGallery()
            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun performMultiImgClick() {


        val intent = Lassi(this@DemoComplaintActivity)
            .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA, GALLERY or CAMERA_AND_GALLERY
            .setMaxCount(3)
            .setGridSize(3)
            .setMediaType(MediaType.IMAGE) // MediaType : VIDEO IMAGE, AUDIO OR DOC
            .build()
        receiveData.launch(intent)


       /* // start multiple photos selector
        val intent = Intent(this, ImagesSelectorActivity::class.java)
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false)
        // max number of images to be selected

        if (mMultileImagesAL.size == 0) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3)
        } else if (mMultileImagesAL.size == 1) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2)
        } else if (mMultileImagesAL.size == 2) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1)
        }

        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100)
        // pass current selected images as the initial value
        // intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mArrayList);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE)*/
    }


//    private fun openGallery() {
//        val pictureIntent = Intent(Intent.ACTION_GET_CONTENT)
//        pictureIntent.type = "image/*" // 1
//        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE) // 2
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            val mimeTypes = arrayOf("image/jpeg", "image/png") // 3
//            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
//        }
//        if (pictureIntent != null) {
//            startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), 1) // 4
//        }
//    }



    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as java.util.ArrayList<MiMedia>
                if (!selectedMedia.isNullOrEmpty()) {
                    imageListInUri!!.addAll(selectedMedia)
                    imageCount -= selectedMedia.size
                    initRV()
                }
            }
        }


override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == 112) {
        if (data != null)
            audioPath = data.extras!!.getString(Constants.AUDIO_PATH)
        if (audioPath != "") {
            binding.imgAudioIV.visibility = View.GONE
            binding.microPhoneLL.visibility = View.VISIBLE
            binding.microPathTV.text = audioPath
            Log.e(TAG, "PATH::$audioPath")
        }
    }
    if (requestCode == 2 && resultCode == RESULT_OK) {
        val uri = Uri.parse(currentPhotoPath)
        uri?.let {
            showImage(it)
        }
    }
    else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
        val sourceUri = data.data // 1
        if (data != null) {
            showImage(sourceUri)
        }
    }
}

    fun initRV() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding.uploadRV.layoutManager = layoutManager
        uploadAdapter = UploadAdapter(imageListInUri, mActivity, mRemoveImageInterface)
        binding.uploadRV.adapter = uploadAdapter
    }

    private fun openCamera() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file = imageFile // 1
        val uri: Uri
        uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
           FileProvider.getUriForFile(mActivity, "com.gahiragro.app", file!!)
        else Uri.fromFile(file) // 3
       pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri) // 4
        if (pictureIntent != null) {
            startActivityForResult(pictureIntent, 2)
        }
    }
    private val imageFile: File?
        private get() {
            val imageFileName = "JPEG_" + System.currentTimeMillis() + "_"
            val storageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera"
            )
            if (!storageDir.exists()) {
                storageDir.mkdirs();
            }
            var file: File? = null
            try {
                file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (file != null && file.absolutePath != null) {
                currentPhotoPath = "file:" + file.absolutePath
            }
            return file
        }

    private fun showImage(imageUri: Uri?) {
        Log.e(TAG, "IMAGEVALUE::$imageUri")
        fileName = getRealPathFromURI_API19(mActivity, imageUri)
        Log.e(TAG, "PATH::$fileName")
        var inputStream: InputStream? = null
        try {
            inputStream = FileInputStream(fileName)
        } catch (e: FileNotFoundException) {
            Log.e(TAG, "EXCEPTION::$e")
            e.printStackTrace()
        }
        if (inputStream != null) {
            selectedImage = BitmapFactory.decodeStream(inputStream)
            var ei: ExifInterface? = null
            try {
                ei = ExifInterface(fileName)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val orientation = ei!!.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )
//            rotatedBitmap =
//                when (orientation) {
//                    ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(
//                        selectedImage!!,
//                        90
//                    )
//                    ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(
//                        selectedImage!!,
//                        180
//                    )
//                    ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(
//                        selectedImage!!,
//                        270
//                    )
//                    ExifInterface.ORIENTATION_NORMAL -> selectedImage
//                    else -> selectedImage
//                }


            if (inputStream != null) {
                selectedImage = BitmapFactory.decodeStream(inputStream)
                val mImagesModel = MiMedia()
                mImagesModel.path = fileName
                imageListInUri!!.add(mImagesModel)
                initRV()
            }
        }
    }

    private fun executeAddComplaintApi() {
        var mMultipartBody1: MultipartBody.Part? = null
        var requestFile2: RequestBody? = null
        var mMultipartBodyChanged1: MultipartBody.Part? = null
        var mMultipartBodyChanged2: MultipartBody.Part? = null
        var mMultipartBodyChanged3: MultipartBody.Part? = null

        var requestFileC1: RequestBody? = null
        var requestFileC2: RequestBody? = null
        var requestFileC3: RequestBody? = null


        var mMultipartBody: MultipartBody.Part? = null
        var requestFile: RequestBody? = null


        for (i in mMultileImagesAL.indices) {
            if (mMultileImagesAL!!.size==1 || mMultileImagesAL.size==2 || mMultileImagesAL.size==3) {
                val bitmap1 = BitmapFactory.decodeFile(mMultileImagesAL.get(0).fimeNmae)
                requestFileC1 =
                    convertBitmapToByteArrayUncompressed(bitmap1!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }

                mMultipartBodyChanged1 = requestFileC1?.let {
                    MultipartBody.Part.createFormData(
                        "support_img[0]", getAlphaNumericString()!!.toString() + ".jpeg",
                        it
                    )
                }
            }
         if (mMultileImagesAL.size==2 || mMultileImagesAL.size==3) {
                val bitmap2 = BitmapFactory.decodeFile(mMultileImagesAL.get(1).fimeNmae)
                requestFileC2 =
                    convertBitmapToByteArrayUncompressed(bitmap2!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBodyChanged2 = requestFileC2?.let {
                    MultipartBody.Part.createFormData(
                        "support_img[1]", getAlphaNumericString()!!.toString() + ".jpeg",
                        it
                    )
                }
           }
          if (mMultileImagesAL.size==3) {
                val bitmap3 = BitmapFactory.decodeFile(mMultileImagesAL.get(2).fimeNmae)
                requestFileC3 =
                    convertBitmapToByteArrayUncompressed(bitmap3!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBodyChanged3 = requestFileC3?.let {
                    MultipartBody.Part.createFormData(
                        "support_img[2]", getAlphaNumericString()!!.toString() + ".jpeg",
                        it
                    )
                }
            }
        }


//        if (rotatedBitmap != null) {
//            requestFile = RequestBody.create(
//                MediaType.parse("multipart/form-data"),
//                convertBitmapToByteArrayUncompressed(rotatedBitmap)
//            )
//            mMultipartBody2 = MultipartBody.Part.createFormData(
//                "support_img",
//                "$alphaNumericString.jpg", requestFile
//            )
//        }

        if (audioPath != null && audioPath != "") {
            /* for audio */
            Log.e("TAG", "selectedAudio:::" + audioPath)
            val audioFile = File(audioPath)
            Log.e("TAG", "audioFile::::::" + audioFile)

         //   if(convertFileToByteArray(audioFile) != null) {
                val audioBody: RequestBody = RequestBody.create(
                    "audio/*".toMediaTypeOrNull(), convertFileToByteArray(audioFile)!!
                )
                mMultipartBody1 = MultipartBody.Part.createFormData(
                    "support_audio",
                    getAlphaNumericString()!!.toString() + ".mp3",
                    audioBody
                )
        }
        else{
            mMultipartBody1 = null
            mMultipartBody1 = MultipartBody.Part.createFormData(
                "support_audio","")
        }
        val access_token = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            accessToken
        )
        val contactNum = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            binding.customerNumET.text.toString().trim { it <= ' ' })
        val prodId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), product_id)
        val srNo = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),
            binding.proSerialNumtV.text.toString().trim { it <= ' ' })
        val compReason = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), reason1)
        val reasonDetail = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            binding.fullReasonET.text.toString().trim { it <= ' ' })

        val body = RequestBodies.EditProfileBody(
            mMultipartBodyChanged1,
            mMultipartBodyChanged2,
            mMultipartBodyChanged3,
            mMultipartBody1,
            access_token,
            contactNum,
            prodId,
            srNo,
            compReason,
            reasonDetail
        )
        mainViewModel?.complaintUser(body, mActivity)
        mainViewModel?.complaintData?.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { loginResponse ->
                            if (loginResponse.status.equals("1")) {
                                showToast(mActivity, loginResponse.message)
                                finish();
                            } else {
                                showToast(mActivity, loginResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showToast(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                     //   Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


//        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.addCustomerComplaint(access_token, contactNum, prodId, srNo, compReason, reasonDetail, mMultipartBody2, mMultipartBody1).enqueue(new Callback<CustomerComplainModel>() {
//            @Override
//            public void onResponse(Call<CustomerComplainModel> call, Response<CustomerComplainModel> response) {
//                dismissProgressDialog();
//                CustomerComplainModel mModel = response.body();
//                if (mModel.getStatus().equals("1")) {
//                    showToast(mActivity, mModel.getMessage());
//                    finish();
//                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CustomerComplainModel> call, Throwable t) {
//                showAlertDialog(mActivity, t.toString());
//                dismissProgressDialog();
//            }
//        });
    //        else if (selectedImage == null) {

    //            showAlertDialog(mActivity, getString(R.string.please_select_image));
//            flag = false;
//        }
    /*
    * Set up validations for Sign In fields
    * */
    private val isValidate: Boolean
        private get() {
            Constants.dismissProgressDialog()
            var flag = true
            dismissProgressDialog()
            if (reason1 == "") {
                showAlertDialog(mActivity, getString(R.string.please_select_reason))
                flag = false
            }
//            else if (binding.customerNumET!!.text.toString() == "") {
//                 showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
//                 flag = false
//            }
//            else if(binding.customerNumET!!.text.toString().length < 13){
//                showAlertDialog(mActivity, getString(R.string.please_enter_valid_number));
//                flag = false
//            }
            //        else if (selectedImage == null) {
            //            showAlertDialog(mActivity, getString(R.string.please_select_image));
            //            flag = false;
            //        }
            return flag
        }

    fun convertFileToByteArray(f: File): ByteArray? {
        var byteArray: ByteArray? = null
        try {
            val inputStream: InputStream = FileInputStream(f)
            val bos = ByteArrayOutputStream()
            val b = ByteArray(1024 * 8)
            var bytesRead = 0
            while (inputStream.read(b).also { bytesRead = it } != -1) {
                bos.write(b, 0, bytesRead)
            }
            byteArray = bos.toByteArray()
            } catch (e: Exception) {
            e.printStackTrace()
            showAlertDialog(mActivity,"error : "+e.message)
        }
        return byteArray
    }


//    private inner class AsyncTaskExample :
//        AsyncTask<String?, String?, Void?>() {
//        override fun onPreExecute() {
//            super.onPreExecute()
//            showProgressDialog(mActivity)
//        }
//
//        protected override fun doInBackground(vararg strings: String): Void? {
//            if (isNetworkAvailable(mActivity)) {
//                executeAddComplaintApi()
//            } else {
//                showToast(mActivity, getString(R.string.internet_connection_error))
//            }
//            return null
//        }
//
//        override fun onPostExecute(bitmap: Void?) {
//            super.onPostExecute(bitmap)
//        }
//    }
}