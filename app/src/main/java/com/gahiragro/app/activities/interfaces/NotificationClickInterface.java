package com.gahiragro.app.activities.interfaces;

import com.gahiragro.app.model.CustomerComplaintModel;

public interface NotificationClickInterface {
    public void onNotiClick(String notifyType);
}
