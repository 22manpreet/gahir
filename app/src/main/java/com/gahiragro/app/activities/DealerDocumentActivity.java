package com.gahiragro.app.activities;



import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.gahiragro.app.R;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class DealerDocumentActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealerDocumentActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DealerDocumentActivity.this;
    /**
     * Widgets
     */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    /**
     * Widgets
     */
//    @BindView(R.id.pdfView)
    PDFView pdfView;

//    @BindView(R.id.downloadRL)
    RelativeLayout downloadRL;

//    @BindView(R.id.messageTV)
    TextView messageTV;

//    @BindView(R.id.webView)
//    WebView webView;

//    @BindView(R.id.mainHeaderLL)
    LinearLayout mainHeaderLL;
    String path = "";
    String img = "";
    Intent intent;
    WebView webView;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_document);
//        ButterKnife.bind(this);
        pdfView=findViewById(R.id.pdfView);
        downloadRL=findViewById(R.id.downloadRL);
        messageTV=findViewById(R.id.messageTV);
        mainHeaderLL=findViewById(R.id.mainHeaderLL);


        webView=findViewById(R.id.webView);
       intent=getIntent();
       img = intent.getStringExtra("doc_file");
       Log.e(TAG,"IMG::"+img);
        PRDownloader.initialize(mActivity.getApplicationContext());
        // Enabling database for resume support even after the application is killed:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(mActivity.getApplicationContext(), config);

// Setting timeout globally for the download network requests:
        PRDownloaderConfig configs = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(mActivity, configs);

        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GAHIR/";
        Log.e(TAG, "pathname:" + path);
        File dir = new File(path);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDisplayZoomControls(false);
//        webView.getSettings().setSupportZoom(true);
//        webView.setVerticalScrollBarEnabled(true);
//        webView.getSettings().setAllowFileAccessFromFileURLs(true);
//        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//
//        webView.getSettings().setBuiltInZoomControls(false);
//        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        webView.setScrollbarFadingEnabled(true);
//        webView.clearCache(true);
//        webView.invalidate();
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }
//
//
//        //  webView.loadUrl(GahirPreference.readString(mActivity, GahirPreference.DEALER_DOC, ""));
//
//        //    img = GahirPreference.readString(mActivity, GahirPreference.DEALER_DOC, "");
//
//
//        webView.loadUrl(img);
//


//        img= gson.fromJson(json, type);
        if(img == "") {
            webView.setVisibility(View.GONE);
            messageTV.setVisibility(View.VISIBLE);
        }
        else{
            webView.setVisibility(View.VISIBLE);
            messageTV.setVisibility(View.GONE);
        }
        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermission()) {
                    setDownloadMethod();
                } else {
                    requestPermission();
                }
            }
        });
        showProgressDialog(mActivity);
        showPdfFile();
    }

    private void showPdfFile() {

        webView.invalidate();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            boolean checkOnPageStartedCalled = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                checkOnPageStartedCalled = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (checkOnPageStartedCalled) {
                    dismissProgressDialog();
                } else {
                    showPdfFile();
                }
            }
        });
             System.out.println("img :: "+img);
        String url = "https://docs.google.com/viewer?embedded=true&url=" + img;
            webView.loadUrl(url);

    }


    @Override
    public void onResume() {
        super.onResume();
        backPressClick(mainHeaderLL);
    }
    private void setDownloadMethod() {
        showProgressDialog(mActivity);
        DownloadRequest prDownloader = PRDownloader.download(img, path, "image1" + ".jpg")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                });

        prDownloader.start(new OnDownloadListener() {
            @Override
            public void onDownloadComplete() {
                dismissProgressDialog();
                Toast.makeText(mActivity, "saved Succesfully.", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(Error error) {
                dismissProgressDialog();
                Log.e(TAG, "VALUE::" + error);
                Toast.makeText(mActivity, "error" + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getMD5EncryptedString(String encTarget) {
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while (md5.length() < 32) {
            md5 = "0" + md5;
        }
        return md5;
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED /*&& camera == PackageManager.PERMISSION_GRANTED*/;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
//            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED /*&& camera == PackageManager.PERMISSION_GRANTED*/;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage}, 369);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //    onSelectImageClick();
                    setDownloadMethod();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }
}