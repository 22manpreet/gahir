package com.gahiragro.app.activities.Dealer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.model.CheckWarrantyService;
import com.gahiragro.app.model.ChekWarrentyDetailModel;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckWarrantyValidServiceActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = CheckWarrantyValidServiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CheckWarrantyValidServiceActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.enterSrNoET)
    TextView enterSrNoET;

//    @BindView(R.id.modelNumTV)
    TextView modelNumTV;

//    @BindView(R.id.productIV)
    ImageView productIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_warranty_valid_service);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        enterSrNoET=findViewById(R.id.enterSrNoET);
        modelNumTV=findViewById(R.id.modelNumTV);
        productIV=findViewById(R.id.productIV);
        getIntentData();
        Onclick();
    }
    private void getIntentData() {
        if (getIntent() != null) {
            String prodImg = getIntent().getStringExtra("prodImage");
            enterSrNoET.setText(getIntent().getStringExtra("sr_no"));
            if(getIntent().getStringExtra("warranty").equals("0"))
            {
                modelNumTV.setText(getResources().getString(R.string.warranty_expires));

            }
            else {
                modelNumTV.setText(getIntent().getStringExtra("warranty"));

            }
            Glide.with(mActivity).load(prodImg).into(productIV);


        }
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}