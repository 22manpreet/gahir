package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.DemoComplaintActivity;
import com.gahiragro.app.model.RegProductDetailModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProductDetailActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ProductDetailActivity.this;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.btnOperatorBT)
    Button btnOperatorBT;

//    @BindView(R.id.btnPartsCatBT)
    Button btnPartsCatBT;

//    @BindView(R.id.btnCheckWarrantyBT)
    Button btnCheckWarrantyBT;

//    @BindView(R.id.btnRaiseComplaintBT)
    Button btnRaiseComplaintBT;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.modelNoTV)
    TextView modelNoTV;

//    @BindView(R.id.serialNoTV)
    TextView serialNoTV;


//    @BindView(R.id.systemNoTv)
    TextView systemNoTv;


    // List<CustReviewListModel.AllReview> mProductList = new ArrayList<>();

    String product_id = "";
    String id ="";
    String prod_srNo = "";
    String prod_name = "";
    RegProductDetailModel mModel;
    ArrayList<String> mPdfAL = new ArrayList();
    ArrayList<RegProductDetailModel.Part> mPartAL = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        initViews();
        getIntentData();
        onViewClicked();
    }

    private void initViews() {
        imgMenuRL =findViewById(R.id.imgMenuRL);
        btnOperatorBT =findViewById(R.id.btnOperatorBT);
        btnPartsCatBT =findViewById(R.id.btnPartsCatBT);
        btnCheckWarrantyBT =findViewById(R.id.btnCheckWarrantyBT);
        btnRaiseComplaintBT =findViewById(R.id.btnRaiseComplaintBT);
        productIV =findViewById(R.id.productIV);
        modelNoTV =findViewById(R.id.modelNoTV);
        serialNoTV =findViewById(R.id.serialNoTV);
        systemNoTv =findViewById(R.id.systemNoTv);
    }


//    @OnClick({R.id.imgMenuRL, R.id.btnOperatorBT, R.id.btnPartsCatBT, R.id.btnCheckWarrantyBT, R.id.btnRaiseComplaintBT})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnOperatorBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mModel.getProductDetail().getOpManual()== null){
                    showAlertDialog(mActivity,getString(R.string.no_data_found));
                }
                else {
                    executeOperatorClick();
                }
            }
        });
        btnPartsCatBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executePartsCatClick();
            }
        });
        btnCheckWarrantyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCheckWarrentyClick();
            }
        });
        btnRaiseComplaintBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeRaiseComplaintClick();
            }
        });


       /* switch (view.getId()) {
            case R.id.imgMenuRL:

                break;
            case R.id.btnOperatorBT:
                if(mModel.getProductDetail().getOpManual()== null){
                    showAlertDialog(mActivity,getString(R.string.no_data_found));
                }
                else {
                    executeOperatorClick();
                }
                break;
            case R.id.btnPartsCatBT:
                executePartsCatClick();
                break;
            case R.id.btnCheckWarrantyBT:
                executeCheckWarrentyClick();
                break;
            case R.id.btnRaiseComplaintBT:
                executeRaiseComplaintClick();
                break;
        }*/
    }

    private void executeOperatorClick() {
        Intent intent = new Intent(ProductDetailActivity.this, OperatorManualActivity.class);
        intent.putExtra("OP_Image", mModel.getProductDetail().getProdImage());
        intent.putExtra("PD_Name", mModel.getProductDetail().getOpManual());
        startActivity(intent);
    }

    private void executePartsCatClick() {
        Intent intent = new Intent(ProductDetailActivity.this, PartCatalogugeActivity.class);
        intent.putExtra(Constants.LIST, mPartAL);
        intent.putExtra(Constants.D3_IMAGE, mModel.getProductDetail().get3dImage());
        intent.putExtra(Constants.LEFT, mModel.getProductDetail().getLeft());
        intent.putExtra(Constants.RIGHT, mModel.getProductDetail().getRight());
        intent.putExtra(Constants.BACK, mModel.getProductDetail().getBack());
        intent.putExtra(Constants.FRONT, mModel.getProductDetail().getFront());
        intent.putExtra(Constants.SEL_PRODUCT_ID, mModel.getProductDetail().getId());
        startActivity(intent);
    }

    private void executeCheckWarrentyClick() {
        Intent intent = new Intent(ProductDetailActivity.this, CheckWarrantyActivity.class);
        intent.putExtra(Constants.SEL_PRODUCT_ID, mModel.getProductDetail().getId());
        intent.putExtra(Constants.SEL_PRODUCT_NAME, mModel.getProductDetail().getProdName());
        intent.putExtra(Constants.SEL_PRODUCT_IMAGE, mModel.getProductDetail().getProdImage());
        startActivity(intent);
    }

    private void executeRaiseComplaintClick() {
        String phone_no = mModel.getUserDetail().getPhoneNo();
        Intent intent = new Intent(ProductDetailActivity.this, DemoComplaintActivity.class);
        intent.putExtra(Constants.SEL_PRODUCT_ID,product_id);
        intent.putExtra(Constants.SEL_PRODUCT_SNO,prod_srNo);
        intent.putExtra(Constants.SEL_PRODUCT_NAME,prod_name);
        intent.putExtra(Constants.PHONE_NUMBER,phone_no);
        startActivity(intent);

    }

    private void getIntentData() {
        if (getIntent() != null) {
            id = getIntent().getStringExtra(Constants.SEL_ID);
            product_id = getIntent().getStringExtra(Constants.SEL_PRODUCT_ID);
            prod_srNo = getIntent().getStringExtra(Constants.SEL_PRODUCT_SNO);
            prod_name = getIntent().getStringExtra(Constants.SEL_PRODUCT_NAME);
            if (isNetworkAvailable(mActivity)) {
                executeProductDetailApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    /*
     * Get Product Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAMuuuu**" + mMap.toString());
        return mMap;
    }

    private void executeProductDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.regProductDetailApi(mDetailParam()).enqueue(new Callback<RegProductDetailModel>() {
            @Override
            public void onResponse(Call<RegProductDetailModel> call, Response<RegProductDetailModel> response) {
                dismissProgressDialog();
                mModel = response.body();
                if(mModel.getStatus().equals("1")) {
                    modelNoTV.setText(mModel.getProductDetail().getProdModel());
                    systemNoTv.setText(mModel.getProductDetail().getSystemDetail());
                    serialNoTV.setText(mModel.getProductDetail().getSerialNo());
                    Glide.with(mActivity).load(mModel.getProductDetail().getProdImage()).apply(new RequestOptions().override(600, 200)).into(productIV);
                    mPartAL.addAll(mModel.getProductDetail().getPartList());
                }
                else if(mModel.getStatus().equals("0")){
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<RegProductDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

}