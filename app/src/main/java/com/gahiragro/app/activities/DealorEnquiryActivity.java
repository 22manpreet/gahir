package com.gahiragro.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gahiragro.app.EasyLocationProvider;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.model.AddEnquiryModel;
import com.gahiragro.app.model.ProductDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealorEnquiryActivity extends BaseActivity {
    //Permissions
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;

    /**
     * Getting the Current Class Name
     */
    String TAG = DealorEnquiryActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DealorEnquiryActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.accessorySpinner)
    Spinner accessorySpinner;


//    @BindView(R.id.systemSpinner)
    Spinner systemSpinner;

//    @BindView(R.id.btnSubmit)
    Button btnSubmit;

//    @BindView(R.id.numberChangeTV)
    TextView numberChangeTV;

//    @BindView(R.id.decMentIV)
    ImageView decMentIV;

//    @BindView(R.id.incMentIV)
    ImageView incMentIV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.modelNameTV)
    TextView modelNameTV;

//    @BindView(R.id.modelTV)
    TextView modelTV;
//    @BindView(R.id.systemTV)
    TextView systemTV;
//    @BindView(R.id.modelheadingTV)
    TextView modelheadingTV;


    //Initialize
    int count = 1;
    String id = "";
    String enquiryId = "";
    String strAccessoryName = "", strAccesoryId = "", strSystemName = "", strSystemId = "", prodType = "", prodId = "";
    EasyLocationProvider easyLocationProvider;

    List<ProductDetailModel.Accessory> mAccesoriesAL = new ArrayList();
    List<ProductDetailModel.System> mSystemAL = new ArrayList();
    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(mActivity);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealor_enquiry);
//        ButterKnife.bind(this);
        accessorySpinner=findViewById(R.id.accessorySpinner);
        modelheadingTV=findViewById(R.id.modelheadingTV);
        systemTV=findViewById(R.id.systemTV);
        modelTV=findViewById(R.id.modelTV);
        modelNameTV=findViewById(R.id.modelNameTV);
        productIV=findViewById(R.id.productIV);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        incMentIV=findViewById(R.id.incMentIV);
        decMentIV=findViewById(R.id.decMentIV);
        numberChangeTV=findViewById(R.id.numberChangeTV);
        btnSubmit=findViewById(R.id.btnSubmit);
        systemSpinner=findViewById(R.id.systemSpinner);
        accessorySpinner=findViewById(R.id.accessorySpinner);


        setStatusBar(mActivity, Color.WHITE);
        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ID);
            if (isNetworkAvailable(mActivity)) {
                executeProductDetailApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }

        onViewClicked();

    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.decMentIV, R.id.incMentIV, R.id.btnSubmit, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        decMentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDecrementClick();
            }
        });

        incMentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIncrementClick();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executeAddEnquiryApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
            }
        });

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });


       /* switch (view.getId()) {
            case R.id.decMentIV:
                setDecrementClick();
                break;
            case R.id.incMentIV:
                setIncrementClick();
                break;
            case R.id.btnSubmit:
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executeAddEnquiryApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
                break;
            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void setIncrementClick() {
//        if (count <= 100)
            Log.e("Output", "uri-" + count++);
        String value = String.valueOf(count);
        numberChangeTV.setText(value);
    }

    private void setDecrementClick() {
        if (count > 1) {
            Log.e("Output", "uri-" + count--);
            String value = String.valueOf(count);
            numberChangeTV.setText("" + value);
        } else if (count == 1) {
            Toast.makeText(mActivity, "Quantity cannot be less than 1", Toast.LENGTH_SHORT).show();

        }
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    /*
     * Get Product Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProductDetailApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.productDetailApi(mDetailParam()).enqueue(new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                dismissProgressDialog();
                ProductDetailModel mModel = response.body();
                modelNameTV.setText(mModel.getProductDetail().getProdModel());
                prodType = mModel.getProductDetail().getProdType();
                prodId = mModel.getProductDetail().getId();
                modelTV.setText(mModel.getProductDetail().getProdName());
                Glide.with(mActivity).load(mModel.getProductDetail().getProdImage()).into(productIV);

                if (prodType.equals("1"))                                       //Leveller
                {
                    systemTV.setText(getString(R.string.select_system));
                    modelheadingTV.setText(getString(R.string.model));
                } else if (prodType.equals("2"))                                       //spray pump
                {
                    systemTV.setText(getString(R.string.choose_pump));
                    modelheadingTV.setText(getString(R.string.accesorry));
                } else {
                    systemTV.setText(getString(R.string.select_yout_tractor));
                    modelheadingTV.setText(getString(R.string.model));
                }
                mAccesoriesAL.addAll(mModel.getProductDetail().getAccessories());
                mSystemAL.addAll(mModel.getProductDetail().getSystems());
                setAccesorySpinner();
                setSystemSpinner();
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private void setAccesorySpinner() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "Poppins-Regular.otf");
        ArrayAdapter<ProductDetailModel.Accessory> mAccessoryAd = new ArrayAdapter<ProductDetailModel.Accessory>(mActivity, android.R.layout.simple_spinner_item, mAccesoriesAL) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }
                //HideKey();
                TextView itemTextView = v.findViewById(R.id.itemTV);
                ProductDetailModel.Accessory mModel = mAccesoriesAL.get(position);
                itemTextView.setText(mModel.getAccName());
//                itemTextView.setText(categoriesArraylist[position]);
                return v;
            }

        };
        accessorySpinner.setAdapter(mAccessoryAd);

        accessorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item

                ProductDetailModel.Accessory mModel = mAccesoriesAL.get(position);
                strAccessoryName = mModel.getAccName();
                strAccesoryId = mModel.getId();
                String item = parent.getItemAtPosition(position).toString();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorLightGrey));
                ((TextView) view).setText(item);
                view.setPadding(30, 0, 0, 0);
                ((TextView) view).setTextSize(15);
                ((TextView) view).setTypeface(font);
                accessorySpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSystemSpinner() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "Poppins-Regular.otf");
        ArrayAdapter<ProductDetailModel.System> mSystemAd = new ArrayAdapter<ProductDetailModel.System>(mActivity, android.R.layout.simple_spinner_item, mSystemAL) {

            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                //HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                ProductDetailModel.System mModel = mSystemAL.get(position);
                itemTextView.setText(mModel.getTracName());
//                itemTextView.setText(categoriesArraylist[position]);
                return v;
            }

        };
        systemSpinner.setAdapter(mSystemAd);
        systemSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                ProductDetailModel.System mModel = mSystemAL.get(position);
                strSystemName = mModel.getTracName();
                strSystemId = mModel.getId();


                String item = parent.getItemAtPosition(position).toString();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorLightGrey));
                ((TextView) view).setText(item);
                view.setPadding(30, 0, 0, 0);
                ((TextView) view).setTextSize(15);
                ((TextView) view).setTypeface(font);
                systemSpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*
     * Add Enquiry  Api
     * */
    private Map<String, String> mEnquiryParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("product_id", prodId);
        mMap.put("quantity", numberChangeTV.getText().toString().trim());
        mMap.put("accessory", strAccesoryId);
        mMap.put("system", strSystemId);
        mMap.put("type", prodType);
        mMap.put("remark", "");
        mMap.put("lat", GahirPreference.readString(mActivity, GahirPreference.LATITUDE, ""));
        mMap.put("long", GahirPreference.readString(mActivity, GahirPreference.LONGITUDE, ""));
     //   mMap.put("address",GahirPreference.readString(mActivity,GahirPreference.FULL_SELECTED_LOCATION,""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddEnquiryApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addEnquiry(mEnquiryParam()).enqueue(new Callback<AddEnquiryModel>() {
            @Override
            public void onResponse(Call<AddEnquiryModel> call, Response<AddEnquiryModel> response) {
                dismissProgressDialog();
                AddEnquiryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showEnquiryAlertDialog(mActivity, mModel.getMessage(), "enquiryAdded");
                    enquiryId = mModel.getEnquiry_id();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            Intent intent = new Intent(mActivity, ReadyToDispatchActivity.class);
//                            intent.putExtra(Constants.ENQUIRY_ID, mModel.getEnquiry_id());
//                            startActivity(intent);
//                            finish();
//                        }
//                    }, 2000);


                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                } else {
                    showEnquiryAlertDialog(mActivity, mModel.getMessage(), "enableLocation"); //Using this popUp to enable location if user not set lat lng
                }

            }

            @Override
            public void onFailure(Call<AddEnquiryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private Boolean isValidate() {
        boolean flag = true;
        if (strAccessoryName.equals("Accesory")) {
            showAlertDialog(mActivity, getString(R.string.please_select_accesory));
            flag = false;
        } else if (strSystemName.equals("System")) {
            showAlertDialog(mActivity, getString(R.string.please_select_system));
            flag = false;
        }

        return flag;
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, 222);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 222:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getCurrentLATLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    public void getCurrentLATLong() {
        showProgressDialog(mActivity);
        easyLocationProvider = new EasyLocationProvider.Builder(mActivity)
                .setInterval(100)
                .setFastestInterval(100)
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setListener(new EasyLocationProvider.EasyLocationCallback() {
                    @Override
                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {

                        Log.e("EasyLocationProvider", "onGoogleAPIClient: " + message);
                    }

                    @Override
                    public void onLocationUpdated(double latitude, double longitude) {
                        dismissProgressDialog();
                        //Toast.makeText(getActivity(), "successfull", Toast.LENGTH_SHORT).show();
                        TimeZone tz = TimeZone.getDefault();
                        Log.e(TAG, "TimeZone::" + tz.getDisplayName(false, TimeZone.SHORT) + " Timezone id :: " + tz.getID());
                        Log.e(TAG, "LAT::LNG::" + latitude + longitude);
                        GahirPreference.writeString(mActivity, GahirPreference.LATITUDE, String.valueOf(latitude));
                        GahirPreference.writeString(mActivity, GahirPreference.LONGITUDE, String.valueOf(longitude));

                    }

                    @Override
                    public void onLocationUpdateRemoved() {
                        dismissProgressDialog();
                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
                    }
                }).build();
        getLifecycle().addObserver(easyLocationProvider);
    }

    public void showEnquiryAlertDialog(Activity mActivity, String strMessage, String tag) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (tag.equals("enquiryAdded")) {
                    Intent intent = new Intent(mActivity, ReadyToDispatchActivity.class);
                    intent.putExtra(Constants.ENQUIRY_ID, enquiryId);
                    startActivity(intent);
                    finish();
                } else {
                    requestLocationPermission();
                }

            }
        });
        alertDialog.show();
    }


}