package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.ContactUsModel;
import com.gahiragro.app.model.ForgetPasswordModel;

public class ForgotPasswordActivity extends BaseActivity {
//    @BindView(R.id.emailET)
    EditText emailET;
//    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
//    @BindView(R.id.submitBT)
    Button submitBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
//        ButterKnife.bind(this);
        emailET=findViewById(R.id.emailET);
        emailRL=findViewById(R.id.emailRL);
        submitBT=findViewById(R.id.submitBT);

        editTextSelector(emailET, emailRL, "");
        emailET.setImeOptions(EditorInfo.IME_ACTION_DONE);

        onViewClicked();
    }
    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.submitBT})
    public void onViewClicked(/*View view*/) {

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executeForgetPasswordApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
            }
        });

        /*switch (view.getId()) {
            case R.id.submitBT:
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executeForgetPasswordApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
                break;
        }*/
    }
    /*
     * ForgetPassword Api
     * */
    private void executeForgetPasswordApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.forgetPassword(emailET.getText().toString().trim()).enqueue(new Callback<ForgetPasswordModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordModel> call, Response<ForgetPasswordModel> response) {
                dismissProgressDialog();
                if(response.body()!=null)
                {
                    ForgetPasswordModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        showFinishAlertDialog(mActivity, mModel.getMessage());
                    } else {
                        showAlertDialog(mActivity, mModel.getMessage());
                    }
                }

            }
            @Override
            public void onFailure(Call<ForgetPasswordModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }
    private Boolean isValidate() {
        boolean flag = true;
        if (emailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(emailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        }
        return flag;
    }
}
