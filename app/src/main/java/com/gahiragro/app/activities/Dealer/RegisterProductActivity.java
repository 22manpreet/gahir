package com.gahiragro.app.activities.Dealer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.ChangePasswordActivity;
import com.gahiragro.app.fonts.ButtonPoppins;
import com.gahiragro.app.myModel.GetRegisterProductsModel.RegisterProductModel;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterProductActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RegisterProductActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = RegisterProductActivity.this;

    //    @BindView(R.id.srNoET)
    EditText srNoET;

    //    @BindView(R.id.productIV)
    ImageView productIV;

    ButtonPoppins btnRegisterBT;

    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
    String prodID = "";
    String prodImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_product);
//        ButterKnife.bind(this);
        srNoET = findViewById(R.id.srNoET);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        productIV = findViewById(R.id.productIV);
        btnRegisterBT = findViewById(R.id.btnRegisterBT);
        getIntentData();

        onViewClicked();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            prodID = getIntent().getStringExtra(Constants.SEL_PRODUCT_ID);
            prodImage = getIntent().getStringExtra(Constants.SEL_PRODUCT_IMAGE);
            Glide.with(mActivity).load(prodImage).into(productIV);
        }
    }

    //    @OnClick({R.id.btnRegisterBT,R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        btnRegisterBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performRegisterClick();
            }
        });

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
       /* switch (view.getId()) {
            case R.id.btnRegisterBT:
                performRegisterClick();
                break;
            case R.id.imgMenuRL:
               onBackPressed();
                break;

        }*/
    }

    private void performRegisterClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                registerProductsApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void registerProductsApi() {
        showProgressDialog(mActivity);
        RequestBody access_token = RequestBody.create(MediaType.parse("multipart/form-data"), getAccessToken());
        RequestBody sr_no = RequestBody.create(MediaType.parse("multipart/form-data"), srNoET.getText().toString().trim());
        RequestBody prodId = RequestBody.create(MediaType.parse("multipart/form-data"), prodID);
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.registerProducts(access_token, sr_no, prodId).enqueue(new Callback<RegisterProductModel>() {
            @Override
            public void onResponse(Call<RegisterProductModel> call, Response<RegisterProductModel> response) {
                dismissProgressDialog();
                RegisterProductModel model = response.body();
                if (model.getStatus().equals("1")) {
                    showToast(mActivity, model.getMessage());
                    Intent intent = new Intent(mActivity, RegisterCongratulationActivity.class);
                    intent.putExtra(Constants.SEL_PRODUCT_IMAGE, prodImage);
                    startActivity(intent);
                    srNoET.setText("");

                } else if (model.getStatus().equals("0")) {
                    showAlertDialog(mActivity, model.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterProductModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (srNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_srno));
            flag = false;
        }
        return flag;
    }


}