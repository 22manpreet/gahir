package com.gahiragro.app.activities.interfaces;

public interface LogoutInterface {
    public void onLogoutClick();
}
