package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.adapters.Dealer.DealerListAdapter;
import com.gahiragro.app.model.SearchDealerModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchDealerListActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SearchDealerListActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SearchDealerListActivity.this;

//    @BindView(R.id.dealerlistRV)
    RecyclerView dealerlistRV;

//    @BindView(R.id.imgBackRL)
    RelativeLayout imgBackRL;

//    @BindView(R.id.imgHelpRL)
    RelativeLayout imgHelpRL;

//    @BindView(R.id.LL_bt)
    LinearLayout LL_bt;

//    @BindView(R.id.helplineTV)
    TextView helplineTV;

//    @BindView(R.id.noDataFoundTV)
    TextView noDataFoundTV;

    List<SearchDealerModel.AllUser> searchlist = new ArrayList<>();
    List<SearchDealerModel.AllUser> temSearchlist = new ArrayList<>();

    DealerListAdapter dealerListAdapte;
    int page_no = 1;
    String lastPage = "false";
    String stateName = " ";
    String districtName = " ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_dealer_list);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        dealerlistRV =findViewById(R.id.dealerlistRV);
        imgBackRL =findViewById(R.id.imgBackRL);
        imgHelpRL =findViewById(R.id.imgHelpRL);
        LL_bt =findViewById(R.id.LL_bt);
        helplineTV =findViewById(R.id.helplineTV);
        noDataFoundTV =findViewById(R.id.noDataFoundTV);
        getIntentData();
        performClick();

        onViewClicked();

    }

    private void getIntentData() {
        if (getIntent() != null) {
            districtName = getIntent().getStringExtra(Constants.DISTRICT_SEARCH);
            stateName = getIntent().getStringExtra(Constants.STATE_SEARCH);
        }
    }


    private void performClick() {
        if (isNetworkAvailable(this)) {
            if (searchlist != null)
                searchlist.clear();
            if (temSearchlist != null)
                temSearchlist.clear();

            executeSearchDealer();
        } else {
            showToast(this, getString(R.string.internet_connection_error));
        }
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        mMap.put("district", districtName);
        mMap.put("state", stateName);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSearchDealer() {
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getSearchDealer(mParam()).enqueue(new Callback<SearchDealerModel>() {
            @Override
            public void onResponse(Call<SearchDealerModel> call, Response<SearchDealerModel> response) {
                SearchDealerModel model = response.body();
                LL_bt.setVisibility(View.VISIBLE);
                if (model.getStatus().equals("1")) {
                    noDataFoundTV.setVisibility(View.GONE);
                    helplineTV.setText(model.getHelpline());

                    lastPage = String.valueOf(model.getUserList().getLastPage());

                    if (page_no == 1) {
                        searchlist = model.getUserList().getAllUsers();
                    }
                    if (page_no > 1) {
                        temSearchlist = model.getUserList().getAllUsers();
                    }

                    if (temSearchlist.size() > 0) {
                        searchlist.addAll(temSearchlist);
                    }
                    if (page_no == 1) {
                        initRV();
                    } else {
                        dealerListAdapte.notifyDataSetChanged();
                    }

                } else if (model.getStatus().equals("0")) {
                    //showToast(mActivity, model.getMessage());
                    LL_bt.setVisibility(View.VISIBLE);
                    helplineTV.setText(model.getHelpline());
                    noDataFoundTV.setVisibility(View.VISIBLE);
                    noDataFoundTV.setText(model.getMessage());


                }
            }

            @Override
            public void onFailure(Call<SearchDealerModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        dealerlistRV.setLayoutManager(layoutManager);
        DealerListAdapter dealerListAdapter = new DealerListAdapter(searchlist, this);
        dealerlistRV.setAdapter(dealerListAdapter);

    }

//    @OnClick({R.id.LL_bt, R.id.imgBackRL})
    public void onViewClicked(/*View view*/) {

        LL_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performHelpLineCall();
            }
        });
        imgBackRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


       /* switch (view.getId()) {
            case R.id.LL_bt:
                performHelpLineCall();
                break;
            case R.id.imgBackRL:
                onBackPressed();
                break;
        }*/
    }

    private void performHelpLineCall() {
        String phone = helplineTV.getText().toString().trim();
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

}