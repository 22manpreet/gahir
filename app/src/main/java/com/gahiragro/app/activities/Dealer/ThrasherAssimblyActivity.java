package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.ZoomActivity;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.ThrasherInterface;
import com.gahiragro.app.adapters.Dealer.ThrasherAdapter;
import com.gahiragro.app.model.CartListModel;
import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.model.ThrasherModel;
import com.gahiragro.app.utils.Constants;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ortiz.touchview.TouchImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThrasherAssimblyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ThrasherAssimblyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ThrasherAssimblyActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.thrasherRV)
    RecyclerView thrasherRV;

//    @BindView(R.id.btnAddCartBT)
    Button btnAddCartBT;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.catelogueImg)
    ImageView catelogueImg;

//    @BindView(R.id.partNameTV)
    TextView partNameTV;

//    @BindView(R.id.partName2TV)
    TextView partName2TV;

//    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;

    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;


    private long mLastClickTab1 = 0;
    JSONArray mArray;
    ThrasherAdapter thrasherAdapter;
    ArrayList<ProductPartListModel.Part.SubPart> mSubPartAL = new ArrayList<>();
    ProductPartListModel.Part mPartData;
    List<ThrasherModel> mList = new ArrayList<>();
    ThrasherInterface mThrasherAdapter = new ThrasherInterface() {
        @Override
        public void onThrasherClick(int position, ArrayList<ProductPartListModel.Part.SubPart> mList) {
            mSubPartAL = mList;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thrasher_assimbly);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        thrasherRV=findViewById(R.id.thrasherRV);
        btnAddCartBT=findViewById(R.id.btnAddCartBT);
        productIV=findViewById(R.id.productIV);
        catelogueImg=findViewById(R.id.catelogueImg);
        partNameTV=findViewById(R.id.partNameTV);
        partName2TV=findViewById(R.id.partName2TV);
        txtTitleTV=findViewById(R.id.txtTitleTV);
        getIntentData();
        Onclick();
       catelogueImg.setOnTouchListener(new ImageMatrixTouchHandler(mActivity));
   //     mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        thrasherAdapter.notifyDataSetChanged();
    }
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        return mScaleGestureDetector.onTouchEvent(event);
//    }
//
//    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
//        // when a scale gesture is detected, use it to resize the image
//        @Override
//        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
//            mScaleFactor *= scaleGestureDetector.getScaleFactor();
//            catelogueImg.setScaleX(mScaleFactor);
//            catelogueImg.setScaleY(mScaleFactor);
//            return true;
//        }
//    }

    private void getIntentData() {
        if (getIntent() != null) {
//            mSubPartAL = (ArrayList<ProductPartListModel.Part>) getIntent()
//                    .getSerializableExtra(Constants.LIST);
            mPartData = (ProductPartListModel.Part) getIntent().getSerializableExtra(Constants.LIST);
            Glide.with(mActivity).load(mPartData.getPartImg()).into(productIV);
            Glide.with(mActivity).load(mPartData.getPartCatalogImg()).into(catelogueImg);

            partNameTV.setText(mPartData.getPartName());
            partName2TV.setText(mPartData.getPartName());
            txtTitleTV.setText(mPartData.getPartName());

            mSubPartAL.addAll(mPartData.getSubParts());
            for (int i = 0; i < mSubPartAL.size(); i++) {
                mSubPartAL.get(i).setCount(1);
            }
            initRV();
        }
    }


    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//        productIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, FullImageActivity.class);
//                intent.putExtra(Constants.IMAGE,mPartData.getPartImg());
//                startActivity(intent);
//            }
//        });
//        catelogueImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, FullImageActivity.class);
//                intent.putExtra(Constants.IMAGE,mPartData.getPartCatalogImg());
//                startActivity(intent);
//            }
//        });
        btnAddCartBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSubPartAL.size() > 0) {
                    if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
                        return;
                    }
                    mLastClickTab1 = SystemClock.elapsedRealtime();
                    executeAddToCartApi();
                } else {
                    showAlertDialog(mActivity, getString(R.string.please_select_part_to_add));
                }


            }
        });

    }

    void initRV()
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        thrasherRV.setLayoutManager(layoutManager);
         thrasherAdapter = new ThrasherAdapter(mSubPartAL, this, mThrasherAdapter);
        thrasherRV.setAdapter(thrasherAdapter);
    }

    private JsonObject mPram() {
        JSONObject jsonObject = new JSONObject();
        mArray = new JSONArray();
        JSONObject sIdObject;
        try {
            for (int i = 0; i < mSubPartAL.size(); i++) {
                if (mSubPartAL.get(i).getSubPartId() != null && !mSubPartAL.get(i).getSubPartId().equals("")) {
                    sIdObject = new JSONObject();
                    sIdObject.put("psp_id", mSubPartAL.get(i).getSubPartId());
                    sIdObject.put("qty", mSubPartAL.get(i).getCount());
                    mArray.put(sIdObject);
                }

            }
            jsonObject.put("access_token", getAccessToken());
            jsonObject.put("items", mArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    private void executeAddToCartApi() {
        showProgressDialog(mActivity);
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.addToCartListApi(mPram()).enqueue(new Callback<CartListModel>() {
            @Override
            public void onResponse(Call<CartListModel> call, Response<CartListModel> response) {
                CartListModel mModel = response.body();
                dismissProgressDialog();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    Intent intent = new Intent(ThrasherAssimblyActivity.this, SelectedPartsActivity.class);
                    startActivity(intent);
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CartListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }



}