package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.model.ChekWarrentyDetailModel;

import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class CheckWarrantyValidActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = CheckWarrantyValidActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CheckWarrantyValidActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.modelNumTV)
    TextView modelNumTV;

//    @BindView(R.id.productNameTV)
    TextView productNameTV;

//    @BindView(R.id.productIV)
    ImageView productIV;


    ArrayList<ChekWarrentyDetailModel.Warranty> mWarrentyAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_warranty_valid);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        modelNumTV=findViewById(R.id.modelNumTV);
        productNameTV=findViewById(R.id.productNameTV);
        productIV=findViewById(R.id.productIV);
        getIntentData();
        Onclick();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            String prodImg = getIntent().getStringExtra("prodImage");
            productNameTV.setText(getIntent().getStringExtra("prodName"));
            if(getIntent().getStringExtra("warranty").equals("0"))
            {
                modelNumTV.setText(getResources().getString(R.string.warranty_expires));

            }
            else {
                modelNumTV.setText(getIntent().getStringExtra("warranty"));

            }

            Glide.with(mActivity).load(prodImg).into(productIV);


        }
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
