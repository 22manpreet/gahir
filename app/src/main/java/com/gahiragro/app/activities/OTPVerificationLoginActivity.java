package com.gahiragro.app.activities;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.CustomerPhoneLogin;
import com.gahiragro.app.model.DealorPhoneLoginModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPVerificationLoginActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = OTPVerificationLoginActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = OTPVerificationLoginActivity.this;
    /**
     * Widgets
     */

//    @BindView(R.id.ed1)
    EditText ed1;
//    @BindView(R.id.et1)
    EditText et1;
//    @BindView(R.id.ed2)
    EditText ed2;
//    @BindView(R.id.ed3)
    EditText ed3;
//    @BindView(R.id.ed4)
    EditText ed4;
//    @BindView(R.id.ed5)
    EditText ed5;
//    @BindView(R.id.ed6)
    EditText ed6;
//    @BindView(R.id.verifyBT)
    Button verifyBT;
//    @BindView(R.id.resend_code)
    TextView resend_code;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
//    @BindView(R.id.numberTV)
    TextView numberTV;

    //Initialize
    String mobileNo = "";
    String typeParam = "";
    String strPushToken = "";
    String serial_no = "";

    String phoneNumber = "", newPhoneNumber = "", mVerificationId = "", otp = "", code = "";

    PhoneAuthProvider.ForceResendingToken mResendToken;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_o_t_p_verification);
//        ButterKnife.bind(this);
        ed1 =findViewById(R.id.ed1);
        et1 =findViewById(R.id.et1);
        ed2 =findViewById(R.id.ed2);
        ed3 =findViewById(R.id.ed3);
        ed4 =findViewById(R.id.ed4);
        ed5 =findViewById(R.id.ed5);
        ed6 =findViewById(R.id.ed6);
        verifyBT =findViewById(R.id.verifyBT);
        resend_code =findViewById(R.id.resend_code);
        imgMenuRL =findViewById(R.id.imgMenuRL);
        numberTV =findViewById(R.id.numberTV);


        if (getIntent() != null) {
            newPhoneNumber = getIntent().getExtras().getString(Constants.PHONE_NUMBER);
            Log.e(TAG, "VALUE" + phoneNumber);
            numberTV.setText(newPhoneNumber);
        }
        ed1.setText(null);
        ed2.setText(null);
        ed3.setText(null);
        ed4.setText(null);
        ed5.setText(null);
        ed6.setText(null);

        ed1.addTextChangedListener(new GenericTextWatcher(ed1));
        ed2.addTextChangedListener(new GenericTextWatcher(ed2));
        ed3.addTextChangedListener(new GenericTextWatcher(ed3));
        ed4.addTextChangedListener(new GenericTextWatcher(ed4));
        ed5.addTextChangedListener(new GenericTextWatcher(ed5));
        ed6.addTextChangedListener(new GenericTextWatcher(ed6));
        getPushToken();
        sendVerificationCode();

        onViewClicked();
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        GahirPreference.writeString(mActivity, GahirPreference.DEVICE_TOKEN, strPushToken);
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });

    }

    private void sendVerificationCode() {
        showProgressDialog(mActivity);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                newPhoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            dismissProgressDialog();
            Log.d(TAG, "onVerificationCompleted:" + credential);

            code = credential.getSmsCode();

            if (code != null) {

                String c1, c2, c3, c4, c5, c6;

                c1 = code.substring(0, 1);
                c2 = code.substring(1, 2);
                c3 = code.substring(2, 3);
                c4 = code.substring(3, 4);
                c5 = code.substring(4, 5);
                c6 = code.substring(5, 6);

                ed1.setText(c1);
                ed2.setText(c2);
                ed3.setText(c3);
                ed4.setText(c4);
                ed5.setText(c5);
                ed6.setText(c6);

//               verifying the code
                if (!isNetworkAvailable(mActivity)) {
                    showToast(mActivity, getString(R.string.internet_connection_error));
                } else {
                    verifyVerificationCode(code);
                }
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            dismissProgressDialog();
            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
                showToast(mActivity, "The SMS quota for this number has been exceeded.");
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            dismissProgressDialog();
            Log.d(TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...
        }
    };

    private void verifyVerificationCode(String otp) {

        showProgressDialog(mActivity);

        if (mVerificationId != null && !mVerificationId.equals("")) {

            //creating the credential
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            //signing the user
            signInWithPhoneAuthCredential(credential);

        } else {
            dismissProgressDialog();
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            dismissProgressDialog();

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            Intent i = new Intent(mActivity, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finishAffinity();


                        } else {

                            dismissProgressDialog();

                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
                            }
                        }
                    }
                });
    }

    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.ed1:
                    if (text.length() == 1) {
                        ed2.requestFocus();

                    }
                    break;

                case R.id.ed2:
                    if (text.length() == 1) {
                        ed3.requestFocus();
                    } else if (text.length() == 0) {
                        ed1.requestFocus();
                    }
                    break;

                case R.id.ed3:
                    if (text.length() == 1) {
                        ed4.requestFocus();
                    } else if (text.length() == 0) {
                        ed2.requestFocus();
                    }
                    break;

                case R.id.ed4:
                    if (text.length() == 1) {
                        ed5.requestFocus();
                    } else if (text.length() == 0) {
                        ed3.requestFocus();
                    }
                    break;

                case R.id.ed5:
                    if (text.length() == 1) {
                        ed6.requestFocus();
                    } else if (text.length() == 0) {
                        ed4.requestFocus();
                    }
                    break;

                case R.id.ed6:
                    if (text.length() == 1) {
                        verifyBT.requestFocus();
                    } else if (text.length() == 0) {
                        ed5.requestFocus();
                    }
                    break;
            }
        }


        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.verifyBT, R.id.imgMenuRL, R.id.resend_code})
    public void onViewClicked(/*View view*/) {
        verifyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSubmitClick();
            }
        });
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performResendClick();
            }
        });


       /* switch (view.getId()) {
            case R.id.verifyBT:
                performSubmitClick();
                break;
            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.resend_code:
                performResendClick();
                break;

        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseSignOut();

    }

    private void performSubmitClick() {
        validate();
    }

    private void performResendClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            showToast(mActivity, getString(R.string.new_otp_sent_on_registered_number));
            et1.setText(null);
            ed1.setText(null);
            ed2.setText(null);
            ed3.setText(null);
            ed4.setText(null);
            ed5.setText(null);
            ed6.setText(null);
            ed1.requestFocus();
            showProgressDialog(mActivity);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    newPhoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks, mResendToken);        // OnVerificationStateChangedCallbacks
        }
    }


    private void FirebaseSignOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }

    private void validate() {

        //   otp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString() + ed5.getText().toString() + ed6.getText().toString();
        otp = et1.getText().toString().trim();
        if (!otp.isEmpty()) {

            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                verifyVerificationCode(otp);
            }

        } else {

            showAlertDialog(mActivity, getString(R.string.please_enter_code));
        }
    }

    private Map<String, String> mPhoneLoginParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("phone", newPhoneNumber);
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


}
