package com.gahiragro.app.activities.Dealer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.BookingDetailActivity;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class CheckRewardsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = CheckRewardsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CheckRewardsActivity.this;
    /**
     * Widgets
     */

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_rewards);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        Onclick();
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}