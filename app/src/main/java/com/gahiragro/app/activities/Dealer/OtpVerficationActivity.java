package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.model.ImagesModel;
import com.gahiragro.app.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.lassi.data.media.MiMedia;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class OtpVerficationActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = OtpVerficationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = OtpVerficationActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.btnSubmitBT)
    Button btnSubmitBT;

//    @BindView(R.id.et1)
    EditText et1;

//    @BindView(R.id.et2)
    EditText et2;

//    @BindView(R.id.et3)
    EditText et3;

//    @BindView(R.id.et4)
    EditText et4;

//    @BindView(R.id.et5)
    EditText et5;

//    @BindView(R.id.et6)
    EditText et6;

    PhoneAuthProvider.ForceResendingToken mResendToken;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String phoneNumber = "", newPhoneNumber = "", mVerificationId = "", otp = "", code = "", appSignUp = "";
    ArrayList<MiMedia> imagesChanges;
    ArrayList<MiMedia> imagesRepair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verfication);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);

        imgMenuRL =findViewById(R.id.imgMenuRL);
        btnSubmitBT =findViewById(R.id.btnSubmitBT);
        et1 =findViewById(R.id.et1);
        et2 =findViewById(R.id.et2);
        et3 =findViewById(R.id.et3);
        et4 =findViewById(R.id.et4);
        et5 =findViewById(R.id.et5);
        et6 =findViewById(R.id.et6);

        Onclick();
        getIntentData();
        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));
        et5.addTextChangedListener(new GenericTextWatcher(et5));
        et6.addTextChangedListener(new GenericTextWatcher(et6));

    }

    private void getIntentData() {
        if (getIntent() != null) {
            String number = getIntent().getStringExtra(Constants.PHONE_NUMBER);
            Log.e(TAG,"NUMBER::"+number);
            imagesChanges = (ArrayList<MiMedia>) getIntent().getSerializableExtra(Constants.PARTS_CHANGED_IMAGES);
            imagesRepair = (ArrayList<MiMedia>) getIntent().getSerializableExtra(Constants.PARTS_REPAIR_IMAGES);
            sendVerificationCode( number);
        }
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnSubmitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSubmitClick();

            }
        });
    }

    // for otp edit text apply
    public class GenericTextWatcher implements TextWatcher {
        private final View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et1:
                    if (text.length() == 1) {
                        et2.requestFocus();
                    }
                    break;

                case R.id.et2:
                    if (text.length() == 1) {
                        et3.requestFocus();
                    } else if (text.length() == 0) {
                        et1.requestFocus();
                    }
                    break;

                case R.id.et3:
                    if (text.length() == 1) {
                        et4.requestFocus();
                    } else if (text.length() == 0) {
                        et2.requestFocus();
                    }
                    break;


                case R.id.et4:
                    if (text.length() == 1) {
                        et5.requestFocus();
                    } else if (text.length() == 0) {
                        et3.requestFocus();
                    }
                    break;

                case R.id.et5:
                    if (text.length() == 1) {
                        et6.requestFocus();
                    } else if (text.length() == 0) {
                        et4.requestFocus();
                    }
                    break;

                case R.id.et6:
                    if (text.length() == 1) {
                        btnSubmitBT.requestFocus();
                    } else if (text.length() == 0) {
                        et5.requestFocus();
                    }
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    private void performSubmitClick() {
          validate();
      //  setVerifyClick();

    }

    private void validate() {
        //   otp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString() + ed5.getText().toString() + ed6.getText().toString();
        otp = et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString() + et5.getText().toString() + et6.getText().toString().trim();
        if (!otp.isEmpty()) {

            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                verifyVerificationCode(otp);
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_enter_code));
        }
    }

    private void sendVerificationCode(String sNo) {
        showProgressDialog(mActivity);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                sNo,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    private final PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            dismissProgressDialog();
            Log.d(TAG, "onVerificationCompleted:" + credential);

            code = credential.getSmsCode();

            if (code != null) {
                String c1, c2, c3, c4, c5, c6;
                c1 = code.substring(0, 1);
                c2 = code.substring(1, 2);
                c3 = code.substring(2, 3);
                c4 = code.substring(3, 4);
                et1.setText(c1);
                et1.setText(c2);
                et1.setText(c3);
                et1.setText(c4);
//               verifying the code
                if (!isNetworkAvailable(mActivity)) {
                    showToast(mActivity, getString(R.string.internet_connection_error));
                } else {
                    verifyVerificationCode(code);
                }
            }
        }
        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            dismissProgressDialog();
            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                showToast(mActivity, "The SMS quota for this number has been exceeded.");
            }

            // Show a message and update the UI
        }
        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            dismissProgressDialog();
            Log.d(TAG, "onCodeSent:" + verificationId);
            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...
        }
    };
    private void verifyVerificationCode(String otp) {
        showProgressDialog(mActivity);
        if (mVerificationId != null && !mVerificationId.equals("")) {
            //creating the credential
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            //signing the user
            signInWithPhoneAuthCredential(credential);
        } else {
            dismissProgressDialog();
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
        }
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            dismissProgressDialog();
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            setVerifyClick();

                        } else {
                            dismissProgressDialog();
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
                            }
                        }
                    }
                });
    }
    private void setVerifyClick() {
        Intent intent = new Intent(OtpVerficationActivity.this, ClickSelfieActivity.class);
        intent.putExtra(Constants.OTP, et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString() + et5.getText().toString() + et6.getText().toString().trim());
        intent.putExtra(Constants.PARTS_CHANGED_IMAGES,imagesChanges );
        intent.putExtra(Constants.PARTS_REPAIR_IMAGES, imagesRepair);
        startActivity(intent);
        finish();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseSignOut();
    }
    private void FirebaseSignOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }
}