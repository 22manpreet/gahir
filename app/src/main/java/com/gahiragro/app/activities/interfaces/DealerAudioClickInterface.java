package com.gahiragro.app.activities.interfaces;

import com.gahiragro.app.model.AllComplainsItem;
import com.gahiragro.app.model.CustomerComplaintModel;

public interface DealerAudioClickInterface {
    public void onAudioClick(int position, AllComplainsItem mModel);
}
