package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.AdminLoginModel;
import com.gahiragro.app.model.ChangePasswordModel;
import com.gahiragro.app.model.ForgetPasswordModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChangePasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ChangePasswordActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.et_old_password)
    EditText et_old_password;
//    @BindView(R.id.et_new_password)
    EditText et_new_password;
//    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;
//    @BindView(R.id.doneBT)
    Button doneBT;
//    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
//        ButterKnife.bind(this);
        et_old_password=findViewById(R.id.et_old_password);
        et_new_password=findViewById(R.id.et_new_password);
        et_confirm_password=findViewById(R.id.et_confirm_password);
        doneBT=findViewById(R.id.doneBT);
        imgMenuIV=findViewById(R.id.imgMenuIV);

        setStatusBar(mActivity, Color.WHITE);

        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuIV, R.id.doneBT})
    public void onViewClicked(/*View view*/) {

        imgMenuIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        doneBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDoneClick();
            }
        });

      /*  switch (view.getId()) {
            case R.id.imgMenuIV:

                break;
            case R.id.doneBT:
                setDoneClick();
                break;


        }*/
    }

    private void setDoneClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeChangePassswordApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }

    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    /*
     * Phone Login Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("password", et_new_password.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeChangePassswordApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.changePassword(mParam()).enqueue(new Callback<ChangePasswordModel>() {
            @Override
            public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                dismissProgressDialog();
                ChangePasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    GahirPreference.writeString(mActivity,Constants.PASSWORD_VALUE,et_new_password.getText().toString().trim());
                    showToast(mActivity, getString(R.string.password_change_successfully));
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }

    public boolean isValidate() {
        boolean flag = true;
      if (et_new_password.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password));
            flag = false;
        }
        else if (et_confirm_password.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password));
            flag = false;
        }
        else if (!et_new_password.getText().toString().trim().equals(et_confirm_password.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_mismatch));
            flag = false;
        }

        else if (et_new_password.getText().toString().trim().length()< 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit));
            flag = false;
        }
        else if (et_confirm_password.getText().toString().trim().length()< 8) {
          showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit));
            flag = false;
        }

        return flag;
    }

}