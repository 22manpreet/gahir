package com.gahiragro.app.activities;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class CustomerSignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = CustomerSignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CustomerSignUpActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.serail_noET)
    EditText serail_noET;

//    @BindView(R.id.mobileNoET)
    EditText mobileNoET;

//    @BindView(R.id.btngenerateOTPBT)
    Button btngenerateOTPBT;


//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;


    //Initailize
    String strPushToken = "";
    CountryCodePicker ccp;
    String countryCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_sign_up);
//        ButterKnife.bind(this);
        serail_noET =findViewById(R.id.serail_noET);
        mobileNoET =findViewById(R.id.mobileNoET);
        btngenerateOTPBT =findViewById(R.id.btngenerateOTPBT);
        imgMenuRL =findViewById(R.id.imgMenuRL);

        setStatusBar(mActivity, Color.WHITE);
        //Refresh
        mobileNoET.setText("");
        serail_noET.setText("");
        setCountryCodePicker();
        getPushToken();

        onViewClicked();
    }

    private void setCountryCodePicker() {
        ccp = findViewById(R.id.ccp);
        ccp.enableHint(false);
//        ccp.setTextColor(getResources().getColor(R.color.white));
        ccp.registerPhoneNumberTextView(mobileNoET);
        ccp.setDefaultCountryUsingNameCodeAndApply("IN");
        Log.e(TAG, "Code== " + ccp.getSelectedCountryCode());

        countryCode = ccp.getSelectedCountryCodeWithPlus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });
    }


    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.btngenerateOTPBT, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        btngenerateOTPBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performGenerateOtpClick();
            }
        });

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });


       /* switch (view.getId()) {
            case R.id.btngenerateOTPBT:
                performGenerateOtpClick();
                break;

            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void performGenerateOtpClick() {
        if (isValidate()) {
            Intent i = new Intent(mActivity, OTPVerificationLoginActivity.class);
            i.putExtra(Constants.PHONE_NUMBER, countryCode + mobileNoET.getText().toString().trim());
            i.putExtra(Constants.LOGIN_NUMBER, "");
            startActivity(i);
        }
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (mobileNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (!isValidMobile(mobileNoET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_number));
            flag = false;
        } else if (serail_noET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_serial_number));
            flag = false;
        }

        return flag;
    }


}