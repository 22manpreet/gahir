package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class InvoiceActivity extends AppCompatActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = InvoiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = InvoiceActivity.this;


    //Permissions
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;

    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performbackClick();
            }
        });

        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                performbackClick();
                break;
        }*/
    }

    private void performbackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
