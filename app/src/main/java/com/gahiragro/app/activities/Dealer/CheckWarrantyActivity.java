package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.model.ChekWarrentyDetailModel;
import com.gahiragro.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckWarrantyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = CheckWarrantyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CheckWarrantyActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.btnCheckBT)
    Button btnCheckBT;

//    @BindView(R.id.productNameTV)
    TextView productNameTV;

//    @BindView(R.id.productIV)
    ImageView productIV;

    //Initialize
    String selProdId = "";
    String selProdName = "";
    String selProdImage = "";
ArrayList<ChekWarrentyDetailModel.Warranty> mWarrantyAL = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_warranty);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL =findViewById(R.id.imgMenuRL);
        btnCheckBT =findViewById(R.id.btnCheckBT);
        productNameTV =findViewById(R.id.productNameTV);
        productIV =findViewById(R.id.productIV);

        getIntentData();

        onViewClicked();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            selProdId = getIntent().getStringExtra(Constants.SEL_PRODUCT_ID);
            selProdName = getIntent().getStringExtra(Constants.SEL_PRODUCT_NAME);
            selProdImage = getIntent().getStringExtra(Constants.SEL_PRODUCT_IMAGE);
            productNameTV.setText(selProdName);
            Glide.with(mActivity).load(selProdImage).placeholder(R.drawable.dummy_img_product).into(productIV);
        }

    }


//    @OnClick({R.id.imgMenuRL, R.id.btnCheckBT})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCheckBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCheckWarrClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                onBackPressed();
                break;
            case R.id.btnCheckBT:
                setCheckWarrClick();
                break;
        }*/
    }

    private void setCheckWarrClick() {
        if (isNetworkAvailable(mActivity)) {
            executeCheckWarrentyApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    /*
     * Get Product Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("product_id", selProdId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCheckWarrentyApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.checkWarrantyApi(mDetailParam()).enqueue(new Callback<ChekWarrentyDetailModel>() {
            @Override
            public void onResponse(Call<ChekWarrentyDetailModel> call, Response<ChekWarrentyDetailModel> response) {
                dismissProgressDialog();
                ChekWarrentyDetailModel mModel = response.body();
                Intent intent = new Intent(CheckWarrantyActivity.this, CheckWarrantyValidActivity.class);
                intent.putExtra("warranty", mModel.getWarranty().getWarranty());
                intent.putExtra("prodImage" ,mModel.getWarranty().getProductDetail().getProdImage());
                intent.putExtra("prodName" ,mModel.getWarranty().getProductDetail().getProdName());
                startActivity(intent);
            }
            @Override
            public void onFailure(Call<ChekWarrentyDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }


}