package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.FilterReviewListInterface;
import com.gahiragro.app.activities.interfaces.FilterReviewStateInterface;
import com.gahiragro.app.adapters.Dealer.StateAdapter;
import com.gahiragro.app.model.StateDistrictModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.List;

//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatesListingActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = StatesListingActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = StatesListingActivity.this;

    RecyclerView stateRV;
    StateAdapter stateAdapter;
    RelativeLayout imgBackRL;
    TextView txtDoneTV;

    List<StateDistrictModel.StateListItem> mStateList = new ArrayList<>();

    // for state_id get

    String  state_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_states_listing);

//        ButterKnife.bind(this);

        setStatusBar(mActivity, Color.WHITE);
        stateRV = findViewById(R.id.stateRV);
        imgBackRL = findViewById(R.id.imgBackRL);
        txtDoneTV = findViewById(R.id.txtDoneTV);

        executeStateList();
        Onclick();
    }

    void Onclick() {

        imgBackRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtDoneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isNetworkAvailable(mActivity)) {


                    if (state_id.equals("")) {
                        showAlertDialog(mActivity, getString(R.string.please_select_state));
                    } else {
                        GahirPreference.writeString(mActivity, Constants.FILTER_TYPE_ID, state_id);
                        GahirPreference.writeString(mActivity, Constants.FILTER_TYPE, "state");
                        Constants.FILTER_BACK_PRESSED = true;
                        onBackPressed();
                    }

                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }

            }
        });
    }

    private void executeStateList() {
        showProgressDialog(mActivity);

        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getStateDistList().enqueue(new Callback<StateDistrictModel>() {
            @Override
            public void onResponse(Call<StateDistrictModel> call, Response<StateDistrictModel> response) {
                dismissProgressDialog();

                StateDistrictModel stateDistrictModel = response.body();
                if (stateDistrictModel.getStatus().equals("1")) {

                    // set state list
                    mStateList.addAll(stateDistrictModel.getStateList());

                    // set adapter data
                    initRV();

                } else {

                    showAlertDialog(StatesListingActivity.this, stateDistrictModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<StateDistrictModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(StatesListingActivity.this, RecyclerView.VERTICAL, false);
        stateRV.setLayoutManager(layoutManager);
        stateAdapter = new StateAdapter(mStateList, this, filterReviewStateInterface);
        stateRV.setAdapter(stateAdapter);
    }

    FilterReviewStateInterface filterReviewStateInterface = new FilterReviewStateInterface() {
        @Override
        public void onFilterReview(int position, List<String> mNewList, String stateIds) {

            Log.e(TAG, "**ERROR**" + mNewList.toString());

            state_id = TextUtils.join(",", mNewList);


        }
    };


}