package com.gahiragro.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;

import com.gahiragro.app.model.AddOrderModel;
import com.gahiragro.app.model.enquirydetails.EnquiryDetailModelM;
import com.gahiragro.app.model.enquirymodel.EnquiryModel;
import com.gahiragro.app.utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealorBookOrderActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealorBookOrderActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = DealorBookOrderActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.dealorCodeTV)
    TextView dealorCodeTV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.nameOfProductTV)
    TextView nameOfProductTV;

//    @BindView(R.id.modelTV)
    TextView modelTV;

//    @BindView(R.id.accesoriesTV)
    TextView accesoriesTV;

//    @BindView(R.id.qtyTV)
    TextView qtyTV;

//    @BindView(R.id.amountTV)
    TextView amountTV;

//    @BindView(R.id.utrNoET)
    EditText utrNoET;

//    @BindView(R.id.btnSubmitB)
    Button btnSubmitB;
//    @BindView(R.id.amountET)
    EditText amountET;

//    @BindView(R.id.remarksET)
    EditText remarksET;

    //Initialize
    String id = "";
    String enquiryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealor_book_order);
//        ButterKnife.bind(this);
        remarksET=findViewById(R.id.remarksET);
        amountET=findViewById(R.id.amountET);
        btnSubmitB=findViewById(R.id.btnSubmitB);
        utrNoET=findViewById(R.id.utrNoET);
        amountTV=findViewById(R.id.amountTV);
        qtyTV=findViewById(R.id.qtyTV);
        accesoriesTV=findViewById(R.id.accesoriesTV);
        modelTV=findViewById(R.id.modelTV);
        nameOfProductTV=findViewById(R.id.nameOfProductTV);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        dealorCodeTV=findViewById(R.id.dealorCodeTV);

        amountET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                amountET.removeTextChangedListener(this);

                try {
                    String givenstring = s.toString();
                    Long longval;
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replaceAll(",", "");
                    }
                    longval = Long.parseLong(givenstring);
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString = formatter.format(longval);
                    amountET.setText(formattedString);
                    amountET.setSelection(amountET.getText().length());
                    // to place the cursor at the end of text
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                amountET.addTextChangedListener(this);
            }
        });
        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ID);
            if (isNetworkAvailable(mActivity)) {
                executeGetEnquiryDetailApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }


        onViewClicked();
    }

    private Map<String, String> mEnquiryParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetEnquiryDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.enquiryDetailApi(mEnquiryParam()).enqueue(new Callback<EnquiryDetailModelM>() {
            @Override
            public void onResponse(Call<EnquiryDetailModelM> call, Response<EnquiryDetailModelM> response) {
                dismissProgressDialog();
                EnquiryDetailModelM mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    dealorCodeTV.setText(mModel.getEnquiryDetail().getDealer_code());
                    modelTV.setText(mModel.getEnquiryDetail().getProdName());
                    nameOfProductTV.setText(mModel.getEnquiryDetail().getProdName());
                    accesoriesTV.setText(mModel.getEnquiryDetail().getAccName());
                    qtyTV.setText(mModel.getEnquiryDetail().getQty());
                    amountTV.setText("$" + mModel.getEnquiryDetail().getTotal());
                    enquiryId = mModel.getEnquiryDetail().getEnquiryId();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
                // mModel.getEnquiryDetail().
                // showAlertDialog(mActivity, mModel.getMessage());
            }

            @Override
            public void onFailure(Call<EnquiryDetailModelM> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (amountET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_amount));
            flag = false;
        } else if (utrNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_utr_no));
            flag = false;
        }
        return flag;
    }

    private Map<String, String> mOrderParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("enquiry_id", enquiryId);
        mMap.put("utr_no", utrNoET.getText().toString().trim());
        mMap.put("amount", amountET.getText().toString().trim());
        mMap.put("remark", remarksET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddOrderApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addOrderApi(mOrderParam()).enqueue(new Callback<AddOrderModel>() {
            @Override
            public void onResponse(Call<AddOrderModel> call, Response<AddOrderModel> response) {
                dismissProgressDialog();
                AddOrderModel mModel = response.body();
                HomeActivity.isBack = true;
                if (mModel.getStatus().equals("1")) {
                       showThankyouAlertDialog(mActivity,mModel.getMessage());
                   // showNextAlertDialog(mActivity, mModel.getMessage());
                } else {
                    showFinishAlertDialog(mActivity, mModel.getMessage());
                }


            }

            @Override
            public void onFailure(Call<AddOrderModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.btnSubmitB, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performAddOrderClick();
            }
        });
        /*switch (view.getId()) {
            case R.id.btnSubmitB:
                performAddOrderClick();
                break;
            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void performAddOrderClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeAddOrderApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        HomeActivity.isBack = true;
    }
    public void showNextAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity,HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_MASS, "BookOrderActivity");
                startActivity(intent);
            }
        });
        alertDialog.show();
    }
}