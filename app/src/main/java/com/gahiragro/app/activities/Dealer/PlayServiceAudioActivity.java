package com.gahiragro.app.activities.Dealer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.PlayAudioActivity;
import com.gahiragro.app.model.CustomerComplaintModel;
import com.gahiragro.app.model.ServiceComplaintModel;
import com.gahiragro.app.utils.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class PlayServiceAudioActivity extends BaseActivity implements MediaPlayer.OnCompletionListener{
    /**
     * Getting the Current Class Name
     */
    String TAG = PlayServiceAudioActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = PlayServiceAudioActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.cancelIV)
    ImageView cancelIV;

//    @BindView(R.id.forwardIV)
    ImageView forwardIV;

//    @BindView(R.id.backwardIV)
    ImageView backwardIV;

//    @BindView(R.id.audioSeekBar)
    SeekBar audioSeekBar;

//    @BindView(R.id.mViewProgressIV)
    ProgressBar mViewProgressIV;


//    @BindView(R.id.audioPlayingDurationTV)
    TextView audioPlayingDurationTV;

//    @BindView(R.id.audioDurationTV)
    TextView audioDurationTV;

//    @BindView(R.id.audioIV)
    ImageView audioIV;

//    @BindView(R.id.mainViewRL)
    RelativeLayout mainViewRL;


    //INitialize
    ServiceComplaintModel.ComplainList.AllComplain  mModel;
    SeekBar audioTimerSB;
    TextView startTimerTV;
    TextView endTimerTV;
    ImageView imgPlayPauseAudioIV;
    MediaPlayer mp = null;
    String audioType = "";
    private boolean isPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_service_audio);
        cancelIV =findViewById(R.id.cancelIV);
        mainViewRL =findViewById(R.id.mainViewRL);
        audioIV =findViewById(R.id.audioIV);
        audioDurationTV =findViewById(R.id.audioDurationTV);
        audioPlayingDurationTV =findViewById(R.id.audioPlayingDurationTV);
        mViewProgressIV =findViewById(R.id.mViewProgressIV);
        audioSeekBar =findViewById(R.id.audioSeekBar);
        backwardIV =findViewById(R.id.backwardIV);
        forwardIV =findViewById(R.id.forwardIV);
        cancelIV =findViewById(R.id.cancelIV);

//        ButterKnife.bind(this);
        setStatusBar(mActivity, Color.WHITE);
        mp = new MediaPlayer();
        getIntentData();
        audioTimerSB = audioSeekBar;
        startTimerTV = audioPlayingDurationTV;
        endTimerTV = audioDurationTV;
        imgPlayPauseAudioIV = audioIV;

        setUpMediaPlayer(mActivity, audioSeekBar, audioPlayingDurationTV);
        PlayAudio(
                mActivity,
                mModel.getComplainDetail().getSupportAudio(),
                "play",
                audioSeekBar,
                audioPlayingDurationTV,
                audioIV,
                audioDurationTV,
                mViewProgressIV);


        onViewClicked();

    }
//    @OnClick({R.id.audioIV,R.id.forwardIV,R.id.backwardIV,R.id.cancelIV})
    public void onViewClicked(/*View view*/) {
        audioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAudioClick();
            }
        });

        forwardIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performForwardAudioClick();
            }
        });


        backwardIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performBackwardAudioClick();
            }
        });

        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCancelAudioClick();
            }
        });


        /*switch (view.getId())
        {
            case R.id.audioIV:
                setAudioClick();
                break;
            case R.id.forwardIV:
                performForwardAudioClick();
                break;
            case R.id.backwardIV:
                performBackwardAudioClick();
                break;
            case R.id.cancelIV:
                setCancelAudioClick();
                break;
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        StopMediaPlayer(audioSeekBar, audioPlayingDurationTV, audioIV);
    }
    @Override
    protected void onResume() {
        super.onResume();

        //To Refresh homeActivity as calandar data was not refreshing, so Refresh it this way
        mainViewRL.setFocusableInTouchMode(true);
        mainViewRL.requestFocus();
        mainViewRL.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    StopMediaPlayer(audioSeekBar, audioPlayingDurationTV, audioIV);
                    return true;
                }
                return false;
            }
        });

    }
    private void setCancelAudioClick() {
        StopMediaPlayer(audioSeekBar, audioPlayingDurationTV, audioIV);
    }
    private void setAudioClick() {
        if (audioType == "stop") {
            PlayAudio(
                    mActivity,
                    mModel.getComplainDetail().getSupportAudio(),
                    "play",
                    audioSeekBar,
                    audioPlayingDurationTV,
                    audioIV,
                    audioDurationTV,
                    mViewProgressIV);
        } else {
            performPlayPauseAudioClick();
        }
    }


    private void getIntentData() {
        if (getIntent() != null) {
            mModel  = (ServiceComplaintModel.ComplainList.AllComplain) getIntent().getSerializableExtra(Constants.model);
        }
    }
    private void setUpMediaPlayer(Activity mActivity, SeekBar audioSeekBar, TextView audioPlayingDurationTV) {
        Handler mHandler = new Handler();
        //Make sure you update Seekbar on UI thread
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mp != null) {
                    if (mp.isPlaying() == true) {
                        int mCurrentPosition = mp.getCurrentPosition() / 1000;
                        int mFileDuration = mp.getDuration();
                        if (mp.getDuration() != 0) {
                            audioSeekBar.setMax(mFileDuration / 1000);

                        }
                        audioSeekBar.setProgress(mCurrentPosition);
                        audioPlayingDurationTV.setText(setTime(mCurrentPosition));
                    } else {
                        if (audioType == "pause") {
                            int mCurrentPosition = mp.getCurrentPosition() / 1000;
                            audioSeekBar.setProgress(mCurrentPosition);
                            audioPlayingDurationTV.setText(setTime(mCurrentPosition));
                        }
                    }
                }
                mHandler.postDelayed(this, 1000);
            }
        });
        audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mp != null && fromUser) {
                    mp.seekTo(progress * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
    public String setTime(int sec) {
        Date d = new Date(sec * 1000L);
        SimpleDateFormat df = new SimpleDateFormat("mm:ss");// HH for 0-23
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return df.format(d);
    }

    private void PlayAudio(
            Activity activity, String supportAudio, String type, SeekBar audioSeekBar, TextView audioPlayingDurationTV, ImageView audioIV, TextView audioDurationTV, ProgressBar mViewProgressIV)
    {
        if (type == "play") {
            if (supportAudio != "") {

                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

                try {
                    mp.reset();
                    mp.setDataSource(activity, Uri.parse(supportAudio));
                    mp.prepareAsync();
                    audioIV.setImageResource(R.drawable.ic_pause_audio);

                } catch (IllegalArgumentException e) {
                    Toast.makeText(
                            activity,
                            "You might not set the URI correctly!",
                            Toast.LENGTH_SHORT
                    ).show();
                } catch (SecurityException e) {
                    Toast.makeText(
                            activity,
                            "You might not set the URI correctly!",
                            Toast.LENGTH_SHORT
                    ).show();
                } catch (IllegalStateException e) {
                    Toast.makeText(
                            activity,
                            "You might not set the URI correctly!",
                            Toast.LENGTH_SHORT
                    ).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (mp != null) {
                        if (!mp.isPlaying()) {
                            mp.start();
                            isPlaying = true;
                            audioType = "play";
                            if (mp.getDuration() != 0) {
                                int mFileDuration = mp.getDuration();
                                audioSeekBar.setMax(mFileDuration / 1000);
                                mViewProgressIV.setVisibility(View.GONE);
                                audioDurationTV.setVisibility(View.VISIBLE);
                                audioPlayingDurationTV.setVisibility(View.VISIBLE);
                                audioDurationTV.setText(setTime(mp.getDuration() / 1000));

                            }
                        }
                    }
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    StopMediaPlayer(audioSeekBar, audioPlayingDurationTV, audioIV);
                }
            });

        } else if (type == "pause") {
            if (mp != null) {
                if (mp.isPlaying()) {
                    mp.pause();
                    audioIV.setImageResource(R.drawable.ic_play_audio);
                } else {
                    if (mp != null) {
                        StopMediaPlayer(audioSeekBar, audioPlayingDurationTV, audioIV);
                    }
                }
            }
        }
    }

    private void StopMediaPlayer(SeekBar audioSeekBar, TextView audioPlayingDurationTV, ImageView audioIV) {
        audioSeekBar.setProgress(0);
        audioType = "stop";
        audioPlayingDurationTV.setText("00:00");
        audioIV.setImageResource(R.drawable.ic_play_audio);
        mp.stop();
        finish();
    }


    private void performPlayPauseAudioClick() {
        if (isPlaying) {
            pauseAudio();
        } else if (!isPlaying) {
            playAudio();
        }
    }


    public void pauseAudio() {
        if (mp != null) {
            mp.pause();
            audioType = "pause";
            isPlaying = false;
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play_audio);
        }
    }

    private void playAudio() {
        if (mp != null) {
            mp.start();
            audioType = "play";
            isPlaying = true;
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_pause_audio);
        }
    }


    private void performForwardAudioClick() {
        if (mp != null && mp.getDuration() - mp.getCurrentPosition() > 5000) {
            int currentAudioDuration = mp.getCurrentPosition() + 5000;
            mp.seekTo(currentAudioDuration);
        }
    }

    private void performBackwardAudioClick() {
        if (mp != null && mp.getCurrentPosition() > 5000) {
            int currentAudioDuration = mp.getCurrentPosition() - 5000;
            mp.seekTo(currentAudioDuration);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        try {
            mp.pause();
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play_audio);
            isPlaying = false;
            mp.reset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}