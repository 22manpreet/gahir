package com.gahiragro.app.activities.interfaces;

import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.model.ThrasherModel;

import java.util.ArrayList;
import java.util.List;

public interface ThrasherInterface {
    public void onThrasherClick(int position,  ArrayList<ProductPartListModel.Part.SubPart> mList);
}
