package com.gahiragro.app.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;

import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.LocationModel;
import com.gahiragro.app.model.getOrderListMOdel.EnquiryDetail;
import com.gahiragro.app.model.orderDetailModel.OrderDetailModel;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.gahiragro.app.utils.GahirPreference;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class BookingDetailActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = BookingDetailActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = BookingDetailActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
//    @BindView(R.id.productIV)
    ImageView productIV;
//    @BindView(R.id.modelNameTV)
    TextView modelNameTV;
//    @BindView(R.id.bookingIdTV)
    TextView bookingIdTV;
//    @BindView(R.id.modelTV)
    TextView modelTV;

//    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;

//    @BindView(R.id.bookedDateTV)
    TextView bookedDateTV;

    //Initialize
    String id = "";
    EnquiryDetail mModel;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);
//        ButterKnife.bind(this);
        setStatusBar(mActivity, Color.WHITE);
        imgMenuRL =findViewById(R.id.imgMenuRL);
        productIV =findViewById(R.id.productIV);
        modelNameTV =findViewById(R.id.modelNameTV);
        bookingIdTV =findViewById(R.id.bookingIdTV);
        modelTV =findViewById(R.id.modelTV);
        txtTitleTV =findViewById(R.id.txtTitleTV);
        bookedDateTV =findViewById(R.id.bookedDateTV);

        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ORDER_ID);
            if (isNetworkAvailable(mActivity)) {
                executeOrderDetailApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
            if (getIntent().getExtras().getString(Constants.TYPE) != null) {
                type = getIntent().getExtras().getString(Constants.TYPE);
                txtTitleTV.setText(getString(R.string.order_details));
            } else {
                txtTitleTV.setText(getString(R.string.my_order_details));
            }

        }
        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

      /*  switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;
        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Location  Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeOrderDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.orderDetailApi(mParam()).enqueue(new Callback<OrderDetailModel>() {
            @Override
            public void onResponse(Call<OrderDetailModel> call, Response<OrderDetailModel> response) {
                dismissProgressDialog();
                OrderDetailModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Glide.with(mActivity).load(mModel.getOrderDetail().getEnquiryDetail().getProductDetail().getProdImage()).
                            placeholder(R.drawable.gahir_logo_dummy).
                            into(productIV);
                    modelNameTV.setText(mModel.getOrderDetail().getEnquiryDetail().getProductDetail().getProdName());
                    bookingIdTV.setText(mModel.getOrderDetail().getBookingId());

                    bookedDateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getOrderDetail().getEnquiryDetail().getCreationDate())));
                //    bookedDateTV.setText();
                    modelTV.setText(mModel.getOrderDetail().getEnquiryDetail().getProductDetail().getProdModel());
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OrderDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    public String getDateCurrentTimeZone(long timestamp) {
        //convert seconds to milliseconds
        Date date = new Date(timestamp * 1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("dd MMM yyyy ");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return jdf.format(date);

    }


}