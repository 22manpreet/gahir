package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class FullImageActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = FullImageActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = FullImageActivity.this;
//    @BindView(R.id.image1)
    ImageView image1;
//    @BindView(R.id.closeIV)
    ImageView closeIV;
    private float[] lastTouchDownXY = new float[2];
    List<Point> points = new ArrayList<Point>();
    List<Integer> pointDummy = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
//        ButterKnife.bind(this);
        image1=findViewById(R.id.image1);
        closeIV=findViewById(R.id.closeIV);

        image1.setOnTouchListener(new ImageMatrixTouchHandler(mActivity));
        if (getIntent() != null) {
            String img = getIntent().getExtras().getString(Constants.IMAGE);
            Glide.with(mActivity).
                    load(img)
                    .placeholder(R.drawable.gahir_logo_dummy)
                    .into(image1);
        }

//        image1.setOnTouchListener(touchListener);
//        image1.setOnClickListener(clickListener);


        onViewClicked();

    }
//    // the purpose of the touch listener is just to store the touch X,Y coordinates
//    View.OnTouchListener touchListener = new View.OnTouchListener() {
//        @Override
//        public boolean onTouch(View v, MotionEvent event) {
//
//            // save the X,Y coordinates
//            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
//                lastTouchDownXY[0] = event.getX();
//                lastTouchDownXY[1] = event.getY();
//            }
//
//            // let the touch event pass on to whoever needs it
//            return false;
//        }
//    };
//
//    View.OnClickListener clickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            // retrieve the stored coordinates
//            int x = (int) lastTouchDownXY[0];
//            int y = (int) lastTouchDownXY[1];
//            points.add(new Point(x,y));
//            for(int i=0;i<pointDummy.size();i++)
//            {
////                if(pointDummy.get(i).equals(x,y))
////                {
////                    Toast.makeText(getApplicationContext(), "great", Toast.LENGTH_SHORT).show();
////                }
//            }
//            // use the coordinates for whatever
//            Log.e("TAG", "onLongClick: x = " + x + ", y = " + y);
//            Log.e("TAG", "size:::" + points.size());
//        }
//    };
    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.closeIV})
    public void onViewClicked(/*View view*/) {

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCloseClick();
            }
        });

       /* switch (view.getId()) {
            case R.id.closeIV:
                setCloseClick();
                break;
        }*/
    }

    private void setCloseClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
