package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.adapters.Dealer.AddProductsAdapter;
import com.gahiragro.app.model.GetRegisterProductModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductsActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = AddProductsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddProductsActivity.this;


//    @BindView(R.id.addProductsRV)
    RecyclerView addProductsRV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    AddProductsAdapter addProductsAdapter;
    String lastPage = "false";
    List<GetRegisterProductModel.AllProduct> mGetProductsList = new ArrayList<>();
    List<GetRegisterProductModel.AllProduct> mTemGetProductsAL = new ArrayList<>();
    int page_no = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_products);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        addProductsRV=findViewById(R.id.addProductsRV);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        executeGetProductsApi();
        OnClick();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Constants.FILTER_BACK_PRESSED){
            Constants.FILTER_BACK_PRESSED = false;
            finish();
        }
    }

    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetProductsApi() {

        if (page_no == 1) {

            showProgressDialog(mActivity);

        } else if (page_no > 1) {
            // mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getRegisterProucts(mPram()).enqueue(new Callback<GetRegisterProductModel>() {
            @Override
            public void onResponse(Call<GetRegisterProductModel> call, Response<GetRegisterProductModel> response) {

                if (page_no == 1) {
                    dismissProgressDialog();
                }
                GetRegisterProductModel getRegisterProductsModel = response.body();
                if (getRegisterProductsModel.getStatus().equals("1")) {
                    lastPage = getRegisterProductsModel.getProductList().getLastPage();

                    if (page_no == 1) {
                        mGetProductsList = getRegisterProductsModel.getProductList().getAllProducts();
                    } else if (page_no > 1) {
                        mTemGetProductsAL = getRegisterProductsModel.getProductList().getAllProducts();
                    }
                    if (mTemGetProductsAL.size() > 0) {
                        mGetProductsList.addAll(mTemGetProductsAL);
                    }
                    if (page_no == 1) {
                        initRV();
                    } else {
                        addProductsAdapter.notifyDataSetChanged();
                    }

                } else if (getRegisterProductsModel.getStatus().equals("100")) {
                    showToast(mActivity, getRegisterProductsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetRegisterProductModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });

    }

    private void OnClick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        addProductsRV.setLayoutManager(layoutManager);
        addProductsAdapter = new AddProductsAdapter(mGetProductsList, this);
        addProductsRV.setAdapter(addProductsAdapter);

    }
}