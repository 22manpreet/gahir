package com.gahiragro.app.activities.Dealer;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.coach.app.activities.viewModel.MainViewModel;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.utils.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RaiseComplaintActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RaiseComplaintActivity.this.getClass().getSimpleName();
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;

    /*
     * Current Activity Instance
     */
    Activity mActivity = RaiseComplaintActivity.this;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.imgAudioIV)
    ImageView imgAudioIV;


//    @BindView(R.id.selectProductRL)
    RelativeLayout selectProductRL;

//    @BindView(R.id.btnSubmitBT)
    Button btnSubmitBT;

//    @BindView(R.id.selectProductSpinner)
    Spinner selectProductSpinner;

//    @BindView(R.id.audioPlayPauseClickRL)
    RelativeLayout audioPlayPauseClickRL;


//    @BindView(R.id.audioIV)
    ImageView audioIV;

//    @BindView(R.id.audioSeekBar)
    SeekBar audioSeekBar;

//    @BindView(R.id.audioPlayingDurationTV)
    TextView audioPlayingDurationTV;

//    @BindView(R.id.addPhotoLL)
    LinearLayout addPhotoLL;

//    @BindView(R.id.imageTV)
    TextView imageTV;

//    @BindView(R.id.microPhoneLL)
    LinearLayout microPhoneLL;

//    @BindView(R.id.microPathTV)
    TextView microPathTV;

//    @BindView(R.id.customerNumET)
    EditText customerNumET;


//    @BindView(R.id.fullReasonET)
    EditText fullReasonET;

//    @BindView(R.id.proSerialNumtV)
    TextView proSerialNumtV;


//    @BindView(R.id.productNameTV)
    TextView productNameTV;

//    @BindView(R.id.reasonSecondLL)
    LinearLayout reasonSecondLL;

//    @BindView(R.id.reasonFirstLL)
    LinearLayout reasonFirstLL;

//    @BindView(R.id.imgMachineIV)
    ImageView imgMachineIV;

//    @BindView(R.id.imgbearkIV)
    ImageView imgbearkIV;


    Bitmap selectedImage = null;
    ArrayList<String> mlist = new ArrayList<String>();
    String audioPath = "";
    MediaPlayer mPlayer;
    String currentPhotoPath = "";
    String fileName;
    Bitmap rotatedBitmap = null;
    private Handler mHandler;
    private Runnable mRunnable;
    private TextView mPass;
    private TextView mDuration;
    private TextView mDue;
    String clickType = "false";
    String clickType2 = "false";
    String reason1 = "";
    String product_id = "";
    String prod_srNo = "";
    String prod_name = "";
    String phoneNumber = "";
    MainViewModel mainViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raise_complaint);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        imgAudioIV=findViewById(R.id.imgAudioIV);
        selectProductRL=findViewById(R.id.selectProductRL);
        imgbearkIV=findViewById(R.id.imgbearkIV);
        imgMachineIV=findViewById(R.id.imgMachineIV);
        reasonFirstLL=findViewById(R.id.reasonFirstLL);
        reasonSecondLL=findViewById(R.id.reasonSecondLL);
        productNameTV=findViewById(R.id.productNameTV);
        proSerialNumtV=findViewById(R.id.proSerialNumtV);
        fullReasonET=findViewById(R.id.fullReasonET);
        customerNumET=findViewById(R.id.customerNumET);
        microPathTV=findViewById(R.id.microPathTV);
        microPhoneLL=findViewById(R.id.microPhoneLL);
        imageTV=findViewById(R.id.imageTV);
        addPhotoLL=findViewById(R.id.addPhotoLL);
        audioPlayingDurationTV=findViewById(R.id.audioPlayingDurationTV);
        audioSeekBar=findViewById(R.id.audioSeekBar);
        audioIV=findViewById(R.id.audioIV);
        audioPlayPauseClickRL=findViewById(R.id.audioPlayPauseClickRL);
        selectProductSpinner=findViewById(R.id.selectProductSpinner);
        btnSubmitBT=findViewById(R.id.btnSubmitBT);
        getInentData();
onViewClicked();
        // Initialize the handler
        Handler mHandler = new Handler();

        // Click listener for playing button
        audioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // If media player another instance already running then stop it first
                stopPlaying();

                // Initialize media player
                //   mPlayer = MediaPlayer.create(mActivity,Uri.fromFile(new File(audioPath)));
                Uri mediaPath = Uri.parse("android.resource://" + getPackageName() + "/" + audioPath);
                try {
                    mPlayer.setDataSource(getApplicationContext(), mediaPath);
                    mPlayer.prepare();
                    mPlayer.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Start the media player
                //   mPlayer.start();
                Toast.makeText(mActivity, "Media Player is playing.", Toast.LENGTH_SHORT).show();

                // Get the current audio stats
                getAudioStats();
                // Initialize the seek bar
                initializeSeekBar();

            }
        });

        /*
            SeekBar.OnSeekBarChangeListener
                A callback that notifies clients when the progress level has been changed. This
                includes changes that were initiated by the user through a touch gesture or
                arrow key/trackball as well as changes that were initiated programmatically.
        */

        // Set a change listener for seek bar
        audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            /*
                void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser)
                    Notification that the progress level has changed. Clients can use the fromUser
                    parameter to distinguish user-initiated changes from those that occurred programmatically.

                Parameters
                    seekBar SeekBar : The SeekBar whose progress has changed
                    progress int : The current progress level. This will be in the range min..max
                                   where min and max were set by setMin(int) and setMax(int),
                                   respectively. (The default values for min is 0 and max is 100.)
                    fromUser boolean : True if the progress change was initiated by the user.
            */
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mPlayer != null && b) {
                    /*
                        void seekTo (int msec)
                            Seeks to specified time position. Same as seekTo(long, int)
                            with mode = SEEK_PREVIOUS_SYNC.

                        Parameters
                            msec int: the offset in milliseconds from the start to seek to

                        Throws
                            IllegalStateException : if the internal player engine has not been initialized
                    */
                    mPlayer.seekTo(i * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        selectProductSpinner();
    }

    private void getInentData() {

        if (getIntent() != null) {
            product_id = getIntent().getStringExtra(Constants.SEL_PRODUCT_ID);
            prod_srNo = getIntent().getStringExtra(Constants.SEL_PRODUCT_SNO);
            prod_name = getIntent().getStringExtra(Constants.SEL_PRODUCT_NAME);
            phoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
            productNameTV.setText(prod_name);
            proSerialNumtV.setText(prod_srNo);
            customerNumET.setText(phoneNumber);
        }
    }

    protected void initializeSeekBar() {
        audioSeekBar.setMax(mPlayer.getDuration() / 1000);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlayer != null) {
                    int mCurrentPosition = mPlayer.getCurrentPosition() / 1000; // In milliseconds
                    audioSeekBar.setProgress(mCurrentPosition);
                    getAudioStats();
                }
                mHandler.postDelayed(mRunnable, 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 1000);
    }

    protected void getAudioStats() {
        int duration = mPlayer.getDuration() / 1000; // In milliseconds
        int due = (mPlayer.getDuration() - mPlayer.getCurrentPosition()) / 1000;
        int pass = duration - due;

        mPass.setText("" + pass + " seconds");
        mDuration.setText("" + duration + " seconds");
        mDue.setText("" + due + " seconds");
    }

    protected void stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
            Toast.makeText(mActivity, "Stop playing.", Toast.LENGTH_SHORT).show();
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }
        }
    }


//    @OnClick({R.id.audioPlayPauseClickRL, R.id.imgAudioIV, R.id.imgMenuRL, R.id.btnSubmitBT, R.id.reasonFirstLL, R.id.reasonSecondLL, R.id.addPhotoLL, R.id.microPhoneLL})
    public void onViewClicked(/*View view*/) {

        audioPlayPauseClickRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        addPhotoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCameraClick();
            }
        });
        btnSubmitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventMultipleClick();
                setSubmitClick();
            }
        });
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgAudioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAudioButtonClick();
            }
        });
        microPhoneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RaiseComplaintActivity.this, RecordAudioActivity.class);
                startActivityForResult(intent, 112);
            }
        });
        reasonFirstLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReasonFirstClick();
            }
        });
        reasonSecondLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReasonSecondClick();
            }
        });

        /*switch (view.getId()) {
            case R.id.audioPlayPauseClickRL:
                break;
            case R.id.addPhotoLL:
                onCameraClick();
                break;
            case R.id.btnSubmitBT:
                preventMultipleClick();
                setSubmitClick();
                break;

            case R.id.imgMenuRL:
                onBackPressed();
                break;

            case R.id.imgAudioIV:
                setAudioButtonClick();
                break;

            case R.id.microPhoneLL:
                Intent intent = new Intent(RaiseComplaintActivity.this, RecordAudioActivity.class);
                startActivityForResult(intent, 112);
                break;
            case R.id.reasonFirstLL:
                onReasonFirstClick();
                break;
            case R.id.reasonSecondLL:
                onReasonSecondClick();
                break;
        }*/
    }

    public void setSubmitClick() {
        if (isValidate()) {
//            mActivity.runOnUiThread(new Runnable() {
//                public void run() {
//                    new AsyncTaskExample().execute();
//                }
//            });
            executeAddComplaintApi();
        }
    }

    public void setAudioButtonClick() {
        Intent intent = new Intent(RaiseComplaintActivity.this, RecordAudioActivity.class);
        startActivityForResult(intent, 112);
    }

    private void onReasonSecondClick() {
        imgMachineIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_unfill));
        clickType = "false";
        if (clickType2.equals("false")) {
            imgbearkIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_chechkbox_fill));
            reason1 = getString(R.string.breakdown_in_machine);
            clickType2 = "true";
        } else {
            imgbearkIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_unfill));
            reason1 = "";
            clickType2 = "false";
        }

    }


    private void onReasonFirstClick() {
        imgbearkIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_unfill));
        clickType2 = "false";
        if (clickType.equals("false")) {
            imgMachineIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_chechkbox_fill));
            reason1 = getString(R.string.machine_not_working);
            clickType = "true";
        } else {
            imgMachineIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_unfill));
            reason1 = "";
            clickType = "false";
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 112) {
            if (data != null)
                audioPath = data.getExtras().getString(Constants.AUDIO_PATH);
            if (!audioPath.equals("")) {
                imgAudioIV.setVisibility(View.GONE);
                microPhoneLL.setVisibility(View.VISIBLE);
                microPathTV.setText(audioPath);
                Log.e(TAG, "PATH::" + audioPath);


            }
        }
        if (requestCode == 0 && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(currentPhotoPath);
            if (uri != null) {
                showImage(uri);
            }


        } else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Uri sourceUri = data.getData(); // 1
            if (data != null) {
                showImage(sourceUri);
            }
        }
    }

    private void selectProductSpinner() {
        mlist.clear();

        addListData();


        ArrayAdapter<String> mColorAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mlist) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mlist.get(position);
                itemTextView.setText(strName);

                return v;
            }
        };

        selectProductSpinner.setAdapter(mColorAd);
        selectProductSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    void addListData() {
        mlist.add("Laser Leveler");
        mlist.add("Spray Pump");
        mlist.add("Mud Loader");
    }

    private void onCameraClick() {
        if (checkPermission()) {
            openCameraGalleryDialog();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, writeCamera}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    // onSelectImageClick();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    private void openCameraGalleryDialog() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    openCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    openGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openGallery() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");  // 1
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);  // 2
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};  // 3
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        if (pictureIntent != null) {
            startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), 1);  // 4

        }
    }

    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getImageFile(); // 1
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
            uri = FileProvider.getUriForFile(mActivity, "com.gahiragro.app", file);
        else
            uri = Uri.fromFile(file); // 3
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
        if (pictureIntent != null) {
            startActivityForResult(pictureIntent, 0);

        }
    }

    private File getImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        File file = null;
        try {
            file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file != null && file.getAbsolutePath() != null) {
            currentPhotoPath = "file:" + file.getAbsolutePath();
        }

        return file;
    }

    private void showImage(Uri imageUri) {

        Log.e(TAG, "IMAGEVALUE::" + imageUri);
        fileName = getRealPathFromURI_API19(mActivity, imageUri);
        Log.e(TAG, "PATH::" + fileName);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "EXCEPTION::" + e.toString());
            e.printStackTrace();
        }
        imageTV.setText(fileName);

        if (inputStream != null) {
            selectedImage = BitmapFactory.decodeStream(inputStream);
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:

                    rotatedBitmap = rotateImage(selectedImage, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(selectedImage, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(selectedImage, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = selectedImage;
            }

        }

    }

    private void executeAddComplaintApi() {
        //  showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        RequestBody requestFile = null;
        RequestBody requestFile2 = null;
        if (rotatedBitmap != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(rotatedBitmap));
            mMultipartBody2 = MultipartBody.Part.createFormData("support_img", getAlphaNumericString() + ".jpg", requestFile);
        }
        if (audioPath != null && !audioPath.equals("")) {
            File propertyImageFile = new File(audioPath);
            requestFile2 = RequestBody.create(MediaType.parse("audio/mp3"), convertFileToByteArray(propertyImageFile));
            mMultipartBody1 = MultipartBody.Part.createFormData("support_audio", propertyImageFile.getName(), requestFile2);

        }
        RequestBody access_token = RequestBody.create(MediaType.parse("multipart/form-data"), getAccessToken());
        RequestBody contactNum = RequestBody.create(MediaType.parse("multipart/form-data"), customerNumET.getText().toString().trim());
        RequestBody prodId = RequestBody.create(MediaType.parse("multipart/form-data"), product_id);
        RequestBody srNo = RequestBody.create(MediaType.parse("multipart/form-data"), proSerialNumtV.getText().toString().trim());
        RequestBody compReason = RequestBody.create(MediaType.parse("multipart/form-data"), reason1);
        RequestBody reasonDetail = RequestBody.create(MediaType.parse("multipart/form-data"), fullReasonET.getText().toString().trim());


        //mainViewModel.raiseComplaintParams(mMultipartBody1, mMultipartBody2, access_token, contactNum, prodId, srNo, compReason, reasonDetail);

//        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.addCustomerComplaint(access_token, contactNum, prodId, srNo, compReason, reasonDetail, mMultipartBody2, mMultipartBody1).enqueue(new Callback<CustomerComplainModel>() {
//            @Override
//            public void onResponse(Call<CustomerComplainModel> call, Response<CustomerComplainModel> response) {
//                dismissProgressDialog();
//                CustomerComplainModel mModel = response.body();
//                if (mModel.getStatus().equals("1")) {
//                    showToast(mActivity, mModel.getMessage());
//                    finish();
//                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CustomerComplainModel> call, Throwable t) {
//                showAlertDialog(mActivity, t.toString());
//                dismissProgressDialog();
//            }
//        });
    }


    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (customerNumET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (reason1.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_reason));
            flag = false;
        }
//        else if (selectedImage == null) {
//            showAlertDialog(mActivity, getString(R.string.please_select_image));
//            flag = false;
//        }
        return flag;
    }

    public byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try { /*from w  ww.  j  a  v a 2  s .c  om*/
            FileInputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;
            for (int readNum; (readNum = inputStream.read(b)) != -1; ) {
                bos.write(b, 0, readNum);
            }
            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }


    private class AsyncTaskExample extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(String... strings) {

            if (isNetworkAvailable(mActivity)) {
                executeAddComplaintApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void bitmap) {
            super.onPostExecute(bitmap);

        }
    }
}