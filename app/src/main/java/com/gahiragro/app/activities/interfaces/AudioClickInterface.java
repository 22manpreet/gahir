package com.gahiragro.app.activities.interfaces;

import com.gahiragro.app.model.CustomerComplaintModel;
import com.gahiragro.app.model.ServiceComplaintModel;

public interface AudioClickInterface {
    public void onAudioClick(int position, CustomerComplaintModel.ComplainList.AllComplain mModel);

    public void onServiceAudioClick(int position, ServiceComplaintModel.ComplainList.AllComplain mModel);
}
