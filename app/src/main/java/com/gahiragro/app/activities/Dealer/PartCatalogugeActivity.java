package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.adapters.PartListAdapter;
import com.gahiragro.app.adapters.PartsImagesViewPager;
import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.model.RegProductDetailModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PartCatalogugeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = PartCatalogugeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = PartCatalogugeActivity.this;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.productIV)
    ImageView productIV;

//    @BindView(R.id.right_arrow_IV)
    ImageView rightArrowIV;
//    @BindView(R.id.left_arrow_IVV)
    ImageView left_arrow_IVV;

//    @BindView(R.id.viewPagerMain)
    ViewPager mViewPager;

//    @BindView(R.id.partListRV)
    RecyclerView partListRV;

    PartListAdapter mPartListAdapter;
    ArrayList<RegProductDetailModel.Part> mPartAL;
    String frtImg = "";
    String bckImg = "";
    String imgPos = "Front";
    String selPrdId = "";
    String lftImg = "";
    String rgtImg = "";
    String[] images;
    Integer mCurrentPosition=0;
    ArrayList<ProductPartListModel.Part> mPartsAL = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part_cataloguge);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        imgMenuRL =findViewById(R.id.imgMenuRL);
        productIV =findViewById(R.id.productIV);
        left_arrow_IVV =findViewById(R.id.left_arrow_IVV);
        rightArrowIV =findViewById(R.id.right_arrow_IV);
        mViewPager =findViewById(R.id.viewPagerMain);
        partListRV =findViewById(R.id.partListRV);

        getIntentData();
        onViewClicked();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            String image = getIntent().getExtras().getString(Constants.D3_IMAGE);
            frtImg = getIntent().getExtras().getString(Constants.FRONT);
            bckImg = getIntent().getExtras().getString(Constants.BACK);
            lftImg = getIntent().getExtras().getString(Constants.LEFT);
            rgtImg = getIntent().getExtras().getString(Constants.RIGHT);
            selPrdId = getIntent().getExtras().getString(Constants.SEL_PRODUCT_ID);
            mPartAL = (ArrayList<RegProductDetailModel.Part>) getIntent().getSerializableExtra(Constants.LIST);

            if(image == null) {
                productIV.setVisibility(View.GONE);
            }
            else{
                productIV.setVisibility(View.VISIBLE);
                Glide.with(mActivity)
                        .load(image)
                        .placeholder(R.drawable.dummy_img_product)
                        .into(productIV);
            }
            images = new String[]{rgtImg,frtImg, bckImg, lftImg, rgtImg,frtImg};
            setViewPagerItems(images);
            mViewPager.setCurrentItem(1);
        }
    }

    private void setViewPagerItems(String[] images) {
        // Initializing the ViewPagerAdapter
        PartsImagesViewPager mPartsImagesViewPager = new PartsImagesViewPager(PartCatalogugeActivity.this, this.images);
        // Adding the Adapter to the ViewPager
        mViewPager.setAdapter(mPartsImagesViewPager);
        executeGetPartListApi();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i(TAG, "onPageScrolled:"+positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPosition=position;
                String positions = position + "";
                Log.e(TAG, "onPageSelected**" + positions);

                if (mViewPager.getCurrentItem() == 0) {
                    left_arrow_IVV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(3, true);
                        }
                    });

                    rightArrowIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(1, true);
                        }
                    });

                } else if (mViewPager.getCurrentItem() == 1) {
                    left_arrow_IVV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(4, true);
                        }
                    });

                    rightArrowIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(2, true);
                        }
                    });
                } else if (mViewPager.getCurrentItem() == 2) {
                    left_arrow_IVV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(1, true);
                        }
                    });

                    rightArrowIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(3, true);
                        }
                    });
                } else if (mViewPager.getCurrentItem() == 3) {
                    left_arrow_IVV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(2, true);
                        }
                    });

                    rightArrowIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(4, true);
                        }
                    });
                }else if (mViewPager.getCurrentItem() == 4) {
                    left_arrow_IVV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(3, true);
                        }
                    });

                    rightArrowIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(1, true);
                        }
                    });
                }

                if (position == 0) {
                    imgPos = "Right";
                } else if (position == 1) {
                    imgPos = "Front";
                } else if (position == 2) {
                    imgPos = "Back";
                } else if (position == 3) {
                    imgPos = "Left";
                }else if (position == 4) {
                    imgPos = "Right";
                } else if (position == 5) {
                    imgPos = "Front";
                }
                executeGetPartListApi();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                String positions = state + "";
                Log.e(TAG, "onPageScroll**" + state);
                // For going from the first item to the last item, when the 1st A goes to 1st C on the left, again we let the ViewPager do it's job until the movement is completed, we then set the current item to the 2nd C.
                // Set the current item to the item before the last item if the current position is 0
                if (mCurrentPosition == 0)
                    mViewPager.setCurrentItem(4, false); // lastPageIndex is the index of the last item, in this case is pointing to the 2nd A on the list. This variable should be declared and initialzed as a global variable

                // For going from the last item to the first item, when the 2nd C goes to the 2nd A on the right, we let the ViewPager do it's job for us, once the movement is completed, we set the current item to the 1st A.
                // Set the current item to the second item if the current position is on the last
                if (mCurrentPosition == 5)
                    mViewPager.setCurrentItem(1, false);
            }
        });
    }

//    @OnClick({R.id.imgMenuRL, R.id.productIV})
    public void onViewClicked(/*View view*/) {

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       /* switch (view.getId()) {
            case R.id.imgMenuRL:
                onBackPressed();
                break;
//            case R.id.productIV:
//                Intent intent = new Intent(PartCatalogugeActivity.this, ThrasherAssimblyActivity.class);
//                intent.putExtra(Constants.LIST, mPartAL);
//                startActivity(intent);
//                break;
        }*/
    }

    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("product_id", selPrdId);
        mMap.put("position", imgPos);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetPartListApi() {
        mPartsAL.clear();
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllPrdParts(mPram()).enqueue(new Callback<ProductPartListModel>() {
            @Override
            public void onResponse(Call<ProductPartListModel> call, Response<ProductPartListModel> response) {
                ProductPartListModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mPartsAL.clear();
                    mPartsAL.addAll(mModel.getPartList());
                    setPartListAdapter();
                } else {
                    setPartListAdapter();
                }
            }

            @Override
            public void onFailure(Call<ProductPartListModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }

    private void setPartListAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        partListRV.setLayoutManager(layoutManager);
        mPartListAdapter = new PartListAdapter(mActivity, mPartsAL);
        partListRV.setAdapter(mPartListAdapter);
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }
}
