package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class ImageViewActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ImageViewActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ImageViewActivity.this;

//    @BindView(R.id.fullImageIV)
    ImageView fullImageIV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
//        ButterKnife.bind(this);
        fullImageIV=findViewById(R.id.fullImageIV);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        setStatusBar(mActivity, Color.WHITE);
        getIntentData();
        onViewClicked();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            String image = getIntent().getStringExtra(Constants.URL);
            Glide.with(mActivity).load(image).into(fullImageIV);
        }

    }
//    @OnClick(R.id.imgMenuRL)
    public void onViewClicked(/*View view*/)
    {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        /*switch (view.getId())
        {
            case R.id.imgMenuRL:
                onBackPressed();
                break;
        }*/
    }
}