package com.gahiragro.app.activities.interfaces;

public interface DeletePrtInterface {
    public void onDelete(int position, String cartId);
}
