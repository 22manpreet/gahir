package com.gahiragro.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.adapters.VideoPdfAdapter;
import com.gahiragro.app.model.ProductDetailModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideosPdfLIstingActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = VideosPdfLIstingActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = VideosPdfLIstingActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.videoPdfRV)
    RecyclerView videoPdfRV;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
//    @BindView(R.id.dummyTV)
    TextView dummyTV;
    //iNITIALIZE
    String id = "";
    ArrayList<String> mVideoPdfAL = new ArrayList();
    ArrayList<String> mVideoPdfTitle = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_pdf_l_isting);
//        ButterKnife.bind(this);
        videoPdfRV=findViewById(R.id.videoPdfRV);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        dummyTV=findViewById(R.id.dummyTV);

        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.ID);
            executeProductDetailApi();
        }

        onViewClicked();
    }

    /*
     * Get User Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProductDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.productDetailApi(mDetailParam()).enqueue(new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                dismissProgressDialog();
                ProductDetailModel mModel = response.body();

            //    mVideoPdfAL.addAll(mModel.getProductDetail().getProdVideo());

                List<ProductDetailModel.ProdVideo> prodVideo=  new ArrayList<>();

                prodVideo.addAll(mModel.getProductDetail().getProdVideo());
                

                for(int i =0; i<= prodVideo.size()-1;  i++)
                {
                    mVideoPdfAL.add(prodVideo.get(i).getUrl());
                    mVideoPdfTitle.add(prodVideo.get(i).getTitle());
                }

                //    mVideoPdfAL.addAll(mModel.getProductDetail().getProdPdf());
                 List<ProductDetailModel.ProdPdf> prodPdf=new ArrayList<>();
                 prodPdf.addAll(mModel.getProductDetail().getProdPdf());
                for(int i =0; i <= prodPdf.size()-1;  i++)
                {
                    mVideoPdfAL.add(prodPdf.get(i).getUrl());
                    mVideoPdfTitle.add(prodPdf.get(i).getTitle());
                }

                if (mVideoPdfAL.size() == 0) {
                    dummyTV.setVisibility(View.VISIBLE);
                }
                Log.e(TAG, "SIZE::" + mVideoPdfAL.size());
                setAdapter();
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }

    private void setAdapter() {
        videoPdfRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        videoPdfRV.setLayoutManager(layoutManager);
        VideoPdfAdapter mAdapter = new VideoPdfAdapter(mActivity, mVideoPdfAL,mVideoPdfTitle);
        videoPdfRV.setAdapter(mAdapter);
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;

        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}