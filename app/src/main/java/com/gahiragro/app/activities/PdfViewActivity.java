package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class PdfViewActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = PdfViewActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */

    Activity mActivity = PdfViewActivity.this;

//    @BindView(R.id.webView)
    WebView webView;
//    @BindView(R.id.backIV)
    ImageView backIV;
//    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    String url = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);
//        ButterKnife.bind(this);
        webView=findViewById(R.id.webView);
        backIV=findViewById(R.id.backIV);
        progress_bar=findViewById(R.id.progress_bar);

        if (getIntent() != null) {
            url = getIntent().getExtras().getString(Constants.URL);
        }
        setStatusBar(mActivity, Color.WHITE);
        String myPdfUrl = url;
        String url = "https://docs.google.com/viewer?embedded=true&url=" + myPdfUrl;
        url = url.replaceAll(" ", "%20");
        Log.e(TAG, "******FULLURL*****" + url);
        Log.e(TAG, "******URL*****" + myPdfUrl);

        showProgressDialog(mActivity);
          onViewClicked();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showPdfFile();
    }

    private void showPdfFile() {

        webView.invalidate();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);


        webView.setWebViewClient(new WebViewClient() {
            boolean checkOnPageStartedCalled = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                checkOnPageStartedCalled = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (checkOnPageStartedCalled) {
                    dismissProgressDialog();
                } else {
                    showPdfFile();
                }
            }
        });


            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);


    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.backIV})
    public void onViewClicked(/*View view*/) {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

       /* switch (view.getId()) {
            case R.id.backIV:
                setBackClick();
                break;

        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}



