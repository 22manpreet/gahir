package com.gahiragro.app.activities.interfaces;

import com.gahiragro.app.model.TopTabsModel;

public  interface TopFilterClickInterface {
    public void onFilterItemClick(TopTabsModel mModel, int position);
}
