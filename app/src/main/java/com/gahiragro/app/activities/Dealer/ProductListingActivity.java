package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.interfaces.FilterReviewListInterface;
import com.gahiragro.app.adapters.Dealer.ProductFilterAdapter;
import com.gahiragro.app.model.ALlProductsModel;
import com.gahiragro.app.model.FilterReviewModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListingActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProductListingActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ProductListingActivity.this;

//    @BindView(R.id.productRV)
    RecyclerView productRV;

//    @BindView(R.id.imgBackRL)
    RelativeLayout imgBackRL;

//    @BindView(R.id.txtDoneTV)
    TextView txtDoneTV;

    ProductFilterAdapter productFilterAdapter;
    List<ALlProductsModel.AllProduct> mGetProductsList = new ArrayList<>();
    List<ALlProductsModel.AllProduct> mTemGetProductsAL = new ArrayList<>();

    List<FilterReviewModel.AllReview> filterReviewList = new ArrayList<>();


    int page_no = 1;
    String product = "product", productIds = "";
    String lastPage = "false";

    FilterReviewListInterface filterInterface = new FilterReviewListInterface() {
        @Override
        public void onFilterReview(int position, List<String> mNewList) {

            Log.e(TAG, "**ERROR**" + mNewList.toString());

            productIds = TextUtils.join(", ", mNewList);
//            showToast(mActivity, productIds);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_listing);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        productRV =findViewById(R.id.productRV);
        imgBackRL =findViewById(R.id.imgBackRL);
        txtDoneTV =findViewById(R.id.txtDoneTV);
        executeGetProductsApi();
        Onclick();
    }

    void Onclick() {

        imgBackRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtDoneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable(mActivity)) {

                    if (productIds.equals("")) {
                        showAlertDialog(mActivity, getString(R.string.please_select_product));

                    } else  {
                        GahirPreference.writeString(mActivity, Constants.FILTER_TYPE_ID, productIds);
                        GahirPreference.writeString(mActivity, Constants.FILTER_TYPE, "product");
                        Constants.FILTER_BACK_PRESSED = true;
                        onBackPressed();
                    }

                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }
            }
        });
    }
    private Map<String, String> mPram() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetProductsApi() {

        if (page_no == 1) {

            showProgressDialog(mActivity);

        } else if (page_no > 1) {
            // mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllProduct(mPram()).enqueue(new Callback<ALlProductsModel>() {
            @Override
            public void onResponse(Call<ALlProductsModel> call, Response<ALlProductsModel> response) {

                if (page_no == 1) {
                    dismissProgressDialog();
                }
                ALlProductsModel allProductsModel = response.body();
                if (allProductsModel.getStatus().equals("1")) {
                    lastPage = allProductsModel.getProductList().getLastPage();

                    if (page_no == 1) {
                        mGetProductsList = allProductsModel.getProductList().getAllProducts();
                    } else if (page_no > 1) {
                        mTemGetProductsAL = allProductsModel.getProductList().getAllProducts();
                    }
                    if (mTemGetProductsAL.size() > 0) {
                        mGetProductsList.addAll(mTemGetProductsAL);
                    }
                    if (page_no == 1) {
                        initRV();
                    } else {
                        productFilterAdapter.notifyDataSetChanged();
                    }

                } else if (allProductsModel.getStatus().equals("100")) {
                    showToast(mActivity, allProductsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ALlProductsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());

            }
        });

    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(ProductListingActivity.this, RecyclerView.VERTICAL, false);
        productRV.setLayoutManager(layoutManager);
        productFilterAdapter = new ProductFilterAdapter(mGetProductsList, this, filterInterface);
        productRV.setAdapter(productFilterAdapter);
        //addListData();
    }


}