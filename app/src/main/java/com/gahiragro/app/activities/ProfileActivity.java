package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.model.UserDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ProfileActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.editRL)
    RelativeLayout editRL;

//    @BindView(R.id.userNameTV)
    TextView userNameTV;

//    @BindView(R.id.bioTV)
    TextView bioTV;

//    @BindView(R.id.passwordTV)
    TextView passwordTV;

//    @BindView(R.id.emailTV)
    TextView emailTV;

//    @BindView(R.id.profile_pic)
    ImageView profile_pic;

//    @BindView(R.id.flagIV)
    ImageView flagIV;

    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(mActivity);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        editRL=findViewById(R.id.editRL);
        bioTV=findViewById(R.id.bioTV);
        userNameTV=findViewById(R.id.userNameTV);
        passwordTV=findViewById(R.id.passwordTV);
        emailTV=findViewById(R.id.emailTV);
        profile_pic=findViewById(R.id.profile_pic);
        flagIV=findViewById(R.id.flagIV);
        setStatusBar(mActivity, Color.WHITE);

        onViewClicked();
    }


    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL, R.id.editRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEditClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.editRL:
                setEditClick();
                break;

        }*/
    }

    private void setEditClick() {
        Intent i = new Intent(mActivity, EditProfileActivity.class);
        startActivity(i);
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

    }

    /*
     * Get User Detail Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Get User Detail Api
        passwordTV.setText(GahirPreference.readString(mActivity, Constants.PASSWORD_VALUE,""));
        if (isNetworkAvailable(mActivity)) {
            executeUserDetailApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUserDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.userProfile(mDetailParam()).enqueue(new Callback<UserDetailModel>() {
            @Override
            public void onResponse(Call<UserDetailModel> call, Response<UserDetailModel> response) {
                dismissProgressDialog();
                UserDetailModel mModel = response.body();
                if(mModel.getStatus().equals("1"))
                {
                    if(mModel.getUserDetail()!=null)
                    {
                        userNameTV.setText(mModel.getUserDetail().getFirstName());
                        emailTV.setText(mModel.getUserDetail().getUsername());
                        bioTV.setText(mModel.getUserDetail().getBio());
                        GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                        GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                        Glide.with(mActivity)
                                .load(mModel.getUserDetail().getImage())
                                .placeholder(R.drawable.placeholder_img)
                                .error(R.drawable.placeholder_img)
                                .into(profile_pic);


                        Glide.with(mActivity)
                                .load(mModel.getUserDetail().getFlagImage())
                                .placeholder(R.drawable.ic_flag)
                                .error(R.drawable.ic_flag)
                                .into(flagIV);
                    }
                }
                else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                }

            }

            @Override
            public void onFailure(Call<UserDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }
}