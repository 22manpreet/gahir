package com.gahiragro.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.DealorLoginModel;
import com.gahiragro.app.model.UpdateProfileModel;
import com.gahiragro.app.model.UserDetailModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.gahiragro.app.utils.RealPathUtil;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;


//    @BindView(R.id.changePasswordTV)
    TextView changePasswordTV;

//    @BindView(R.id.editImageIV)
    ImageView editImageIV;

//    @BindView(R.id.profile_pic)
    ImageView profile_pic;

//    @BindView(R.id.editBioET)
    EditText editBioET;

//    @BindView(R.id.btnSignUpBT)
    Button btnSignUpBT;


//    @BindView(R.id.bioRL)
    RelativeLayout bioRL;

//    @BindView(R.id.flagIV)
    ImageView flagIV;

//    @BindView(R.id.passwordRL)
    RelativeLayout passwordRL;

//    @BindView(R.id.EmailTV)
    TextView EmailTV;


//    @BindView(R.id.userNameTV)
    TextView userNameTV;

//    @BindView(R.id.editEmailET)
    EditText editEmailET;


    //INitialize
    Bitmap selectedImage;
    String mBase64Image = "";
    String country = "";
    String mBase64FlagImage;
    String userName = "";
    String currentPhotoPath = "";
    Bitmap bitmapImage;
    Bitmap bitmapFlag;
    File mFileImage;
    File mFileFlagImage;
    String mPath = "";
    String mFlagPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
//        ButterKnife.bind(this);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        changePasswordTV = findViewById(R.id.changePasswordTV);
        editImageIV = findViewById(R.id.editImageIV);
        editEmailET = findViewById(R.id.editEmailET);
        userNameTV = findViewById(R.id.userNameTV);
        EmailTV = findViewById(R.id.EmailTV);
        passwordRL = findViewById(R.id.passwordRL);
        flagIV = findViewById(R.id.flagIV);
        bioRL = findViewById(R.id.bioRL);
        btnSignUpBT = findViewById(R.id.btnSignUpBT);
        editBioET = findViewById(R.id.editBioET);
        profile_pic = findViewById(R.id.profile_pic);
        editImageIV = findViewById(R.id.editImageIV);


        setStatusBar(mActivity, Color.WHITE);
        editTextSelector(editBioET, bioRL, "");

        //To set Scroll Focus inside field
        editBioET.setOnTouchListener((v, event) -> {
            if (editBioET.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_SCROLL:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }
            }
            return false;
        });

        //Get User Detail Api
        if (isNetworkAvailable(mActivity)) {
            executeUserDetailApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

        onViewClicked();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EmailTV.setText(GahirPreference.readString(mActivity,Constants.PASSWORD_VALUE,""));
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL, R.id.editImageIV, R.id.changePasswordTV, R.id.btnSignUpBT, R.id.flagIV})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        editImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performProfileClick();
            }
        });
        btnSignUpBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performUpdateClick();
            }
        });

        changePasswordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performChangePasswordClick();
            }
        });

        flagIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFlagClicked();
            }
        });


        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.editImageIV:
                performProfileClick();
                break;

            case R.id.btnSignUpBT:
                performUpdateClick();
                break;
            case R.id.changePasswordTV:
                performChangePasswordClick();
                break;

            case R.id.flagIV:
                performFlagClicked();
                break;

        }*/
    }

    private void performFlagClicked() {
        // if (checkFlagPermission()) {
        performFlagClick();
        // } else {
        // requestFlagPermission();
        // }
    }

    private void performChangePasswordClick() {
        Intent i = new Intent(mActivity, ChangePasswordActivity.class);
        startActivity(i);
    }

    private void performUpdateClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeUpdateProfileApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }

    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            return readImages == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, writeCamera}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        }
    }

    private boolean checkFlagPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestFlagPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage}, 370);
    }

    private void showUserImage(Uri imageUri) {
        String file = getRealPathFromURI_API19(mActivity, imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap selectedImage = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (selectedImage != null) {
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
            bitmapImage = selectedImage;
            // showImage(imageUri, selectedImage);
        }

    }


    private void performFlagClick() {
        CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                // Implement your code here
                flagIV.setImageResource(flagDrawableResID);
                country = name;
                bitmapFlag = BitmapFactory.decodeResource(mActivity.getResources(), flagDrawableResID);
//                if (bitmapFlag != null) {
//                    Uri uri = getImageUri(mActivity, bitmapFlag);
//                    mFlagPath = RealPathUtil.getRealPath(mActivity, uri);
//                }


//                getImageUri(mActivity, bitmapFlag);
//                mFileFlagImage = new File(getImageUri(mActivity, bitmapFlag).getPath());
                mBase64FlagImage = encodeTobase64(bitmapFlag);
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");

    }

    private void executeUpdateProfileApi() {
        showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;


        // if (mFileImage != null) {

        if (mPath.equals("")) {
            RequestBody ppostImage1 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            mMultipartBody1 = MultipartBody.Part.createFormData("image", "", ppostImage1);
        } else {
            File file = new File(mPath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpg", requestFile);

        }

//        if (mFlagPath.equals("")) {
//            RequestBody ppostImage2 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//            mMultipartBody2 = MultipartBody.Part.createFormData("flag_image", "", ppostImage2);
//        } else {
//            File file1 = new File(mFlagPath);
//            RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
//            mMultipartBody2 = MultipartBody.Part.createFormData("flag_image", getAlphaNumericString() + ".jpg", requestFile2);
//
//        }
        RequestBody access_token = RequestBody.create(MediaType.parse("multipart/form-data"), getAccessToken());
        RequestBody first_name = RequestBody.create(MediaType.parse("multipart/form-data"), userName);
        RequestBody bio = RequestBody.create(MediaType.parse("multipart/form-data"), editBioET.getText().toString().trim());
        //  RequestBody strcountry = RequestBody.create(MediaType.parse("multipart/form-data"), country);
        // RequestBody strFlagIMage = RequestBody.create(MediaType.parse("multipart/form-data"), mBase64FlagImage);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.updateProfile(access_token, first_name, bio, mMultipartBody1).enqueue(new Callback<UpdateProfileModel>() {


            @Override
            public void onResponse(Call<UpdateProfileModel> call, Response<UpdateProfileModel> response) {
                dismissProgressDialog();
                UpdateProfileModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    /*
     * Get User Detail Api
     * */
    private Map<String, String> mDetailParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUserDetailApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.userProfile(mDetailParam()).enqueue(new Callback<UserDetailModel>() {
            @Override
            public void onResponse(Call<UserDetailModel> call, Response<UserDetailModel> response) {
                dismissProgressDialog();
                UserDetailModel mModel = response.body();
                userNameTV.setText(mModel.getUserDetail().getFirstName());
                userName = mModel.getUserDetail().getFirstName();
                editEmailET.setText(mModel.getUserDetail().getUsername());
                editBioET.setText(mModel.getUserDetail().getBio());
                mBase64FlagImage = mModel.getUserDetail().getFlagImage();
                mBase64Image = mModel.getUserDetail().getImage();
                country = mModel.getUserDetail().getCountry();
                Glide.with(mActivity)
                        .load(mModel.getUserDetail().getImage())
                        .placeholder(R.drawable.placeholder_img)
                        .into(profile_pic);
                Glide.with(mActivity)
                        .load(mModel.getUserDetail().getFlagImage())
                        .placeholder(R.drawable.ic_flag)
                        .into(flagIV);
                mFlagPath = "";
                mPath = "";
//                mFileFlagImage = new File(mModel.getUserDetail().getFlagImage());
//                mFileImage = new File(mModel.getUserDetail().getImage());
            }

            @Override
            public void onFailure(Call<UserDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });
    }
    private Boolean isValidate() {
        boolean flag = true;
            if (editBioET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_message));
            flag = false;
        }

        return flag;
    }


    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }


    private void performProfileClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;

//            case 370:
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    performFlagClick();
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Log.e(TAG, "**Permission Denied**");
//                }

            //break;
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick()
    {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    mPath = RealPathUtil.getRealPath(mActivity, result.getUri());
                    File imgFile = new File(mPath);
                    Log.e(TAG, "**Path**" + mPath);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        // profile_pic.setImageBitmap(myBitmap);
                        Glide.with(mActivity)
                                .load(mPath)
                                .into(profile_pic);
                    }

//                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
//                    selectedImage = BitmapFactory.decodeStream(imageStream);
////                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "Title", null);
//
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
//                    bitmapImage = selectedImage;
//                    profile_pic.setImageBitmap(bitmapImage);

                    //  mFileImage = new File(result.getUri().getPath());
//                    if (path != null) {
//                        Glide.with(mActivity).load(path).into(profile_pic);
//                    }


//                    Log.e(TAG, "**Image Base 64**" + mBase64Image);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        if (inImage != null) {
            String path = "";
            if (inImage != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
                Log.e(TAG, "PATH" + path);

            }
            return Uri.parse(path);
        } else {
            return null;
        }
    }

    public Uri bitmapToUriConverter(Bitmap mBitmap) {
        Uri uri = null;
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, 100, 100);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap newBitmap = Bitmap.createScaledBitmap(mBitmap, 200, 200,
                    true);
            File file = new File(getFilesDir(), "Image"
                    + new Random().nextInt() + ".jpeg");
            FileOutputStream out = openFileOutput(file.getName(),
                    Context.MODE_WORLD_READABLE);
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            //get absolute path
            String realPath = file.getAbsolutePath();
            File f = new File(realPath);
            uri = Uri.fromFile(f);

        } catch (Exception e) {
            Log.e("Your Error Message", e.getMessage());
        }
        return uri;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
