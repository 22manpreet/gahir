package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.activities.HomeActivity;
import com.gahiragro.app.utils.Constants;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class AddReviewPostActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddReviewPostActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddReviewPostActivity.this;

//    @BindView(R.id.btnPostBT)
    Button btnPostBT;

    Boolean mCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review_post);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);
        btnPostBT=findViewById(R.id.btnPostBT);

        OnClick();
    }

    private void OnClick() {

        btnPostBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getCustomer = getIntent().getExtras().getString(Constants.FRAGMENT);
                if (getCustomer.equals("Customer Review")){
                    Intent intent = new Intent(AddReviewPostActivity.this, HomeActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}