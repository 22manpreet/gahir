package com.gahiragro.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.HomeListAdapter;
import com.gahiragro.app.adapters.HomeNewTopFilterAdapter;
import com.gahiragro.app.adapters.HomeTopFilterAdapter;
import com.gahiragro.app.model.EmailLoginModel;
import com.gahiragro.app.model.FilterCategoryModel;
import com.gahiragro.app.model.FilterProductModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewProductListActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = NewProductListActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = NewProductListActivity.this;
    /**
     * Widgets
     */

//    @BindView(R.id.productRV)
    RecyclerView productRV;
//    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
//    @BindView(R.id.backIV)
    ImageView backIV;


    int page_no = 1;
    String lastPage = "false";
    String id = "";
    HomeListAdapter mHomeListAdapter;
    List<FilterCategoryModel.ProductList.AllProduct> mProductAL = new ArrayList<>();
    List<FilterCategoryModel.ProductList.AllProduct> mTempAL = new ArrayList<>();
    PaginationHomeInterface mInterfaceData = new PaginationHomeInterface() {
        @Override
        public void mHomePagination(boolean isLastScrolled) {
            if (isLastScrolled == true) {
                if (lastPage.equals("FALSE")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (lastPage.equals("FALSE")) {
                            executeFilterCategoryApi();
                        } else {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    }
                }, 1000);
            }
        }
    };
    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            SharedPreferences preferences = GahirPreference.getPreferences(Objects.requireNonNull(mActivity));
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.apply();
            Intent intent = new Intent(mActivity, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product_list);
//        ButterKnife.bind(this);
        productRV=findViewById(R.id.productRV);
        mProgressBar=findViewById(R.id.mProgressBar);
        backIV=findViewById(R.id.backIV);


        if (getIntent() != null) {
            id = getIntent().getExtras().getString(Constants.CATEGORY_ID);
            if (isNetworkAvailable(mActivity)) {
                executeFilterCategoryApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }

        onViewClicked();

    }


    /*
     * FilterCategory Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("id", id);
        mMap.put("page_no", String.valueOf(page_no));

        return mMap;
    }

    private void executeFilterCategoryApi() {
        if (page_no == 1) {
            showProgressDialog(mActivity);
        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.filterCategory(mParam()).enqueue(new Callback<FilterCategoryModel>() {
            @Override
            public void onResponse(Call<FilterCategoryModel> call, Response<FilterCategoryModel> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                FilterCategoryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    lastPage = mModel.getProductList().getLastPage();
                    if (page_no == 1) {
                        mProductAL = mModel.getProductList().getAllProducts();
                    } else if (page_no > 1) {
                        mTempAL = mModel.getProductList().getAllProducts();
                    }
                    if (mTempAL.size() > 0) {
                        mProductAL.addAll(mTempAL);
                    }
                    if (mProductAL.size() > 0) {
                        setProductAdapter();
                    } else {
                        mHomeListAdapter.notifyDataSetChanged();
                    }
                    Log.e(TAG, "SIZE::" + mProductAL.size());
//                    if (getLoginRoleType().equals(Constants.DEALER)) {
//                        executeUpdateLocationApi();
//                    }
                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<FilterCategoryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }


    private void setProductAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        productRV.setLayoutManager(layoutManager);
        mHomeListAdapter = new HomeListAdapter(mActivity, mProductAL, mInterfaceData);
        productRV.setAdapter(mHomeListAdapter);
    }

    /*
     *  Alert Dialog Finish
     * */

    public void showFinishLogoutAlertDialog(Activity mActivity, String strMessage, LogoutInterface logoutInterface) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutInterface.onLogoutClick();
            }
        });
        alertDialog.show();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.backIV})
    public void onViewClicked(/*View view*/) {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        /*switch (view.getId()) {
            case R.id.backIV:
                setBackClick();
                break;
        }*/
    }
    private void setBackClick() {
        onBackPressed();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}