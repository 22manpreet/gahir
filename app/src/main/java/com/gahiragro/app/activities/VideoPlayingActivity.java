package com.gahiragro.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.text.PrecomputedText;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import androidx.annotation.NonNull;

import com.gahiragro.app.R;
import com.gahiragro.app.fonts.TextViewMedium;
import com.gahiragro.app.utils.Constants;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class VideoPlayingActivity extends BaseActivity {

    String TAG = VideoPlayingActivity.this.getClass().getSimpleName();

    Activity mActivity = VideoPlayingActivity.this;

    // creating a variable for exoplayerview.
   // SimpleExoPlayerView exoPlayerView;
     VideoView videoView;

    // creating a variable for exoplayer
    SimpleExoPlayer exoPlayer;
    String url;
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

//    @BindView(R.id.orientationChangeRL)
    RelativeLayout orientationChangeRL;

//    @BindView(R.id.videoText)
    TextViewMedium videoText;
    int stopPosition;
    MediaPlayer mp;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_playing2);
        setStatusBar(mActivity, Color.WHITE);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        progress_bar=findViewById(R.id.progress_bar);
        orientationChangeRL=findViewById(R.id.orientationChangeRL);
        videoText=findViewById(R.id.videoText);

        if (getIntent() != null) {
            if (getIntent().getExtras().getString(Constants.class_type) != null && getIntent().getExtras().getString(Constants.class_type).equals("customerReviewAdapter")) {
                url = getIntent().getExtras().getString(Constants.URL);
            } else {
                url = getIntent().getExtras().getString(Constants.URL);
            }
        }
//        ButterKnife.bind(this);

    //    exoPlayerView = findViewById(R.id.idExoPlayerVIew);
        videoView=findViewById(R.id.idVideoView);

        //Creating MediaController
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);
        Uri uri=Uri.parse(url);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();

        orientationChangeRL.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SourceLockedOrientationActivity")
            @Override
            public void onClick(View view) {

                int orientation = VideoPlayingActivity.this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                    videoView.setLayoutParams(layoutParams);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                } else {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    videoView.setLayoutParams(layoutParams);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
                    params.width = metrics.widthPixels;
                    params.height = metrics.heightPixels;
                    params.leftMargin = 0;
                    params.rightMargin=0;
                    params.topMargin=0;
                    params.bottomMargin=0;
                    videoView.setLayoutParams(params);
                  // exoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

                }
             }
        });

        onViewClicked();
    }

    private void onPauseClick() {
        //   super.onPause();
        if (videoView.isPlaying())
            videoView.pause();
        stopPosition = videoView.getCurrentPosition(); //stopPosition is an int
    }
    private void onResumeClick() {
        // super.onResume();
        if (videoView != null) {
            videoView.seekTo(stopPosition);
            videoView.start();
        }
    }
//    @OnClick({R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });

       /* switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;

        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
