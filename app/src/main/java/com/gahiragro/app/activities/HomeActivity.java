package com.gahiragro.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.Prediction;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.gahiragro.app.EasyLocationProvider;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.Dealer.FilterActivity;
import com.gahiragro.app.activities.Dealer.SelectedPartsActivity;
import com.gahiragro.app.activities.Dealer.SendMessageActivity;
import com.gahiragro.app.activities.interfaces.GuestLoginInterface;
import com.gahiragro.app.activities.interfaces.IOnBackPressed;
import com.gahiragro.app.activities.interfaces.LogoutInterface;
import com.gahiragro.app.fragments.AdminHomeFragment;
import com.gahiragro.app.fragments.ContactUsFragment;
import com.gahiragro.app.fragments.Dealer.AllProductListFragment;
import com.gahiragro.app.fragments.Dealer.CheckWarrantyFragment;
import com.gahiragro.app.fragments.Dealer.ComplaintsFragment;
import com.gahiragro.app.fragments.Dealer.ComplaintsServiceFragment;
import com.gahiragro.app.fragments.Dealer.CustomerComplaintsFragment;
import com.gahiragro.app.fragments.Dealer.CustomerReviewFragment;
import com.gahiragro.app.fragments.Dealer.MessageFragment;
import com.gahiragro.app.fragments.Dealer.OfferFragment;
import com.gahiragro.app.fragments.Dealer.RegisterProductListFragment;
import com.gahiragro.app.fragments.Dealer.RewardsFragment;
import com.gahiragro.app.fragments.Dealer.ServiceManualFragment;
import com.gahiragro.app.fragments.Dealer.TroubleshootingFragment;
import com.gahiragro.app.fragments.Dealer.searchDealerFragment;
import com.gahiragro.app.fragments.DealerDocumentFragment;
import com.gahiragro.app.fragments.DealorEnquiryListFragment;
import com.gahiragro.app.fragments.DealorOrderFragment;
import com.gahiragro.app.fragments.DocumentFragment;
import com.gahiragro.app.fragments.MyCustomersFragment;
import com.gahiragro.app.fragments.NewHomeFragment;
import com.gahiragro.app.fragments.NotificationFragment;
import com.gahiragro.app.fragments.PrivacyPolicyFragment;
import com.gahiragro.app.model.CustomersListModel;
import com.gahiragro.app.model.LocationModel;
import com.gahiragro.app.model.PredictionsItem;
import com.gahiragro.app.model.SearchLocationModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {
    public static RelativeLayout downloadRL;
    public static boolean isBack = false;
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;
    boolean mCheck = false;
    /**
     * Widgets
     */


    TextView userNameTV;
    ImageView imageprofileIV;
    //Initialize
    NavigationView nav_bar;
    LinearLayout homeLl;
    LinearLayout searchDealerLL;
    LinearLayout customerReviewLL;
    LinearLayout registerYourProLL;
    LinearLayout myProductsLL;
    LinearLayout myCustomersLL;
    LinearLayout privacyPolicyLL;
    LinearLayout offerLL;
    LinearLayout orderLL;
    LinearLayout convertorLL;
    LinearLayout complaintsLL;
    LinearLayout checkWarrantyLL;
    LinearLayout troubleLL;
    LinearLayout serviceManualLL;
    LinearLayout rewardslLL;
    LinearLayout documentLL;
    LinearLayout notificationLL;
    LinearLayout contactLL;
    LinearLayout enquiryLL;
    LinearLayout loginLL;
    LinearLayout logoutLL;
    LinearLayout myMessageLL;
    LinearLayout customerComplaintsLL;
    RelativeLayout bottomRL;


    ImageView homeIV;
    ImageView searchDealerIV;
    ImageView customerReviewIV;
    ImageView registerYourProIV;
    ImageView myProductsIV;
    ImageView myCustomerIV;
    ImageView privacyPolicyIV;
    ImageView offerIV;
    ImageView complaintsIV;
    ImageView checkWarrantyIV;
    ImageView troubleIV;
    ImageView serviceManualIV;
    ImageView rewardslIV;
    ImageView notificationIV;
    ImageView myMessageIV;
    ImageView customerComplaintsIV;
    ImageView searchIV;
    ImageView contactIV;
    ImageView loginIV;
    ImageView enquiryIV;
    ImageView documentIV;
    ImageView logoutIV;
    ImageView convertorIV;
    ImageView orderIV;
    TextView homeTV;
    TextView searchDealerTV;
    TextView customerReviewTV;
    TextView registerYourProTV;
    TextView myProductsTV;
    TextView myCustomerTV;
    TextView privacyPolicyTV;
    TextView offerTV;
    TextView complaintsTV;
    TextView checkWarrantyTV;
    TextView troubleTV;
    TextView serviceManualTV;
    TextView rewardsTV;
    TextView notificationTV;
    TextView myMessageTV;
    TextView customerComplaintsTV;
    TextView contactTV;
    TextView loginTV;
    TextView enquiryTV;
    TextView documentTV;
    TextView logoutTV;
    TextView convertorTV;
    TextView orderTV;
    View homeView;
    View logoutView;
    View searchDealerView;
    View customerReviewView;
    View registerYourProView;
    View myProductsView;
    View privacyPolicyView;
    View offerView;
    View convertorView;
    View checkWarrantyView;
    View contactView;
    View enquiryView;
    View orderView;
    View notificationView;
    View myCustomerView;
    View troubleView;
    View serviceManualView;
    View rewardsView;
    View complaintsView;
    View loginView;
    View documentView;
    View myMessageView;
    View customerComplaintsView;
    String classType = "";
    EasyLocationProvider easyLocationProvider;
    boolean callIntent = false;
    double mLatitude = 0;
    double mLongitude = 0;
    String Full_address;

    boolean isCurrentLocation = true;
    String mPlaceID = "";
    LogoutInterface mLogoutInterface = new LogoutInterface() {
        @Override
        public void onLogoutClick() {
            setLogoutClick(mActivity);
        }
    };
    GuestLoginInterface mGuestLoginInterface = new GuestLoginInterface() {
        @Override
        public void onGuestLogin() {
            GahirPreference.writeString(mActivity, GahirPreference.GUEST_LOGIN, "true");
            Intent i = new Intent(mActivity, SplashActivity.class);
            startActivity(i);
        }
    };
    private long mTabClick1 = 0;
    //Permissions
    private final String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private final String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    private final long mLastClickTabOrder = 0;

    //    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    //    @BindView(R.id.containerRL)
    RelativeLayout containerRL;
    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;
    //    @BindView(R.id.menuIV)
    ImageView menuIV;

    //    @BindView(R.id.imgFilterRL)
    RelativeLayout imgFilterRL;

    //    @BindView(R.id.filterIV)
    ImageView filterIV;

    //    @BindView(R.id.imgCartRL)
    RelativeLayout imgCartRL;

    //    @BindView(R.id.imgsendRL)
    RelativeLayout imgsendRL;

    //    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    //    @BindView(R.id.imgSearchRL)
    RelativeLayout imgSearchRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        ButterKnife.bind(this);

        drawer_layout = findViewById(R.id.drawer_layout);
        containerRL = findViewById(R.id.containerRL);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        menuIV = findViewById(R.id.menuIV);
        imgFilterRL = findViewById(R.id.imgFilterRL);
        filterIV = findViewById(R.id.filterIV);
        imgCartRL = findViewById(R.id.imgCartRL);
        imgsendRL = findViewById(R.id.imgsendRL);
        txtTitleTV = findViewById(R.id.txtTitleTV);
        imgSearchRL = findViewById(R.id.imgSearchRL);

        callIntent = true;
        searchIV = findViewById(R.id.searchIV);
        downloadRL = findViewById(R.id.downloadRL);
        setStatusBar(mActivity, Color.WHITE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.POST_NOTIFICATIONS}, 23);
            } else {
                // repeat the permission or open app details
            }
        }

        //Set Navigation Drawer
        setNavigationViewListener();
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        if (IsLogin()) {
            //loginLL.setVisibility(View.VISIBLE);
            logoutLL.setVisibility(View.VISIBLE);
            logoutView.setVisibility(View.VISIBLE);
        } else {
            userNameTV.setText(getString(R.string.login_signup));
            userNameTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mActivity, SplashActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            loginLL.setVisibility(View.GONE);
            logoutLL.setVisibility(View.GONE);
            logoutView.setVisibility(View.GONE);
        }

        if (getLoginRoleType().equals(Constants.DEALER) || getLoginRoleType().equals(Constants.CUSTOMER)) {
            isCurrentLocation = true;
            if (checkLocationPermission()) {
                getCurrentLATLong();
            } else {
                requestLocationPermission();
            }
        }

        //location api execute
        executeUpdateLocation();

        getIntentData();
        onViewClicked();
    }


    public void onViewClicked() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerOpenClick();
            }
        });

        imgSearchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsLogin()) {
                    setSearchClick();
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }
            }
        });
        imgFilterRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilterClick();
            }
        });
        imgCartRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImageCartClick();
            }
        });

        imgsendRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.imgMenuRL:
                drawerOpenClick();
                break;
            case R.id.containerRL:
                break;
            case R.id.imgSearchRL:
                if (IsLogin()) {
                    setSearchClick();
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }

                break;
            case R.id.imgFilterRL:
                setFilterClick();
                break;

            case R.id.imgCartRL:
                setImageCartClick();
                break;
            case R.id.imgsendRL:
                sendClick();
                break;
        }*/
    }


    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra(Constants.class_type) != null && !getIntent().getStringExtra(Constants.class_type).equals("")) {
                classType = getIntent().getStringExtra(Constants.class_type);
                Log.e(TAG, "HGH" + classType);
            }
            if (getIntent().getStringExtra(Constants.NOTIFICATION_MASS) != null) {
                String notification_type = getIntent().getExtras().getString(Constants.NOTIFICATION_MASS);
                if (notification_type.equals("mass_push")) {
                    txtTitleTV.setText(getString(R.string.notifications));
                    imgSearchRL.setVisibility(View.GONE);
                    setLeftMenuSelection(4);
                    switchFragment(new NotificationFragment(), "", false, null);
                    setDataAccToRole();
                } else if (notification_type.equals("BookOrderActivity"))  //To open My order fragment after dealer book order
                {
                    imgSearchRL.setVisibility(View.GONE);
                    if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                        txtTitleTV.setText(getString(R.string.orders));
                    } else {
                        txtTitleTV.setText(getString(R.string.my_order));
                    }
                    setLeftMenuSelection(6);
                    switchFragment(new DealorOrderFragment(), "", false, null);
                    setDataAccToRole();
                } else {
                    setHomeTabClick();
                }
            } else if (getIntent().getStringExtra(Constants.NOTIFICATION_REVIEW) != null) {
                String noti_review = getIntent().getStringExtra(Constants.NOTIFICATION_REVIEW);
                if (noti_review.equals("review")) {
                    callIntent = false;
                    imgSearchRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.VISIBLE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    drawer_layout.closeDrawer(GravityCompat.START);
                    txtTitleTV.setText(getString(R.string.customer_review));
                    switchFragment(new CustomerReviewFragment(), "", false, null);
                    setLeftMenuSelection(3);
                    setDataAccToRole();
                }
            } else if (getIntent().getStringExtra(Constants.NOTIFICATION_PART_ENQUIRY) != null) {
                String noti_part_enquiry = getIntent().getStringExtra(Constants.NOTIFICATION_PART_ENQUIRY);
                if (noti_part_enquiry.equals("part_enquiry")) {
                    callIntent = false;
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgFilterRL.setVisibility(View.GONE);
                    imgSearchRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.VISIBLE);
                    imgsendRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.products));
                    switchFragment(new RegisterProductListFragment(), "", false, null);
                    setLeftMenuSelection(5);
                    setDataAccToRole();
                    //    switchFragment(new RegisterProductListFragment(), "", false, null);
                }
            } else if (getIntent().getStringExtra(Constants.NOTIFICATION_REGISTER_PRODUCT) != null) {
                String noti_regi_type = getIntent().getStringExtra(Constants.NOTIFICATION_REGISTER_PRODUCT);
                if (noti_regi_type.equals("register_product")) {
                    callIntent = false;
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgFilterRL.setVisibility(View.GONE);
                    imgSearchRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.VISIBLE);
                    imgsendRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.products));
                    switchFragment(new RegisterProductListFragment(), "", false, null);
                    setLeftMenuSelection(5);
                    setDataAccToRole();
                    //    switchFragment(new RegisterProductListFragment(), "", false, null);
                }
            } else if (getIntent().getStringExtra(Constants.NOTIFICATION_COMPLAIN) != null) {
                String noti_comp_type = getIntent().getStringExtra(Constants.NOTIFICATION_COMPLAIN);
                if (noti_comp_type.equals("complain")) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.complaints));
                    redirectComplaint();
//                    switchFragment(new ComplaintsFragment(), "", false, null);
//                    setLeftMenuSelection(12);
//                    setDataAccToRole();
                }
            } else if (getIntent().getStringExtra(Constants.NOTIFICATION_COSTUM) != null) {
                String noti_custom = getIntent().getStringExtra(Constants.NOTIFICATION_COSTUM);
                if (noti_custom.equals("custom")) {
                    if (IsLogin()) {
                        downloadRL.setVisibility(View.GONE);
                        imgFilterRL.setVisibility(View.GONE);
                        imgCartRL.setVisibility(View.GONE);
                        imgsendRL.setVisibility(View.GONE);
                        preventMultipleClick();
                        drawer_layout.closeDrawer(GravityCompat.START);
                        imgSearchRL.setVisibility(View.GONE);
                        txtTitleTV.setText(getString(R.string.notifications));
                        switchFragment(new NotificationFragment(), "", false, null);
                        setLeftMenuSelection(8);
                        setDataAccToRole();
                    } else {
                        showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                    }

                }
            } else {
                setHomeTabClick();
            }
        } else {
            setHomeTabClick();
        }
    }

    private void redirectComplaint() {
        if (getLoginRoleType().equals(Constants.CUSTOMER)) {
            orderLL.setVisibility(View.GONE);
            orderView.setVisibility(View.GONE);
            enquiryLL.setVisibility(View.GONE);
            enquiryView.setVisibility(View.GONE);
            notificationLL.setVisibility(View.VISIBLE);
            notificationView.setVisibility(View.VISIBLE);
            myCustomerView.setVisibility(View.GONE);
            myMessageView.setVisibility(View.GONE);
            customerComplaintsView.setVisibility(View.GONE);
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
            myMessageLL.setVisibility(View.GONE);
            customerComplaintsLL.setVisibility(View.GONE);
            myCustomersLL.setVisibility(View.GONE);


            homeLl.setVisibility(View.VISIBLE);
            searchDealerLL.setVisibility(View.VISIBLE);
            customerReviewLL.setVisibility(View.VISIBLE);
            registerYourProLL.setVisibility(View.VISIBLE);
            myProductsLL.setVisibility(View.VISIBLE);
            privacyPolicyLL.setVisibility(View.VISIBLE);
            offerLL.setVisibility(View.VISIBLE);
            convertorLL.setVisibility(View.VISIBLE);
            contactLL.setVisibility(View.VISIBLE);
            complaintsLL.setVisibility(View.VISIBLE);


            homeView.setVisibility(View.VISIBLE);
            searchDealerView.setVisibility(View.VISIBLE);
            customerReviewView.setVisibility(View.VISIBLE);
            registerYourProView.setVisibility(View.VISIBLE);
            myProductsView.setVisibility(View.VISIBLE);
            privacyPolicyView.setVisibility(View.VISIBLE);
            offerView.setVisibility(View.VISIBLE);
            convertorView.setVisibility(View.VISIBLE);
            contactView.setVisibility(View.VISIBLE);
            complaintsView.setVisibility(View.VISIBLE);

            switchFragment(new ComplaintsFragment(), "", false, null);
            setLeftMenuSelection(12);
        } else if (getLoginRoleType().equals(Constants.SERVICE)) {
            mCheck = true;
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            homeLl.setVisibility(View.GONE);
            homeView.setVisibility(View.GONE);
            enquiryTV.setText(getString(R.string.enquiries));
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
            orderLL.setVisibility(View.GONE);
            orderView.setVisibility(View.GONE);
            enquiryLL.setVisibility(View.GONE);
            enquiryView.setVisibility(View.GONE);
            notificationLL.setVisibility(View.GONE);
            myMessageLL.setVisibility(View.GONE);
            customerComplaintsLL.setVisibility(View.GONE);
            myCustomersLL.setVisibility(View.GONE);
            notificationView.setVisibility(View.GONE);
            myCustomerView.setVisibility(View.GONE);
            myMessageView.setVisibility(View.GONE);
            customerComplaintsView.setVisibility(View.GONE);
            privacyPolicyLL.setVisibility(View.GONE);
            privacyPolicyView.setVisibility(View.GONE);
            troubleLL.setVisibility(View.GONE);
            serviceManualLL.setVisibility(View.GONE);


            complaintsLL.setVisibility(View.VISIBLE);
            rewardslLL.setVisibility(View.VISIBLE);
            checkWarrantyLL.setVisibility(View.VISIBLE);
            homeLl.setVisibility(View.VISIBLE);


            complaintsView.setVisibility(View.VISIBLE);
            checkWarrantyView.setVisibility(View.VISIBLE);
            troubleView.setVisibility(View.GONE);
            serviceManualView.setVisibility(View.GONE);
            rewardsView.setVisibility(View.VISIBLE);
            homeView.setVisibility(View.VISIBLE);

            switchFragment(new ComplaintsServiceFragment(), "", false, null);
            setLeftMenuSelection(12);
        } else {
            homeLl.setVisibility(View.VISIBLE);
            homeView.setVisibility(View.VISIBLE);
            orderLL.setVisibility(View.VISIBLE);
            orderView.setVisibility(View.VISIBLE);
            enquiryLL.setVisibility(View.VISIBLE);
            enquiryView.setVisibility(View.VISIBLE);
            notificationLL.setVisibility(View.VISIBLE);
            myMessageLL.setVisibility(View.VISIBLE);
            customerComplaintsLL.setVisibility(View.VISIBLE);
            myCustomersLL.setVisibility(View.VISIBLE);
            notificationView.setVisibility(View.VISIBLE);
            myCustomerView.setVisibility(View.VISIBLE);
            myMessageView.setVisibility(View.VISIBLE);
            customerComplaintsView.setVisibility(View.VISIBLE);
            contactLL.setVisibility(View.VISIBLE);
            contactView.setVisibility(View.VISIBLE);
            documentLL.setVisibility(View.VISIBLE);
            documentView.setVisibility(View.VISIBLE);


            privacyPolicyLL.setVisibility(View.GONE);
            privacyPolicyView.setVisibility(View.GONE);
            troubleLL.setVisibility(View.GONE);
            serviceManualLL.setVisibility(View.GONE);
            searchDealerLL.setVisibility(View.GONE);
            customerReviewLL.setVisibility(View.GONE);
            registerYourProLL.setVisibility(View.GONE);
            myProductsLL.setVisibility(View.GONE);
            privacyPolicyLL.setVisibility(View.GONE);
            offerLL.setVisibility(View.GONE);
            convertorLL.setVisibility(View.GONE);
            complaintsLL.setVisibility(View.GONE);
            switchFragment(new CustomerComplaintsFragment(), "", false, null);
            setLeftMenuSelection(22);
        }
    }

    private void setHomeTabClick() {
        if (getLoginRoleType().equals(Constants.ADMIN)) {
            setAdminFirstTabSelection();
        } else if (getLoginRoleType().equals(Constants.DEALER)) {
            setDealerFirstTabSelection();
        } else if (getLoginRoleType().equals(Constants.CUSTOMER)) {
            setDealerFirstTabSelection();
        } else if (getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
            setAdminFirstTabSelection();
        } else {
            setDealerFirstTabSelection();
        }
    }

    private void setDealerFirstTabSelection() {
        setDataAccToRole();
        if (classType != null && !classType.equals("")) {
            if (classType.equals("RegisterCongratulationScreen")) {
                setMyProductClick();
            }
        } else {
            txtTitleTV.setText(getString(R.string.home));
            switchFragment(new NewHomeFragment(), "", false, null);
            setLeftMenuSelection(1);
        }
    }


    private void setAdminFirstTabSelection() {
        setDataAccToRole();
        imgSearchRL.setVisibility(View.GONE);
        txtTitleTV.setText(getString(R.string.home));
        switchFragment(new NewHomeFragment(), "", false, null);
        setLeftMenuSelection(1);
    }


    private void setDataAccToRole() {
        if (getLoginRoleType().equals(Constants.ADMIN)) {
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            homeLl.setVisibility(View.GONE);
            homeView.setVisibility(View.GONE);
            enquiryTV.setText(getString(R.string.enquiries));
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
        } else if (getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            homeLl.setVisibility(View.VISIBLE);
            homeView.setVisibility(View.VISIBLE);
            enquiryTV.setText(getString(R.string.enquiries));
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
        } else if (getLoginRoleType().equals(Constants.CUSTOMER)) {
            orderLL.setVisibility(View.GONE);
            orderView.setVisibility(View.GONE);
            enquiryLL.setVisibility(View.GONE);
            enquiryView.setVisibility(View.GONE);
            notificationLL.setVisibility(View.VISIBLE);
            notificationView.setVisibility(View.VISIBLE);
            myCustomerView.setVisibility(View.GONE);
            myMessageView.setVisibility(View.GONE);
            customerComplaintsView.setVisibility(View.GONE);
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
            myMessageLL.setVisibility(View.GONE);
            customerComplaintsLL.setVisibility(View.GONE);
            myCustomersLL.setVisibility(View.GONE);


            homeLl.setVisibility(View.VISIBLE);
            searchDealerLL.setVisibility(View.VISIBLE);
            customerReviewLL.setVisibility(View.VISIBLE);
            registerYourProLL.setVisibility(View.VISIBLE);
            myProductsLL.setVisibility(View.VISIBLE);
            privacyPolicyLL.setVisibility(View.VISIBLE);
            offerLL.setVisibility(View.VISIBLE);
            convertorLL.setVisibility(View.VISIBLE);
            contactLL.setVisibility(View.VISIBLE);
            complaintsLL.setVisibility(View.VISIBLE);


            homeView.setVisibility(View.VISIBLE);
            searchDealerView.setVisibility(View.VISIBLE);
            customerReviewView.setVisibility(View.VISIBLE);
            registerYourProView.setVisibility(View.VISIBLE);
            myProductsView.setVisibility(View.VISIBLE);
            privacyPolicyView.setVisibility(View.VISIBLE);
            offerView.setVisibility(View.VISIBLE);
            convertorView.setVisibility(View.VISIBLE);
            contactView.setVisibility(View.VISIBLE);
            complaintsView.setVisibility(View.VISIBLE);


        } else if (getLoginRoleType().equals(Constants.SERVICE)) {
            mCheck = true;
            contactLL.setVisibility(View.GONE);
            contactView.setVisibility(View.GONE);
            homeLl.setVisibility(View.GONE);
            homeView.setVisibility(View.GONE);
            enquiryTV.setText(getString(R.string.enquiries));
            documentLL.setVisibility(View.GONE);
            documentView.setVisibility(View.GONE);
            orderLL.setVisibility(View.GONE);
            orderView.setVisibility(View.GONE);
            enquiryLL.setVisibility(View.GONE);
            enquiryView.setVisibility(View.GONE);
            notificationLL.setVisibility(View.GONE);
            myMessageLL.setVisibility(View.GONE);
            customerComplaintsLL.setVisibility(View.GONE);
            myCustomersLL.setVisibility(View.GONE);
            notificationView.setVisibility(View.GONE);
            myCustomerView.setVisibility(View.GONE);
            myMessageView.setVisibility(View.GONE);
            customerComplaintsView.setVisibility(View.GONE);
            privacyPolicyLL.setVisibility(View.GONE);
            privacyPolicyView.setVisibility(View.GONE);
            troubleLL.setVisibility(View.GONE);
            serviceManualLL.setVisibility(View.GONE);


            complaintsLL.setVisibility(View.VISIBLE);
            rewardslLL.setVisibility(View.VISIBLE);
            checkWarrantyLL.setVisibility(View.VISIBLE);
            homeLl.setVisibility(View.VISIBLE);


            complaintsView.setVisibility(View.VISIBLE);
            checkWarrantyView.setVisibility(View.VISIBLE);
            troubleView.setVisibility(View.GONE);
            serviceManualView.setVisibility(View.GONE);
            rewardsView.setVisibility(View.VISIBLE);
            homeView.setVisibility(View.VISIBLE);
        } else if (getLoginRoleType().equals(Constants.DEALER)) {
            setDealerAndGuestRequest();
        } else {
            setDealerAndGuestRequest();

        }
    }

    public void setDealerAndGuestRequest() {
        homeLl.setVisibility(View.VISIBLE);
        homeView.setVisibility(View.VISIBLE);
        orderLL.setVisibility(View.VISIBLE);
        orderView.setVisibility(View.VISIBLE);
        enquiryLL.setVisibility(View.VISIBLE);
        enquiryView.setVisibility(View.VISIBLE);
        notificationLL.setVisibility(View.VISIBLE);
        myMessageLL.setVisibility(View.VISIBLE);
        customerComplaintsLL.setVisibility(View.VISIBLE);
        myCustomersLL.setVisibility(View.VISIBLE);
        notificationView.setVisibility(View.VISIBLE);
        myCustomerView.setVisibility(View.VISIBLE);
        myMessageView.setVisibility(View.VISIBLE);
        customerComplaintsView.setVisibility(View.VISIBLE);
        contactLL.setVisibility(View.VISIBLE);
        contactView.setVisibility(View.VISIBLE);
        documentLL.setVisibility(View.VISIBLE);
        documentView.setVisibility(View.VISIBLE);


        privacyPolicyLL.setVisibility(View.GONE);
        privacyPolicyView.setVisibility(View.GONE);
        troubleLL.setVisibility(View.GONE);
        serviceManualLL.setVisibility(View.GONE);
        searchDealerLL.setVisibility(View.GONE);
        customerReviewLL.setVisibility(View.GONE);
        registerYourProLL.setVisibility(View.GONE);
        myProductsLL.setVisibility(View.GONE);
        privacyPolicyLL.setVisibility(View.GONE);
        offerLL.setVisibility(View.GONE);
        convertorLL.setVisibility(View.GONE);
        complaintsLL.setVisibility(View.GONE);


    }


    @Override
    protected void onResume() {
        super.onResume();
        imgsendRL.setClickable(true);
        imgsendRL.setFocusable(true);

        GahirPreference.writeString(mActivity, GahirPreference.LATITUDE, "");
        GahirPreference.writeString(mActivity, GahirPreference.LONGITUDE, "");
        if (IsLogin()) {
            if (getUserProfilePic() != null && getUserProfilePic().contains("http")) {
                Glide.with(mActivity)
                        .load(getUserProfilePic())
                        .placeholder(R.drawable.placeholder_img)
                        .error(R.drawable.placeholder_img)
                        .into(imageprofileIV);
            } else {
                imageprofileIV.setImageResource(R.drawable.placeholder_img);
            }
            if (getUserName() != null) {
                userNameTV.setText(getUserName());
            }
        } else {
            userNameTV.setText(getString(R.string.login_signup));
        }


    }

    private void setNavigationViewListener() {

        NavigationView navigationView = findViewById(R.id.nav_bar);
        bottomRL = findViewById(R.id.bottomRL);
        userNameTV = findViewById(R.id.userNameTV);
        imageprofileIV = findViewById(R.id.imageprofileIV);
        View header = navigationView.getHeaderView(0);


        homeLl = navigationView.findViewById(R.id.homeLL);
        searchDealerLL = navigationView.findViewById(R.id.searchDealerLL);
        customerReviewLL = navigationView.findViewById(R.id.customerReviewLL);
        registerYourProLL = navigationView.findViewById(R.id.registerYourProLL);
        myMessageLL = navigationView.findViewById(R.id.myMessageLL);
        customerComplaintsLL = navigationView.findViewById(R.id.customerComplaintsLL);
        myProductsLL = navigationView.findViewById(R.id.myProductsLL);
        myCustomersLL = navigationView.findViewById(R.id.myCustomerLL);
        privacyPolicyLL = navigationView.findViewById(R.id.privacyPolicyLL);
        offerLL = navigationView.findViewById(R.id.offerLL);
        orderLL = navigationView.findViewById(R.id.orderLL);
        convertorLL = navigationView.findViewById(R.id.convertorLL);
        complaintsLL = navigationView.findViewById(R.id.complaintsLL);
        checkWarrantyLL = navigationView.findViewById(R.id.checkWarrantyLL);
        troubleLL = navigationView.findViewById(R.id.troubleLL);
        serviceManualLL = navigationView.findViewById(R.id.serviceManualLL);
        rewardslLL = navigationView.findViewById(R.id.rewardslLL);
        enquiryLL = navigationView.findViewById(R.id.enquiryLL);
        documentLL = navigationView.findViewById(R.id.documentLL);
        notificationLL = navigationView.findViewById(R.id.notificationLL);
        contactLL = navigationView.findViewById(R.id.contactLL);
        loginLL = navigationView.findViewById(R.id.loginLL);
        logoutLL = navigationView.findViewById(R.id.logoutLL);
        convertorLL = navigationView.findViewById(R.id.convertorLL);


        homeIV = navigationView.findViewById(R.id.homeIV);
        searchDealerIV = navigationView.findViewById(R.id.searchDealerIV);
        customerReviewIV = navigationView.findViewById(R.id.customerReviewIV);
        registerYourProIV = navigationView.findViewById(R.id.registerYourProIV);
        myProductsIV = navigationView.findViewById(R.id.myProductsIV);
        myCustomerIV = navigationView.findViewById(R.id.myCustomerIV);
        privacyPolicyIV = navigationView.findViewById(R.id.privacyPolicyIV);
        offerIV = navigationView.findViewById(R.id.offerIV);
        complaintsIV = navigationView.findViewById(R.id.complaintsIV);
        checkWarrantyIV = navigationView.findViewById(R.id.checkWarrantyIV);
        troubleIV = navigationView.findViewById(R.id.troubleIV);
        serviceManualIV = navigationView.findViewById(R.id.serviceManualIV);
        rewardslIV = navigationView.findViewById(R.id.rewardslIV);
        notificationIV = navigationView.findViewById(R.id.notificationIV);
        myMessageIV = navigationView.findViewById(R.id.myMessageIV);
        customerComplaintsIV = navigationView.findViewById(R.id.customerComplaintsIV);
        searchIV = findViewById(R.id.searchIV);
        contactIV = navigationView.findViewById(R.id.contactIV);
        loginIV = navigationView.findViewById(R.id.loginIV);
        logoutIV = navigationView.findViewById(R.id.logoutIV);
        convertorIV = navigationView.findViewById(R.id.convertorIV);
        orderIV = navigationView.findViewById(R.id.orderIV);
        logoutIV = navigationView.findViewById(R.id.logoutIV);
        enquiryIV = navigationView.findViewById(R.id.enquiryIV);


        homeTV = navigationView.findViewById(R.id.homeTV);
        searchDealerTV = navigationView.findViewById(R.id.searchDealerTV);
        customerReviewTV = navigationView.findViewById(R.id.customerReviewTV);
        registerYourProTV = navigationView.findViewById(R.id.registerYourProTV);
        myProductsTV = navigationView.findViewById(R.id.myProductsTV);
        myCustomerTV = navigationView.findViewById(R.id.myCustomerTV);
        privacyPolicyTV = navigationView.findViewById(R.id.privacyPolicyTV);
        offerTV = navigationView.findViewById(R.id.offerTV);
        complaintsTV = navigationView.findViewById(R.id.complaintsTV);
        checkWarrantyTV = navigationView.findViewById(R.id.checkWarrantyTV);
        troubleTV = navigationView.findViewById(R.id.troubleTV);
        serviceManualTV = navigationView.findViewById(R.id.serviceManualTV);
        rewardsTV = navigationView.findViewById(R.id.rewardsTV);
        contactTV = navigationView.findViewById(R.id.contactTV);
        loginTV = navigationView.findViewById(R.id.loginTV);
        notificationTV = navigationView.findViewById(R.id.notificationTV);
        myMessageTV = navigationView.findViewById(R.id.myMessageTV);
        customerComplaintsTV = navigationView.findViewById(R.id.customerComplaintsTV);
        logoutTV = navigationView.findViewById(R.id.logoutTV);
        convertorTV = navigationView.findViewById(R.id.convertorTV);
        orderTV = navigationView.findViewById(R.id.orderTV);
        enquiryTV = navigationView.findViewById(R.id.enquiryTV);

        homeView = navigationView.findViewById(R.id.homeView);
        searchDealerView = navigationView.findViewById(R.id.searchDealerView);
        customerReviewView = navigationView.findViewById(R.id.customerReviewView);
        registerYourProView = navigationView.findViewById(R.id.registerYourProView);
        myProductsView = navigationView.findViewById(R.id.myProductsView);
        myMessageView = navigationView.findViewById(R.id.myMessageView);
        customerComplaintsView = navigationView.findViewById(R.id.customerComplaintsView);
        privacyPolicyView = navigationView.findViewById(R.id.privacyPolicyView);
        offerView = navigationView.findViewById(R.id.offerView);
        convertorView = navigationView.findViewById(R.id.convertorView);
        checkWarrantyView = navigationView.findViewById(R.id.checkWarrantyView);
        documentIV = navigationView.findViewById(R.id.documentIV);
        documentTV = navigationView.findViewById(R.id.documentTV);
        logoutView = navigationView.findViewById(R.id.logoutView);


        troubleView = navigationView.findViewById(R.id.troubleView);
        serviceManualView = navigationView.findViewById(R.id.serviceManualView);
        rewardsView = navigationView.findViewById(R.id.rewardsView);
        complaintsView = navigationView.findViewById(R.id.complaintsView);
        loginView = navigationView.findViewById(R.id.loginView);


        orderView = navigationView.findViewById(R.id.orderView);
        documentIV = navigationView.findViewById(R.id.documentIV);
        documentTV = navigationView.findViewById(R.id.documentTV);
        enquiryView = navigationView.findViewById(R.id.enquiryView);
        notificationView = navigationView.findViewById(R.id.notificationView);
        myCustomerView = navigationView.findViewById(R.id.myCustomerView);
        contactView = navigationView.findViewById(R.id.contactView);
        privacyPolicyView = navigationView.findViewById(R.id.privacyPolicyView);
        //  logoutView = navigationView.findViewById(R.id.logoutView);
        documentView = navigationView.findViewById(R.id.documentView);


        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.contact_us));
                    switchFragment(new ContactUsFragment(), "", false, null);
                    setLeftMenuSelection(17);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);

                }

            }
        });
        orderLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                        txtTitleTV.setText(getString(R.string.orders));
                    } else {
                        txtTitleTV.setText(getString(R.string.my_order));
                    }
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    switchFragment(new DealorOrderFragment(), "", false, null);
                    setLeftMenuSelection(6);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);

                }
            }
        });

        enquiryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (IsLogin()) {
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    downloadRL.setVisibility(View.GONE);
                    if (getLoginRoleType().equals(Constants.ADMIN) || getLoginRoleType().equals(Constants.SALES_EXECUTIVE)) {
                        txtTitleTV.setText(getString(R.string.enquiries));
                    } else {
                        txtTitleTV.setText(getString(R.string.MY_ENQUIRIES));
                    }
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    switchFragment(new DealorEnquiryListFragment(), "", false, null);
                    setLeftMenuSelection(7);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);

                }


            }
        });

        homeLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadRL.setVisibility(View.GONE);
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                preventMultipleClick();
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.VISIBLE);
                searchIV.setImageResource(R.drawable.ic_search);
                txtTitleTV.setText(getString(R.string.home));
                callIntent = true;
                switchFragment(new NewHomeFragment(), "", false, null);
                setLeftMenuSelection(1);
            }
        });

        offerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.search_dealer));
                    switchFragment(new PrivacyPolicyFragment(), "", false, null);
                    setLeftMenuSelection(10);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);

                }
            }
        });

        searchDealerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.search_dealer));
                switchFragment(new searchDealerFragment(), "", false, null);
                setLeftMenuSelection(2);
            }
        });
        privacyPolicyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.privacy_policy));
                    switchFragment(new PrivacyPolicyFragment(), "", false, null);
                    setLeftMenuSelection(9);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }


            }
        });

        notificationLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.notifications));
                    switchFragment(new NotificationFragment(), "", false, null);
                    setLeftMenuSelection(8);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }
            }
        });

        myMessageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.messages));
                    switchFragment(new MessageFragment(), "", false, null);
                    setLeftMenuSelection(20);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }

            }
        });

        customerComplaintsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.customer_complaints));
                    switchFragment(new CustomerComplaintsFragment(), "", false, null);
                    setLeftMenuSelection(22);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }
            }
        });

        documentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    txtTitleTV.setText(getString(R.string.document));
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    switchFragment(new DocumentFragment(), "", false, null);
                    setLeftMenuSelection(18);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }

            }
        });
        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    imgFilterRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.GONE);
                    preventMultipleClick();
                    drawer_layout.closeDrawer(GravityCompat.START);
                    //  setLeftMenuSelection(7);
                    showLogoutAlert(mActivity, mLogoutInterface);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }
            }
        });

        customerReviewLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntent = false;
                imgSearchRL.setVisibility(View.GONE);
                imgFilterRL.setVisibility(View.VISIBLE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                txtTitleTV.setText(getString(R.string.customer_review));
                switchFragment(new CustomerReviewFragment(), "", false, null);
                setLeftMenuSelection(3);

            }
        });

//        imgSearchRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(HomeActivity.this, FilterActivity.class);
//                startActivity(intent);
//
//
//            }
//        });


        registerYourProLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntent = false;
                drawer_layout.closeDrawer(GravityCompat.START);
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgSearchRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.choose_product_to_register));
                switchFragment(new AllProductListFragment(), "", false, null);
                setLeftMenuSelection(4);
            }
        });

        myProductsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMyProductClick();
            }
        });
        myCustomersLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IsLogin()) {
                    executeCustomersApi();
                    callIntent = true;
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgFilterRL.setVisibility(View.GONE);
                    imgSearchRL.setVisibility(View.GONE);
                    imgCartRL.setVisibility(View.GONE);
                    imgsendRL.setVisibility(View.VISIBLE);
                    txtTitleTV.setText(getString(R.string.my_customers));
                    switchFragment(new MyCustomersFragment(), "", false, null);
                    setLeftMenuSelection(21);
                } else {
                    showGuestLoginAlertDialog(mActivity, mGuestLoginInterface);
                }
            }
        });

        offerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.offers));
                switchFragment(new OfferFragment(), "", false, null);
                setLeftMenuSelection(10);

            }
        });

        complaintsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                if (!mCheck) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.complaints));
                    switchFragment(new ComplaintsFragment(), "", false, null);
                    setLeftMenuSelection(12);
                } else {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    imgSearchRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.complaints));
                    switchFragment(new ComplaintsServiceFragment(), "", false, null);
                    setLeftMenuSelection(12);
                }
            }
        });

        checkWarrantyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.check_warranty));
                switchFragment(new CheckWarrantyFragment(), "", false, null);
                setLeftMenuSelection(13);
            }
        });

        troubleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.products));
                switchFragment(new TroubleshootingFragment(), "", false, null);
                setLeftMenuSelection(14);
            }
        });

        serviceManualLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.products));
                switchFragment(new ServiceManualFragment(), "", false, null);
                setLeftMenuSelection(15);
            }
        });

        rewardslLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFilterRL.setVisibility(View.GONE);
                imgCartRL.setVisibility(View.GONE);
                imgsendRL.setVisibility(View.GONE);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSearchRL.setVisibility(View.GONE);
                txtTitleTV.setText(getString(R.string.rewards));
                switchFragment(new OfferFragment(), "", false, null);
                setLeftMenuSelection(16);
            }
        });


//        loginLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mCheck = true;
//                downloadRL.setVisibility(View.GONE);
//                setLeftMenuService(1);
//
//            }
//        });


        bottomRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick1 < 1500) {
                    return;
                }
                mTabClick1 = SystemClock.elapsedRealtime();
                if (IsLogin()) {
                    downloadRL.setVisibility(View.GONE);
                    txtTitleTV.setText(getString(R.string.profile));
                    Intent i = new Intent(mActivity, ProfileActivity.class);
                    startActivity(i);
                    drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    GahirPreference.writeString(mActivity, GahirPreference.GUEST_LOGIN, "false");
                    Intent i = new Intent(mActivity, SplashActivity.class);
                    startActivity(i);
                }

            }
        });
    }




    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL,R.id.imgCartRL, R.id.searchIV,R.id.imgFilterRL, R.id.containerRL, R.id.imgSearchRL,R.id.imgsendRL})


    private void sendClick() {
        imgsendRL.setClickable(false);
        imgsendRL.setFocusable(false);
        Intent intent = new Intent(HomeActivity.this, SendMessageActivity.class);
        startActivity(intent);
        imgsendRL.setClickable(true);
        imgsendRL.setFocusable(true);
    }

    private void setImageCartClick() {
        Intent intent = new Intent(HomeActivity.this, SelectedPartsActivity.class);
        startActivity(intent);
    }

    private void setFilterClick() {
        Intent intent = new Intent(HomeActivity.this, FilterActivity.class);
        startActivity(intent);
    }

    private void setSearchClick() {
        Intent i = new Intent(mActivity, SearchActivity.class);
        startActivity(i);
        //  finish();
    }

    private void drawerOpenClick() {
        hideCloseKeyboard(mActivity);
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerRL, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
    }

    public void setLeftMenuSelection(int pos) {
        homeIV.setImageResource(R.drawable.ic_home_us);
        searchDealerIV.setImageResource((R.drawable.ic_search_dealer));
        customerReviewIV.setImageResource((R.drawable.ic_review));
        registerYourProIV.setImageResource((R.drawable.ic_register));
        myProductsIV.setImageResource((R.drawable.ic_product));
        myCustomerIV.setImageResource(R.drawable.ic_customer);
        privacyPolicyIV.setImageResource((R.drawable.ic_privacy_us));
        offerIV.setImageResource((R.drawable.ic_offer));
        complaintsIV.setImageResource((R.drawable.ic_complaints));
        checkWarrantyIV.setImageResource(R.drawable.ic_chechk_warranty);
        troubleIV.setImageResource(R.drawable.ic_troubleshooting);
        serviceManualIV.setImageResource(R.drawable.ic_service_manual);
        rewardslIV.setImageResource(R.drawable.ic_rewards);
        contactIV.setImageResource((R.drawable.ic_contact_us));
        loginIV.setImageResource((R.drawable.ic_login));
        convertorIV.setImageResource((R.drawable.ic_convertor_sv));
        orderIV.setImageResource((R.drawable.ic_order_us));
        enquiryIV.setImageResource((R.drawable.ic_enquiry_us));
        notificationIV.setImageResource((R.drawable.ic_notification_us));
        myMessageIV.setImageResource(R.drawable.ic_msg);
        customerComplaintsIV.setImageResource(R.drawable.ic_complaints);
        documentIV.setImageResource((R.drawable.ic_doc_us));

        contactTV.setTextColor(getResources().getColor(R.color.colorBlack));
        homeTV.setTextColor(getResources().getColor(R.color.colorBlack));
        searchDealerTV.setTextColor(getResources().getColor(R.color.colorBlack));
        customerReviewTV.setTextColor(getResources().getColor(R.color.colorBlack));
        registerYourProTV.setTextColor(getResources().getColor(R.color.colorBlack));
        myProductsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        myCustomerTV.setTextColor(getResources().getColor(R.color.colorBlack));
        privacyPolicyTV.setTextColor(getResources().getColor(R.color.colorBlack));
        offerTV.setTextColor(getResources().getColor(R.color.colorBlack));
        complaintsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        checkWarrantyTV.setTextColor(getResources().getColor(R.color.colorBlack));
        troubleTV.setTextColor(getResources().getColor(R.color.colorBlack));
        serviceManualTV.setTextColor(getResources().getColor(R.color.colorBlack));
        rewardsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        loginTV.setTextColor(getResources().getColor(R.color.colorBlack));
        enquiryTV.setTextColor(getResources().getColor(R.color.colorBlack));
        convertorTV.setTextColor(getResources().getColor(R.color.colorBlack));
        orderTV.setTextColor(getResources().getColor(R.color.colorBlack));
        notificationTV.setTextColor(getResources().getColor(R.color.colorBlack));
        myMessageTV.setTextColor(getResources().getColor(R.color.colorBlack));
        customerComplaintsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        documentTV.setTextColor(getResources().getColor(R.color.colorBlack));

        if (pos == 1) {
            homeIV.setImageResource(R.drawable.ic_home_s);
            homeTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 2) {
            searchDealerIV.setImageResource((R.drawable.ic_search_red));
            searchDealerTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 3) {
            customerReviewIV.setImageResource((R.drawable.ic_review_red));
            customerReviewTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 4) {
            registerYourProIV.setImageResource((R.drawable.ic_register_red));
            registerYourProTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 5) {
            myProductsIV.setImageResource((R.drawable.ic_product_red));
            myProductsTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 6) {
            orderIV.setImageResource((R.drawable.ic_order_s));
            orderTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 7) {
            enquiryIV.setImageResource((R.drawable.ic_enquiry_s));
            enquiryTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 8) {
            notificationIV.setImageResource((R.drawable.ic_notification_s));
            notificationTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 9) {
            privacyPolicyIV.setImageResource((R.drawable.ic_privacy_s));
            privacyPolicyTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 10) {
            offerIV.setImageResource((R.drawable.ic_doc_s));
            offerTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 11) {
            convertorIV.setImageResource((R.drawable.ic_convertor_sv_red));
            convertorTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 12) {
            complaintsIV.setImageResource((R.drawable.ic_complaints_sv_red));
            complaintsTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 13) {
            checkWarrantyIV.setImageResource((R.drawable.ic_check_warranty_red));
            checkWarrantyTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 14) {
            troubleIV.setImageResource((R.drawable.ic_troubleshooting_red));
            troubleTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 15) {
            serviceManualIV.setImageResource((R.drawable.ic_service_manual_red));
            serviceManualTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 16) {
            rewardslIV.setImageResource((R.drawable.ic_rewards_red));
            rewardsTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 17) {
            contactIV.setImageResource((R.drawable.ic_contact_s));
            contactTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 18) {
            documentIV.setImageResource((R.drawable.ic_doc_s));
            documentTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 19) {
            logoutIV.setImageResource((R.drawable.ic_login_red));
            logoutTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 20) {
            myMessageIV.setImageResource((R.drawable.ic_msg_s));
            myMessageTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 21) {
            myCustomerIV.setImageResource(R.drawable.ic_customer_s);
            myCustomerTV.setTextColor(getResources().getColor(R.color.colorRed));
        } else if (pos == 22) {
            customerComplaintsIV.setImageResource(R.drawable.ic_complaints_red);
            customerComplaintsTV.setTextColor(getResources().getColor(R.color.colorRed));
        }

    }


    public void getCurrentLATLong() {
        easyLocationProvider = new EasyLocationProvider.Builder(mActivity)
                .setInterval(3000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setListener(new EasyLocationProvider.EasyLocationCallback() {
                    @Override
                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {
                        Log.e("EasyLocationProvider", "onGoogleAPIClient: " + message);
                    }

                    @Override
                    public void onLocationUpdated(double latitude, double longitude) {

                        Log.e("EasyLocationProvider", "onLocationUpdated:: " + "Latitude: " + latitude + " Longitude: " + longitude);
                        if (isCurrentLocation) {
                            //Toast.makeText(getActivity(), "successfull", Toast.LENGTH_SHORT).show();
                            TimeZone tz = TimeZone.getDefault();
//                            Log.e(TAG, "TimeZone::" + tz.getDisplayName(false, TimeZone.SHORT) + " Timezone id :: " + tz.getID());
//                            Log.e(TAG, "LAT::LNG::" + latitude + longitude);

                            mLatitude = latitude;
                            mLongitude = longitude;

                            GahirPreference.writeString(mActivity, GahirPreference.LATITUDE, String.valueOf(latitude));
                            GahirPreference.writeString(mActivity, GahirPreference.LONGITUDE, String.valueOf(longitude));

                            if (isNetworkAvailable(mActivity))
                                getAddressFromLatLong();
                            else
                                showToast(mActivity, getString(R.string.internet_connection_error));
                        }
                    }

                    @Override
                    public void onLocationUpdateRemoved() {
                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
                    }
                }).build();
        getLifecycle().addObserver(easyLocationProvider);
    }

    private void getAddressFromLatLong() {
        isCurrentLocation = false;
        String mSelectedLocation = getAddressFromLatLong(mActivity, Double.parseDouble(getLatitude()), Double.parseDouble(getLongitude()));
        System.out.println("mSelectedLocation: " + mSelectedLocation);

        //    executeGetAddressApi(mSelectedLocation);

    }
//    private void executeGetAddressApi(String mSelection) {
//   //     showProgressDialog(mActivity);
//        String mApiUrl = Constants.GOOGLE_PLACES_SEARCH + "input=" + mSelection + "&fields=name,address_component,place_id&key=" + Constants.PLACES_KEY;
////        String mApiUrl = Constants.GOOGLE_PLACES_SEARCH + "input=" + mSelection + "&components=country:us" + "&radius=" + 1000 + "&key=" + Constants.API_KEY;
//        Log.e(TAG, "**SearchAddressFromLatLong**" + mApiUrl);
//        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.getAddressFromLatLngRequest(mApiUrl).enqueue(new Callback<SearchLocationModel>() {
//            @Override
//            public void onResponse(Call<SearchLocationModel> call, Response<SearchLocationModel> response) {
//  //              if (progressDialog.isShowing())
//      //              dismissProgressDialog();
//                if (response.code() == 200) {
//                    if (response.body().getPredictions().size() > 0) {
//                        for (int j = 0; j < 1; j++) {
//                            PredictionsItem mModel = response.body().getPredictions().get(j);
//                            String mCity = "";
//                            String mCity1 = "";
//                            String mState = "";
//                            int n = mModel.getTerms().size();
//                            for (int i = n - 1; i >= 0; i--) {
//                                if (i == n) {
//                                    mCity = "";
//                                } else if (i == n - 2) {
//                                    if (mModel.getTerms().get(i).getValue().length() == 2) {
//                                        mState = mModel.getTerms().get(i).getValue();
//                                    } else {
//                                        mState = getStateAbbreviation(mModel.getTerms().get(i).getValue());
//                                    }
//                                } else if (i == n - 3) {
//                                    mCity1 = mModel.getTerms().get(i).getValue();
//                                    mCity = mModel.getTerms().get(i).getValue();
//                                    if (mCity.length() > 9) {
//                                        mCity = mCity.substring(0, 9);
//                                        mCity = mCity + "..";
//                                    }
//                                }
//                            }
//
//                            String mShowCityState = mCity + ", " + mState.toUpperCase();
//                            String mShowActualCityState = mCity1 + ", " + mState.toUpperCase();
//
//                            mModel.setShowCityState(mShowCityState);
//                            mModel.setShowActualCityState(mShowActualCityState);
//
//                            mPlaceID = mModel.getPlaceId();
//                            GahirPreference.writeString(mActivity, GahirPreference.SELECTED_LOCATION, mModel.getShowCityState());
//                        //    GahirPreference.writeString(mActivity, GahirPreference.FULL_SELECTED_LOCATION, mModel.getShowActualCityState());
//                            GahirPreference.writeString(mActivity, GahirPreference.FULL_SELECTED_LOCATION, mModel.getDescription());
////                            Intent mIntent = new Intent();
////                            mIntent.putExtra("LOC", response.body().getPredictions().get(0).getShowCityState());
////                            mIntent.putExtra("PID", mPlaceID);
////                            setResult(567, mIntent);
////                            finish();
//
//
//                            Full_address=GahirPreference.readString(mActivity, GahirPreference.FULL_SELECTED_LOCATION, "");
//
//                        }} else {
//                        showToast(mActivity, getString(R.string.not_able_to_find));
//                    }
//                } else if (response.code() == 401) {
//                    showToast(mActivity, getString(R.string.Unauthorized));
//                } else if (response.code() == 500) {
//                    showToast(mActivity, getString(R.string.server_error));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SearchLocationModel> call, Throwable t) {
//     //           dismissProgressDialog();
//                Log.e(TAG, "**ERROR**" + t.getMessage());
//            }
//        });
//    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
//        mMap.put("lat", String.valueOf(mLatitude));
//        mMap.put("long", String.valueOf(mLongitude));
        mMap.put("lat", GahirPreference.readString(mActivity, GahirPreference.LATITUDE, ""));
        mMap.put("long", GahirPreference.readString(mActivity, GahirPreference.LONGITUDE, ""));
        mMap.put("address", GahirPreference.readString(mActivity, GahirPreference.FULL_SELECTED_LOCATION, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateLocation() {

        ApiInterface apiInterface = RestClient.getApiClient().create(ApiInterface.class);
        apiInterface.locationModel(mParam()).enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Call<LocationModel> call, Response<LocationModel> response) {

                LocationModel locationModel = response.body();
                if (locationModel.getStatus().equals("1")) {
                    Log.e(TAG, "VALUE::::" + "wORKING");
                }
            }

            @Override
            public void onFailure(Call<LocationModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }


    public boolean checkLocationPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, 222);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 222:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getCurrentLATLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    public void setMyProductClick() {
        callIntent = true;
        drawer_layout.closeDrawer(GravityCompat.START);
        imgFilterRL.setVisibility(View.GONE);
        imgSearchRL.setVisibility(View.GONE);
        imgCartRL.setVisibility(View.VISIBLE);
        txtTitleTV.setText(getString(R.string.products));
        switchFragment(new RegisterProductListFragment(), "", false, null);
        setLeftMenuSelection(5);
    }

    private void executeCustomersApi() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("access_token", getAccessToken());
        mMap.put("page_no", "1");
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.customersListApi(mMap).enqueue(new Callback<CustomersListModel>() {
            @Override
            public void onResponse(Call<CustomersListModel> call, Response<CustomersListModel> response) {
                CustomersListModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {

                } else if (mModel.getStatus().equals("100")) {
                    showFinishLogoutAlertDialog(mActivity, mModel.getMessage(), mLogoutInterface);
                } else if (mModel.getStatus().equals("0")) {
                    imgsendRL.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CustomersListModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.toString());
            }
        });

    }


}
