package com.gahiragro.app.activities.Dealer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.devlomi.record_view.RecordButton;
import com.devlomi.record_view.RecordView;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;

public class RecordAudioActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = RecordAudioActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = RecordAudioActivity.this;


//    @BindView(R.id.time_txt)
    TextView time_txt;

//    @BindView(R.id.audioIV)
    ImageView audioIV;

//    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;

//    @BindView(R.id.iv_audio)
    RecordButton iv_audio;

//    @BindView(R.id.record_view)
    RecordView record_view;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String recordAudio = Manifest.permission.RECORD_AUDIO;
    public String readMediaImages = Manifest.permission.READ_MEDIA_IMAGES;
    public String readMediaAudio = Manifest.permission.READ_MEDIA_AUDIO;
    MediaRecorder mediaRecorder;
    CountDownTimer countDownTimer;
    String AudioSavePathInDevice = "";
    int timers_second = -1;
    int second = -1;
    int minute = 0;
    int hour = 0;
    boolean isRecording = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_audio);
        setStatusBar(mActivity, Color.WHITE);
//        ButterKnife.bind(this);

        time_txt=findViewById(R.id.time_txt);
        audioIV=findViewById(R.id.audioIV);
        imgBackIV=findViewById(R.id.imgBackIV);
        iv_audio=findViewById(R.id.iv_audio);
        record_view=findViewById(R.id.record_view);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //IMPORTANT
        if (checkPermission()) {
            iv_audio.isListenForRecord();
        } else {
            requestPermission();
        }

        onViewClicked();



//        File root = Environment.getExternalStorageDirectory();
//        File file = new File(root.getAbsolutePath() + "/VoiceRecorder/Audios");
//
//        if (!file.exists()) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                try {
//                    Files.createDirectory(Paths.get(file.getAbsolutePath()));
//
//                    Log.e(TAG, "onCreate: folder created at"+file.getAbsolutePath() );
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else  {
//
//                Log.e(TAG, "onCreate else: folder created at"+file.getAbsolutePath() );
//
//                file.mkdir();
//                file.mkdirs();
//            }
//        }
//

    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            int readImages = ContextCompat.checkSelfPermission(mActivity, readMediaImages);
            int readAudio = ContextCompat.checkSelfPermission(mActivity, readMediaAudio);
            int recordAudios = ContextCompat.checkSelfPermission(mActivity, recordAudio);
            return readImages == PackageManager.PERMISSION_GRANTED && readAudio == PackageManager.PERMISSION_GRANTED&& recordAudios == PackageManager.PERMISSION_GRANTED;
        }else {
            int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
            int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
//            int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED /*&& camera == PackageManager.PERMISSION_GRANTED*/;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(mActivity, new String[]{readMediaImages, readMediaAudio, recordAudio}, 369);
        }else {
            ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage}, 369);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    // onSelectImageClick();
                    iv_audio.setListenForRecord(true);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    public void startAudioRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.release();
        }

         mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

/**In the lines below, we create a directory named VoiceRecorder/Audios in the phone storage
 //         * and the audios are being stored in the Audios folder **/
//        File root = Environment.getExternalStorageDirectory();
//        File file = new File(root.getAbsolutePath() + "/VoiceRecorder/Audios");
//
//        if (!file.exists()) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                try {
//                    Files.createDirectory(Paths.get(file.getAbsolutePath()));
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else  {
//                file.mkdir();
//                file.mkdirs();
//            }
//        }
//        AudioSavePathInDevice = root.getAbsolutePath() + "/VoiceRecorder/Audios/" + System.currentTimeMillis() + ".mp3";

        File dir_ = new File(this.getFilesDir(), "VOICE_RECORDING");
        dir_.mkdirs();
        AudioSavePathInDevice = dir_ .getPath()+ System.currentTimeMillis() + ".mp3";
        Log.e("filename", AudioSavePathInDevice);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IllegalStateException e) {
           showToast(mActivity,e.getMessage());
        } catch (Exception e) {
            showToast(mActivity,e.getMessage());
        }
        showTimer();
    }

    public void showTimer() {

        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long l) {
                second++;
                timers_second++;
                if (timers_second < 120) {
                    //  time_txt?.setText(recorderTime())
                    time_txt.setText(recorderTime());
                } else {
                    stopRecording();
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        };

        countDownTimer.start();
    }

    private void stopRecording() {
        //cancel count down timer
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        timers_second = -1;
        second = -1;
        minute = 0;
        hour = 0;
        if (mediaRecorder != null) {
            try {
                //stop mediaRecorder
                mediaRecorder.stop();
                mediaRecorder.reset();
            } catch (java.lang.IllegalStateException e) {
                e.printStackTrace();
            }
        }
        FileObserver observer = new FileObserver(AudioSavePathInDevice) {
            @Override
            public void onEvent(int event, @Nullable String file) {
                if (event == CREATE && file.endsWith(".mp4")) {
                    Log.d(
                            "TAG",
                            "File created [" + AudioSavePathInDevice + file + "]"
                    );
                    Toast.makeText(RecordAudioActivity.this, "$file was saved!", Toast.LENGTH_LONG).show();
                    // upLoadFileToServer();
                }
            }
        };
        observer.startWatching();//START OBSERVING
        Log.e("TAG", "****Recorded audio path****" + AudioSavePathInDevice);
        if (AudioSavePathInDevice != "") {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.AUDIO_PATH, AudioSavePathInDevice);
            setResult(RESULT_OK, returnIntent);
            finish();

        } else {
        }
    }

    //recorder time
    public String recorderTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }
        if (minute == 60) {
            hour++;
            minute = 0;
        }
//        return String.format("%02d:%02d:%02d", hour, minute, second)
        return String.format("%02d:%02d", minute, second);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaRecorder != null) {
            mediaRecorder.release();
        }
    }


    public void performAudioClick() {
        if (!isRecording) {
            //Start Recording..
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startAudioRecorder();
                }
            }, 400);

            Log.d("RecordView", "onStart");

            isRecording = true;
            //   audioIV?.setImageResource(R.drawable.ic_stop_audio);
            // audioIV?.circleBackgroundColor = resources.getColor(R.color.chatBgColor);
            audioIV.setImageResource(R.drawable.ic_play_microphone);
        } else {
            if (timers_second >= 1) {
                //Stop Recording..
                stopRecording();
                //                String time = getHumanTimeText(recordTime);
                Log.d("RecordView", "onFinish");
                isRecording = false;
                // audioIV?.setImageResource(R.drawable.ic_record_audio);
                // audioIV?.circleBackgroundColor = resources.getColor(R.color.colorWhite);
                audioIV.setImageResource(R.drawable.ic_pause_microphone);


            }
        }
    }


//    @OnClick({R.id.audioIV, R.id.imgBackIV})
    public void onViewClicked() {


        audioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    iv_audio.isListenForRecord();
                    performAudioClick();
                } else {
//                    requestPermission()
                    showAlertDialog(mActivity, getString(R.string.dont_have_access_to_use));
                }
            }
        });

        imgBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        /*switch (view.getId()) {
            case R.id.audioIV:
                if (checkPermission()) {
                    iv_audio.isListenForRecord();
                    performAudioClick();
                } else {
//                    requestPermission()
                    showAlertDialog(mActivity, getString(R.string.dont_have_access_to_use));
                }
                break;
            case R.id.imgBackIV:
                break;

        }*/
    }
}
