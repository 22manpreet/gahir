package com.gahiragro.app.activities.Dealer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.BaseActivity;
import com.gahiragro.app.utils.Constants;

public class FilterActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = FilterActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = FilterActivity.this;
    LinearLayout stateLL, productLL;
    RelativeLayout imgMenuRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        setStatusBar(mActivity, Color.WHITE);
        stateLL = findViewById(R.id.stateLL);
        productLL = findViewById(R.id.productLL);
        imgMenuRL = findViewById(R.id.imgMenuRL);

        Onclick();
    }

    void Onclick() {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        stateLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FilterActivity.this, StatesListingActivity.class);
                startActivity(intent);
            }
        });

        productLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FilterActivity.this, ProductListingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Constants.FILTER_BACK_PRESSED){
            finish();
        }
    }
}