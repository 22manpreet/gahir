package com.gahiragro.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.DealorPhoneLoginModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.HashMap;
import java.util.Map;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneLoginActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = PhoneLoginActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = PhoneLoginActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

//    @BindView(R.id.btnGenerateOTP)
    Button btnGenerateOTP;

//    @BindView(R.id.editNumberET)
    EditText editNumberET;

//    @BindView(R.id.numberRL)
    RelativeLayout numberRL;

    //Initialize
    CountryCodePicker ccp;
    String countryCode = "";
    String strPushToken = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);
//        ButterKnife.bind(this);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        btnGenerateOTP=findViewById(R.id.btnGenerateOTP);
        editNumberET=findViewById(R.id.editNumberET);
        numberRL=findViewById(R.id.numberRL);

        editTextSelector(editNumberET, numberRL, "");
        setCountryCodePicker();
        getPushToken();

        onViewClicked();
    }
    @Override
    protected void onResume() {
        super.onResume();
        editNumberET.setText("");
    }
    private void setCountryCodePicker() {
        ccp = findViewById(R.id.ccp);
        ccp.enableHint(false);
        hideCloseKeyboard(mActivity);
//        ccp.setTextColor(getResources().getColor(R.color.white));
        ccp.registerPhoneNumberTextView(editNumberET);
        ccp.setDefaultCountryUsingNameCodeAndApply("IN");
        Log.e(TAG, "Code== " + ccp.getSelectedCountryCode());
        countryCode = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });
    }
    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.imgMenuRL, R.id.btnGenerateOTP})
    public void onViewClicked(/*View view*/) {
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        btnGenerateOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executePhoneLoginApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
            }
        });


       /* switch (view.getId()) {
            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.btnGenerateOTP:
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executePhoneLoginApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }
                break;
        }*/
    }
    private Boolean isValidate() {
        boolean flag = true;
        if (editNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (!isValidMobile(editNumberET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_number));
            flag = false;
        }
        return flag;
    }
    private void setBackClick() {
        onBackPressed();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private Map<String, String> mPhoneLoginParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("phone", countryCode + editNumberET.getText().toString().trim());
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        GahirPreference.writeString(mActivity, GahirPreference.DEVICE_TOKEN, strPushToken);
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });
    }
    private void executePhoneLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.PhoneLOgin(mPhoneLoginParam()).enqueue(new Callback<DealorPhoneLoginModel>() {
            @Override
            public void onResponse(Call<DealorPhoneLoginModel> call, Response<DealorPhoneLoginModel> response) {
                dismissProgressDialog();
                DealorPhoneLoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Intent i = new Intent(mActivity, OTPVerificationLoginActivity.class);
                    i.putExtra(Constants.PHONE_NUMBER, countryCode + editNumberET.getText().toString().trim());
                    startActivity(i);
                    GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
                    GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                    GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
                    GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                    GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, mModel.getUserDetail().getRole());
                   // GahirPreference.writeString(mActivity, GahirPreference.DEALER_DOC, mModel.getUserDetail().getDealerDoc());
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
                    SharedPreferences.Editor editor = prefs.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(mModel.getUserDetail().getDealerDoc());
                    editor.putString("doc", json);
                    editor.apply();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }
            @Override
            public void onFailure(Call<DealorPhoneLoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }
}
