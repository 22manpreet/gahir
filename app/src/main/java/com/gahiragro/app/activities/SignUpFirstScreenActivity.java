package com.gahiragro.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.CustomerPhoneLogin;
import com.gahiragro.app.model.DealorPhoneLoginModel;
import com.gahiragro.app.model.ForgetPasswordModel;
import com.gahiragro.app.model.VerifyDealerModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignUpFirstScreenActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpFirstScreenActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpFirstScreenActivity.this;
    /**
     * Widgets
     */


//    @BindView(R.id.roleTV)
    TextView roleTV;
    //    @BindView(R.id.roleSpinner)
    Spinner roleSpinner;
    //    @BindView(R.id.mobileNumberET)
    EditText mobileNumberET;
    //    @BindView(R.id.generateOTPBT)
    Button generateOTPBT;
    //    @BindView(R.id.roleSpinnerRL)
    RelativeLayout roleSpinnerRL;
    //    @BindView(R.id.dealorCodeLL)
    LinearLayout dealorCodeLL;

    //    @BindView(R.id.serialNoLL)
    LinearLayout serialNoLL;

    //    @BindView(R.id.titleTV)
    TextView titleTV;

    //    @BindView(R.id.serail_noET)
    EditText serail_noET;

    //    @BindView(R.id.dealorCodeET)
    EditText dealorCodeET;

    //    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;

    //    @BindView(R.id.hintIV)
    ImageView hintIV;


    CountryCodePicker ccp;
    String selRole = "";


    List<String> role = new ArrayList<String>();
    String strPushToken = "";
    String countryCode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_first_screen);
//        ButterKnife.bind(this);
        initViews();
        setStatusBar(mActivity, Color.WHITE);

        if (getIntent() != null) {
            selRole = getIntent().getStringExtra("selRole");
        }
        mobileNumberET.setText("");
        getPushToken();
        role.add("Role");
        role.add("Dealor");
        role.add("Sales");
        setRoleSpinner();
        setCountryCodePicker();
        setRoleSlectionData();
    }


    private void initViews() {
        roleTV = findViewById(R.id.roleTV);
        hintIV = findViewById(R.id.hintIV);
        imgMenuRL = findViewById(R.id.imgMenuRL);
        dealorCodeET = findViewById(R.id.dealorCodeET);
        serail_noET = findViewById(R.id.serail_noET);
        titleTV = findViewById(R.id.titleTV);
        serialNoLL = findViewById(R.id.serialNoLL);
        dealorCodeLL = findViewById(R.id.dealorCodeLL);
        roleSpinnerRL = findViewById(R.id.roleSpinnerRL);
        generateOTPBT = findViewById(R.id.generateOTPBT);
        mobileNumberET = findViewById(R.id.mobileNumberET);
        roleSpinner = findViewById(R.id.roleSpinner);

        onViewClicked();
    }


    private void setRoleSlectionData() {
        if (selRole.equals("Customer")) {
            serialNoLL.setVisibility(View.GONE);
            titleTV.setText(getString(R.string.customer_signup));
        } else if (selRole.equals("Dealer")) {
            dealorCodeLL.setVisibility(View.VISIBLE);
            mobileNumberET.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            titleTV.setText(getString(R.string.dealor_signup));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mobileNumberET.setText("");
        dealorCodeET.setText("");
        serail_noET.setText("");
    }

    private void setCountryCodePicker() {
        ccp = findViewById(R.id.ccp);
        ccp.enableHint(false);
        hideCloseKeyboard(mActivity);
//        ccp.setTextColor(getResources().getColor(R.color.white));
        ccp.registerPhoneNumberTextView(mobileNumberET);
        ccp.setDefaultCountryUsingNameCodeAndApply("IN");
        Log.e(TAG, "Code== " + ccp.getSelectedCountryCode());

        countryCode = ccp.getSelectedCountryCodeWithPlus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });
    }

    private void setRoleSpinner() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "Poppins-Regular.otf");
        ArrayAdapter<String> mColorAd = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, role) {

        };
        roleSpinner.setAdapter(mColorAd);

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorBlack));
                ((TextView) view).setText(item);
                view.setPadding(30, 0, 0, 0);
                ((TextView) view).setTextSize(15);
                ((TextView) view).setTypeface(font);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.generateOTPBT, R.id.roleTV, R.id.imgMenuRL, R.id.hintIV})
    public void onViewClicked(/*View view*/) {
        generateOTPBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performGenerateOtpClick();
            }
        });
        roleTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformOptionsClick();
            }
        });
        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });
        hintIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHintClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.generateOTPBT:
                performGenerateOtpClick();
                break;
            case R.id.roleTV:
                PerformOptionsClick();
                break;

            case R.id.imgMenuRL:
                setBackClick();
                break;
            case R.id.hintIV:
                setHintClick();
                //   showDealerAlertDialog();
                break;

        }*/
    }

    private void setHintClick() {
        Intent intent = new Intent(mActivity, InvoiceActivity.class);
        startActivity(intent);
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performGenerateOtpClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeDealerCustomerVerifyApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }


        }


    }

    private Boolean isValidate() {
        boolean flag = true;
        if (mobileNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (dealorCodeLL.getVisibility() == View.VISIBLE) {
            if (dealorCodeET.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_dealer_code));
                flag = false;
            }
        } else if (serialNoLL.getVisibility() == View.VISIBLE) {
            if (serail_noET.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_serial_number));
                flag = false;
            }
        } else if (!isValidMobile(mobileNumberET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_number));
            flag = false;
        }
        return flag;
    }


    private void sendConfimations() {
        Intent verificationIntent = new Intent(mActivity, OTPVerificationActivity.class);
        verificationIntent.putExtra(Constants.PHONE_NUMBER, mobileNumberET.getText().toString().trim());
        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(verificationIntent);
        finish();
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        GahirPreference.writeString(mActivity, GahirPreference.DEVICE_TOKEN, strPushToken);
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });

    }


    private void PerformOptionsClick() {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout dealorRL = view.findViewById(R.id.dealorRL);
        RelativeLayout customerRL = view.findViewById(R.id.customerRL);
        RelativeLayout salesRL = view.findViewById(R.id.salesRL);
        ImageView SalesIV = view.findViewById(R.id.SalesIV);
        ImageView CustomerIV = view.findViewById(R.id.CustomerIV);
        ImageView dealorIV = view.findViewById(R.id.dealorIV);

        dealorRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roleTV.setText(getString(R.string.dealor));
                SalesIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_checked));
                CustomerIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));
                dealorIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));

            }
        });
        customerRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roleTV.setText(getString(R.string.customer));
                SalesIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));
                CustomerIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_checked));
                dealorIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));
            }
        });
        salesRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roleTV.setText(getString(R.string.sales));
                SalesIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));
                CustomerIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_unchecked));
                dealorIV.setBackground(getResources().getDrawable(R.drawable.ic_radio_checked));
            }
        });


    }


    private Map<String, String> mDealerParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("phone", countryCode + mobileNumberET.getText().toString().trim());
        mMap.put("dealer_code", dealorCodeET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private Map<String, String> mCustomerParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("phone", countryCode + mobileNumberET.getText().toString().trim());
//        mMap.put("serial_no", serail_noET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDealerCustomerVerifyApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        if (selRole.equals("Dealer")) {
            mApiInterface.verifyDealerApi(mDealerParam()).enqueue(new Callback<VerifyDealerModel>() {
                @Override
                public void onResponse(Call<VerifyDealerModel> call, Response<VerifyDealerModel> response) {
                    dismissProgressDialog();
                    Log.e(TAG, "RESPONCE::" + response.body().toString());
                    VerifyDealerModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        String appSignUp = String.valueOf(mModel.getAppSignup());
                        Intent i = new Intent(mActivity, OTPVerificationActivity.class);
                        GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, selRole);
                        i.putExtra(Constants.PHONE_NUMBER, countryCode + mobileNumberET.getText().toString().trim());
                        i.putExtra(Constants.DEALER_CODE, dealorCodeET.getText().toString().trim());
                        i.putExtra(Constants.APP_SIGNUP, appSignUp);
                        i.putExtra(Constants.ADDRESS, mModel.getDealerDetail().getAddress());
                        i.putExtra(Constants.STREET, mModel.getDealerDetail().getStreet());
                        i.putExtra(Constants.DEALER_NAME, mModel.getDealerDetail().getName());
                        i.putExtra(Constants.DISTRICT, mModel.getDealerDetail().getDistrict());
                        i.putExtra(Constants.STATE, mModel.getDealerDetail().getState());
                        i.putExtra(Constants.CITY_NAME, mModel.getDealerDetail().getCity());
                        startActivity(i);
                    } else {
                        showAlertDialog(mActivity, mModel.getMessage());

                    }


                }

                @Override
                public void onFailure(Call<VerifyDealerModel> call, Throwable t) {
                    dismissProgressDialog();
                    Log.e(TAG, "**ERROR**" + t.toString());
                    showToast(mActivity, t.toString());
                }
            });
        } else if (selRole.equals("Customer")) {
            mApiInterface.verifyCustomerApi(mCustomerParam()).enqueue(new Callback<VerifyDealerModel>() {
                @Override
                public void onResponse(Call<VerifyDealerModel> call, Response<VerifyDealerModel> response) {
                    dismissProgressDialog();
                    VerifyDealerModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        String appSignUp = String.valueOf(mModel.getAppSignup());
                        Intent i = new Intent(mActivity, OTPVerificationActivity.class);
                        GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, selRole);
                        i.putExtra(Constants.PHONE_NUMBER, countryCode + mobileNumberET.getText().toString().trim());
                        i.putExtra(Constants.SERIAL_NO, serail_noET.getText().toString().trim());
                        i.putExtra(Constants.APP_SIGNUP, appSignUp);
                        startActivity(i);
                    } else {
                        showAlertDialog(mActivity, mModel.getMessage());

                    }

                }

                @Override
                public void onFailure(Call<VerifyDealerModel> call, Throwable t) {
                    dismissProgressDialog();
                    Log.e(TAG, "**ERROR**" + t.toString());
                    showToast(mActivity, t.toString());
                }
            });
        }


    }

}
