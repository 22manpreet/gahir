package com.gahiragro.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.SSLCertificateSocketFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.downloader.httpclient.DefaultHttpClient;
import com.downloader.httpclient.HttpClient;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.model.DealorLoginModel;
import com.gahiragro.app.model.EmailLoginModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.net.HttpURLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignInActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignInActivity.this;
    /**
     * Widgets
     */
//    @BindView(R.id.editEmailET)
    EditText editEmailET,editPasswordET;
//    @BindView(R.id.editPasswordET)
//    EditText editPasswordET;
//    @BindView(R.id.txtForgotPwdTV)
    TextView txtForgotPwdTV;
//    @BindView(R.id.btnLoginB)
    Button btnLoginB;
//    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
//    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
//    @BindView(R.id.passwordRL)
    RelativeLayout passwordRL;

//    @BindView(R.id.imgMenuRL)
    RelativeLayout imgMenuRL;


    //INitialize
    String strPushToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        txtForgotPwdTV= findViewById(R.id.txtForgotPwdTV);
        btnLoginB=findViewById(R.id.btnLoginB);
        txtSignUpTV= findViewById(R.id.txtSignUpTV);
        passwordRL=findViewById(R.id.passwordRL);
        emailRL= findViewById(R.id.emailRL);
        imgMenuRL=findViewById(R.id.imgMenuRL);
        editEmailET= findViewById(R.id.editEmailET11);
        editPasswordET= findViewById(R.id.editPasswordET);


        setStatusBar(mActivity, Color.WHITE);
        editTextSelector(editEmailET, emailRL, "");
        editTextSelector(editPasswordET, passwordRL, "");
        getPushToken();

        onViewClicked();
    }

    /*
     * Widget Click Listner
     * */
//    @OnClick({R.id.txtForgotPwdTV, R.id.txtSignUpTV, R.id.btnLoginB, R.id.imgMenuRL})
    public void onViewClicked(/*View view*/) {

        txtSignUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignUpClick();
            }
        });

        btnLoginB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLoginClick();
            }
        });
        txtForgotPwdTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performForgotPwdClick();
            }
        });

        imgMenuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackClick();
            }
        });


        /*switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.btnLoginB:
                performLoginClick();
                break;
            case R.id.txtForgotPwdTV:
                performForgotPwdClick();
                break;
            case R.id.imgMenuRL:
                setBackClick();
                break;

        }*/
    }

    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performSignUpClick() {
        Intent i = new Intent(mActivity, RoleSelectActivity.class);
        startActivity(i);
        //finish();
    }


    private void performLoginClick() {
        if (isValidate())
            executeLoginApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void performForgotPwdClick() {
        Intent i = new Intent(mActivity, ForgotPasswordActivity.class);
        startActivity(i);
    }

    private Boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }

        return flag;
    }

    /*
     * Phone Login Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type", Constants.DEVICE_TYPE);
        mMap.put("device_token", strPushToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = RestClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emailLogin(mParam()).enqueue(new Callback<EmailLoginModel>() {
            @Override
            public void onResponse(Call<EmailLoginModel> call, Response<EmailLoginModel> response) {
                dismissProgressDialog();
                EmailLoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Intent i = new Intent(mActivity, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finishAffinity();
                    GahirPreference.writeString(mActivity, GahirPreference.USER_NAME, mModel.getUserDetail().getFirstName());
                    GahirPreference.writeString(mActivity, GahirPreference.USER_PROFILE_PIC, mModel.getUserDetail().getImage());
                    GahirPreference.writeString(mActivity, GahirPreference.ACCESS_TOKEN, mModel.getAccessToken());
                    GahirPreference.writeString(mActivity, GahirPreference.LOGIN_TYPE, mModel.getUserDetail().getRole());
                    GahirPreference.writeBoolean(mActivity, GahirPreference.IS_LOGIN, true);
                    GahirPreference.writeString(mActivity, Constants.PASSWORD_VALUE, editPasswordET.getText().toString().trim());
               //     GahirPreference.writeString(mActivity, GahirPreference.DEALER_DOC, mModel.getUserDetail().getDealerDoc());

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
                    SharedPreferences.Editor editor = prefs.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(mModel.getUserDetail().getDealerDoc());
                    editor.putString("doc", json);
                    editor.apply();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<EmailLoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.toString());
                showToast(mActivity, t.toString());
            }
        });
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        Log.e(TAG, "**PushToken**" + strPushToken);
                    }
                });
    }
}

