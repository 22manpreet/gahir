package com.gahiragro.app;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import com.gahiragro.app.activities.BookingDetailActivity;
import com.gahiragro.app.activities.HomeActivity;
import com.gahiragro.app.activities.ReadyToDispatchActivity;
import com.gahiragro.app.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;
    String CHANNEL_ID = "my_channel_01";
    private String TAG = MyFirebaseMessagingService.this.getClass().getSimpleName();
    private String userId = "";
    private NotificationManager mNotificationManager;
    JSONObject data = null;
    String title = "", message = "", enquiryId = "", notificationType = "", orderId = "";
    Intent intent;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Log.e(TAG, "*****Notifications********" + remoteMessage.getNotification().getBody());
            Log.e(TAG, "*****Notifications********" + remoteMessage.getNotification().getTitle());
            Log.e(TAG, "*****Notifications********" + remoteMessage.getNotification().getSound());
            Log.e(TAG, "*****Notifications********" + remoteMessage.getNotification().getColor());
            Log.e(TAG, "*****Notifications********" + remoteMessage.getData());
            Log.e(TAG, "*****Notifications********" + remoteMessage.getData().get("enquiry_id"));
            Log.e(TAG, "*****Notifications********" + remoteMessage.getData().get("notification_type"));

            message = remoteMessage.getNotification().getBody();
            title = remoteMessage.getNotification().getTitle();
            enquiryId = remoteMessage.getData().get("enquiry_id");
            notificationType = remoteMessage.getData().get("notification_type");
            orderId = remoteMessage.getData().get("order_id");
        } catch (Exception e) {
            e.printStackTrace();
        }

        sendNotification(title, message);
    }

    private void sendNotification(String title, String message) {
        if (notificationType != null) {
            if (notificationType.equals("enquiry")) {
                intent = new Intent(this, ReadyToDispatchActivity.class);
                intent.putExtra(Constants.ENQUIRY_ID, enquiryId);
                intent.putExtra(Constants.TYPE, "adminDealerAdapter");
                intent.putExtra(Constants.PUSH, "PUSH");
            } else if (notificationType.equals("mass")) {
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_MASS, "mass_push");
            } else if (notificationType.contains("order")) {
                intent = new Intent(this, BookingDetailActivity.class);
                intent.putExtra(Constants.ORDER_ID, orderId);
            }
            else if(notificationType.equals("register_product")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_REGISTER_PRODUCT, "register_product");
            }
            else if(notificationType.equals("part_enquiry")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_PART_ENQUIRY, "part_enquiry");
            }
            else if(notificationType.equals("review")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_REVIEW, "review");
            }
            else if(notificationType.equals("complain")){
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_COMPLAIN, "complain");
            }
            else if(notificationType.equals("custom")){
                intent=new Intent(this,HomeActivity.class);
                intent.putExtra(Constants.NOTIFICATION_COSTUM,"custom");
            }
            else {
                intent = new Intent(this, HomeActivity.class);
            }

        }
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_MUTABLE);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_app_icon_noti2)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }
}
