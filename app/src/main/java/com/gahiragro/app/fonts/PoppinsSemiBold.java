package com.gahiragro.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PoppinsSemiBold {

    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "Poppins-SemiBold.otf";

    /*
     * Default Constructor
     * */
    public PoppinsSemiBold() {
    }

    /*
     * Constructor with Context
     * */
    public PoppinsSemiBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }
}
