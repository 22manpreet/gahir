package com.gahiragro.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class RailwaySemiBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "Raleway-SemiBold.ttf";

    /*
     * Default Constructor
     * */
    public RailwaySemiBold() {
    }

    /*
     * Constructor with Context
     * */
    public RailwaySemiBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}


