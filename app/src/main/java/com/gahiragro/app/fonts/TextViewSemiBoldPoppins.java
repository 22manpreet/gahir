package com.gahiragro.app.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

public class TextViewSemiBoldPoppins extends TextView {
    /*
     * Getting Current Class Name
     * */
    private String mTag = TextViewSemiBoldPoppins.this.getClass().getSimpleName();
    public TextViewSemiBoldPoppins(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextViewSemiBoldPoppins(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextViewSemiBoldPoppins(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TextViewSemiBoldPoppins(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}


