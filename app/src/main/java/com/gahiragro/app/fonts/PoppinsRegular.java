package com.gahiragro.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PoppinsRegular {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "Poppins-Regular.otf";
    /*
     * Default Constructor
     * */
    public PoppinsRegular() {
    }
    /*
     * Constructor with Context
     * */
    public PoppinsRegular(Context context)
    {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }
}

