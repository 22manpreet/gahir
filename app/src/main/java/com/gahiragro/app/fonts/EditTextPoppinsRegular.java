package com.gahiragro.app.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

public class EditTextPoppinsRegular extends EditText {
    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextPoppinsRegular.this.getClass().getSimpleName();
    public EditTextPoppinsRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextPoppinsRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}


