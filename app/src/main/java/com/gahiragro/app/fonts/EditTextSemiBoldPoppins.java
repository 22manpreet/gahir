package com.gahiragro.app.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

public class EditTextSemiBoldPoppins extends EditText {
    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextSemiBoldPoppins.this.getClass().getSimpleName();
    public EditTextSemiBoldPoppins(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextSemiBoldPoppins(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextSemiBoldPoppins(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextSemiBoldPoppins(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}


