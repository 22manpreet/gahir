package com.coach.app.activities.viewModel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.coach.app.repository.ApisRepository
import com.gahiragro.app.R
import com.gahiragro.app.model.CustomerComplainModel
import com.gahiragro.app.utils.Constants
import com.gahiragro.app.utils.Event
import com.gahiragro.app.utils.GahirApplication
import com.gahiragro.app.utils.Resource
import com.icma.app.dataobject.RequestBodies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class MainViewModel(val repository: ApisRepository,val app: Application) : AndroidViewModel(app) {

    val complaintLiveDta = MutableLiveData<Event<Resource<CustomerComplainModel>>>()
    val complaintData: LiveData<Event<Resource<CustomerComplainModel>>>
        get() = complaintLiveDta

    fun complaintUser(body: RequestBodies.EditProfileBody, mActivity: Activity?) {
        complaintLiveDta.postValue(Event(Resource.Loading()))
        try {
            if (Constants.isNetworkAvailable(getApplication<GahirApplication>())) {

                val response = repository.ComplaintDataRequest(body.accessToken, body.contactNum,body.prodId,body.srNo,body.compReason,body.reasonDetail,body.mMultipartBodyChanged1,body.mMultipartBodyChanged2,body.mMultipartBodyChanged3,body.mMultipartBody1)
                response!!.enqueue(object : Callback<CustomerComplainModel> {
                    override fun onResponse(
                        call: Call<CustomerComplainModel>,
                        response: Response<CustomerComplainModel>
                    ) {
                        Log.e("Check",""+response.code())
                        Log.e("Check",":::"+response.body()!!.status)

                        if (response.body()!!.status.equals("1")) {
                            if (response.isSuccessful) {
                                complaintLiveDta.postValue(handleResponse(response))
                            } else {
                                complaintLiveDta.postValue(
                                    Event(
                                        Resource.Error(
                                            response.message()
                                        )
                                    )
                                )
                            }
                        }
                        else
                        {
                            Constants.dismissProgressDialog()
                        }

                    }

                    override fun onFailure(call: Call<CustomerComplainModel>, t: Throwable) {
                        Log.e("Check","failure"+t.message.toString())

                        complaintLiveDta.postValue(
                            Event(Resource.Error(
                                t.message.toString()
                            ))
                        )
                    }
                })
            } else {
                complaintLiveDta.postValue(Event(Resource.Error(getApplication<GahirApplication>().getString(R.string.no_internet_connection))))
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    complaintLiveDta.postValue(
                        Event(Resource.Error(
                            getApplication<GahirApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    complaintLiveDta.postValue(
                        Event(Resource.Error(
                            getApplication<GahirApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }

    }

    private fun handleResponse(response: Response<CustomerComplainModel>): Event<Resource<CustomerComplainModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }

}
