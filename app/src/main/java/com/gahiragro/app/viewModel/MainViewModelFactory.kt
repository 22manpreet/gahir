package com.gahiragro.app.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import com.coach.app.activities.viewModel.MainViewModel
import com.coach.app.repository.ApisRepository

class MainViewModelFactory(private val repository: ApisRepository,val app: Application):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return MainViewModel(repository,app) as T
    }
}