package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.PdfViewActivity;
import com.gahiragro.app.activities.ReadyToDispatchActivity;
import com.gahiragro.app.activities.VideoPlayingActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.enquirymodel.AllEnquiriesItem;
import com.gahiragro.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class VideoPdfAdapter extends RecyclerView.Adapter<VideoPdfAdapter.MyViewHolder> {
    ArrayList<String> mVideoPdfAL = new ArrayList();
    Context context;
    ArrayList<String> mVideoPdfTitle=new ArrayList<>();

    public VideoPdfAdapter(Activity mActivity, ArrayList<String> mVideoPdfAL, ArrayList<String> mVideoPdfTitle) {
        this.context = mActivity;
        this.mVideoPdfAL = mVideoPdfAL;
        this.mVideoPdfTitle=mVideoPdfTitle;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_pdf, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoPdfAdapter.MyViewHolder holder, int position) {
        String mModel = mVideoPdfAL.get(position);
        holder.nameTV.setText(mVideoPdfTitle.get(position));
        if (mModel.contains("pdf")) {
            holder.profileIV.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_pdf));
        } else {
            holder.profileIV.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_video_dummy));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mModel.contains("pdf")) {
                    Intent intent = new Intent(context, PdfViewActivity.class);
                    intent.putExtra(Constants.URL, mModel);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, VideoPlayingActivity.class);
                    intent.putExtra(Constants.URL, mModel);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVideoPdfTitle.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        ImageView profileIV;

        MyViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
            profileIV = itemView.findViewById(R.id.profileIV);
        }


    }


}

