package com.gahiragro.app.adapters.adminAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BookingDetailActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.adminDealorOrderModel.AllOrdersItem;
import com.gahiragro.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class AdminDealerOrderAdapter extends RecyclerView.Adapter<AdminDealerOrderAdapter.MyViewHolder> {
    List<AllOrdersItem> mAdminOrderAL = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;

    public AdminDealerOrderAdapter(FragmentActivity activity, List<AllOrdersItem> mAdminOrderAL, PaginationHomeInterface mInterfaceData) {
        this.context = activity;
        this.mAdminOrderAL = mAdminOrderAL;
        this.mInterfaceData = mInterfaceData;

    }


    @NonNull
    @Override
    public AdminDealerOrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        AdminDealerOrderAdapter.MyViewHolder viewHolder = new AdminDealerOrderAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminDealerOrderAdapter.MyViewHolder holder, int position) {
        holder.dealernameLL.setVisibility(View.GONE);
        holder.contactLL.setVisibility(View.GONE);
        AllOrdersItem mModel = mAdminOrderAL.get(position);
        if (position >= mAdminOrderAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        holder.orderIdTV.setText(context.getString(R.string.order_id));
        holder.dateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getEnquiryDetail().getCreationDate())));

        holder.productNameTV.setText(mModel.getEnquiryDetail().getProdName());
        holder.productIDTV.setText(mModel.getEnquiryDetail().getProductId());
        holder.quantityTV.setText(mModel.getEnquiryDetail().getQty());
        holder.productPriceTV.setText("\u20B9" + mModel.getEnquiryDetail().getTotal());
        Glide.with(context).load(mModel.getEnquiryDetail().getProductDetail().getProdImage()).
                placeholder(R.drawable.gahir_logo_dummy).
                into(holder.productIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookingDetailActivity.class);
                intent.putExtra(Constants.ORDER_ID, mModel.getId());
                intent.putExtra(Constants.TYPE, "adminOrderAdapter");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAdminOrderAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTV, orderIdTV, dateTV, productPriceTV, productIDTV, quantityTV;
        ImageView productIV;
        LinearLayout contactLL,dealernameLL;

        MyViewHolder(View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            productPriceTV = itemView.findViewById(R.id.productPriceTV);
            productIDTV = itemView.findViewById(R.id.productIDTV);
            quantityTV = itemView.findViewById(R.id.quantityTV);
            productIV = itemView.findViewById(R.id.productIV);
            dateTV = itemView.findViewById(R.id.dateTV);
            orderIdTV = itemView.findViewById(R.id.orderIdTV);
            contactLL=itemView.findViewById(R.id.contactLL);
            dealernameLL=itemView.findViewById(R.id.dealernameLL);
        }


    }

    public String getDateCurrentTimeZone(long timestamp) {
        //convert seconds to milliseconds
        Date date = new Date(timestamp * 1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("dd MMM yyyy ");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return jdf.format(date);
    }


}

