package com.gahiragro.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.NotificationClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.AllNotificationsItem;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder>{

    Context context;
    List<AllNotificationsItem> mNotificationAL;
    PaginationHomeInterface mInterfaceData;
    NotificationClickInterface mNotificationClickInterface;
    public MessageAdapter(FragmentActivity activity, List<AllNotificationsItem> mNotificationAL, PaginationHomeInterface mInterfaceData, NotificationClickInterface mNotificationClickInterface) {
        this.context = activity;
        this.mNotificationAL=mNotificationAL;
        this.mInterfaceData = mInterfaceData;
        this.mNotificationClickInterface=mNotificationClickInterface;
    }

    @NonNull
    @Override
    public MessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_messages, parent, false);
        return new MessageAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.MyViewHolder holder, int position) {
        if (position == mNotificationAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        AllNotificationsItem mModel=mNotificationAL.get(position);
        holder.nameTV.setText(mModel.getNotifyTitle());
        holder.mssgTV.setText(mModel.getNotifyMessage());
        holder.timeTV.setText(mModel.getDisplayDate());

        if (position == mNotificationAL.size() - 1) {
            holder.viewLine.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotificationClickInterface.onNotiClick(mModel.getNotifyType());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNotificationAL.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView profileIV;
        TextView nameTV, mssgTV, timeTV;
        View viewLine;

        MyViewHolder(View itemView) {
            super(itemView);
            viewLine = itemView.findViewById(R.id.viewLine);
            timeTV = itemView.findViewById(R.id.timeTV);
            mssgTV = itemView.findViewById(R.id.mssgTV);
            nameTV = itemView.findViewById(R.id.nameTV);
            profileIV = itemView.findViewById(R.id.profileIV);
        }
    }
}
