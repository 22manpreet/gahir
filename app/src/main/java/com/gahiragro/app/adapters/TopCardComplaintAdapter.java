package com.gahiragro.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gahiragro.app.R;
import java.util.ArrayList;

public class TopCardComplaintAdapter extends PagerAdapter {
    LayoutInflater inflater;
    Context context;
    ArrayList<String> mCategoryAL = new ArrayList<>();
    public TopCardComplaintAdapter(Context activity, ArrayList<String> mCategoryAL) {
        this.context = activity;
        this.mCategoryAL = mCategoryAL;
    }
    @Override
    public int getCount() {
        return mCategoryAL.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ImageView image;
        CardView cardview;
       // AllCategoryModel.AllCategory mModel = mCategoryAL.get(position);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.item_top_card_category, container, false);
        image = (ImageView) itemview.findViewById(R.id.img);
        Log.e("TAG","image::"+mCategoryAL.get(position));
        image.setEnabled(true);
        Glide.with(context)
                .asBitmap()
                .load(mCategoryAL.get(position))
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(mModel.getFeatureProduct().equals("0"))
//                {
//                }
//                else
//                {
//                    image.setEnabled(false);
//                    Intent intent = new Intent(context, DealorProductDetailActivity.class);
//                    intent.putExtra(Constants.ID, mModel.getFeatureProduct());
//                    context.startActivity(intent);
//                }
//            }
//        });
        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object); }
//    @Override
//    public float getPageWidth(int position) {
//        return .20f;   //it is used for set page widht of view pager
//    }

}
