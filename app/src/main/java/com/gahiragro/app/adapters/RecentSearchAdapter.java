package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.SearchItemClickInterface;
import com.gahiragro.app.model.RecentSearchModel;

import java.util.ArrayList;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.MyViewHolder> {
    // ArrayList<RecentSearchModel.SearchList> mRecentSearchAL = new ArrayList<>();
    Context context;
    SearchItemClickInterface mSearchItemClickInterface;
    ArrayList<String> mRecentSearchAL = new ArrayList<String>();

    public RecentSearchAdapter(Activity mActivity, ArrayList<String> mRecentSearchAL, SearchItemClickInterface mSearchItemClickInterface) {
        this.context = mActivity;
        this.mRecentSearchAL = mRecentSearchAL;
        this.mSearchItemClickInterface = mSearchItemClickInterface;
    }

    @NonNull
    @Override
    public RecentSearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent_search, parent, false);
        RecentSearchAdapter.MyViewHolder viewHolder = new RecentSearchAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecentSearchAdapter.MyViewHolder holder, int position) {
        String mItem = mRecentSearchAL.get(position);
        holder.itemTV.setText(mItem);

        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchItemClickInterface.onClick(mItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mRecentSearchAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemTV;
        LinearLayout itemLL;

        MyViewHolder(View itemView) {
            super(itemView);
            itemTV = itemView.findViewById(R.id.itemTV);
            itemLL = itemView.findViewById(R.id.itemLL);
        }


    }


}
