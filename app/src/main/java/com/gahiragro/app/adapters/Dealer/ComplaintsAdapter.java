package com.gahiragro.app.adapters.Dealer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.Telephony;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.ProductDetailActivity;
import com.gahiragro.app.activities.interfaces.AudioClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.TopCardComplaintAdapter;
import com.gahiragro.app.fragments.Dealer.MyModel;
import com.gahiragro.app.model.CustomerComplaintModel;
import com.squareup.picasso.Picasso;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ComplaintsAdapter extends RecyclerView.Adapter<ComplaintsAdapter.MyViewHolder> {
    Context context;
    PaginationHomeInterface mInterfaceData;
    AudioClickInterface mAudioClickInterface;
    List<CustomerComplaintModel.ComplainList.AllComplain> mlist = new ArrayList<>();
    int currentPage = 0;
    TopCardComplaintAdapter mTopCardComplaintAdapter;


    public ComplaintsAdapter(Context context, List<CustomerComplaintModel.ComplainList.AllComplain> mlist, AudioClickInterface mAudioClickInterface, PaginationHomeInterface mInterfaceData) {
        this.context = context;
        this.mlist = mlist;
        this.mAudioClickInterface = mAudioClickInterface;
        this.mInterfaceData=mInterfaceData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complaints, parent, false);
        return new ComplaintsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (position == mlist.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        ArrayList<String> mImageslist = new ArrayList<>();
        CustomerComplaintModel.ComplainList.AllComplain mModel = mlist.get(position);
        mImageslist.addAll(mModel.getSupportImg());
        holder.contactNoTV.setText(mModel.getContactNo());
        holder.serialNoTV.setText(mModel.getProdSrNo());
        holder.reasonTV.setText(mModel.getCompReason());
        holder.reasonDetailTV.setText(mModel.getReasonDetail());
        holder.productNameTV.setText(mModel.getProductDetail().getProdName());
//        if(mImageslist!=null || mImageslist.size()!=0)
//        {
//            for(int i = 0;i<mImageslist.size();i++)
//            {
//                Picasso.get().load(mImageslist.get(i)).
//                    placeholder(R.drawable.gahir_logo_dummy).
//                    into(holder.productIV);
//            }
//        }

        if (mModel.getReasonDetail().equals("")) {
            holder.extraReasonRL.setVisibility(View.GONE);
        } else {
            holder.extraReasonRL.setVisibility(View.VISIBLE);
            holder.reasonDetailTV.setText(mModel.getReasonDetail());
        }
        //status 1 means assign 2 means Closed complaint
        if (mModel.getCompStatus().equals("2")) {
            holder.btnClosedBT.setVisibility(View.VISIBLE);
            holder.btnOpenBT.setVisibility(View.GONE);
            holder.btnAssignedBT.setVisibility(View.GONE);
        } else if (mModel.getCompStatus().equals("1")) {
            holder.btnClosedBT.setVisibility(View.GONE);
            holder.btnOpenBT.setVisibility(View.GONE);
            holder.btnAssignedBT.setVisibility(View.VISIBLE);
        } else {
            holder.btnClosedBT.setVisibility(View.GONE);
            holder.btnOpenBT.setVisibility(View.VISIBLE);
            holder.btnAssignedBT.setVisibility(View.GONE);
        }
        if (mModel.getSupportAudio().equals("")) {
            holder.seekbarRL.setVisibility(View.GONE);
        } else {
            holder.seekbarRL.setVisibility(View.VISIBLE);
        }
        holder.seekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        holder.seekbarRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAudioClickInterface.onAudioClick(position, mModel);
            }
        });


        if(mImageslist.size()==0){
            holder.productIV.setVisibility(View.VISIBLE);
            holder.viewpager.setVisibility(View.GONE);
            holder.indicator.setVisibility(View.GONE);
        }
        else if(mImageslist.size() > 0) {
            holder.productIV.setVisibility(View.GONE);
            holder.viewpager.setVisibility(View.VISIBLE);
            holder.indicator.setVisibility(View.VISIBLE);
            if (holder.viewpager != null) {
                if (holder.indicator != null) {
                    holder.indicator.setVisibility(View.VISIBLE);
                }
                if (holder.indicator != null) {
                    mTopCardComplaintAdapter = new TopCardComplaintAdapter(context, mImageslist);
                    holder.viewpager.setAdapter(mTopCardComplaintAdapter);
                    holder.indicator.setViewPager(holder.viewpager);
                    //   setAdapter();
                    final Handler handler = new Handler();
                    Timer timer = new Timer();
                    final Runnable runnable = new Runnable() {
                        public void run() {
                            int numPages = holder.viewpager.getAdapter().getCount();
                            currentPage = (currentPage + 1) % numPages;
                            holder.viewpager.setCurrentItem(currentPage);

                        }
                    };
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(runnable);
                        }
                    }, 3000, 3000);

                }
                holder.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        currentPage = position;
                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }
    }
    @Override
    public int getItemCount() {
        return mlist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout seekbarRL;
        ImageView productIV;
        SeekBar seekbar;
        Button btnClosedBT, btnOpenBT, btnAssignedBT;
        TextView contactNoTV, serialNoTV, reasonTV, reasonDetailTV, productNameTV;
        RelativeLayout extraReasonRL;
        ViewPager viewpager;
        SpringDotsIndicator indicator;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            seekbarRL = itemView.findViewById(R.id.seekbarRL);
            contactNoTV = itemView.findViewById(R.id.contactNoTV);
            productIV = itemView.findViewById(R.id.productIV);
            serialNoTV = itemView.findViewById(R.id.serialNoTV);
            reasonTV = itemView.findViewById(R.id.reasonTV);
            reasonDetailTV = itemView.findViewById(R.id.reasonDetailTV);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            seekbar = itemView.findViewById(R.id.seekbar);
            extraReasonRL = itemView.findViewById(R.id.extraReasonRL);
            btnClosedBT = itemView.findViewById(R.id.btnClosedBT);
            btnOpenBT = itemView.findViewById(R.id.btnOpenBT);
            btnAssignedBT = itemView.findViewById(R.id.btnAssignedBT);
            viewpager =  itemView.findViewById(R.id.pager);
            indicator=itemView.findViewById(R.id.indicator);
        }
    }

}

