package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.DealorProductDetailActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.FilterCategoryModel;
import com.gahiragro.app.model.FilterProductModel;
import com.gahiragro.app.utils.Constants;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.MyViewHolder> {
    Context context;
    List<FilterProductModel.AllProduct> mNewProductAL = new ArrayList<>();
    List<FilterCategoryModel.ProductList.AllProduct> mProductArrayList = new ArrayList<>();

    PaginationHomeInterface mInterfaceData;
    String type = "";

    public HomeListAdapter(Activity mActivity, List<FilterCategoryModel.ProductList.AllProduct> mProductAL, PaginationHomeInterface mInterfaceData) {
        this.mProductArrayList = mProductAL;
        this.context = mActivity;
        this.mInterfaceData = mInterfaceData;

    }

    @NonNull
    @Override
    public HomeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_list, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.MyViewHolder holder, int position) {
        FilterCategoryModel.ProductList.AllProduct mModel = mProductArrayList.get(position);
        if (position >= mProductArrayList.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        Glide.with(context)
                .load(mModel.getProdImage())
                .placeholder(R.drawable.gahir_logo_dummy)// Uri of the picture
                .apply(new RequestOptions().override(600, 600))
                .into(holder.productImgIV);
        holder.productNameTV.setText(mModel.getProdName());
        //  holder.detailTV.setText(mModel.getProdDesc());
        holder.productTV.setText("\u20B9" + mModel.getProdPrice());
        holder.modelNameTV.setText(mModel.getProdModel());
        if (type.equals("Customer")) {
            holder.checkAvailabilityTV.setText(context.getString(R.string.more_details));
        } else {
            holder.checkAvailabilityTV.setText(context.getString(R.string.more_availability));
        }
        holder.headerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DealorProductDetailActivity.class);
                intent.putExtra(Constants.ID, mModel.getId());
                context.startActivity(intent);
            }
        });
        holder.checkAvailabilityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DealorProductDetailActivity.class);
                intent.putExtra(Constants.ID, mModel.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView checkAvailabilityTV;
        ImageView productImgIV;
        RelativeLayout headerLL;
        CardView cardView;
        TextView productNameTV, modelNameTV, detailTV, productTV;

        MyViewHolder(View itemView) {
            super(itemView);
            checkAvailabilityTV = itemView.findViewById(R.id.checkAvailabilityTV);
            productImgIV = itemView.findViewById(R.id.productImgIV);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            detailTV = itemView.findViewById(R.id.detailTV);
            productTV = itemView.findViewById(R.id.productTV);
            modelNameTV = itemView.findViewById(R.id.modelNameTV);
            headerLL = itemView.findViewById(R.id.headerLL);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
