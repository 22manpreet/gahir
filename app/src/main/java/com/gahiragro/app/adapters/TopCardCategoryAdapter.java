package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gahiragro.app.HttpsTrustManager;
import com.gahiragro.app.R;
import com.gahiragro.app.RetrofitApi.ApiInterface;
import com.gahiragro.app.RetrofitApi.RestClient;
import com.gahiragro.app.activities.DealorProductDetailActivity;
import com.gahiragro.app.activities.NewProductListActivity;
import com.gahiragro.app.model.AllCategoryModel;
import com.gahiragro.app.utils.Constants;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.Random;

public class TopCardCategoryAdapter extends PagerAdapter {
    LayoutInflater inflater;
    Context context;
    ArrayList<AllCategoryModel.AllCategory> mCategoryAL = new ArrayList<>();
    public TopCardCategoryAdapter(FragmentActivity activity, ArrayList<AllCategoryModel.AllCategory> mCategoryAL) {
        this.context = activity;
        this.mCategoryAL = mCategoryAL;
    }
    @Override
    public int getCount() {
        return mCategoryAL.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        RoundedImageView image;
        CardView cardview;
        AllCategoryModel.AllCategory mModel = mCategoryAL.get(position);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.item_top_card_category, container, false);
        image = (RoundedImageView) itemview.findViewById(R.id.img);
        Log.e("TAG","image::"+mModel.getCatBanner());
        image.setEnabled(true);
        Glide.with(context)
                .asBitmap()
                .load(mModel.getCatBanner())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
//        Glide.with(context)
//                .load(mModel.getCatBanner())
//                .placeholder(R.drawable.gahir_logo_dummy)
//                .into(image);

       // Picasso.get().load(mModel.getCatImage()).into(image);
//        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
//        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
//        cardview.setCardBackgroundColor(randomAndroidColor);
        image.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(mModel.getFeatureProduct().equals("0"))
        {
        }
        else
            {
                image.setEnabled(false);
            Intent intent = new Intent(context, DealorProductDetailActivity.class);
            intent.putExtra(Constants.ID, mModel.getFeatureProduct());
            context.startActivity(intent);
        }
    }
});
        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
         ((ViewPager) container).removeView((RelativeLayout) object); }
//    @Override
//    public float getPageWidth(int position) {
//        return .20f;   //it is used for set page widht of view pager
//    }

}



