package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.SendMessageActivity;
import com.gahiragro.app.activities.HomeActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.AllCustomersItem;
import com.gahiragro.app.model.CustomersListModel;

import java.util.ArrayList;
import java.util.List;

public class MyCustomersAdapter extends RecyclerView.Adapter<MyCustomersAdapter.MyViewHolder>{
    List<AllCustomersItem> mAllCustomers = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;
  //  int checkUncheck = 0;
    public MyCustomersAdapter(FragmentActivity activity, List<AllCustomersItem> mAllCustomers, PaginationHomeInterface mInterfaceData) {
        this.context = activity;
        this.mAllCustomers = mAllCustomers;
        this.mInterfaceData=mInterfaceData;
    }

    @NonNull
    @Override
    public MyCustomersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customer_list, parent, false);
        return new MyCustomersAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCustomersAdapter.MyViewHolder holder, int position) {
        if (position == mAllCustomers.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        AllCustomersItem mModel = mAllCustomers.get(position);
        holder.customerNameTV.setText(mModel.getFirstName());
        holder.contactTV.setText(mModel.getPhoneNo());
        holder.addressTV.setText(mModel.getAddress());

        holder.selectcheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SendMessageActivity.class);
                intent.putExtra("Customer_id",mModel.getId());
                context.startActivity(intent);
//                if (checkUncheck == 0) {
//                    holder.selectcheckBox.setBackgroundResource(R.drawable.ic_check1);
//                    checkUncheck++;
//
//                } else {
//                    holder.selectcheckBox.setBackgroundResource(R.drawable.ic_uncheck1);
//                    checkUncheck--;
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAllCustomers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView customerNameTV;
        TextView contactTV;
        TextView addressTV;
        RelativeLayout selectcheckBox;

        MyViewHolder(View itemView) {
            super(itemView);
            customerNameTV=itemView.findViewById(R.id.customerNameTV);
            contactTV=itemView.findViewById(R.id.contactTV);
            addressTV=itemView.findViewById(R.id.addressTV);
            selectcheckBox=itemView.findViewById(R.id.selectcheckBox);
        }
    }
}
