package com.gahiragro.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.AudioClickInterface;
import com.gahiragro.app.activities.interfaces.NotificationClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.NotificationModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    List<NotificationModel.AllNotification> mNotificationAL = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;

    public NotificationAdapter(FragmentActivity activity, List<NotificationModel.AllNotification> mNotificationAL, PaginationHomeInterface mInterfaceData) {
        this.context = activity;
        this.mNotificationAL = mNotificationAL;
        this.mInterfaceData = mInterfaceData;

    }

//    @NonNull
//    @Override
//    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.item_notification, false);
//        return new MyViewHolder(view);
//    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (position == mNotificationAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }

        NotificationModel.AllNotification mModel = mNotificationAL.get(position);
        holder.nameTV.setText(mModel.getNotifyTitle());
        holder.mssgTV.setText(mModel.getNotifyMessage());
        holder.timeTV.setText(mModel.getDisplay_date());



        if (position == mNotificationAL.size() - 1) {
            holder.viewLine.setVisibility(View.GONE);
        }

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mNotificationClickInterface.onNotiClick(mModel.getNotifyType());
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mNotificationAL.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView profileIV;
        TextView nameTV, mssgTV, timeTV;
        View viewLine;

        MyViewHolder(View itemView) {
            super(itemView);
            viewLine = itemView.findViewById(R.id.viewLine);
            timeTV = itemView.findViewById(R.id.timeTV);
            mssgTV = itemView.findViewById(R.id.mssgTV);
            nameTV = itemView.findViewById(R.id.nameTV);
            profileIV = itemView.findViewById(R.id.profileIV);
        }
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

}


