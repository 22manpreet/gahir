package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.DealerDocumentActivity;
import java.util.ArrayList;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> contactList;

    public DocumentAdapter(FragmentActivity activity, ArrayList<String> contactList) {
        this.context = activity;
        this.contactList=contactList;
    }

    @Override
    public DocumentAdapter.MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document, parent, false);
        return new DocumentAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentAdapter.MyViewHolder holder, int position) {
         holder.docText.setText("PDF "+(position+1));

        holder.docText.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DealerDocumentActivity.class);
                intent.putExtra("doc_file",contactList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView docText;

        MyViewHolder(View itemView) {
            super(itemView);
            docText= itemView.findViewById(R.id.docText);
        }
    }
}
