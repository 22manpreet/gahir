package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.FilterReviewListInterface;
import com.gahiragro.app.activities.interfaces.FilterReviewStateInterface;
import com.gahiragro.app.model.StateDistrictModel;

import java.util.ArrayList;
import java.util.List;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.MyViewHolder> {
    List<StateDistrictModel.StateListItem> mList;
    Context context;
    List<String> mNewList = new ArrayList<>();
    FilterReviewStateInterface listInterface;

    public StateAdapter(List<StateDistrictModel.StateListItem> mList, Context context, FilterReviewStateInterface listInterface) {
        this.mList = mList;
        this.context = context;
        this.listInterface = listInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_state_list, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        StateDistrictModel.StateListItem myModel = mList.get(position);
        holder.stateNameTV.setText(myModel.getStateTitle());
        String stateIds = myModel.getStateTitle();


        if (mNewList != null)
            if (mNewList.contains(myModel.getStateId())) {
                holder.checkIV.setImageResource(R.drawable.ic_check_green);
            } else {
                holder.checkIV.setImageResource(R.drawable.ic_uncheck_green);
            }

        holder.imgCheckRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNewList != null)
                    if (mNewList.contains(myModel.getStateTitle())) {
                        mNewList.remove(myModel.getStateTitle());
                        holder.checkIV.setImageResource(R.drawable.ic_uncheck_green);
                    } else {
                      //  mNewList.add(myModel.getStateId());
                        mNewList.add(myModel.getStateTitle());
                        holder.checkIV.setImageResource(R.drawable.ic_check_green);
                    }
                listInterface.onFilterReview(position, mNewList, stateIds);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView stateNameTV;
        RelativeLayout imgCheckRL;
        LinearLayout stateLL;
        ImageView checkIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            stateNameTV = itemView.findViewById(R.id.stateNameTV);
            imgCheckRL = itemView.findViewById(R.id.imgCheckRL);
            stateLL = itemView.findViewById(R.id.stateLL);
            checkIV = itemView.findViewById(R.id.checkIV);
        }
    }
}
