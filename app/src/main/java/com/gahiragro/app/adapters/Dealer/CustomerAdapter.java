package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.ImageViewActivity;
import com.gahiragro.app.activities.VideoPlayingActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.FilterReviewModel;
import com.gahiragro.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {
    List<FilterReviewModel.AllReview> mlist = new ArrayList<>();
    Context context;
    boolean expand = false;
    PaginationHomeInterface mInterfaceData;
    private long mLastClickTab1 = 0;
    Boolean expandable = true;
    int isSelected = -1;

    public CustomerAdapter(List<FilterReviewModel.AllReview> mlist, Context context, PaginationHomeInterface mInterfaceData) {
        this.mlist = mlist;
        this.context = context;
        this.mInterfaceData = mInterfaceData;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_review, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//        holder.setIsRecyclable(false);
        FilterReviewModel.AllReview myModel = mlist.get(position);
        myModel.getUserDetail().getImage();
        if (position >= mlist.size() - 1) {
            mInterfaceData.mHomePagination(true);

        }
        Glide.with(context).load(myModel.getUserDetail().getImage())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(holder.imageprofileIV);

        Glide.with(context)
                .load(myModel.getReviewImg())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(holder.productIV);
        holder.userNameTV.setText(myModel.getUserDetail().getFirstName());
        holder.dateTV.setText(unixToDate(myModel.getCreationDate()));
        holder.titleNameTV.setText(myModel.getProductDetail().getProdName());
        if (myModel.getReviewText() != null && !myModel.getReviewText().equals("")) {
                holder.detailsTV.setText(myModel.getReviewText());
                holder.detailsTV.setTrimCollapsedText("see more");
        }


        if(isSelected==position)
        {
            Log.e("TAG","IsSelected"+isSelected);
            holder.detailsTV.setTrimExpandedText("see less");
        }
        else
        { holder.detailsTV.setTrimCollapsedText("see more"); }
        holder.detailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelected = position;
                Log.e("TAG","IsSelected"+isSelected);
            }
        });


        if (myModel.getReviewImg().contains(".mp4")) {
            holder.playIV.setVisibility(View.VISIBLE);
            holder.productIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
                        return;
                    }
                    mLastClickTab1 = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(context, VideoPlayingActivity.class);
                    intent.putExtra(Constants.class_type, "customerReviewAdapter");
                    intent.putExtra(Constants.URL, myModel.getReviewImg());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                }
            });
        } else {
            preventMultipleClick();
            holder.playIV.setVisibility(View.GONE);
            holder.productIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
                        return;
                    }
                    mLastClickTab1 = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(context, ImageViewActivity.class);
                    intent.putExtra(Constants.URL, myModel.getReviewImg());
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(intent);

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public String unixToDate(String creationDate) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(Integer.parseInt(creationDate) * 1000L);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        return (sdf.format(d));
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView userNameTV, dateTV, titleNameTV;
        ReadMoreTextView detailsTV;
        ImageView productIV, playIV, imageprofileIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            // sample code snippet to set the text content on the ExpandableTextView

            userNameTV = itemView.findViewById(R.id.userNameTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            titleNameTV = itemView.findViewById(R.id.titleNameTV);
            detailsTV = itemView.findViewById(R.id.detailsTV);
            productIV = itemView.findViewById(R.id.productIV);
            imageprofileIV = itemView.findViewById(R.id.imageprofileIV);
            playIV = itemView.findViewById(R.id.playIV);
        }
    }

    public void preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
