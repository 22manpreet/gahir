package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.FilterReviewListInterface;
import com.gahiragro.app.model.ALlProductsModel;

import java.util.ArrayList;
import java.util.List;

public class ProductFilterAdapter extends RecyclerView.Adapter<ProductFilterAdapter.MyViewHolder> {
    List<ALlProductsModel.AllProduct> mList;
    Context context;
    List<String> mNewList = new ArrayList<>();
    FilterReviewListInterface listInterface;


    public ProductFilterAdapter(List<ALlProductsModel.AllProduct> mList, Context context, FilterReviewListInterface listInterface) {
        this.mList = mList;
        this.context = context;
        this.listInterface = listInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list, parent, false);
        return new ProductFilterAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ALlProductsModel.AllProduct myModel = mList.get(position);
        holder.productNameTV.setText(myModel.getProdName());

        if (mNewList != null)
            if (mNewList.contains(myModel.getId())) {
                holder.checkIV.setImageResource(R.drawable.ic_check_green);
            } else {
                holder.checkIV.setImageResource(R.drawable.ic_uncheck_green);
            }

        holder.imgCheckRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mNewList != null)
                    if (mNewList.contains(myModel.getId())) {
                        mNewList.remove(myModel.getId());
                        holder.checkIV.setImageResource(R.drawable.ic_uncheck_green);
                    } else {
                        mNewList.add(myModel.getId());
                        holder.checkIV.setImageResource(R.drawable.ic_check_green);
                    }

                listInterface.onFilterReview(position, mNewList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTV;
        RelativeLayout imgCheckRL;
        ImageView checkIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            imgCheckRL = itemView.findViewById(R.id.imgCheckRL);
            checkIV = itemView.findViewById(R.id.checkIV);
        }
    }
}
