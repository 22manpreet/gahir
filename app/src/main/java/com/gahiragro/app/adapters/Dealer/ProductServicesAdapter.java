package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.ServiceManualActivity;
import com.gahiragro.app.activities.Dealer.TroubleshootingDetailsActivity;
import com.gahiragro.app.fragments.Dealer.MyModel;

import java.util.List;

public class ProductServicesAdapter extends RecyclerView.Adapter<ProductServicesAdapter.MyViewHolder> {
    List<MyModel> mList;
    Context context;

    public ProductServicesAdapter(List<MyModel> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_product, parent, false);
        return new ProductServicesAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        MyModel myModel = mList.get(position);
        holder.productSericeNameTV.setText(myModel.getName());
        holder.productSericModelNameTV.setText(myModel.getNameTitle());
        Glide.with(context).load(myModel.getImage()).into(holder.productIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, TroubleshootingDetailsActivity.class);
                context.startActivity(intent);
            }
        });

        if (position == 0) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
            holder.headerView.setVisibility(View.VISIBLE);
        } else if (position == 1) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_green));

        } else if (position == 2) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.pink));

        } else if (position == 3) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productSericModelNameTV, productSericeNameTV;
        ImageView productIV;
        View headerView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            productSericModelNameTV = itemView.findViewById(R.id.productSericModelNameTV);
            productSericeNameTV = itemView.findViewById(R.id.productSericeNameTV);
            productIV = itemView.findViewById(R.id.productIV);
            headerView = itemView.findViewById(R.id.headerView);

        }
    }
}
