package com.gahiragro.app.adapters.Dealer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.AttendComplaintsActivity;
import com.gahiragro.app.activities.interfaces.RemoveImageInterface;
import com.gahiragro.app.model.ImagesModel;
import com.lassi.data.media.MiMedia;

import java.util.ArrayList;
import java.util.List;

public class UploadRepairedAdapter extends RecyclerView.Adapter<UploadRepairedAdapter.MyViewHolder> {
    ArrayList<MiMedia> mList;
    Context context;
    RemoveImageInterface mRemoveImageInterface;



    public UploadRepairedAdapter(ArrayList<MiMedia> mPartsChangedImagesAL, AttendComplaintsActivity context, RemoveImageInterface mRemoveImageInterface) {
        this.mList = mPartsChangedImagesAL;
        this.context = context;
        this.mRemoveImageInterface = mRemoveImageInterface;

    }

    @NonNull
    @Override
    public UploadRepairedAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_upload_img, parent, false);
        return new UploadRepairedAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UploadRepairedAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        MiMedia myModel = mList.get(position);
        Bitmap bitmap = BitmapFactory.decodeFile(myModel.getPath());
        holder.uploadImg.setImageBitmap(bitmap);
    //    Glide.with(context).load(bitmap).into(holder.uploadImg);
        holder.imgCrossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long mLastClickTime = 0;
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                mRemoveImageInterface.onRemoveClick(position,"repairedImages");
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView uploadImg, imgCrossIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            uploadImg = itemView.findViewById(R.id.uploadImg);
            imgCrossIV = itemView.findViewById(R.id.imgCrossIV);

        }
    }
}

