package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.DeletePrtInterface;
import com.gahiragro.app.model.AllcartItemsModel;

import java.util.List;

public class SelectPartsAdapter extends RecyclerView.Adapter<SelectPartsAdapter.MyViewHolder> {
    List<AllcartItemsModel.CartItem> mList;
    Context context;
    DeletePrtInterface mDeletePrtInterface;

    public SelectPartsAdapter(List<AllcartItemsModel.CartItem> mList, Context context, DeletePrtInterface mDeletePrtInterface) {
        this.mList = mList;
        this.context = context;
        this.mDeletePrtInterface = mDeletePrtInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_parts, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        AllcartItemsModel.CartItem myModel = mList.get(position);
        if (myModel.getPspDetail() != null) {
            holder.thrasherName.setText(myModel.getPspDetail().getPspName());
            holder.numberTv.setText(myModel.getPspDetail().getPspKey());
            holder.valueTV.setText(myModel.getQty());

        }
        holder.right_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeletePrtInterface.onDelete(position, myModel.getCartId());
            }
        });

//        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                mDeletePrtInterface.onDelete(position, myModel.getCartId());
//                return false;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView thrasherName, numberTv, valueTV;
        RelativeLayout right_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            right_view = itemView.findViewById(R.id.right_view);
            thrasherName = itemView.findViewById(R.id.thrasherName);
            numberTv = itemView.findViewById(R.id.numberTv);
            valueTV = itemView.findViewById(R.id.valueTV);


        }
    }
}
