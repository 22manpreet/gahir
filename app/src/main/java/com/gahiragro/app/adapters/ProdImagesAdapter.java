package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.ProdSelImageInterface;

import java.util.ArrayList;

public class ProdImagesAdapter extends RecyclerView.Adapter<ProdImagesAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> mProdImageAL = new ArrayList<>();
    int itemPos = 0;
    String type = "";
    ProdSelImageInterface mProdSelImageInterface;

    public ProdImagesAdapter(Activity mActivity, ArrayList<String> mProdImagesAL, ProdSelImageInterface mProdSelImageInterface) {
        this.mContext = mActivity;
        this.mProdImageAL = mProdImagesAL;
        this.mProdSelImageInterface = mProdSelImageInterface;
    }

    @NonNull
    @Override
    public ProdImagesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prod_image, parent, false);
        ProdImagesAdapter.MyViewHolder viewHolder = new ProdImagesAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProdImagesAdapter.MyViewHolder holder, int position) {
        String image = mProdImageAL.get(position);
        Glide.with(mContext).load(image).into(holder.imageIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPos = position;
                notifyDataSetChanged();
                mProdSelImageInterface.onImageClick(image);
            }
        });
        if (itemPos == position ) {
            holder.imageIV.setBackground(mContext.getDrawable(R.drawable.bg_line_view));
        } else  {
            holder.imageIV.setBackground(mContext.getDrawable(R.drawable.bg_transparent_line_view));
        }
    }

    @Override
    public int getItemCount() {
        return mProdImageAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIV;

        MyViewHolder(View itemView) {
            super(itemView);
            imageIV = itemView.findViewById(R.id.imageIV);
        }


    }
}
