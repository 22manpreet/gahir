package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.ProductDetailActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.GetRegisterProductModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    List<GetRegisterProductModel.AllProduct> mGetProductsList = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;

    public ProductAdapter(List<GetRegisterProductModel.AllProduct> mGetProductsList, Context context, PaginationHomeInterface mInterfaceData) {
        this.mGetProductsList = mGetProductsList;
        this.context = context;
        this.mInterfaceData = mInterfaceData;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        GetRegisterProductModel.AllProduct allProductsItem =mGetProductsList.get(position);

        if (position >= mGetProductsList.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.productIV.setBackgroundColor(randomAndroidColor);
        holder.productNameTV.setText(allProductsItem.getProdName());
        holder.modelNumTV.setText(allProductsItem.getProdModel());
        Glide.with(context).
                load(allProductsItem.getProdImage())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(holder.productIV);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(Constants.SEL_ID,allProductsItem.getId());
                intent.putExtra(Constants.SEL_PRODUCT_ID,allProductsItem.getRegProdId());
                intent.putExtra(Constants.SEL_PRODUCT_SNO,allProductsItem.getProdSno());
                intent.putExtra(Constants.SEL_PRODUCT_NAME,allProductsItem.getProdName());
                context.startActivity(intent);
            }
        });

//        if (position == 0) {
//            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
//
//        } else if (position == 1) {
//            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_green));
//
//        } else if (position == 2) {
//            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.pink));
//
//        } else if (position == 3) {
//            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
//        }
    }

    @Override
    public int getItemCount() {
        return mGetProductsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView modelNumTV, productNameTV;
        ImageView productIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            modelNumTV = itemView.findViewById(R.id.modelNumTV);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            productIV = itemView.findViewById(R.id.productIV);

        }
    }
}
