package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.RegisterProductActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.ALlProductsModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class AllProductListAdapter extends RecyclerView.Adapter<AllProductListAdapter.MyViewHolder> {
    List<ALlProductsModel.AllProduct> mGetProductsList = new ArrayList<>();
    Context context;
    String tag;
    PaginationHomeInterface mInterfaceData;

    public AllProductListAdapter(List<ALlProductsModel.AllProduct> mGetProductsList, FragmentActivity activity, PaginationHomeInterface mInterfaceData) {
        this.mGetProductsList = mGetProductsList;
        this.context = activity;
        this.mInterfaceData=mInterfaceData;
    }

    @NonNull
    @Override

    public AllProductListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_product, parent, false);
        return new AllProductListAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllProductListAdapter.MyViewHolder holder, int position) {
        if (position == mGetProductsList.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        ALlProductsModel.AllProduct myModel = mGetProductsList.get(position);
        holder.productSericeNameTV.setText(myModel.getProdName());
        holder.productSericModelNameTV.setText(myModel.getProdModel());
        Glide.with(context).load(myModel.getProdImage()).into(holder.productIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RegisterProductActivity.class);
                intent.putExtra(Constants.SEL_PRODUCT_ID, myModel.getId());
                intent.putExtra(Constants.SEL_PRODUCT_IMAGE, myModel.getProdImage());
                context.startActivity(intent);
            }
        });

        if (position == 0) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
            holder.headerView.setVisibility(View.VISIBLE);
        } else if (position == 1) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_green));

        } else if (position == 2) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.pink));

        } else if (position == 3) {
            holder.productIV.setBackgroundColor(context.getResources().getColor(R.color.dark_orange));
        }
    }

    @Override
    public int getItemCount() {
        return mGetProductsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productSericModelNameTV, productSericeNameTV;
        ImageView productIV;
        View headerView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            productSericModelNameTV = itemView.findViewById(R.id.productSericModelNameTV);
            productSericeNameTV = itemView.findViewById(R.id.productSericeNameTV);
            productIV = itemView.findViewById(R.id.productIV);
            headerView = itemView.findViewById(R.id.headerView);

        }
    }
}
