package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.BookingDetailActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.getOrderListMOdel.AllOrdersItem;
import com.gahiragro.app.model.getOrderListMOdel.EnquiryDetail;
import com.gahiragro.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DealorOrderListAdapter extends RecyclerView.Adapter<DealorOrderListAdapter.MyViewHolder> {
    ArrayList<EnquiryDetail> mEnquiryAL = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;
    //  ArrayList<List<com.gahiragro.app.model.getOrderListMOdel.AllOrdersItem>> mEnquiryAL = new ArrayList<List<com.gahiragro.app.model.getOrderListMOdel.AllOrdersItem>>();


//    public DealorOrderListAdapter(FragmentActivity activity, ArrayList<EnquiryDetail> mEnquiryAL) {
//        this.context = activity;
//        this.mEnquiryAL = mEnquiryAL;
//    }


    public DealorOrderListAdapter(FragmentActivity activity, ArrayList<EnquiryDetail> mEnquiryAL, PaginationHomeInterface mInterfaceData) {
        this.context = activity;
        this.mEnquiryAL = mEnquiryAL;
        this.mInterfaceData = mInterfaceData;

    }


    @NonNull
    @Override
    public DealorOrderListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        DealorOrderListAdapter.MyViewHolder viewHolder = new DealorOrderListAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DealorOrderListAdapter.MyViewHolder holder, int position) {
        holder.dealernameLL.setVisibility(View.GONE);
        holder.contactLL.setVisibility(View.GONE);
        EnquiryDetail mModel = mEnquiryAL.get(position);
        if (position >= mEnquiryAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        holder.orderIdTV.setText(context.getString(R.string.order_id));
        holder.dateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getCreationDate())));
       holder.dateTV.setText(mModel.getDispatchDay());
        holder.productNameTV.setText(mModel.getProductDetail().getProdName());
        holder.productIDTV.setText(mModel.getOrder_id());
        holder.quantityTV.setText(mModel.getQty());
       // holder.productPriceTV.setText("$" + mModel.getTotal());
        holder.productPriceTV.setText("\u20B9" + mModel.getTotal());
        Glide.with(context).load(mModel.getProductDetail().
                getProdImage()).
                placeholder(R.drawable.gahir_logo_dummy).
                into(holder.productIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookingDetailActivity.class);
                intent.putExtra(Constants.ORDER_ID, mModel.getOrder_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEnquiryAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTV, orderIdTV, dateTV, productPriceTV, productIDTV, quantityTV;
        ImageView productIV;
        LinearLayout dealernameLL,contactLL;
        MyViewHolder(View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            productPriceTV = itemView.findViewById(R.id.productPriceTV);
            productIDTV = itemView.findViewById(R.id.productIDTV);
            quantityTV = itemView.findViewById(R.id.quantityTV);
            productIV = itemView.findViewById(R.id.productIV);
            dateTV = itemView.findViewById(R.id.dateTV);
            orderIdTV = itemView.findViewById(R.id.orderIdTV);
            dealernameLL=itemView.findViewById(R.id.dealernameLL);
            contactLL=itemView.findViewById(R.id.contactLL);
        }


    }


    public String getDateCurrentTimeZone(long timestamp) {
        //convert seconds to milliseconds
        Date date = new Date(timestamp * 1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("dd MMM yyyy ");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return jdf.format(date);

    }
}


