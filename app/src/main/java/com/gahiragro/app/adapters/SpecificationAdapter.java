package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.DealorProductDetailActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.FilterProductModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SpecificationAdapter extends RecyclerView.Adapter<SpecificationAdapter.MyViewHolder> {
    Context context;
    List<String> mNewProductDetails;

    public SpecificationAdapter(Activity activity, List<String> mNewProductDetails) {
        this.context = activity;
        this.mNewProductDetails = mNewProductDetails;
    }

    @NonNull
    @Override
    public SpecificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specification_detail, parent, false);
        SpecificationAdapter.MyViewHolder viewHolder = new SpecificationAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SpecificationAdapter.MyViewHolder holder, int position) {
        String currentString = mNewProductDetails.get(position);
        if (currentString.contains(":")) {
            String[] separated = currentString.split(":");
            String s = separated[0];
            String s1 = separated[1];

            holder.text1.setText(s + ":");
            if (s1.contains("'")) {
                holder.text2.setText(s1.replace("'", ""));
            } else {
                holder.text2.setText(s1);
            }
            if (currentString.contains("\r")) {
                holder.text2.setText(s1.replace("\r", ""));
            } else {
                holder.text2.setText(s1.replace(" ", ""));
            }
        } else {
            holder.headerLL.setVisibility(View.GONE);
            holder.strTextTV.setVisibility(View.VISIBLE);
            holder.strTextTV.setText(currentString);
        }

    }

    @Override
    public int getItemCount() {
        return mNewProductDetails.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView text1, text2, strTextTV;
        LinearLayout headerLL;

        MyViewHolder(View itemView) {
            super(itemView);
            text1 = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
            strTextTV = itemView.findViewById(R.id.strTextTV);
            headerLL = itemView.findViewById(R.id.headerLL);
        }
    }
}
