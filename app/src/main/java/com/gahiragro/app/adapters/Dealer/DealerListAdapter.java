package com.gahiragro.app.adapters.Dealer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.fragments.Dealer.MyModel;
import com.gahiragro.app.model.SearchDealerModel;

import java.util.List;

public class DealerListAdapter extends RecyclerView.Adapter<DealerListAdapter.MyViewHolder> {

    List<SearchDealerModel.AllUser> mSearchList;
    Context context;

    public DealerListAdapter(List<SearchDealerModel.AllUser> mSearchList, Context context) {
        this.mSearchList = mSearchList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dealerlist_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SearchDealerModel.AllUser model = mSearchList.get(position);

        holder.companyNameTV.setText(model.getFirstName());
        holder.mobileNumTV.setText(model.getPhoneNo());
        holder.districNameTV.setText(model.getDistrict());

        Glide.with(context).load(model.getImage())
                .placeholder(R.drawable.placeholder_img)
                .into(holder.imageprofileIV);

    }

    @Override
    public int getItemCount() {
        return mSearchList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView companyNameTV, mobileNumTV, districNameTV;
        ImageView imageprofileIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            companyNameTV = itemView.findViewById(R.id.companyNameTV);
            mobileNumTV = itemView.findViewById(R.id.mobileNumTV);
            districNameTV = itemView.findViewById(R.id.districNameTV);
            imageprofileIV = itemView.findViewById(R.id.imageprofileIV);

        }
    }
}
