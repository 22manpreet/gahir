package com.gahiragro.app.adapters.Dealer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.AttendComplaintsActivity;
import com.gahiragro.app.activities.interfaces.AudioClickInterface;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.adapters.TopCardComplaintAdapter;
import com.gahiragro.app.fragments.Dealer.MyModel;
import com.gahiragro.app.model.ServiceComplaintModel;
import com.gahiragro.app.utils.Constants;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ComplaintsServiceAdapter extends RecyclerView.Adapter<ComplaintsServiceAdapter.MyViewHolder> {
    Context context;
    List<ServiceComplaintModel.ComplainList.AllComplain> mlist = new ArrayList<>();
    int currentPage = 0;
    TopCardComplaintAdapter mTopCardComplaintAdapter;
    AudioClickInterface mAudioClickInterface;
    PaginationHomeInterface mInterfaceData;

    public ComplaintsServiceAdapter(Context context, List<ServiceComplaintModel.ComplainList.AllComplain> mList, AudioClickInterface mAudioClickInterface, PaginationHomeInterface mInterfaceData) {
        this.context = context;
        this.mlist = mList;
        this.mAudioClickInterface = mAudioClickInterface;
        this.mInterfaceData = mInterfaceData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complaints_service, parent, false);
        return new ComplaintsServiceAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (position == mlist.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        ArrayList<String> mImageslist = new ArrayList<>();
        ServiceComplaintModel.ComplainList.AllComplain myModel = mlist.get(position);
        mImageslist.addAll(myModel.getComplainDetail().getSupportImg());
        holder.userName.setText(myModel.getUserDetail().getFirstName());
        holder.addressTV.setText(myModel.getUserDetail().getAddress());
        holder.faultTypeTV.setText(myModel.getComplainDetail().getCompReason());
        holder.phnNoTV.setText(myModel.getComplainDetail().getContactNo());
        holder.productNameTV.setText(myModel.getProduct_name());

        if (mImageslist.size() == 0) {
            holder.productIV.setVisibility(View.VISIBLE);
            holder.viewpager.setVisibility(View.GONE);
            holder.indicator.setVisibility(View.GONE);
        } else if (mImageslist.size() > 0) {
            holder.productIV.setVisibility(View.GONE);
            holder.viewpager.setVisibility(View.VISIBLE);
            holder.indicator.setVisibility(View.VISIBLE);
            if (holder.viewpager != null) {
                if (holder.indicator != null) {
                    holder.indicator.setVisibility(View.VISIBLE);
                }
                if (holder.indicator != null) {
                    mTopCardComplaintAdapter = new TopCardComplaintAdapter(context, mImageslist);
                    holder.viewpager.setAdapter(mTopCardComplaintAdapter);
                    holder.indicator.setViewPager(holder.viewpager);
                    //   setAdapter();
                    final Handler handler = new Handler();
                    Timer timer = new Timer();
                    final Runnable runnable = new Runnable() {
                        public void run() {
                            int numPages = holder.viewpager.getAdapter().getCount();
                            currentPage = (currentPage + 1) % numPages;
                            holder.viewpager.setCurrentItem(currentPage);

                        }
                    };
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(runnable);
                        }
                    }, 3000, 3000);

                }
                holder.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        currentPage = position;
                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }
        holder.btnAttendBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AttendComplaintsActivity.class);
                intent.putExtra(Constants.model, myModel);
                context.startActivity(intent);
            }
        });
        if (myModel.getComplainDetail().getCompStatus().equals("1"))    //1 assign 2 closed
        {
            holder.btnOpenBT.setVisibility(View.VISIBLE);
            holder.btnClosedBT.setVisibility(View.GONE);
            holder.btnAttendBT.setVisibility(View.VISIBLE);
        } else {
            holder.btnOpenBT.setVisibility(View.GONE);
            holder.btnClosedBT.setVisibility(View.VISIBLE);
            holder.btnAttendBT.setVisibility(View.GONE);
        }
        if (myModel.getComplainDetail().getSupportAudio().equals("")) {
            holder.seekbarRL.setVisibility(View.GONE);
        } else {
            holder.seekbarRL.setVisibility(View.VISIBLE);
        }
        holder.seekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        holder.seekbarRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAudioClickInterface.onServiceAudioClick(position, myModel);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        Button btnAttendBT, btnOpenBT, btnClosedBT;
        TextView userName, faultTypeTV, addressTV, productNameTV, phnNoTV;
        ImageView productIV;
        ViewPager viewpager;
        SpringDotsIndicator indicator;
        SeekBar seekbar;
        RelativeLayout seekbarRL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            btnAttendBT = itemView.findViewById(R.id.btnAttendBT);
            btnOpenBT = itemView.findViewById(R.id.btnOpenBT);
            btnClosedBT = itemView.findViewById(R.id.btnClosedBT);
            userName = itemView.findViewById(R.id.userName);
            faultTypeTV = itemView.findViewById(R.id.faultTypeTV);
            addressTV = itemView.findViewById(R.id.addressTV);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            phnNoTV = itemView.findViewById(R.id.phnNoTV);
            productIV = itemView.findViewById(R.id.productIV);
            viewpager = itemView.findViewById(R.id.pager);
            indicator = itemView.findViewById(R.id.indicator);
            seekbar = itemView.findViewById(R.id.seekbar);
            seekbarRL = itemView.findViewById(R.id.seekbarRL);
        }
    }
}
