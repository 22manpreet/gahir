package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.Dealer.ThrasherAssimblyActivity;
import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;

public class PartListAdapter extends RecyclerView.Adapter<PartListAdapter.MyViewHolder> {

    ArrayList<ProductPartListModel.Part> mPartsAL = new ArrayList<>();
    Context context;

    public PartListAdapter(Activity mActivity, ArrayList<ProductPartListModel.Part> mPartsAL) {
        this.context = mActivity;
        this.mPartsAL = mPartsAL;
    }

    @NonNull
    @Override
    public PartListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_part_list, parent, false);
        return new PartListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PartListAdapter.MyViewHolder holder, int position) {
        ProductPartListModel.Part mModel = mPartsAL.get(position);
        holder.thrasherName.setText(mModel.getPartName());
        holder.numberTv.setText(mModel.getPartKey());
        Glide.with(context).load(mModel.getPartImg()).into(holder.imgProdIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ThrasherAssimblyActivity.class);
                intent.putExtra(Constants.LIST,mModel);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPartsAL.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProdIV;
        TextView thrasherName,numberTv;
        MyViewHolder(View itemView) {
            super(itemView);
            imgProdIV = itemView.findViewById(R.id.imgProdIV);
            thrasherName = itemView.findViewById(R.id.thrasherName);
            numberTv = itemView.findViewById(R.id.numberTv);
        }

    }
}
