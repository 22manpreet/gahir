package com.gahiragro.app.adapters.Dealer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.ThrasherInterface;
import com.gahiragro.app.fragments.Dealer.MyModel;
import com.gahiragro.app.model.ProductPartListModel;
import com.gahiragro.app.model.ThrasherModel;

import java.util.ArrayList;
import java.util.List;

public class ThrasherAdapter extends RecyclerView.Adapter<ThrasherAdapter.MyViewHolder> {
    ArrayList<ProductPartListModel.Part.SubPart> mList;
    Context context;
    List<String> mTryList = new ArrayList<>();
    List<ThrasherModel> mNewList = new ArrayList<>();
    ThrasherInterface thrasherInterface;

    public ThrasherAdapter(ArrayList<ProductPartListModel.Part.SubPart> mList, Context context, ThrasherInterface thrasherInterface) {
        this.mList = mList;
        this.context = context;
        this.thrasherInterface = thrasherInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_thrasher_assimbly, parent, false);
        return new ThrasherAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ProductPartListModel.Part.SubPart myModel = mList.get(position);
        holder.thrasherName.setText(myModel.getPspName());
        holder.numberTv.setText(myModel.getPspKey());
        holder.psp_numberTv.setText(myModel.getPspNo());


        //this check use to prevent value changes on scroll of recycleview
        if (mTryList != null)
            if (mTryList.contains(myModel.getPspId())) {
                holder.radioIV.setImageResource(R.drawable.ic_check_green);
            } else {
                holder.radioIV.setImageResource(R.drawable.ic_uncheck_green);
            }

        //this check use to prevent value changes on scroll of recycleview
        if(mList.get(position)!=null) {
            holder.valueTV.setText(String.valueOf(mList.get(position).getCount()));
        }
        else {
            holder.valueTV.setText("1");
        }
        holder.radioIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTryList != null)
                    if (mTryList.contains(myModel.getPspId())) {
                        mTryList.remove(myModel.getPspId());
                        myModel.setSubPartId("");
                        holder.radioIV.setImageResource(R.drawable.ic_uncheck_green);
                    } else {
                        mTryList.add(myModel.getPspId());
                        myModel.setSubPartId(myModel.getPspId());
                        myModel.setCount(myModel.getCount());
                       // notifyDataSetChanged();
                        Log.e("TAG", "COUNT**" + mNewList.size());
                        Log.e("TAG", "COUNT**" + mList.size());
                        holder.radioIV.setImageResource(R.drawable.ic_check_green);
                    }
                thrasherInterface.onThrasherClick(position,mList);
                holder.minusRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (myModel.getCount() > 1) {
                            int count = myModel.getCount();
                            Log.e("Output", "uri-" + count--);
                            holder.valueTV.setText(String.valueOf(count));
                            myModel.setCount(count);
                            myModel.setSubPartId(myModel.getPspId());
                            thrasherInterface.onThrasherClick(position,mList);

                        } else if (myModel.getCount() == 1) {
                            Toast.makeText(context, "Quantity cannot be less than 1", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.plusRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int count = myModel.getCount();
                        Log.e("Output", "uri-" + count++);
                        holder.valueTV.setText(String.valueOf(count));
                        myModel.setCount(count);
                        myModel.setSubPartId(myModel.getPspId());
                        thrasherInterface.onThrasherClick(position,mList);
                    }
                });

            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView thrasherName, numberTv, valueTV,psp_numberTv;
        RelativeLayout minusRL, plusRL;
        ImageView radioIV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            thrasherName = itemView.findViewById(R.id.thrasherName);
            numberTv = itemView.findViewById(R.id.numberTv);
            valueTV = itemView.findViewById(R.id.valueTV);
            minusRL = itemView.findViewById(R.id.minusRL);
            plusRL = itemView.findViewById(R.id.plusRL);
            radioIV = itemView.findViewById(R.id.radioIV);
            psp_numberTv=itemView.findViewById(R.id.psp_numberTv);
        }
    }

}