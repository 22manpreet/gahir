package com.gahiragro.app.adapters.adminAdapters;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.ReadyToDispatchActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.adminDealorEnquiry.AllEnquiriesItem;
import com.gahiragro.app.utils.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class AdminDealorEnquiryAdapter extends RecyclerView.Adapter<AdminDealorEnquiryAdapter.MyViewHolder> {
    List<com.gahiragro.app.model.adminDealorEnquiry.AllEnquiriesItem> mAdminEnquiryAL = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;
    String type = "";
    double lat;
    double lng;
    Geocoder mGeocoder;

    public AdminDealorEnquiryAdapter(FragmentActivity activity, List<AllEnquiriesItem> mAdminEnquiryAL, PaginationHomeInterface mInterfaceData, String strEventType) {
        this.context = activity;
        this.mAdminEnquiryAL = mAdminEnquiryAL;
        this.mInterfaceData = mInterfaceData;
        this.type = strEventType;
    }


    @NonNull
    @Override
    public AdminDealorEnquiryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        AdminDealorEnquiryAdapter.MyViewHolder viewHolder = new AdminDealorEnquiryAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminDealorEnquiryAdapter.MyViewHolder holder, int position) {
        AllEnquiriesItem mModel = mAdminEnquiryAL.get(position);
        if (position >= mAdminEnquiryAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }


        holder.orderIdTV.setText(context.getString(R.string.enquiry_id));
        if (!mModel.getDealerDetail().getUser_lat().equals("") && !mModel.getDealerDetail().getUser_long().equals("")) {
            lat = Double.parseDouble(mModel.getDealerDetail().getUser_lat());
            lng = Double.parseDouble(mModel.getDealerDetail().getUser_long());
        } else {

        }


        if (type.equals("admin/sales")) {
            holder.addressLL.setVisibility(View.VISIBLE);
            holder.addressTV.setText(getAddress(lat, lng));
        } else {
            holder.addressLL.setVisibility(View.GONE);
        }
        holder.dateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getCreationDate())));
        holder.productNameTV.setText(mModel.getProductDetail().getProdName());
        holder.productIDTV.setText(mModel.getEnquiryId());
        holder.nameTV.setText(mModel.getDealerDetail().getFirstName()+" "+mModel.getDealerDetail().getLastName());
        holder.contactTV.setText(mModel.getDealerDetail().getPhoneNo());
        holder.quantityTV.setText(mModel.getQty());
        holder.productPriceTV.setText("\u20B9" + mModel.getTotal());
        Glide.with(context).
                load(mModel.getProductDetail().getProdImage())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(holder.productIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ReadyToDispatchActivity.class);
                intent.putExtra(Constants.ENQUIRY_ID, mModel.getEnquiryId());
                intent.putExtra(Constants.TYPE, "adminDealerAdapter");
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mAdminEnquiryAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTV, dateTV, orderIdTV, addressTV, productPriceTV, productIDTV, quantityTV,contactTV,nameTV;
        ImageView productIV;
        LinearLayout addressLL;

        MyViewHolder(View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            productPriceTV = itemView.findViewById(R.id.productPriceTV);
            productIDTV = itemView.findViewById(R.id.productIDTV);
            quantityTV = itemView.findViewById(R.id.quantityTV);
            productIV = itemView.findViewById(R.id.productIV);
            dateTV = itemView.findViewById(R.id.dateTV);
            addressLL = itemView.findViewById(R.id.addressLL);
            addressTV = itemView.findViewById(R.id.addressTV);
            orderIdTV = itemView.findViewById(R.id.orderIdTV);
            contactTV = itemView.findViewById(R.id.contactTV);
            nameTV = itemView.findViewById(R.id.nameTV);
        }


    }


    public String getDateCurrentTimeZone(long timestamp) {
        //convert seconds to milliseconds
        Date date = new Date(timestamp * 1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("dd MMM yyyy ");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return jdf.format(date);

    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getLocality()).append(",");
                result.append(address.getCountryName());
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }
}


