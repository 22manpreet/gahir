package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.NewProductListActivity;
import com.gahiragro.app.activities.SplashActivity;
import com.gahiragro.app.activities.interfaces.TopFilterClickInterface;
import com.gahiragro.app.model.AllCategoryModel;
import com.gahiragro.app.model.DummyModel;
import com.gahiragro.app.model.TopTabsModel;
import com.gahiragro.app.utils.Constants;

import java.util.ArrayList;
import java.util.Random;

public class HomeNewTopFilterAdapter extends RecyclerView.Adapter<HomeNewTopFilterAdapter.MyViewHolder> {
    Context context;
    ArrayList<DummyModel> mDummyAL = new ArrayList<>();
    ArrayList<AllCategoryModel.AllCategory> mCategoryAL = new ArrayList<>();
    private long mLastClickTab1 = 0;

    public HomeNewTopFilterAdapter(FragmentActivity activity, ArrayList<DummyModel> mCategoryAL) {
        this.context = activity;
        this.mDummyAL = mCategoryAL;
    }

    @NonNull
    @Override
    public HomeNewTopFilterAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_category, parent, false);
        HomeNewTopFilterAdapter.MyViewHolder viewHolder = new HomeNewTopFilterAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeNewTopFilterAdapter.MyViewHolder holder, int position) {

        DummyModel mModel = mDummyAL.get(position);
        holder.nameTV.setText(mModel.getName());

        Glide.with(context)
                .load(mModel.getImg()) // Uri of the picture
                .apply(new RequestOptions().override(800, 800))
                .into(holder.img);

        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.cardView.setCardBackgroundColor(randomAndroidColor);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1000) {
                    return;
                }
                mLastClickTab1 = SystemClock.elapsedRealtime();
                Intent intent = new Intent(context, NewProductListActivity.class);
                intent.putExtra(Constants.CATEGORY_ID, mModel.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDummyAL.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        ImageView img;
        CardView cardView;

        MyViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
            img = itemView.findViewById(R.id.img);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    public void preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 4000) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


}


