package com.gahiragro.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gahiragro.app.R;
import com.gahiragro.app.activities.interfaces.TopFilterClickInterface;
import com.gahiragro.app.model.TopTabsModel;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HomeTopFilterAdapter extends RecyclerView.Adapter<HomeTopFilterAdapter.MyViewHolder> {
    ArrayList<TopTabsModel> mTopProductAL = new ArrayList<>();
    Context context;
    TopFilterClickInterface mTopFilterClickInterface;
    private int lastCheckedPosition = -1;





    public HomeTopFilterAdapter(FragmentActivity activity, ArrayList<TopTabsModel> mTopProductAL, TopFilterClickInterface mTopFilterClickInterface) {
        this.mTopProductAL = mTopProductAL;
        this.context = activity;
        this.mTopFilterClickInterface = mTopFilterClickInterface;

    }

    @NonNull
    @Override
    public HomeTopFilterAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_top_filter, parent, false);
        HomeTopFilterAdapter.MyViewHolder viewHolder = new HomeTopFilterAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeTopFilterAdapter.MyViewHolder holder, int position) {
        TopTabsModel mTopTabsModel = mTopProductAL.get(position);
        holder.itemTV.setText(mTopTabsModel.getTitle());

        if (mTopTabsModel.isSelected() == true) {
            holder.topRL.setBackground(context.getDrawable(R.drawable.bg_red_field));
            holder.itemTV.setTextColor(context.getResources().getColor(R.color.colorWhite));
        } else
            {
            holder.topRL.setBackground(context.getDrawable(R.drawable.bg_filter_field));
            holder.itemTV.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }


          holder.bindClick(mTopTabsModel);


    }

    @Override
    public int getItemCount() {
        return mTopProductAL.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView addImageIV;
        TextView itemTV;
        LinearLayout topRL;
        MyViewHolder(View itemView) {
            super(itemView);
            itemTV = itemView.findViewById(R.id.itemTV);
            topRL = itemView.findViewById(R.id.topRL);


        }


        public void bindClick(TopTabsModel mTopTabsModel) {
            topRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTopFilterClickInterface.onFilterItemClick(mTopTabsModel,getAdapterPosition());
                }
            });
        }
    }
}

