package com.gahiragro.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.DealorBookOrderActivity;
import com.gahiragro.app.activities.ReadyToDispatchActivity;
import com.gahiragro.app.activities.interfaces.PaginationHomeInterface;
import com.gahiragro.app.model.enquirymodel.AllEnquiriesItem;
import com.gahiragro.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DealorEnquiryAdapter extends RecyclerView.Adapter<DealorEnquiryAdapter.MyViewHolder> {
    List<AllEnquiriesItem> mEnquiryAL = new ArrayList<>();
    Context context;
    PaginationHomeInterface mInterfaceData;

    public DealorEnquiryAdapter(FragmentActivity activity, List<AllEnquiriesItem> mEnquiryAL, PaginationHomeInterface mInterfaceData) {
        this.context = activity;
        this.mEnquiryAL = mEnquiryAL;
        this.mInterfaceData = mInterfaceData;
    }

    @NonNull
    @Override
    public DealorEnquiryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        DealorEnquiryAdapter.MyViewHolder viewHolder = new DealorEnquiryAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DealorEnquiryAdapter.MyViewHolder holder, int position) {
        holder.dealernameLL.setVisibility(View.GONE);
        holder.contactLL.setVisibility(View.GONE);
        AllEnquiriesItem mModel = mEnquiryAL.get(position);
        if (position >= mEnquiryAL.size() - 1) {
            mInterfaceData.mHomePagination(true);
        }
        holder.orderIdTV.setText(context.getString(R.string.enquiry_id));
       // holder.dateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getCreationDate())));
        holder.dateTV.setText(mModel.getDispatchDay());
        holder.productNameTV.setText(mModel.getProductDetail().getProdName());
        holder.productIDTV.setText(mModel.getEnquiryId());



        holder.quantityTV.setText(mModel.getQty());
        holder.productPriceTV.setText("\u20B9" + mModel.getTotal());
        Glide.with(context).
                load(mModel.getProductDetail().getProdImage())
                .placeholder(R.drawable.gahir_logo_dummy)
                .into(holder.productIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ReadyToDispatchActivity.class);
                intent.putExtra(Constants.ENQUIRY_ID, mModel.getEnquiryId());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mEnquiryAL.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTV, orderIdTV, addressTV, dateTV, productPriceTV, productIDTV, quantityTV;
        LinearLayout addressLL;
        ImageView productIV;
        LinearLayout dealernameLL,contactLL;

        MyViewHolder(View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            productPriceTV = itemView.findViewById(R.id.productPriceTV);
            productIDTV = itemView.findViewById(R.id.productIDTV);
            quantityTV = itemView.findViewById(R.id.quantityTV);
            productIV = itemView.findViewById(R.id.productIV);
            dateTV = itemView.findViewById(R.id.dateTV);
            orderIdTV = itemView.findViewById(R.id.orderIdTV);
            dealernameLL = itemView.findViewById(R.id.dealernameLL);
            contactLL=itemView.findViewById(R.id.contactLL);
        }


    }


    public String getDateCurrentTimeZone(long timestamp) {
        //convert seconds to milliseconds
        Date date = new Date(timestamp * 1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("dd MMM yyyy ");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return jdf.format(date);

    }
}

