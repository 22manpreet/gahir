package com.gahiragro.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gahiragro.app.R;
import com.gahiragro.app.activities.DealorProductDetailActivity;
import com.gahiragro.app.model.SearchModel;
import com.gahiragro.app.utils.Constants;
import com.gahiragro.app.utils.GahirPreference;

import java.util.ArrayList;

public class  SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
    ArrayList<SearchModel.AllProduct> mAllProductAL = new ArrayList<>();
    Context context;

    public SearchAdapter(Activity mActivity, ArrayList<SearchModel.AllProduct> mAllProductAL) {
        this.context = mActivity;
        this.mAllProductAL = mAllProductAL;
    }

    @NonNull
    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_list, parent, false);
        SearchAdapter.MyViewHolder viewHolder = new SearchAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.MyViewHolder holder, int position) {
        SearchModel.AllProduct mModel = mAllProductAL.get(position);
        Glide.with(context)
                .load(mModel.getProdImage()) // Uri of the picture
                .into(holder.productImgIV);
//        holder.detailTV.setText(mModel.getProdDesc());
        holder.productTV.setText(mModel.getProdPrice());
        holder.productNameTV.setText(mModel.getProdName());
        //  holder.detailTV.setText(mModel.getProdDesc());
        holder.productTV.setText(mModel.getProdPrice());
        holder.modelNameTV.setText(mModel.getProdModel());
        if (GahirPreference.readString(context,GahirPreference.LOGIN_TYPE,"").equals(Constants.CUSTOMER)) {
            holder.checkAvailabilityTV.setText(context.getString(R.string.more_details));
        } else {
            holder.checkAvailabilityTV.setText(context.getString(R.string.check_availability));
        }
        holder.checkAvailabilityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DealorProductDetailActivity.class);
                intent.putExtra(Constants.ID,mModel.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mAllProductAL.size();
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView checkAvailabilityTV,modelNameTV;
        ImageView productImgIV;
        TextView productNameTV, detailTV, productTV;
        MyViewHolder(View itemView) {
            super(itemView);
            checkAvailabilityTV = itemView.findViewById(R.id.checkAvailabilityTV);
            productImgIV = itemView.findViewById(R.id.productImgIV);
            productNameTV = itemView.findViewById(R.id.productNameTV);
            detailTV = itemView.findViewById(R.id.detailTV);
            productTV = itemView.findViewById(R.id.productTV);
            modelNameTV = itemView.findViewById(R.id.modelNameTV);
        }
    }
    public void updateList(ArrayList mRecentItemsList){
        mAllProductAL = mRecentItemsList;
        notifyDataSetChanged();
    }
}
